﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P22g_VectorDeFloats
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int tamanyo = CapturaEntero("Introduce el tamaño del vector", 10, 100);
            float[] vFloat = ConstruyeVectorFloats(tamanyo);
            int numeroColumnas = CapturaEntero("Introduce el número de columnas", 1, 7);
            MuestraVectorFloats(vFloat, numeroColumnas);

            // Para salir
            Console.Write("\n\n\n\tPulse una tecla para salir");
            Console.ReadKey();
        }

        static float[] ConstruyeVectorFloats(int tamanyo)
        {
            float[] vNumerosAleatorios = new float[tamanyo];
            Random numAzar = new Random();
            for (int i = 0; i < vNumerosAleatorios.Length; i++)
            {
                vNumerosAleatorios[i] = (float)numAzar.Next(1000, 9999 + 1) / 100;
            }
            return vNumerosAleatorios;
        }

        static void MuestraVectorFloats(float[] vector, int nc)
        {
            for (int i = 0; i < vector.Length; i++)
            {
                if (i % nc == 0)
                    Console.WriteLine();
                Console.Write("\t{0}) {1}", i, vector[i].ToString("00.00"));
            }
        }

        static int CapturaEntero(string texto, int min, int max)
        {
            int valor;
            bool ok;
            do
            {
                Console.Write("\n\t{0} [{1}..{2}]:", texto, min, max);
                ok = Int32.TryParse(Console.ReadLine(), out valor);
                if (!ok)
                    Console.Write("\n\t** Error: No has introducido un número **");
                else if (valor < min || valor > max)
                    Console.Write("\n\t** Error: Número no está dentro del rango *+");
            } while (!ok || valor < min || valor > max);
            return valor;
        }
    }
}
