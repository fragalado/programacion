﻿using System;

namespace P22f1_PresentaFecha1
{
    internal class Program
    {
        static string[] nombreMeses = { "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" };
        static int[] maximoDiasMeses = {31,28,31,30,31,30,31,31,30,31,30,31};
        static void Main(string[] args)
        {
            // CapturaFechaString
            Console.Write(CapturaFechaString());

            // FechaString
            int anyo, mes, dia;
            anyo = CapturaEntero("Indroduce el año", 1, 2022);
            mes = CapturaEntero("Indroduce el mes", 1, 12);
            dia = CapturaEntero("Indroduce el dia", 1, maximoDiasMeses[mes - 1]);
            Console.Write("\n\n{0}", FechaString(anyo,mes,dia));

            // FechaString2
            Console.Write("\n\n{0}", FechaString2(2023, 11, 28));


            Console.WriteLine("\n\n\tPulsa INTRO para salir");
            Console.ReadLine();
        }
        static int CapturaEntero(string texto, int min, int max)
        {
            int valor;
            bool ok;
            do
            {
                Console.Write("\n\t{0} [{1}..{2}]: ", texto, min, max);
                ok = Int32.TryParse(Console.ReadLine(), out valor);

                if (!ok)
                    Console.Write("\n\t** Error: Valor introducido no es un número **");
                else if (valor < min || valor > max)
                    Console.Write("\n\t** Error: Valor introducido no está dentro del rango **");
            } while (!ok || valor < min || valor > max);

            return valor;
        }

        static string CapturaFechaString()
        {
            int anyo, mes, dia;
            anyo = CapturaEntero("Indroduce el año", 1, 2022);
            mes = CapturaEntero("Indroduce el mes", 1, 12);
            dia = CapturaEntero("Indroduce el dia", 1, maximoDiasMeses[mes-1]);

            return String.Format("\n\t{0} de {1} de {2}",dia, nombreMeses[mes-1].ToLower(),anyo);
        }

        static string FechaString(int anyo, int mes, int dia)
        {
            return String.Format("\n\t{0} de {1} de {2}", dia, nombreMeses[mes - 1].ToLower(), anyo);
        }

        static string FechaString2(int anyo, int mes, int dia)
        {
            do
            {
                if (anyo < 1 || anyo > 2022)
                {
                    Console.Write("\n\t** Error: Valor introducido [{0}] no está dentro del rango **",anyo);
                    anyo = CapturaEntero("Indroduce el año", 1, 2022);
                }
            } while (anyo < 1 || anyo > 2022);

            do
            {
                if (mes < 1 || mes > 12)
                {
                    Console.Write("\n\t** Error: Valor introducido [{0}] no está dentro del rango **",mes);
                    mes = CapturaEntero("Indroduce el mes", 1, 12);
                }
            } while (mes < 1 || mes>12);

            do
            {
                if(dia < 1 || dia > maximoDiasMeses[mes - 1])
                {
                    Console.Write("\n\t** Error: Valor introducido [{0}] no está dentro del rango **",dia);
                    dia = CapturaEntero("Indroduce el dia", 1, maximoDiasMeses[mes - 1]);
                }

            } while (dia < 1 || dia > maximoDiasMeses[mes - 1]);

            return String.Format("\n\t{0} de {1} de {2}", dia, nombreMeses[mes - 1].ToLower(), anyo);
        }
    }
}
