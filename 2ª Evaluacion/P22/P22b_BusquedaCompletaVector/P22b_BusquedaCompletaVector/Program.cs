﻿using System;
using System.Linq;

namespace P22b_BusquedaCompletaVector
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int numeroBuscar;
            // Creamos un vector y le damos un tamaño
            int[] vEnt = new int[CapturaEntero("Introduce el tamaño", 5, 100)];

            // Cargamos el vector con números aleatorios y mostramos por pantalla
            CargarListaValoresAleatoriosPositivos(vEnt);
            MostrarValoresLista(vEnt);

            // SIMPLE, SE SALE CON EL 0
            // Pedimos un número para buscar
            //do
            //{
            //    numeroBuscar = CapturaEntero("Introduce un número a buscar", 0,99);
            //    // Buscamos dentro del vector el número
            //    if (vEnt.Contains(numeroBuscar))
            //    {
            //        Console.Write("\n\tEl número {0} se encuentra en la/s posición/es", numeroBuscar);
            //        for (int i = 0; i < vEnt.Length; i++)
            //            if (vEnt[i] == numeroBuscar)
            //                Console.Write("\n\t\t{0}) {1}", i, vEnt[i]);
            //    }
            //    else
            //        Console.Write("\n\tEl número {0} no existe en la tabla", numeroBuscar);
            //} while (numeroBuscar != 0);

            // Otra forma para buscar dentro del vector el número
            //bool seEncuentra = false;
            //do
            //{
            //    numeroBuscar = CapturaEntero("Introduce un número a buscar", 0, 99);
            //    for (int i = 0; i < vEnt.Length; i++)
            //    {
            //        if (vEnt[i] == numeroBuscar)
            //        {
            //            if (!seEncuentra)
            //                Console.Write("\n\tEl número {0} se encuentra en la/s posición/es", numeroBuscar);
            //            Console.Write("\n\t\t{0}) {1}", i, vEnt[i]);
            //            seEncuentra = true;
            //        }
            //    }
            //    if (!seEncuentra)
            //        Console.Write("\n\tEl número {0} no existe en la tabla", numeroBuscar);
            //} while (numeroBuscar!=0);


            // AVANZADO, SE SALE PULSANDO INTRO Y CONTROLA PLURALES
            // Pedimos un número para buscar
            int numeroEncuentros;
            numeroBuscar = CapturaEntero2("Introduce un número a buscar", 10, 99);
            while (numeroBuscar!=0)
            {
                //Recorro el vector para buscar el número
                numeroEncuentros = 0;
                // Recorro el vector para saber cuántas veces aparece
                for (int i = 0; i < vEnt.Length; i++)
                    if (vEnt[i] == numeroBuscar)
                        numeroEncuentros++;

                // Si aparece 0 veces es que no existe
                if(numeroEncuentros==0)
                    Console.Write("\n\tEl número {0} no existe en la tabla", numeroBuscar);
                else if (numeroEncuentros == 1)
                    //Presentación de la cabecera controlando plurales
                    Console.Write("\n\tEl número {0} se encuentra en la posición: ", numeroBuscar);
                else
                    Console.Write("\n\tEl número {0} se encuentra en las posiciones: \n", numeroBuscar);

                for (int i = 0; i < vEnt.Length; i++)
                    if (vEnt[i] == numeroBuscar)
                        Console.WriteLine("{0}) {1}",i, vEnt[i]); ;
                numeroBuscar = CapturaEntero2("Introduce un número a buscar", 10, 99);
            }


            // Para terminar programa
            Console.Write("\n\n\n\tPulse una tecla para salir");
            Console.ReadKey();
        }

        static int CapturaEntero(string texto, int min, int max)
        {
            int valor;
            bool ok;
            do
            {
                Console.Write("\n\t{0} [{1}..{2}]:", texto, min, max);
                ok = Int32.TryParse(Console.ReadLine(), out valor);
                if (!ok)
                    Console.Write("\n\t** Error: No has introducido un número **");
                else if (valor < min || valor > max)
                    Console.Write("\n\t** Error: Número no está dentro del rango **");
            } while (!ok || valor < min || valor > max);
            return valor;
        }

        static int[] CargarListaValoresAleatoriosPositivos(int[] lista)
        {
            Random random = new Random();
            for (int i = 0; i < lista.Length; i++)
                lista[i] = random.Next(10,99+1);

            return lista;
        } 

        static void MostrarValoresLista(int[] lista)
        {
            for (int i = 0; i < lista.Length; i++)
                Console.Write("\n\t{0}) {1}",i, lista[i]);
        }

        static int CapturaEntero2(string texto, int min, int max)
        {
            int valor;
            string captura;
            bool ok;
            do
            {
                Console.Write("\n\t{0} [{1}..{2}]:", texto, min, max);
                captura = Console.ReadLine();
                if (captura == "")
                    return 0;
                ok = Int32.TryParse(captura, out valor);
                if (!ok)
                    Console.Write("\n\t** Error: No has introducido un número **");
                else if (valor < min || valor > max)
                    Console.Write("\n\t** Error: Número no está dentro del rango **");
            } while (!ok || valor < min || valor > max);
            return valor;
        }

    }
}
