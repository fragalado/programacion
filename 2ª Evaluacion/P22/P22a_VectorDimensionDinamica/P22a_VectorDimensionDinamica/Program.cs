﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P22a_VectorDimensionDinamica
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int tamanyo = CapturaEntero("Introduce el tamaño de la lista", 5, 20);
            int[] vEnt = new int[tamanyo];

            // Para guardar valores en la lista
            int valor = 1;
            for (int i = 0; i < vEnt.Length && valor != 0; i++)
            {
                //Console.Write("\tPosición {0}",i);
                //valor = CapturaEntero("¿valor?", -30, 50);

                // valor = CapturaEntero(String.Format("Posición {0} ¿valor?",i), -30, 50);
                valor = CapturaEntero("Posición "+i+ "  ¿valor?", -30, 50);

                vEnt[i] = valor;
            }
            
            //for (int i = 0; i < vEnt.Length && valor != 0; i++)
            //{
            //    vEnt[i] = CapturaEntero("Posición " + i + "  ¿valor?", -30, 50);
            //    if (vEnt[i] == 0)
            //        break;
            //}


            // Para imprimir
            Console.Clear();
            Console.WriteLine("\n Los valores guardados son: \n");
            for (int i = 0; i < vEnt.Length; i++)
                Console.WriteLine("\t{0}) {1}", i, vEnt[i]);


            // Para salir
            Console.Write("\n\n\n\tPulse una tecla para salir");
            Console.ReadKey();
        }

        static int CapturaEntero(string texto,int min, int max)
        {
            int valor;
            bool ok;
            do
            {
                Console.Write("\n\t{0} [{1}..{2}]:",texto,min,max);
                ok = Int32.TryParse(Console.ReadLine(),out valor);
                if (!ok)
                    Console.Write("\n\t** Error: No has introducido un número **");
                else if (valor <min || valor > max)
                    Console.Write("\n\t** Error: Número no está dentro del rango *+");
            } while (!ok || valor < min || valor > max);
            return valor;
        }
    }
}
