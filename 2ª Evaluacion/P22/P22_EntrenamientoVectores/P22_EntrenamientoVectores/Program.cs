﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P22_EntrenamientoVectores
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Random azar = new Random();
            // 0)
            int[] tabla0 = new int[10];
            int numAzar;

            for (int i = 0; i < tabla0.Length; i++)
            {
                numAzar = azar.Next(1, 1000 + 1);
                tabla0[i] = numAzar;
                Console.WriteLine(tabla0[i]);
            }

            //
            Console.Write("\n\n\nPulse una tecla para ir al siguiente entrenamiento");
            Console.ReadKey();
            Console.Clear();

            // 1)
            int tamaño = CapturaEntero("Introduce el tamaño que desees");
            int[] tabla1 = new int[tamaño];
            Console.Clear();
            for (int i = 0; i < tabla1.Length; i++)
            {
                numAzar = azar.Next(1, 1000 + 1);
                tabla1[i] = numAzar;
                Console.WriteLine("{0}- {1}",i,tabla1[i]);
            }

            //
            Console.Write("\n\n\nPulse una tecla para ir al siguiente entrenamiento");
            Console.ReadKey();
            Console.Clear();

            // 2)
            char[] tabla2 = new char[CapturaEntero("Introduce el tamaño que desees")];
            Console.Clear();
            for (int i = 0; i < tabla2.Length; i++)
            {
                numAzar = azar.Next(32, 126 + 1);
                tabla2[i] = (char)numAzar;
                Console.WriteLine("{0}){1} --> {2}", i, tabla2[i], (int)tabla2[i]);
            }

            Console.Write("\n\n\nPulse una tecla para salir");
            Console.ReadKey();
        }

        static int CapturaEntero(string texto)
        {
            int num;
            bool ok;
            do
            {
                Console.Write("\n\t{0}: ", texto);
                ok = Int32.TryParse(Console.ReadLine(), out num);

                if (!ok)
                    Console.Write("\n\t** Error: El valor introducido no es un entero **");
                else if (num < 10 || num > 100)
                    Console.Write("\n\t** Error: El valor introducido no está dentro del rango **");
            } while (!ok || num < 10 || num > 100);
            
            return num;
        }
    }
}
