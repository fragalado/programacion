﻿using System;

namespace P22f1_PresentaFecha1
{
    internal class Program
    {
        static string[] mesesCadena = { "enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre" };
        static void Main(string[] args)
        {
            // Forma 1
            string cadena = CapturaFechaString();
            Console.Write("\t{0}",cadena);

            // Para separar
            Console.Write("\n\n\n\tPulse una tecla para hacerlo de la segunda forma");
            Console.ReadKey();
            Console.Clear();

            // Forma 2
            int anyo, mes, dia;
            anyo = CapturaEntero("\n\t\tIndroduce el año", 1, 2022);
            mes = CapturaEntero("\n\t\tIndroduce el mes", 1, 12);
            do
            {
                dia = CapturaEntero("\n\t\tIndroduce el dia", 1, 31);

                if (dia == 31 && (mes == 4 || mes == 6 || mes == 9 || mes == 11)
                    || dia > 28 && mes == 2)
                    Console.Write("\n\t** Error: Día fuera de rango");
            } while (dia == 31 && (mes == 4 || mes == 6 || mes == 9 || mes == 11) || dia > 28 && mes == 2);
            cadena = CapturaFechaString(anyo, mes, dia);
            Console.Write("\t{0}", cadena);

            // Para salir
            Console.Write("\n\n\n\tPulse una tecla para salir");
            Console.ReadKey();
        }

        static string CapturaFechaString()
        {
            int anyo, mes, dia;
            anyo = CapturaEntero("\n\t\tIndroduce el año", 1, 2022);
            mes = CapturaEntero("\n\t\tIndroduce el mes", 1, 12);
            do
            {
                dia = CapturaEntero("\n\t\tIndroduce el dia", 1, 31);

                if (dia == 31 && (mes == 4 || mes == 6 || mes == 9 || mes == 11)
                    || dia > 28 && mes == 2)
                    Console.Write("\n\t** Error: Día fuera de rango");
            } while (dia == 31 && (mes == 4 || mes == 6 || mes == 9 || mes == 11) || dia > 28 && mes == 2);

            return String.Format("{0} de {1} de {2}", dia, mesesCadena[mes-1], anyo);
        }

        static string CapturaFechaString(int anyo, int mes, int dia)
        {
            return String.Format("{0} de {1} de {2}", dia, mesesCadena[mes - 1], anyo);
        }

        static int CapturaEntero(string texto, int min, int max)
        {
            int valor = 0;
            bool esCorrecto;
            do
            {
                Console.Write("{0} [{1}..{2}]: ", texto, min, max);
                // Intentamos convertir la cadena introducida por el teclado
                esCorrecto = Int32.TryParse(Console.ReadLine(), out valor);
                if (esCorrecto)
                {
                    // como se ha introducido un entero, ahora comprobamos si está en el rango correcto
                    if (valor < min || valor > max)
                    {
                        Console.WriteLine("\n*** Error: valor fuera de rango ***");
                        esCorrecto = false;
                    }
                }
                else
                    Console.WriteLine("\n*** Error: No se ha introducido un número entero ***");
            } while (!esCorrecto);
            return valor;
        }
    }
}
