﻿// Alumno: Gallego Dorado, Francisco José
using System;

namespace P22f2_PresentaFechaOK
{
    internal class Program
    {
        static string[] nombreMeses = { "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" };
        static int[] maximoDiasMeses = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
        static void Main(string[] args)
        {
            // CapturaFechaString
            Console.Write(CapturaFechaString());

            // FechaString
            int anyo, mes, dia;
            anyo = CapturaEntero("Indroduce el año", 1, 2022);
            mes = CapturaEntero("Indroduce el mes", 1, 12);
            if (EsBisiesto(anyo) && mes==2)
                dia = CapturaEntero("Indroduce el dia", 1, maximoDiasMeses[mes - 1] + 1);
            else
                dia = CapturaEntero("Indroduce el dia", 1, maximoDiasMeses[mes - 1]);
            // El ifelse equivale a:
            //dia = (EsBisiesto(anyo) && mes == 2) ? CapturaEntero("Indroduce el dia", 1, maximoDiasMeses[mes - 1] + 1) : CapturaEntero("Indroduce el dia", 1, maximoDiasMeses[mes - 1]);
            Console.Write("\n\n{0}", FechaString(anyo, mes, dia));



            Console.WriteLine("\n\n\tPulsa INTRO para salir");
            Console.ReadLine();
        }

        // Método CapturaEntero
        static int CapturaEntero(string texto, int min, int max)
        {
            int valor;
            bool ok;
            do
            {
                Console.Write("\n\t{0} [{1}..{2}]: ", texto, min, max);
                ok = Int32.TryParse(Console.ReadLine(), out valor);

                if (!ok)
                    Console.Write("\n\t** Error: Valor introducido no es un número **");
                else if (valor < min || valor > max)
                    Console.Write("\n\t** Error: Valor introducido no está dentro del rango **");
            } while (!ok || valor < min || valor > max);

            return valor;
        }

        // Método CapturaFechaString

        static string CapturaFechaString()
        {
            // Declaramos las variables que vamos a utilizar
            int anyo, mes, dia;
            // Capturamos el valor del año y el mes
            anyo = CapturaEntero("Indroduce el año", 1, 2022);
            mes = CapturaEntero("Indroduce el mes", 1, 12);

            // Preguntamos si quire introducir un nuevo año
            char opcion;
            do
            {
                Console.Write("\n\t¿Quieres proponer otra fecha? [Fecha actual: {0}] [s/S (Sí) | n/N (No)]", anyo);
                opcion = Console.ReadKey().KeyChar;
                if (!(opcion == 's' || opcion == 'S' || opcion == 'n' || opcion == 'N'))
                    Console.Write("\n\t** Error: Carácter introducido no válido **");
            } while (!(opcion == 's' || opcion == 'S' || opcion == 'n' || opcion == 'N'));

            if (opcion == 's' || opcion == 'S')
            {
                Console.Write("\n\t¿nuevo año?: ");
                anyo = Convert.ToInt32(Console.ReadLine());
            }

            // Llamamos al método EsBisiesto para comprobar si el año que tenemos es bisiesto o no y actuar en consecuencia
            if (EsBisiesto(anyo))
                dia = CapturaEntero("Indroduce el dia", 1, maximoDiasMeses[mes - 1]+1);
            else
                dia = CapturaEntero("Indroduce el dia", 1, maximoDiasMeses[mes - 1]);

            // Devolvemos el string con la fecha completa
            return String.Format("\n\t{0} de {1} de {2}", dia, nombreMeses[mes - 1].ToLower(), anyo);
        }

        // Método FechaString
        static string FechaString(int anyo, int mes, int dia)
        {
            return String.Format("\n\t{0} de {1} de {2}", dia, nombreMeses[mes - 1].ToLower(), anyo);
        }

        // Método EsBisiesto
        static bool EsBisiesto(int anyo)
        {
            bool bisiesto = false;
            if (anyo < 10) // Hacemos este if ya que si ponemos un anyo de menos de 2 cifras el SubString nos va a fallar
            {
                if (anyo % 4 == 0)
                    bisiesto = true;
            }
            else if (anyo % 4 ==0 && anyo.ToString().Substring(anyo.ToString().Length - 2, 2) != "00") // Comprobamos que es mútiplo de 4 y no acaba en '00'
                bisiesto = true;
            return bisiesto;
        }
    }
}
