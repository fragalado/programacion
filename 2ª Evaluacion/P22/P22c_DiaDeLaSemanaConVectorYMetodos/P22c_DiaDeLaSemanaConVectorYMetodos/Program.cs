﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P22c_DiaDeLaSemanaConVectorYMetodos
{
    internal class Program
    {
        static string[] tabDiaSemana = {"Domingo","Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"};
        static void Main(string[] args)
        {
            int numDias, diaDeLaSemana, diaDeLaSemanaFinal;
            string nombreDiaSemana = "", nombreDiaSemanaFinal = "";
            // Muestro los días
            MuestraDiasSemana();

            // Capturamos el número del día
            diaDeLaSemana = CapturaOpcion(0, 6);

            // Forma sin usar metodo, Nos quedamos con el nombre pasando el numero como indice del array 
            //nombreDiaSemana = tabDiaSemana[diaDeLaSemana];
            // Forma con metodo:
            nombreDiaSemana = NombreDiaSemana(diaDeLaSemana);

            // Si el día es correcto continuamos
            Console.WriteLine("\n\tHoy es {0}", nombreDiaSemana);

            Console.Write("\tIntroduce cuántos días quieres avanzar: ");
            numDias = Convert.ToInt32(Console.ReadLine());

            if (numDias > 0)
            {
                nombreDiaSemanaFinal = ProximoDiaSemana(diaDeLaSemana, numDias);

                Console.WriteLine("\n\tEstamos a {0} y dentro de {1} días será {2}", nombreDiaSemana, numDias, nombreDiaSemanaFinal);
            }
            else Console.WriteLine("\n\t¡¡Avanzar significa un número de días mayor de cero!!");

            Console.Write("\n\n\t\tPulsa Intro para salir");
            Console.ReadLine();
        }

        static void MuestraDiasSemana()
        {
            /*
            Console.WriteLine("\n\t\t0) Domingo");
            Console.WriteLine("\n\t\t1) Lunes");
            Console.WriteLine("\n\t\t2) Martes");
            Console.WriteLine("\n\t\t3) Miércoles");
            Console.WriteLine("\n\t\t4) Jueves");
            Console.WriteLine("\n\t\t5) Viernes");
            Console.WriteLine("\n\t\t6) Sábado");
            */
            for (int i = 0; i < tabDiaSemana.Length; i++)
                Console.Write("\t\t{0}) {1}\n", i, tabDiaSemana[i]);
        }

        static int CapturaOpcion(int min, int max)
        {
            int opcion;
            Console.Write("\n\t\tPulse su opcion: ");
            opcion = Console.ReadKey().KeyChar - '0';
            // Comprobamos si la opción no es correcta
            while (opcion < min || opcion > max)
            {
                Console.WriteLine("\n\t\t** Error: No ha introducido una opción correcta **");
                Console.Write("\n\t\tPulse su opcion: ");
                opcion = Console.ReadKey().KeyChar - '0';
            }
            return opcion;
        }

        static string NombreDiaSemana(int idDiaSemana)
        {
            string nombreDiaSemana = tabDiaSemana[idDiaSemana];
            return nombreDiaSemana;
        }

        static string ProximoDiaSemana(int idDiaSemana, int diasAvance)
        {
            int diaDeLaSemanaFinal = (diasAvance + idDiaSemana) % 7;
            // Forma sin llamar al metodo
            //string nombreDiaSemana = tabDiaSemana[diaDeLaSemanaFinal]; ;
            // Forma llamando al metodo anterior
            string nombreDiaSemana = NombreDiaSemana(diaDeLaSemanaFinal);
            return nombreDiaSemana;
        }
    }
}
