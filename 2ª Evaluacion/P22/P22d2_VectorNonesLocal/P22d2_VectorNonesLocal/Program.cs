﻿// Alumno: Gallego Dorado, Francisco José
using System;

namespace P22d2_VectorNonesLocal
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int[] vNones;
            int tamTabla, colum;

            // Hacemos un bucle que se repetirá sólo si se introduce 's' o 'S'
            do
            {
                Console.Clear();
                // Capturamos el tamaño de la tabla
                tamTabla = CapturaEntero("¿Cuántos impares vas a cargar en la tabla?", 5, 100);

                // Cargamos la tabla con números impares y del tamaño deseado
                vNones = new int[tamTabla];
                vNones = CargaTablaNones(vNones);

                // Preguntamos cuantas columnas vamos a querer
                colum = CapturaEntero("¿Cuántas columnas quieres?", 1, 8);
                // Mostramos por pantalla
                MuestraTabla(vNones, colum);
            } while (PreguntaSiNo("¿Quiere repetir?"));

            // Para salir
            Console.Write("\n\n\n\n\tPulse una tecla para salir");
            Console.ReadKey();
        }

        static int CapturaEntero(string texto, int min, int max)
        {
            bool ok;
            int num;
            do
            {
                Console.Write("\n\t{0} [{1}..{2}] ", texto, min, max);
                ok = Int32.TryParse(Console.ReadLine(), out num);
                if (!ok)
                    Console.Write("\n\t** Error: No sea introducido un número **");
                else if (num < min || num > max)
                    Console.Write("\n\t** Error: El valor no está dentro del rango **");
            } while (!ok || num < min || num > max);

            return num;
        }

        static int[] CargaTablaNones(int[] lista)
        {
            int contador = 1;
            for (int i = 0; i < lista.Length; i++)
            {
                lista[i] = contador;
                contador += 2;
            }

            return lista;
        }

        static void MuestraTabla(int[] lista, int nc)
        {
            int contador = 0;
            for (int i = 0; i < lista.Length; i++)
            {
                if (contador % nc == 0)
                    Console.WriteLine();
                Console.Write("\t{0}", lista[i]);
                contador++;
            }
        }

        static bool PreguntaSiNo(string texto)
        {
            string captura;
            do
            {
                Console.Write("\n\n\n\t{0}", texto);
                captura = Console.ReadLine();
                captura = captura.ToLower();
                if (!(captura == "s" || captura == "n"))
                    Console.Write("\n\t** Error: Valor introducido no válido **");
            } while (!(captura=="s" || captura=="n"));
            
            if (captura == "s")
                return true;
            else
                return false;
        }
    }
}
