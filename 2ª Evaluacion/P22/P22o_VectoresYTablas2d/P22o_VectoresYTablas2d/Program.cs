﻿using System;

namespace P22o_VectoresYTablas2d
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // Declaramos los vectores nombres y apellidos
            string[] vNombres = {"Álvaro", "Daniel Luis",
"Juan Manuel", "Agustín", "Fco. Javier",
"José Manuel", "Tomás", "Carlos",
"Jose Carlos", "Juan Luis", "Daniel",
"Angel", "Jacobo", "Alejandro",
"Francisco", "Alfredo", "Francisco",
"Antonio", "Constantino", "Roberto",
"Rafael", "Antonio"};
            string[] vApellidos = {"Sánchez Elegante", "Arenas Mata", "García Solís","Rodríguez Vázquez",
"Hurtado Miranda", "Pinto Mirinda", "Barrios Garrobo", "Márquez Salazar",
"Medina Gómez", "Alonso Pérez","López Mora", "González Chaparro", "Ferrer Jiménez", "Morales Moncayo",
"Fernández Perea", "Blanco Roldán", "Navarro Romero", "Aguilar Rubio", "Baena Fernández", "Barco Ramírez", "Delgado Rodríguez", "Duque Martínez"};

            //2. Construimos una tabla con el número de nombres de fila y dos columnas
            string[,] tab2dGente = new string[vNombres.Length, 2];
            //2.1 La cargamos colocando el nombre en la primera columna y los apellidos en la segunda
            for (int i = 0; i < vNombres.Length; i++)
            {
                tab2dGente[i,0] = vNombres[i];
                tab2dGente[i, 1] = vApellidos[i];
            }

            //3. Presentar <Nombre Apellidos> de cada persona a partir de la tabla tab2dGente
            Pausa("mostrar <Nombre Apellidos>\n");
            for (int i = 0; i < vNombres.Length; i++)
            {
                Console.Write("\t");
                if (i < 9)
                    Console.Write(" ");
                Console.Write("{2}) {0} {1}\n", tab2dGente[i, 0], tab2dGente[i, 1], i + 1);
                // Si no queremos hacerlo con un salto con el if:
                // Para presentar el indice con dos digitos
                // Console.Write("{2}) {0} {1}\n", tab2dGente[i, 0], tab2dGente[i, 1], (i + 1).ToString("00"));
            }

            Console.Write("\n\n\n"); // PARA SEPARAR

            //4. Construye el vector vApellNomb string y rellenalos con los <Apellidos, Nombre> de tab2dGente
            string[] vApellNomb = new string[vNombres.Length];
            for (int i = 0; i < vApellNomb.Length; i++)
                vApellNomb[i] = tab2dGente[i, 1]+", "+tab2dGente[i, 0];

            //5. Muestra vApellNomb
            Pausa("mostrar <Apellidos, Nombre>");
            for (int i = 0; i < vApellNomb.Length; i++)
                Console.Write("\n\t{0}", vApellNomb[i]);

            //6. Muestra la persona cuyos <Apellidos, Nombres> tiene más carácteres.
            Pausa("mostrar <Apellidos, Nombre> que tiene más carácteres\n");
            int maxLong=0;
            int indiceMayor=0;
            for (int i = 0; i < vApellNomb.Length; i++)
            {
                if (vApellNomb[i].Length > maxLong)
                {
                    indiceMayor = i;
                    maxLong = vApellNomb[i].Length;
                }
            }
            Console.Write(vApellNomb[indiceMayor]);
            // Forma mas eficiente
            Pausa("mostrar <Apellidos, Nombre> que tiene más carácteres FORMA EFICIENTE\n");
            string personaMasCaracteres = vApellNomb[0];
            for (int i = 0; i < vApellNomb.Length; i++)
            {
                if (vApellNomb[i].Length > personaMasCaracteres.Length)
                    personaMasCaracteres = vApellNomb[i];
            }
            Console.Write(personaMasCaracteres);
            // Otra forma
            Pausa("mostrar <Apellidos, Nombre> que tiene más carácteres OTRA FORMA\n");
            int posLongMax = 0;
            for (int i = 0; i < vApellNomb.Length; i++)
            {
                if (vApellNomb[i].Length > vApellNomb[posLongMax].Length)
                    posLongMax = i;
            }
            Console.Write(vApellNomb[posLongMax]);


            // Para salir
            Pausa();
        }

        static void Pausa()
        {
            Console.Write("\n\n\n\tPulse una tecla para continuar");
            Console.ReadKey(true);
        }

        static void Pausa(string texto)
        {
            Console.Write("\nPulse una tecla para {0}",texto);
            Console.ReadKey(true);
            Console.Clear();
        }
    }
}
