﻿using System;

namespace P22d_VectorNonesGlobal
{
    internal class Program
    {
        static int[] vNones;
        static void Main(string[] args)
        {
            // Capturamos el tamaño de la tabla
            int tamTabla = CapturaEntero("¿Cuántos impares vas a cargar en la tabla?",5,100);

            // Cargamos la tabla con números impares y del tamaño deseado
            CargaTablaNones(tamTabla);

            // Preguntamos cuantas columnas vamos a querer
            int colum = CapturaEntero("¿Cuántas columnas quieres?", 1, 8);
            // Mostramos por pantalla
            MuestraTabla(colum);

            // Para salir
            Console.Write("\n\n\n\n\tPulse una tecla para salir");
            Console.ReadKey(true); // Asi la tecla que pulsamos no se ve
        }

        static int CapturaEntero(string texto, int min, int max)
        {
            bool ok; 
            int num;
            do
            {
                Console.Write("\n\t{0} [{1}..{2}] ",texto,min,max);
                ok = Int32.TryParse(Console.ReadLine(), out num);
                if (!ok)
                    Console.Write("\n\t** Error: No sea introducido un número **");
                else if (num < min || num > max)
                    Console.Write("\n\t** Error: El valor no está dentro del rango **");
            } while (!ok || num < min || num > max);

            return num;
        }

        static void CargaTablaNones(int numE)
        {
            // Inicializamos y cargamos el vector con el tamaño introducido
            vNones = new int[numE];

            int contador = 1;
            for (int i = 0; i < vNones.Length; i++)
            {
                vNones[i] = contador;
                contador += 2;
            }
        }

        static void MuestraTabla(int nc)
        {
            /*int contador = 0;
            for (int i = 0; i < vNones.Length; i++)
            {
                if (contador % nc == 0)
                    Console.WriteLine();
                Console.Write("\t{0}", vNones[i]);
                contador++;
            }*/

            for (int i = 0; i < vNones.Length; i++)
            {
                if (i % nc == 0)
                {
                    Console.WriteLine();
                    Console.Write("\t");
                }
                if (i < 10)
                    Console.Write(" ");
                Console.Write("{0}) {1}\t", i, vNones[i]);
            }
        }
    }
}
