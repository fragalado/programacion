﻿// Alumno: Gallego Dorado, Francisco José
using System;

namespace P22p_TablaDoubles2D
{
    internal class Program
    {
        static void Main(string[] args)
        {
            double[,] tabla = new double[9,7];
            tabla = CargarTabla(tabla);

            // Mostramos
            MostrarValores(tabla);

            // Para salir;
            Console.Write("\n\n\n\tPulse una tecla para salir");
            Console.ReadKey();
        }

        private static double[,] CargarTabla(double[,] tabla)
        {
            Random azar = new Random();
            for (int i = 0; i < tabla.GetLength(0); i++)
            {
                for (int j = 0; j < tabla.GetLength(1); j++)
                {
                    tabla[i, j] = (double)azar.Next(100, 999+1) / 10;
                }
            }
            return tabla;
        }

        private static void MostrarValores(double[,] tabla)
        {
            Console.ForegroundColor= ConsoleColor.Green;
            // Encabezado fila
            for (int i = 0; i < tabla.GetLength(0); i++)
            {
                ColocaValor(i+1, 0);
                Console.Write("f"+i);
            }
            // Encabezado columnas
            for (int j = 0; j < tabla.GetLength(1); j++)
            {
                ColocaValor(0, j+1);
                Console.Write("c" + j);
            }
            Console.ResetColor();
            // Ponemos el color en amarillo
            Console.ForegroundColor = ConsoleColor.Yellow;

            // Recorremos la tabla para mostrar sus valores en su correspondiente fila y columna
            for (int i = 0; i < tabla.GetLength(0); i++)
            {
                for (int j = 0; j < tabla.GetLength(1); j++)
                {
                    ColocaValor(i+1, j+1);
                    Console.Write(tabla[i, j]);
                }
            }

            Console.ResetColor();
            Console.ForegroundColor= ConsoleColor.Green;

            // Pregunta si quiera cambiar valor
            char opcion;
            bool repetir = true;
            int filaPedida, colPedida;
            double valorPedido;
            do
            {
                BorrarPantalla();
                Console.ForegroundColor = ConsoleColor.Green;
                Console.SetCursorPosition(12,23 );
                Console.Write("\n\n\t¿Desea cambiar algun valor? (s=Sí; n=No)");
                opcion = Console.ReadKey(true).KeyChar;
                // Si se pulsa n no pedirá valores
                // Si pulsa s, va a pedir los valores
                if (opcion.ToString().ToLower() == "n")
                    repetir = false;
                else if (opcion.ToString().ToLower() == "s")
                {
                    // Pedimos la fila y la columna
                    filaPedida = CapturaEntero("¿valor de la fila?", 0, 8);
                    colPedida = CapturaEntero("¿valor de la columna?", 0, 6);
                    // Pedimos el valor a introducir
                    valorPedido = CapturaDoble("¿valor a introducir?", 10.0, 99.9);
                    valorPedido = Math.Round(valorPedido, 1);

                    // En la posición que hemos pedido vamos a borrar el contenido que ya hay, por lo tanto:
                    ColocaValor(filaPedida + 1, colPedida + 1);
                    Console.Write("    ");

                    // Vamos a poner de color de fondo rojo y de texto blanco
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.ForegroundColor = ConsoleColor.White;

                    // Introducimos el valor pedido en la posicion pedida
                    ColocaValor(filaPedida + 1, colPedida + 1);
                    Console.Write(valorPedido);
                    // Reseteamos el color para que no se ponga en algo que no queremos
                    Console.ResetColor();
                }
                else
                    Console.Write("\n\t** Error: Valor introducido no es correcto **");
            } while (repetir);
        }

        private static void ColocaValor(int fila, int columna)
        {
            int fila2 = 1 + 2 * fila;
            int columna2 = 10 + 7 * columna;
            Console.SetCursorPosition(columna2, fila2);
        }

        private static int CapturaEntero(string texto, int min , int max)
        {
            int num;
            bool ok;
            do
            {
                Console.Write("\n\t{0} [{1}..{2}]",texto, min, max);
                ok = Int32.TryParse(Console.ReadLine(), out num);
                if (!ok)
                    Console.Write("\n\t** Error: Valor introducido no es un entero **");
                else if (num < min || num > max)
                    Console.Write("\n\t** Error: Valor introducido no está dentro del rango **");
            } while (!ok || num < min || num > max);
            return num;
        }

        private static double CapturaDoble(string texto, double min, double max)
        {
            double num;
            bool ok;
            do
            {
                Console.Write("\n\t{0} [{1}..{2}]", texto, min, max);
                ok = Double.TryParse(Console.ReadLine(), out num);
                if (!ok)
                    Console.Write("\n\t** Error: Valor introducido no es un entero **");
                else if (num < min || num > max)
                    Console.Write("\n\t** Error: Valor introducido no está dentro del rango **");
            } while (!ok || num < min || num > max);

            return num;
        }

        private static void BorrarPantalla()
        {
            Console.SetCursorPosition(12, 23);
            Console.Write("                                                                         ");
            Console.Write("                                                                         ");
            Console.Write("                                                                         ");
            Console.Write("                                                                         ");
            Console.Write("                                                                         ");
            Console.Write("                                                                         ");
            Console.Write("                                                                         ");
            Console.Write("                                                                         ");
            Console.Write("                                                                         ");
            Console.Write("                                                                         ");
            Console.Write("                                                                         ");
            Console.Write("                                                                         ");
            Console.Write("                                                                         ");
            Console.Write("                                                                         ");
            Console.Write("                                                                         ");
            Console.Write("                                                                         ");
        }
    }
}
