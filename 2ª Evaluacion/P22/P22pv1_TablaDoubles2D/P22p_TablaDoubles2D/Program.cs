﻿using System;

namespace P22p_TablaDoubles2D
{
    internal class Program
    {
        static void Main(string[] args)
        {
            double[,] tDoubleAleatorios = new double[9, 7];
            CargaTabla(tDoubleAleatorios);

            MostrarValores(tDoubleAleatorios);

            

            // Para salir
            Console.Write("\n\n\n\tPulse una tecla para salir");
            Console.ReadKey();
        }

        static double[,] CargaTabla(double[,] tabla)
        {
            Random azar = new Random();
            for (int i = 0; i < tabla.GetLength(1); i++)
            {
                for (int j = 0; j < tabla.GetLength(0); j++)
                    tabla[j, i] = (double)azar.Next(100,999+1)/10;
            }
            return tabla;
        }

        static void MostrarValores(double[,] tabla)
        {
            // Mostrar cabecera columna
            for (int i = 0; i < 7; i++)
            {
                Console.ForegroundColor= ConsoleColor.Green;
                Console.SetCursorPosition(10 + 10 * i, 1 + 2 * 0);
                Console.Write("c{0}", i);
            }
            // Mostrar cabecera fila
            for (int i = 0; i < 9; i++)
            {
                Console.SetCursorPosition(3, 3 + 2 * i);
                Console.Write("f{0}", i);
            }
            Console.ResetColor();

            for (int i = 0; i < tabla.GetLength(0); i++)
            {
                for (int j = 0; j < tabla.GetLength(1); j++)
                {
                    ColocaValor(j+1, i+1);
                    Console.Write(tabla[j,i]);
                }
            }
        }

        static void ColocaValor(int fila, int columna)
        {
            Console.SetCursorPosition(10 + 10 * columna, 1 + 2 * fila);
        }
    }
}
