﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P21ba_DiaDeLaSemana
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int numDias, diaDeLaSemana;
            string nombreDiaSemana = "", nombreDiaSemanaFinal = "";
            // Muestro los días
            MuestraDiasSemana();


            diaDeLaSemana = CapturaOpcion(0, 6);

            nombreDiaSemana = NombreDiaSemana(diaDeLaSemana);



            // Si el día es correcto continuamos
            Console.WriteLine("\n\tHoy es {0}", nombreDiaSemana);

            numDias = CapturaOpcion("Introduce cuántos días quieres avanzar");

            
            nombreDiaSemanaFinal = ProximoDiaSemana(diaDeLaSemana, numDias);

            Console.WriteLine("\n\tEstamos a {0} y dentro de {1} días será {2}", nombreDiaSemana, numDias, nombreDiaSemanaFinal);
            

            Console.Write("\n\n\t\tPulsa Intro para salir");
            Console.ReadLine();
        }

        static void MuestraDiasSemana()
        {
            Console.WriteLine("\n\t\t0) Domingo");
            Console.WriteLine("\n\t\t1) Lunes");
            Console.WriteLine("\n\t\t2) Martes");
            Console.WriteLine("\n\t\t3) Miércoles");
            Console.WriteLine("\n\t\t4) Jueves");
            Console.WriteLine("\n\t\t5) Viernes");
            Console.WriteLine("\n\t\t6) Sábado");

        }

        static int CapturaOpcion(int min, int max)
        {
            int opcion;
            Console.Write("\n\t\tPulse su opcion: ");
            opcion = Console.ReadKey().KeyChar - '0';
            // Comprobamos si la opción no es correcta
            while (opcion < min || opcion > max)
            {
                Console.WriteLine("\n\t\t** Error: No ha introducido una opción correcta **");
                Console.Write("\n\t\tPulse su opcion: ");
                opcion = Console.ReadKey().KeyChar - '0';
            }
            return opcion;

        }

        static int CapturaOpcion(string textoPregunta)
        {
            int numDias;
            do
            {
                Console.Write("\n\t{0}: ", textoPregunta);
                numDias = Convert.ToInt32(Console.ReadLine());

                if (numDias <= 0)
                    Console.WriteLine("\n\t¡¡Avanzar significa un número de días mayor de cero!!");
            } while (numDias <= 0);
            return numDias;
        }

        static string NombreDiaSemana(int idDiaSemana)
        {
            switch (idDiaSemana)
            {
                case 0:
                    return "Domingo";
                case 1:
                    return "Lunes";
                case 2:
                    return "Martes";
                case 3:
                    return "Miércoles";
                case 4:
                    return "Jueves";
                case 5:
                    return "Viernes";
                case 6:
                    return "Sábado";
            }
            return "";
        }

        static string ProximoDiaSemana(int idDiaSemana, int diasAvance)
        {
            string nombreDiaSemana = String.Empty;
            int diaDeLaSemanaFinal = (diasAvance + idDiaSemana) % 7;

            nombreDiaSemana = NombreDiaSemana(diaDeLaSemanaFinal);

            return nombreDiaSemana;
        }
    }
}
