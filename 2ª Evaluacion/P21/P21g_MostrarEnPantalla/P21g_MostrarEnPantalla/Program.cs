﻿using System;
using System.Linq;

namespace P21g_MostrarEnPantalla
{
    internal class Program
    {
        static void Main(string[] args)
        {
            MuestraNums0a99();

            // Para cambiar al siguiente metodo
            Console.Write("\n\n\n\tPulse una tecla para cambiar de metodo");
            Console.ReadKey();
            Console.Clear();

            ColocaNums0a99();

            // Para cambiar al siguiente metodo
            Console.Write("\n\n\n\tPulse una tecla para cambiar de metodo");
            Console.ReadKey();
            Console.Clear();

            ColocaNúms0a99Azar();
            int num;
            do
            {
                Console.SetCursorPosition(10, 24);
                Console.BackgroundColor = ConsoleColor.Black;
                Console.ForegroundColor = ConsoleColor.Yellow;
                
                num = CapturaEntero("Introduce número faltante ['0' para salir]",0,99);
                ColocaElNumero(num);
                Console.BackgroundColor = ConsoleColor.Red;
                Console.ForegroundColor = ConsoleColor.White;
                Console.Write("{0}", num);
                Console.ResetColor();
                Console.SetCursorPosition(10, 24);
                Console.Write("                                                                   ");
                Console.Write("\n                                                                 ");
                Console.Write("\n                                                                  ");
                Console.Write("\n                                                                 ");
                Console.Write("\n                                                                  ");
                Console.Write("\n                                                                   ");

            } while (num != 0);

            // Para salir
            ColocaElNumero(101);
            Console.Write("\n\n\n\tPulse una tecla para salir");
            Console.ReadKey();
        }

        static void MuestraNums0a99()
        {
            for (int i = 0; i < 100; i++)
            {
                
                if (i % 10 == 0)
                    Console.WriteLine("\n");
                Console.Write("\t");
                if (i < 10)
                    Console.Write(" ");
                Console.Write("{0}     ",i);
            }
        }

        static void ColocaNums0a99()
        {
            for (int i = 0; i < 100; i++)
            {
                ColocaElNumero(i);
                Console.Write("{0}", i); // Esto se podría poner e ColocaElNumero
            }
        }

        static void ColocaElNumero(int num)
        {
            int columna, fila;
            int decena = num / 10;
            int unidad = (num - decena*10); // num % 10
            columna = 10 + 5 * unidad; // El 10 es separalo de la izquierda, el 5 es separarlo entre los numeros
            fila = 1 + 2 * decena; // El 1 es separarlo de arriba, el 2 es separar entre los numeros
            Console.SetCursorPosition(columna, fila);
        }

        static void ColocaNúms0a99Azar()
        {
            int numAzar;
            Random azar = new Random();
            for (int i = 0; i < 200; i++)
            {
                numAzar = azar.Next(0, 99 + 1);
                ColocaElNumero(numAzar);
                Console.Write("{0}",numAzar);
            }
        }

        static int CapturaEntero(string texto, int min, int max)
        {
            bool esCorrecto;
            int valor;
            do
            {
                Console.Write("{0} [{1}..{2}]: ", texto, min, max);

                esCorrecto = Int32.TryParse(Console.ReadLine(), out valor);
                if (!esCorrecto)
                    Console.WriteLine("\n\n\t** Error: el valor introducido no es un número entero");
                else if (valor < min || valor > max)
                {
                    esCorrecto = false;
                    Console.WriteLine("\n\n\t** Error: el valor introducido no está dentro del rango");
                }
            } while (!esCorrecto);
            return valor;
        }
    }
}
