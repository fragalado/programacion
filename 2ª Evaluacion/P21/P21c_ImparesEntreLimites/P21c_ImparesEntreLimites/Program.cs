﻿// Alumno: Gallego Dorado, Francisco José
using System;

namespace P21c_ImparesEntreLimites
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int min, max;
            do
            {
                min = CapturaEntero("Introduce el valor mínimo", 100, 998);
                max = CapturaEntero("Introduce el valor máximo", min+1, 999);

                if (min >= max)
                {
                    Console.Clear();
                    Console.Write("\n\t** ERROR: El mínimo no puede ser mayor o igual que el máximo **\n");
                }
            } while (min >= max);

            ImparesEntreLimites(min, max);
            Console.Write("\n\n\n\tPulse una tecla para mostrarlo en 6 columnas");
            Console.ReadKey();
            Console.Clear();
            ImparesEntreLimites(min, max, 6);
            
            Console.Write("\n\n\n\tPulse una tecla para mostrarlo en las columnas que usted quiera");
            Console.ReadKey();
            Console.Clear();
            Console.Write("\n\tIntroduce el número de columnas que quiere: ");
            ImparesEntreLimites(min, max, Convert.ToInt32(Console.ReadLine()));
            
            Console.Write("\n\n\n\tPulse una tecla para salir");
            Console.ReadKey();
        }

        static int CapturaEntero(string txt, int min, int max)
        {
            int valor;
            bool esCorrecto;

            Console.Write("{0} [{1}..{2}]: ", txt,min,max);
            esCorrecto = Int32.TryParse(Console.ReadLine(), out valor);
            while (!esCorrecto || valor < min || valor > max)
            {
                // comprobamos si lo que he escrito es un entero
                if (!esCorrecto)
                    Console.WriteLine("\n** Error: Valor introducido no es un número entero **");
                // comprobamos si lo que he escrito está dentro del rango
                else
                    Console.WriteLine("\n** Error: El número no está en el rango pedido **");

                Console.Write("{0}: ", txt);
                esCorrecto = Int32.TryParse(Console.ReadLine(), out valor);
            }
            return valor;
        }

        static void ImparesEntreLimites(int min, int max)
        {
            int num, cont=1;
            if (min % 2 == 0)
                num = min + 1;
            else
                num= min + 2;

            for (int i = num; i < max; i += 2)
                Console.WriteLine((cont++) + "ª- " + i);

            /* Esta forma va a comprobar todos los números, asi que es menos correcta
            int contador = 1;
            for (int i = min+1; i < max; i++)
            {
                if (i % 2 != 0)
                    Console.WriteLine((contador++) + "ª- " + i);
            }
            */
        }

        static void ImparesEntreLimites(int min, int max, int nc)
        {
            int num, cont = 0;
            if (min % 2 == 0)
                num = min + 1;
            else
                num = min + 2;

            for (int i = num; i < max; i += 2)
            {
                if (cont++ % nc == 0)
                    Console.WriteLine();
                Console.Write(i+"\t");
            }

        }
    }
}