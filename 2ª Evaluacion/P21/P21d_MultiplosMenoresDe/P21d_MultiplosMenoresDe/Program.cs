﻿// Alumno: Gallego Dorado, Francisco José
using System;

namespace P21d_MultiplosMenoresDe
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int num, max,suma;
            num = CapturaEntero("Introduce un número entero",10,99);
            max = CapturaEntero("Introduce un número tope (positivo)",num);

            suma = MultiplosMenoresDe(num, max);
            Console.Write("\n\tLa suma es: {0}",suma);

            Console.Write("\n\n\tPulse una tecla para salir");
            Console.ReadKey();

        }

        static int CapturaEntero(string texto, int min, int max)
        {
            int valor;
            bool ok;
            do
            {
                Console.Write("\n\t{0} [{1}..{2}]: ",texto,min,max);
                ok = Int32.TryParse(Console.ReadLine(), out valor);
                if (!ok)
                    Console.WriteLine("\n** Error: Valor introducido no es un número entero **");
                else if(valor < min || valor > max)
                    Console.WriteLine("\n** Error: El número no está en el rango pedido **");
            } while (!ok || valor < min || valor > max);
            return valor;
        }

        static int CapturaEntero(string texto, int min)
        {
            int valor;
            bool ok;
            do
            {
                Console.Write("\n\t{0} ({1}...]: ", texto,min);
                ok = Int32.TryParse(Console.ReadLine(), out valor);
                if (!ok)
                    Console.WriteLine("\n** Error: Valor introducido no es un número entero **");
                else if (valor<=min)
                    Console.WriteLine("\n** Error: El número no está en el rango pedido **");
            } while (!ok || valor <=min);
            return valor;
        }

        static int MultiplosMenoresDe(int num, int limiteSup)
        {
            int suma = 0;
            for (int i = num; i < limiteSup; i+=num)
            {
                Console.Write(i+"\t");
                suma += i;
            }
            return suma;
        }
    }
}
