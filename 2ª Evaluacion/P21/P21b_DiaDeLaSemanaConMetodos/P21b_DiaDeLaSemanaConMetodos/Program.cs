﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P21b_DiaDeLaSemanaConMetodos
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // Muestro los días
            MuestraDiasSemana();

            // Capturamos el día inicial
            int diaDeLaSemana = CapturaOpcion(0,6);

            //Comprobamos que el dia es correcto
            string nombreDiaSemana= NombreDiaSemana(diaDeLaSemana);
            Console.Write("\n"+nombreDiaSemana);

            // Pedimos cuántos días queremos avanzar
            Console.Write("\n\tIntroduce cuántos días quieres avanzar: ");
            int numDias = Convert.ToInt32(Console.ReadLine());

            // Si el día es correcto continuamos:
            string proximoDiaSemana= ProximoDiaSemana(diaDeLaSemana, numDias);

            // Imprimimos
            Console.WriteLine("\n\tEstamos a {0} y dentro de {1} días será {2}", nombreDiaSemana, numDias, proximoDiaSemana);
            // Para salir
            Console.Write("\n\n\t\tPulsa Intro para salir");
            Console.ReadLine();
        }
        
        static void MuestraDiasSemana()
        {
            Console.WriteLine("\n\t\t0) Domingo");
            Console.WriteLine("\n\t\t1) Lunes");
            Console.WriteLine("\n\t\t2) Martes");
            Console.WriteLine("\n\t\t3) Miércoles");
            Console.WriteLine("\n\t\t4) Jueves");
            Console.WriteLine("\n\t\t5) Viernes");
            Console.WriteLine("\n\t\t6) Sábado");
        }

        static int CapturaOpcion(int min, int max)
        {
            /*int diaDeLaSemana;
            bool ok;
            do
            {
                Console.Write("\n\tIntroduce un dia de la semana [{0}..{1}]: ", min, max);
                ok = Int32.TryParse(Console.ReadLine(), out diaDeLaSemana);
                if (!ok)
                    Console.Write("\n\t** Error: El valor introducido no es un número **");
                else if (diaDeLaSemana < 0 || diaDeLaSemana > 6)
                    Console.Write("\n\t** Error: El valor introducido no está dentro del rango **");
            } while (!ok || diaDeLaSemana < 0 || diaDeLaSemana > 6);
            Console.Clear();
            return diaDeLaSemana;*/
            int opcion;
            Console.Write("\n\tIntroduce un dia de la semana [{0}..{1}]: ", min, max);
            opcion = Console.ReadKey().KeyChar - '0';
            // Comprobamos que está bien
            while (opcion <min || opcion > max)
            {
                    Console.Write("\n\t** Error: No ha introducido una opción correcta **");
                    Console.Write("\n\tIntroduce un dia de la semana [{0}..{1}]: ", min, max);
                    opcion = Console.ReadKey().KeyChar - '0';
            }
            return opcion;
        }

        static string NombreDiaSemana(int idDiaSemana)
        {
            switch (idDiaSemana)
            {
                case 0:
                    return "Domingo"; break;
                case 1:
                    return "Lunes"; break;
                case 2:
                    return "Martes"; break;
                case 3:
                    return "Miércoles"; break;
                case 4:
                    return "Jueves"; break;
                case 5:
                    return "Viernes"; break;
                case 6:
                    return "Sábado"; break;
            }
            return String.Empty;
        }

        static string ProximoDiaSemana(int idDiaSemana, int numDias)
        {
            int diaDeLaSemanaFinal;
            do
            {
                if (numDias <= 0)
                {
                    Console.Clear();
                    Console.WriteLine("\n\t¡¡Avanzar significa un número de días mayor de cero!!");
                    Console.Write("\n\n\tIntroduce un nuevo número de días para avanzar: ");
                    numDias = Convert.ToInt32(Console.ReadLine());
                }
            } while (numDias <= 0);

            // Cálculo del día final
            diaDeLaSemanaFinal = (numDias + idDiaSemana) % 7;

            switch (diaDeLaSemanaFinal)
            {
                case 0:
                    return "Domingo"; break;
                case 1:
                    return "Lunes"; break;
                case 2:
                    return "Martes"; break;
                case 3:
                    return "Miércoles"; break;
                case 4:
                    return "Jueves"; break;
                case 5:
                    return "Viernes"; break;
                case 6:
                    return "Sábado"; break;
            }
            return String.Empty;
        }


    }
}
