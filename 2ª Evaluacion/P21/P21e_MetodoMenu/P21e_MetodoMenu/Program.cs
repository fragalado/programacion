﻿using System;

namespace P21e_MetodoMenu
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int opcion, num;

            do
            {
                opcion = Menu();
                if (opcion != 0)
                {
                    do
                    {
                        Console.Write("\n\tIntroduzca un número de dos cifras: ");
                        num = Convert.ToInt32(Console.ReadLine());
                        if (num < 10 || num > 99)
                            Console.Write("\n\t** Error!! número fuera de rango **\n");
                    } while (num < 10 || num > 99);

                    switch (opcion)
                    {
                        case 1:
                            MultiplosMenoresDe(num, 300);
                            break;
                        case 2:
                            PrimerosMultiplos(num, 100);
                            break;
                        case 3:
                            MultiplosEntre(num, 500, 700);
                            break;
                        case 4:
                            NMultiplosDesde(num, 80, 700);
                            break;
                    }

                    Console.Write("\n\n\t\tPulse una tecla para volver al menú");
                    Console.ReadKey();
                }

            } while (opcion != 0);

            Console.Write("\n\n\tPulsa una tecla para salir");
            Console.ReadKey();
        }

        static int Menu()
        {
            int opcion;
            Console.Clear();
            Console.WriteLine("\n\n\t\t╔════════════════════════════════════╗");
            Console.WriteLine("\t\t║           Menú Múltiplos           ║");
            Console.WriteLine("\t\t╠════════════════════════════════════╣");
            Console.WriteLine("\t\t║                                    ║");
            Console.WriteLine("\t\t║   1) Múltiplos menores de Max      ║");
            Console.WriteLine("\t\t║                                    ║");
            Console.WriteLine("\t\t║   2) N Primeros Múltiplos          ║");
            Console.WriteLine("\t\t║                                    ║");
            Console.WriteLine("\t\t║   3) Múltiplos Entre Min y Max     ║");
            Console.WriteLine("\t\t║                                    ║");
            Console.WriteLine("\t\t║   4) N Múltiplos Desde Min         ║");
            Console.WriteLine("\t\t║____________________________________║");
            Console.WriteLine("\t\t║                                    ║");
            Console.WriteLine("\t\t║           0) Salir                 ║");
            Console.WriteLine("\t\t╚════════════════════════════════════╝");

            Console.Write("\t\tIntroduce una opción: ");
            opcion = Console.ReadKey().KeyChar - '0';
            // Comprobamos que se ha pulsado una opción correcta
            while (opcion < 0 || opcion > 4)
            {
                Console.WriteLine("\n\t\t\t*ERROR*");
                Console.Write("\t\tIntroduce una opción: ");
                opcion = Console.ReadKey().KeyChar - '0';
            }
            Console.Clear();

            return opcion;
        }

        static void MultiplosMenoresDe(int num, int max)
        {
            Console.Clear();
            Console.WriteLine("\n\t----- Múltiplos de {0} menores de {1} -----", num,max);
            Console.WriteLine("\t------------------------------------------\n");
            int multiplo = num; // <-- Primer múltiplo
            while (multiplo < max)
            {
                Console.Write("\t" + multiplo);
                // siguiente múltiplo
                multiplo += num;
            }
        }

        static void PrimerosMultiplos(int num, int numeroDeMultiplos)
        {
            Console.Clear();
            Console.WriteLine("\n\t----- {1} primeros múltiplos de {0} -----", num, numeroDeMultiplos);
            Console.WriteLine("\t----------------------------------------\n");

            for (int i = 1; i <= numeroDeMultiplos; i++)
                Console.Write("\t{0}", i * num);

        }

        static void MultiplosEntre(int num, int min, int max)
        {
            Console.Clear();
            Console.WriteLine("\n\t----- Múltiplos de {0} entre {1} y {2} -----", num, min, max);
            Console.WriteLine("\t-------------------------------------------\n");
            //---- Averiguamos el primer múltiplo:
            // Múltiplo igual o inmediatamente anterior a min
            int multiplo = (min / num) * num;

            if (multiplo < min)
                multiplo += num;// múltiplo de num inmediatamente superior a min

            //---- Busco y muestro todos los demás múltiplos (menores o iguales a max)
            while (multiplo <= max)
            {
                Console.Write("\t{0}", multiplo);
                multiplo += num;
            }
        }

        static void NMultiplosDesde(int num, int n, int min)
        {
            Console.Clear();
            Console.WriteLine("\n\t----- {1} primeros múltiplos de {0} a partir de {2} -----", num,n,min);
            Console.WriteLine("\t-------------------------------------------------------\n");

            //---- Averiguamos el primer múltiplo:
            // Múltiplo igual o inmediatamente anterior a min
            int multiplo = (min / num) * num;
            //Como el min está incuido, sólo incremento múltiplo si ha salido menor
            if (multiplo < min)
                multiplo += num;

            // A partir de ese número incluido, escribo n más 
            for (int i = 0; i < n; i++)
            {
                Console.Write("\t" + multiplo);
                multiplo += num;
            }
        }
    }
}
