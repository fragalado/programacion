﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P20_PracticasDeEntrenamiento2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string nombre;
            int edad;

            Console.Write("\n\n\tDime tu nombre: ");

            nombre = Console.ReadLine();

            Console.Write("\n\n\tDime tu edad: ");

            edad = Convert.ToInt32(Console.ReadLine());

            Console.Write("\n\t\tBuenos días {0}: ", nombre);

            if (edad >= 18)
                Console.WriteLine("Ya eres mayor de edad"); // No hacen falta llaves porque es una sola instrucción
            else
            {
                if (edad == 17)
                    Console.WriteLine("Dentro de 1 año serás mayor de edad");
                else
                    Console.WriteLine("Dentro de {0} años serás mayor de edad", 18 - edad);
            }


            Console.Write("\n\n\n¿Es mayor de edad? "+ EsMayorDeEdad(edad));
            Console.Write("\n\n\n\t\tPulsa Intro para salir");
            Console.ReadLine();
        }

        static bool EsMayorDeEdad(int edad)
        {
            if (edad >= 18)
                return true;
            else
                return false;

            // return edad > 17;
        }
    }
}
