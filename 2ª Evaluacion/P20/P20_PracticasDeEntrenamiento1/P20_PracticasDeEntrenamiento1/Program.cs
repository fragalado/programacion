﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P20_PracticasDeEntrenamiento1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int a = Captura('a');
            int b = Captura('b');
            int c = Captura('c');

            // Calculamos la media con todos sus decimales, convirtiendo en double el numerador o el denominador
            double media = (a + b + c) / 3.0;  

            // Presentación en forma chachi
            Console.WriteLine("\n\n\tLa media de {0}, {1} y {2} es: {3}", a, b, c, MediaDeTres(a, b, c));

            Console.Write("\n\n\tPulse Intro para salir");
            Console.ReadLine();
        }
        static int Captura(char nombreVariable)
        {
            Console.Write("\n¿valor de {0}? ",nombreVariable);
            return Convert.ToInt32(Console.ReadLine());
        }
        static double MediaDeTres(int a, int b, int c)
        {
            return Math.Round((a + b + c) / 3.0, 3);
        }
    }
}
