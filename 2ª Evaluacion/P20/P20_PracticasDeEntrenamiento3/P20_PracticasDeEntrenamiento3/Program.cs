﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace P20_PracticasDeEntrenamiento3
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int anyo, mes, dia;

            anyo = Captura("año");

            mes = Captura("mes");

            if (mes < 1 || mes > 12) // <-- Mes incorrecto
                Console.WriteLine("\n\t\t** Error: Mes Fuera de rango **");
            else
            {
                dia = Captura("dia");
                // Averiguo si el día es incorrecto -   
                if ((dia < 1 || dia > 31) || // <-- vale para todos los meses
                        (dia == 31 && (mes == 4 || mes == 6 || mes == 9 || mes == 11)) || // <-- meses de 30 días
                        (dia > 28 && mes == 2)) // febrero
                    Console.WriteLine("\n\t\t** Error: día Fuera de rango **");
                else
                {
                    // Si llegamos aquí es que tanto el mes como el día es correcto
                    Console.WriteLine("\n\n\t\t"+ FechaEnTexto(dia, mes, anyo));
                }
            }


            Console.WriteLine("\n\n\t Pulsa INTRO para salir");
            Console.ReadLine();
        }

        static int Captura(string var)
        {
            Console.Write("\n\tIntroduce el {0}: ",var);
            return Convert.ToInt32(Console.ReadLine());
        }
        /*
        static void FechaEnTexto(int dia, string nombreMes, int anyo)
        {
            Console.WriteLine("\n\n\t\t La fecha es: {0} de {1} de {2} ", dia, nombreMes, anyo); ;
        }*/
        static string FechaEnTexto(int dia, int mes, int anyo)
        {
            string nombreMes = "";
            /*string nombreMes=String.Empty;
            string fehca = "La fecha es: " + dia + "-" + nombreMes + "-" + anyo;
            string fecha = String.Format("La fecha es: {0}-{1}-{2}",dia,mes,anyo);*/
            switch (mes)
            {
                case 1:
                    nombreMes = "Enero";
                    break;
                case 2:
                    nombreMes = "Febrero";
                    break;
                case 3:
                    nombreMes = "Marzo";
                    break;
                case 4:
                    nombreMes = "Abril";
                    break;
                case 5:
                    nombreMes = "Mayo";
                    break;
                case 6:
                    nombreMes = "Junio";
                    break;
                case 7:
                    nombreMes = "Julio";
                    break;
                case 8:
                    nombreMes = "Agosto";
                    break;
                case 9:
                    nombreMes = "Septiembre";
                    break;
                case 10:
                    nombreMes = " Octubre ";
                    break;
                case 11:
                    nombreMes = " Noviembre ";
                    break;
                case 12:
                    nombreMes = "Diciembre";
                    break;
            }
            return "La fecha es: " + dia + "-" + nombreMes + "-" + anyo;
        }
    }
}
