﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P20_PracticasDeEntrenamiento
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int radio;
            Console.Write("\n\tIntroduce el radio del círculo: ");
            radio = Convert.ToInt32(Console.ReadLine());
            double pi = System.Math.PI;

            PresentarResultado(radio, pi);
            Console.Write("\n\n\n\tPulse INTRO para salir");
            Console.ReadLine();
        }

        static double AreaCirculo(int r, double pi)
        {
            double area = pi * Math.Pow(r, 2);
            return area;
        }

        static double PerimetroCirculo(int r, double pi)
        {
            return 2 * pi * r;
        }

        static void PresentarResultado(int r, double pi)
        {
            Console.Write("\n\tLa longitud de su circunferencia es: " + PerimetroCirculo(r, pi));
            Console.Write("\n\tEl área es: " + AreaCirculo(r, pi));
        }
    }
}
