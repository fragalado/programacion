﻿using System;

namespace P21a_CapturaEntero
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int cant, min, num, nc;

            cant = CapturaEntero("Cantidad de múltiplos a presentar?",100,300);
            num = CapturaEntero("¿Número del que obtener sus múltiplos?", 11,77);
            min = CapturaEntero("¿Valor mínimo?",1000,2000);
            nc = CapturaEntero("¿Número de columnas?",3,8);

            // cálculo del primer múltiplo suponiendo que el límite inferior está incluido
            int multiplo = (min / num) * num;

            if (multiplo < min)
                multiplo += num;

            // presentación
            Console.Clear();
            Console.WriteLine("\n{0} primeros múltiplos de {1} a partir de {2} en {3} columnas", cant, num, min, nc);

            for (int i = 0; i < cant; i++)
            {
                // cuando el contador sea múltiplo del nº de columnas...
                // Salto de línea
                if (i % nc == 0)
                    Console.WriteLine();

                Console.Write("{0}\t", multiplo); // <-- Presentamos el múltiplo
                multiplo += num;                  // <-- obtenemos el siguiente
            }

            Console.Write("\n\n\tPulsa una tecla para salir");
            Console.ReadKey();
        }

        static int CapturaEntero(string texto, int min, int max)
        {
            /*
            int numero;
            bool ok;
            
            do
            {
                Console.Write("\n\t{2} [{0}..{1}]: ", min, max, texto);
                ok = Int32.TryParse(Console.ReadLine(), out numero);
                if (!ok)
                    Console.Write("\n\t** Error: El valor introducido no es un entero **");
                else if (ok && (numero < min || numero > max))
                    Console.Write("\n\t** Error: El valor introducido no está entre los límites indicados");

            } while (!ok || numero < min || numero > max);
            */
            // Lo mismo pero con while
            int numero = 0;
            bool ok = false;
            while (!ok)
            {
                Console.Write("\n\t{2} [{0}..{1}]: ", min, max, texto);
                ok = Int32.TryParse(Console.ReadLine(), out numero);
                if (!ok)
                    Console.Write("\n\t** Error: El valor introducido no es un entero **");
                else if (numero < min || numero > max)
                {
                    Console.Write("\n\t** Error: El valor introducido no está entre los límites indicados");
                    ok = false;
                }
            }
            return numero;
            
        }
    }
}