﻿using System;
using System.Collections.Generic;

namespace P23c_ListaDeGente
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // 1)
            string[] vApellidos = { "Sánchez Elegante", "Arenas Mata", "García Solís", "Rodríguez Vázquez", "Hurtado Miranda", "Pinto Mirinda", "Barrios Garrobo", "Márquez Salazar", "Medina Gómez", "Alonso Pérez", "López Mora", "González Chaparro", "Ferrer Jiménez", "Morales Moncayo", "Fernández Perea", "Blanco Roldán", "Navarro Romero", "Aguilar Rubio", "Baena Fernández", "Barco Ramírez", "Delgado Rodríguez", "Duque Martínez" };
            string[] vNombres = { "Álvaro", "Daniel Luis", "Juan Manuel", "Agustín", "Fco. Javier", "José Manuel", "Tomás", "Carlos", "Jose Carlos", "Juan Luis", "Daniel", "Angel", "Jacobo", "Alejandro", "Francisco", "Alfredo", "Francisco", "Antonio", "Constantino", "Roberto", "Rafael", "Antonio" };
            // 2)
            List<string> listaApellNomb = DevuelveListaGente(vNombres, vApellidos);
            // 3)
            MuestraColeccion(listaApellNomb, 0);
            // 4)
            int posicion1 = CapturaEntero("¿posición 1?", 0, listaApellNomb.Count - 1);
            int posicion2 = CapturaEntero("¿posición 2?", 0, listaApellNomb.Count - 1);
            IntercambiaPos(listaApellNomb, posicion1, posicion2);
            MuestraColeccion(listaApellNomb, 40);
            // 5)
            Pausa("limpiar la pantalla");
            Console.Clear();
            MuestraColeccion(listaApellNomb, 0);
            int posicion = CapturaEntero("¿posición para eliminar?", 0, listaApellNomb.Count - 1);
            listaApellNomb.RemoveAt(posicion);
            MuestraColeccion(listaApellNomb, 40);
            // 6)
            Pausa("limpiar la pantalla");
            Console.Clear();
            MuestraColeccion(listaApellNomb, 0);
            Console.Write("\n\n\tEscribe 'Apellidos, Nombre' de la persona ha eliminar: ");
            string persona = Console.ReadLine();
            if (!BorraElemento(listaApellNomb, persona))
            {
                Console.Write("\n\tLa persona '{0}' no existe, luego, se va a insertar en medio de la tabla", persona);
                listaApellNomb.Insert(listaApellNomb.Count / 2, persona);
            }
            MuestraColeccion(listaApellNomb, 40);
            // 7)
            Pausa("limpiar la pantalla");
            Console.Clear();
            MuestraColeccion(listaApellNomb, 0);
            listaApellNomb.Sort();
            MuestraColeccion(listaApellNomb, 40);



            // Para salir
            Console.Write("\n\n\n\n\n\tPulse una tecla para salir");
            Console.ReadKey();
        }

        private static int CapturaEntero(string txt, int min, int max)
        {
            int numero;
            bool ok;
            do
            {
                Console.Write("\n\n\t{0} [{1}..{2}]: ", txt, min, max);
                ok = Int32.TryParse(Console.ReadLine(), out numero);
                if (!ok)
                    Console.Write("\n\t** Error: No se ha introducido un valor **");
                else if(numero <min || numero > max)
                    Console.Write("\n\t** Error: Se ha introducido un valor fuera del rango **");
            } while (!ok || numero < min || numero > max);

            return numero;
        }

        private static List<string> DevuelveListaGente(string[] nombre, string[] apellidos)
        {
            List<string> lista = new List<string>();
            // También podríamos haber puesto apellidos.Length
            for (int i = 0; i < nombre.Length; i++)
            {
                lista.Add(String.Format("{0}, {1}", apellidos[i], nombre[i]));
            }
            return lista;
        }

        /// <summary>
        /// Recibe una lista de string y un int colum. Muestra los elementos de la lista, con su índice delante, colocados en la columna de la pantalla que indique colum.
        /// </summary>
        /// <param name="lista">Lista con los elementos a presentar</param>
        /// <param name="colum">Columna de la pantalla donde se va a presentar</param>
        private static void MuestraColeccion(List<string> lista, int colum)
        {
            // Recorremos la lista
            for (int i = 0; i < lista.Count; i++)
            {
                // Ponemos el cursor en la fila y columna correspondiente
                Console.SetCursorPosition(colum, i+2);
                // Mostramos el elemento de cada posición con su índice delante
                // Si el indice es menor de 10 vamos a dar un salto
                if (i < 10)
                    Console.Write(" ");
                // También se podría simplicar:
                // Console.Write("{0}) {1}", i.toString("00"), lista[i]);
                Console.Write("{0}) {1}", i, lista[i]);
            }
        }

        private static void IntercambiaPos(List<string> lista, int pos1, int pos2)
        {
            int auxiliar;
            if(pos1 > pos2)
            {
                auxiliar = pos1;
                pos1 = pos2;
                pos2 = auxiliar;
            }
            // Intercambiamos los elementos de las posiciones dadas
            lista.Insert(pos1, lista[pos2]);
            // Para insertar el segundo, como el valor anterior ha pasado al siguiente indice
            // vamos a tener que coger el indice siguiente
            lista.Insert(pos2+1, lista[pos1+1]);

            // Eliminamos los valores anteriores
            lista.RemoveAt(pos1 + 1);
            lista.RemoveAt(pos2 + 1);



            // Mucho mas resumido
            /*
            string aux = listaGente[pos1];
            listaGente[pos1] = listaGente[pos2];
            listaGente[pos2] = aux;
            */
        }

        private static bool BorraElemento(List<string> lista, string persona)
        {
            // Devueve true si lo ha podido borrar o false si no ha podido
            return lista.Remove(persona);
        }

        private static void Pausa(string texto)
        {
            Console.Write("\n\n\tPulse una tecla para "+ texto);
            Console.ReadKey();
        }
    }
}
