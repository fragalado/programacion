﻿using System;
using System.Collections.Generic;

namespace P23_Ejemplo1UsosLista_Int
{
    internal class Program
    {
        static int filaMensajes = 4;
        static void Main(string[] args)
        {
            Random azar = new Random();
            //1) Declaramos lista enteros
            List<int> miLista;

            //2) Construimos la lista
            miLista= new List<int>();

            //3) Preguntamos con cuántos elementos la queremos cargar [10..24]
            int tamanyoLista = CapturaEntero("¿Con cuántos elemetos la queremos cargar?",10,24);
            filaMensajes += tamanyoLista;

            //4) Cargamos la lista con números de dos cifras
            for (int i = 0; i < tamanyoLista; i++)
            {
                miLista.Add(azar.Next(10,99+1));
            }

            //5) Mostramos los elementos de la lista
            Console.Clear();
            MuestraListaEnColumna(miLista, 4 , "ORIGINAL");

            //6) Pedimos al usuario un número a insertar [100..300] y la posición donde insertarla
            int numeroAInsertar = CapturaEntero("¿número a insertar?", 100,300);
            int posicionInsertar = CapturaEntero("¿posición donde insertarla?", 0,tamanyoLista);
            // Se inserta y mostramos la lista a la derecha de la anterior
            miLista.Insert(posicionInsertar, numeroAInsertar);
            MuestraListaEnColumna(miLista, 16, "CAMBIADA");


            //7) Pedimos un número a eliminar [10..300]. Si existe se elimina y se presenta la lista a la derecha de la anterior.
            //   Si no existe, da el mensaje de que no existe
            int numeroAEliminar = CapturaEntero("\n\n\t¿número a eliminar?", 10, 300);
            if (miLista.Remove(numeroAEliminar))
                MuestraListaEnColumna(miLista, 28, "CAMBIADA 2");
            else
                Console.Write("\tEl número {0} no existe.", numeroAEliminar);


            //8) Se pide una posición a eliminar. Se elimina y se presenta la lista a la derecha de la anterior.
            int posicionAEliminar = CapturaEntero("\n\n\t¿posicion a eliminar?", 0, tamanyoLista-1);


            // Para salir
            Console.Write("\n\n\n\tPulse una tecla para salir");
            Console.ReadKey();
        }

        private static void MuestraListaEnColumna(List<int> lista, int columna, string cabecera)
        {
            Console.SetCursorPosition(columna,2);
            Console.Write(cabecera);
            for (int i = 0; i < lista.Count; i++)
            {
                Console.SetCursorPosition(columna, i+3);
                Console.Write("{0}) {1}",i, lista[i]);
            }
        }

        private static int CapturaEntero(string texto, int min, int max)
        {
            int valor;
            bool ok;
            do
            {
                LimpiaLinea(filaMensajes);
                Console.Write("\n\t{0} [{1}..{2}]: ",texto,min,max);
                ok = Int32.TryParse(Console.ReadLine(), out valor);
                if (!ok)
                {
                    Console.Write("\n\t** Error: No se ha introducido un valor entero **");
                }
                else if(valor <min || valor > max)
                    Console.Write("\n\t** Error: No se ha introducido un valor dentro del rango **");
            } while (!ok || valor < min || valor > max);
            return valor;
        }

        private static void LimpiaLinea(int fila)
        {
            Console.SetCursorPosition(0, filaMensajes);
            Console.Write("                                                                                              ");
            Console.SetCursorPosition(0, filaMensajes);
        }
    }
}
