﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P41b4_ProyParalelogramo
{
    class Cuadrado : Paralelogramo
    {
        // Atributos -> Clase padre

        // Constructores

        public Cuadrado(string nombre, int ladoBase) : base(nombre, ladoBase, ladoBase, 90)
        {
        }

        // Propiedades básicas

        // Propiedades especiales

        public override double Area
        {
            get
            {
                return LadoBase * LadoBase;
            }
        }

        // Métodos
    }
}
