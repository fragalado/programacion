﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P41b4_ProyParalelogramo
{
    class Rectangulo : Paralelogramo
    {
        // Atributos -> Clase padre

        // Constructores

        public Rectangulo(string nombre, int ladoBase, int ladoLateral) : base(nombre, ladoBase, ladoLateral, 90)
        {
        }

        // Propiedades básicas -> Clase padre

        // Propiedades especiales

        public override double Area
        {
            get
            {
                return LadoBase * LadoLateral;
            }
        }

        // Métodos -> Clase padre
    }
}
