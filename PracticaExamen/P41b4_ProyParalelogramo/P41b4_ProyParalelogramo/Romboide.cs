﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P41b4_ProyParalelogramo
{
    class Romboide : Paralelogramo
    {
        // Atributos -> Clase padre

        // Constructores

        public Romboide(string nombre, int ladoBase, int ladoLateral, double angulo) : base(nombre, ladoBase, ladoLateral, angulo)
        {
        }

        // Propiedades básicas -> Clase padre

        // Propiedades especiales -> Clase padre

        // Métodos -> Clase padre

    }
}
