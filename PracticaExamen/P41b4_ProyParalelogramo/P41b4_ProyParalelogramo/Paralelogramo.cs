﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P41b4_ProyParalelogramo
{
    class Paralelogramo
    {
        // Atributos

        string nombre;
        int ladoBase, ladoLateral;
        double angulo;

        // Constructores

        public Paralelogramo(string nombre, int ladoBase, int ladoLateral, double angulo)
        {
            this.nombre = nombre;
            this.ladoBase = ladoBase;
            this.ladoLateral = ladoLateral;
            this.angulo = angulo;
        }

        // Propiedades básicas

        public string Nombre { get => nombre; set => nombre = value; }
        public int LadoBase { get => ladoBase; set => ladoBase = value; }
        public int LadoLateral { get => ladoLateral; set => ladoLateral = value; }
        public double Angulo { get => angulo; set => angulo = value; }

        // Propiedades especiales

        public virtual int Perimetro
        {
            get
            {
                return LadoBase * 2 + ladoLateral * 2;
            }
        }

        public virtual double Area
        {
            get
            {
                return Math.Round(ladoBase * ladoLateral * Math.Sin(angulo * Math.PI / 180), 2);
            }
        }

        // Métodos

        public virtual string ComoString()
        {
            return String.Format("\t{0}\t{1}\t{2}\t{3}\t{4}\t{5}", nombre, ladoBase, ladoLateral, angulo, Perimetro, Area);
        }
    }
}
