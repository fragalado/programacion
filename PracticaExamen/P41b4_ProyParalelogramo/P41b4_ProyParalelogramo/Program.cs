﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P41b4_ProyParalelogramo
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Paralelogramo> listaFiguras = new List<Paralelogramo>();

            int opcion = Util.Menu();
            int ladoBase=0, ladoLateral=0, contCuadrado=0, contRectang=0, contRomb=0, contRomboide=0;
            double angulo=0;
            while(opcion != 0)
            {
                Console.Clear();
                if(opcion != 5)
                {
                    // Declaramos un objeto de tipo Paralelogramo
                    Paralelogramo p = null;

                    // Pedimos el lado base
                    ladoBase = Util.CapturaEntero("Introduzca el lado base de la figura", 0, 20);

                    // Si es rectangulo o romboide pedimos el lado lateral
                    if (opcion == 2 || opcion == 4)
                        ladoLateral = Util.CapturaEntero("Introduzca el lado lateral de la figura", 0, 20);

                    // Si es rombo o romboide pedimos el angulo
                    if (opcion == 3 || opcion == 4)
                        angulo = Util.CapturaEntero("Introduzca el angulo de la figura", 0, 90);

                    // Construimos la figura con los datos o presentamos
                    switch (opcion)
                    {
                        case 1:
                            // Cuadrado
                            p = new Cuadrado("cuad-" + (contCuadrado++), ladoBase);
                            break;
                        case 2:
                            // Rectangulo
                            p = new Rectangulo("rectn-" + (contRectang++), ladoBase, ladoLateral);
                            break;
                        case 3:
                            // Rombo
                            p = new Rombo("romb-" + (contRomb++), ladoBase, angulo);
                            break;
                        case 4:
                            // Romboide
                            p = new Romboide("roide-" + (contRomboide++), ladoBase, ladoLateral, angulo);
                            break;
                    }
                    // Añadimos a la lista el paralelogramo
                    listaFiguras.Add(p);
                }
                else
                {
                    // Presentar las figuras
                    Console.WriteLine("\tNombre\tLado base\tLado lateral\tAngulo\tPerimetro\tArea");
                    Console.WriteLine("\t------\t---------\t------------\t------\t---------\t----");
                    foreach (Paralelogramo aux in listaFiguras)
                    {
                        Console.WriteLine(aux.ComoString());
                    }
                }


                Console.Write("\n\tPulse una tecla para volver al menú");
                Console.ReadKey(true);
                // Presentamos el menu de nuevo
                opcion = Util.Menu();

            }


            // para salir
            Console.WriteLine("\n\n\n\tPulse una tecla para salir");
            Console.ReadKey();
        }
    }
}
