﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P41b4_ProyParalelogramo
{
    class Rombo : Paralelogramo
    {
        // Atributos -> Clase padre

        // Constructores

        public Rombo(string nombre, int ladoBase, double angulo) : base(nombre, ladoBase, ladoBase, angulo)
        {
        }

        // Propiedades básicas -> Clase padre

        // Propiedades especiales -> Clase padre

        // Métodos -> Clase padre
    }
}
