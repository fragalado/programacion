﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P41b4_ProyParalelogramo
{
    class Util
    {
        public static int Menu()
        {
            Console.Clear();
            Console.WriteLine("\n\t\t╔═══════════════════════════════════════════╗");
            Console.WriteLine("\t\t║             * MENÚ *                      ║");
            Console.WriteLine("\t\t╠═════════════════════╦═════════════════════╣");
            Console.WriteLine("\t\t║  1. Cuadrado        ║  2. Rectángulo      ║");
            Console.WriteLine("\t\t╠═════════════════════╬═════════════════════╣");
            Console.WriteLine("\t\t║  3. Rombo           ║  4. Romboide        ║");
            Console.WriteLine("\t\t╠═════════════════════╬═════════════════════╣");
            Console.WriteLine("\t\t║  5. Presentar       ║  0. Salir           ║");
            Console.WriteLine("\t\t╚═════════════════════╩═════════════════════╝");

            int opcion = CapturaOpcion("Selecciona la opcion a seguir", 0, 5);

            return opcion;
        }

        public static int CapturaEntero(string txt, int min, int max)
        {
            int num;
            bool ok;

            do
            {
                Console.Write("\n\t{0} [{1}..{2}]: ", txt, min, max);
                ok = Int32.TryParse(Console.ReadLine(), out num);

                if (!ok)
                    Console.Write("\n\t** ERROR: No se ha introducido el formato correcto **");
                else if (num < min || num > max)
                    Console.Write("\n\t** ERROR: El valor debe estar dentro del rango **"); 
            } while (!ok || num < min || num > max);

            return num;
        }

        public static int CapturaOpcion(string txt, int min, int max)
        {
            int num;
            bool ok;

            do
            {
                Console.Write("\n\t{0} [{1}..{2}]: ", txt, min, max);
                ok = Int32.TryParse(Console.ReadKey().KeyChar.ToString(), out num);

                if (!ok)
                    Console.Write("\n\t** ERROR: No se ha introducido el formato correcto **");
                else if (num < min || num > max)
                    Console.Write("\n\t** ERROR: El valor debe estar dentro del rango **");
            } while (!ok || num < min || num > max);

            return num;
        }
    }
}
