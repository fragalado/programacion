﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P42a1_PruebasExcepciones
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int estatura;
            float peso;
            double imc;
            bool hayError;
            do
            {
                Console.Clear();
                hayError = false;
                try
                {
                    // Pedimos la estatura
                    Console.Write("\n\tIntroduzca su estatura en cm: ");
                    estatura = Convert.ToInt32(Console.ReadLine());
                    if (estatura < 50 || estatura > 220)
                        throw new OverflowException("La estatura tiene que estar entre 50 y 220 cm");

                    // Pedimos el peso
                    Console.Write("\n\tIntroduzca su peso en kg: ");
                    peso = Convert.ToSingle(Console.ReadLine());

                    // Calculamos el imc
                    imc = peso / Math.Pow(estatura * 0.01, 2);

                    if (imc < 18.5)
                        Console.WriteLine("TE FALTA PESO");
                    else if (18.5 <= imc && imc < 25)
                        Console.WriteLine("Peso saludable");
                    else if (25 <= imc && imc < 30)
                        Console.WriteLine("Tienes SOBREPESO");
                    else if (30 <= imc && imc < 31)
                        Console.WriteLine("LEVE OBESIDAD");
                    else if (31 <= imc)
                        Console.WriteLine("¡¡ERES OBESO!!");
                }
                catch(OverflowException e)
                {
                    hayError = true;
                    Console.WriteLine("** ERROR: {0} **",e.Message);
                }
                // Error de formato
                catch(FormatException e)
                {
                    hayError = true;
                    Console.WriteLine("** ERROR: {0} **", e.Message);
                }

                if (hayError)
                    Console.ReadKey(true);
            } while (hayError || PreguntaSiNo("¿Quieres repetir?"));
        }

        public static bool PreguntaSiNo(string txt)
        {
            char opcion;

            do
            {
                Console.Write("\n\t{0} [s=Si/n=No]: ", txt);
                opcion = Console.ReadKey().KeyChar;

                if (opcion == 's' || opcion == 'S')
                    return true;
                else if (opcion == 'n' || opcion == 'N')
                    return false;

                // Si llega aqui es porque ha fallado
                Console.Write("\n\t** ERROR: No se ha introducido una opcion correcta **");
            } while (true);
        }
    }
}
