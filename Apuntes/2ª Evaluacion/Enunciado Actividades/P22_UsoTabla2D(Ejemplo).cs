﻿
using System;

class Program
{
    static void Main(string[] args)
    {
        // Ejemplo declaración de un array de 50 x 70
        int[,] a2D = new int[50, 70];

        // declaración y construcción de un array de 3 x 5
        int[,] tabla2D = { { 1, 2, 3, 0, 8 },
                           { 2, 4, 6, 8, 0 },
                           { 3, 5, 7, 9, 1}};
        //                                                      Tamaño total ─┐    Primera dimensión ─┐            segunda ─┐
        Console.WriteLine("\n\n\t Tamaño total = {0};  en {1} x {2}\n", tabla2D.Length, tabla2D.GetLength(0), tabla2D.GetLength(1));

        Console.WriteLine("\n\n\t Mostrar Todos usando los límites genéricos\n");
        for (int i = 0; i < tabla2D.GetLength(0); i++) // <-- Recorriendo las filas
        {
            for (int j = 0; j < tabla2D.GetLength(1); j++)// <-- Recorriendo las columnas
            {
                Console.Write("\t {0}", tabla2D[i, j]);
            }
            Console.WriteLine("\n");
        }


        Console.WriteLine("\n\n\t Sólo los 4 primeros números de las dos primeras filas\n");
        for (int i = 0; i < 2; i++)
        {
            for (int j = 0; j < 4; j++)
            {
                Console.Write("\t {0}", tabla2D[i, j]);
            }
            Console.WriteLine("\n");
        }

        int fila = CapturaEntero("\n\n\t ¿Qué fila quieres leer?", 0, tabla2D.GetLength(0) - 1);

        Console.WriteLine("\n\n\t Mostrar los elementos de la fila elegida\n");
        for (int i = 0; i < tabla2D.GetLength(1); i++)
            Console.Write("\t {0}", tabla2D[fila, i]);

        int columna = CapturaEntero("\n\n\t ¿Qué columna quieres leer?", 0, tabla2D.GetLength(1) - 1);

        Console.WriteLine("\n\n\t Mostrar los elementos de la columna elegida\n");
        for (int i = 0; i < tabla2D.GetLength(0); i++)
            Console.WriteLine("\t {0}", tabla2D[i, columna]);

        Console.WriteLine("\n\n\t Mostrar el elemento de la (fila, columna) ({0}, {1})\n", fila, columna);
        Console.WriteLine("\t {0}", tabla2D[fila, columna]);

        Console.WriteLine("\n\n\t Asignar un valor a la posición de la fila y columna elegida\n");
        tabla2D[fila, columna] = CapturaEntero(String.Format("\n\t¿Nuevo valor de la posición ({0}, {1})?", fila, columna), -100, 100);

        Console.WriteLine("\n\n\t Mostrando Todos de nuevo\n");
        for (int i = 0; i < tabla2D.GetLength(0); i++) // <-- Recorriendo las filas
        {
            for (int j = 0; j < tabla2D.GetLength(1); j++)// <-- Recorriendo las columnas
                Console.Write("\t {0}", tabla2D[i, j]);

            Console.WriteLine("\n");
        }


        Console.Write("\n\n\n\t\tPulsa una tecla para salir");
        Console.ReadKey(true);
    }



    public static bool PreguntaSiNo(string pregunta)
    {
        char tecla;
        do
        {
            // Hacemos la pregunta
            Console.Write("\n\n{0} (s=Sí; n=No): ", pregunta);
            // Capturamos la respuesta (una pulsación)
            tecla = (Console.ReadKey()).KeyChar;
            if (tecla == 's' || tecla == 'S')
                return true;
            if (tecla == 'n' || tecla == 'N')
                return false;
            // Si llega aquí es que no ha pulsado ninguna de las teclas correctas
            Console.Write("\n\n\t** Error: por favor, responde S o N **");

        } while (true);

    }


    static int CapturaEntero(string texto, int min, int max)
    {
        bool esCorrecto;
        int valor;
        do
        {
            Console.Write("{0} [{1}..{2}]: ", texto, min, max);

            esCorrecto = Int32.TryParse(Console.ReadLine(), out valor);
            if (!esCorrecto)
                Console.WriteLine("\n\n\t** Error: el valor introducido no es un número entero");
            else if (valor < min || valor > max)
            {
                esCorrecto = false;
                Console.WriteLine("\n\n\t** Error: el valor introducido no está dentro del rango");
            }
        } while (!esCorrecto);
        return valor;
    }

}
