﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P43b1_ProyPorton
{
    class Puerta : Porton
    {
        // Atributos -> Clase padre

        // Constructores

        // Constructor 1 -> Recibe alto, ancho y color
        public Puerta(int alto, int ancho, ColorPuerta color) : base(alto, ancho, color)
        {
        }

        // Constructor 2 -> Recibe alto y ancho y se asigna el color blanco
        public Puerta(int alto, int ancho) : base(alto, ancho)
        {
        }

        // Propiedades básicas -> Clase padre

        // Propiedades especiales

        public override string Estado
        {
            get
            {
                return (Abierta) ? "abierta" : "cerrada";
            }
        }

        // Métodos
        public override void Abrir()
        {
            if (!Abierta) // Si esta cerrada
            {
                Abierta = true; // La abrimos
                Console.Write("\n\t¡puerta abierta!");
            }
            else
                Console.Write("\n\t¡La puerta ya estaba abierta!");
        }

        public override void Cerrar()
        {
            if (Abierta) // Si esta abierta
            {
                Abierta = false; // La cerramos
                Console.Write("\n\t¡puerta cerrada!");
            }
            else
                Console.Write("\n\t¡La puerta ya estaba cerrada!");
        }
    }
}
