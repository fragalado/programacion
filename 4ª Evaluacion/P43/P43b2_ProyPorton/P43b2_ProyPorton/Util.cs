﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P43b1_ProyPorton
{
    class Util
    {

        public static int CapturaEntero(string mensaje, int min, int max)
        {
            int valor = 0;
            bool ok = false;

            do
            {
                Console.Write("\n\t{0} [{1}..{2}]: ", mensaje, min, max);
                ok = Int32.TryParse(Console.ReadLine(), out valor);

                if (!ok)
                    Console.Write("\n\t** ERROR: Formato no correcto **");
                else if (valor < min || valor > max)
                    Console.Write("\n\t** ERROR: El valor no está dentro de los rangos **");
            } while (!ok || valor < min || valor > max);

            return valor;
        }

        public static int CapturaNumPulsado(string mensaje, int min, int max)
        {
            int valor = 0;
            bool ok = false;

            do
            {
                Console.Write("\n\t{0} [{1}..{2}]: ", mensaje, min, max);
                ok = Int32.TryParse(Console.ReadKey(true).KeyChar.ToString(), out valor);

                if (!ok)
                    Console.Write("\n\t** ERROR: Formato no correcto **");
                else if (valor < min || valor > max)
                    Console.Write("\n\t** ERROR: El valor no está dentro de los rangos **");
            } while (!ok || valor < min || valor > max);

            return valor;
        }

        public static bool PreguntaSiNo(string txt)
        {
            char opcion;

            do
            {
                Console.Write("\n\t{0} [s=Si/n=No]: ", txt);
                opcion = Console.ReadKey(true).KeyChar;

                if (opcion == 's' || opcion == 'S')
                    return true;
                if (opcion == 'n' || opcion == 'N')
                    return false;

                // Si llega aqui será porque ha introducido mal
                Console.Write("\n\t** ERROR: No se ha introducido una opción correcta **");
            } while (true);

        }

        public static int Menu()
        {
            Console.Clear();
            Console.WriteLine("\n\n\n");
            Console.WriteLine("\t\t\t╔══════════════════════════╗");
            Console.WriteLine("\t\t\t║   MENÚ de PUERTA         ║");
            Console.WriteLine("\t\t\t╠══════════════════════════╣");
            Console.WriteLine("\t\t\t║     1) Abrir             ║");
            Console.WriteLine("\t\t\t║                          ║");
            Console.WriteLine("\t\t\t║     2) Cerrar            ║");
            Console.WriteLine("\t\t\t║                          ║");
            Console.WriteLine("\t\t\t║     3) Bloquear          ║");
            Console.WriteLine("\t\t\t║                          ║");
            Console.WriteLine("\t\t\t║     4) Desbloquear       ║");
            Console.WriteLine("\t\t\t║                          ║");
            Console.WriteLine("\t\t\t║     5) Mostrar           ║");
            Console.WriteLine("\t\t\t║                          ║");
            Console.WriteLine("\t\t\t║     6) Pintar            ║");
            Console.WriteLine("\t\t\t║                          ║");
            Console.WriteLine("\t\t\t║     7) Fabricar Puerta   ║");
            Console.WriteLine("\t\t\t║                          ║");
            Console.WriteLine("\t\t\t║     8) Fabricar Porton   ║");
            Console.WriteLine("\t\t\t║                          ║");
            Console.WriteLine("\t\t\t║     9) Eliminar          ║");
            Console.WriteLine("\t\t\t║__________________________║");
            Console.WriteLine("\t\t\t║                          ║");
            Console.WriteLine("\t\t\t║     0) Salir             ║");
            Console.WriteLine("\t\t\t╚══════════════════════════╝");

            return CapturaNumPulsado("\t\t\tPulse su opción", 0, 9);
        }

        public static string CuadraTexto(string txt, int longitud, bool conPuntos)
        {
            string aux;

            if (conPuntos)
                aux = "....................................................................";
            else
                aux = "                                                                    ";

            txt += aux;

            return txt.Substring(0, longitud);
        }
    }
}
