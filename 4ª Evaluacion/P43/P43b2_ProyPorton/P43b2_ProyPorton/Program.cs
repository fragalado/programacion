﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P43b1_ProyPorton
{
    class Program
    {
        static ColorPuerta[] vColorPuerta = {new ColorPuerta("Gris", ConsoleColor.DarkGray), new ColorPuerta("Azul", ConsoleColor.Blue)
                    , new ColorPuerta("Verde", ConsoleColor.Green), new ColorPuerta("Cyan", ConsoleColor.Cyan), new ColorPuerta("Rojo", ConsoleColor.Red)
                    , new ColorPuerta("Magenta", ConsoleColor.DarkMagenta), new ColorPuerta("Amarillo", ConsoleColor.Yellow), new ColorPuerta("Blanco", ConsoleColor.White)};

        static void Main(string[] args)
        {
            // Objetos del tipo Puerta y Objetos del tipo Porton
            Porton p = new Puerta(100, 80);
            Porton p1 = new Puerta(120, 50, vColorPuerta[4]);
            Porton p2 = new Porton(160, 80, vColorPuerta[3]);
            Porton p3 = new Porton(200, 80, vColorPuerta[7]);

            // Variables necesarias
            int opcion, id;
            List<Porton> listaPortones = new List<Porton>();

            // Cuando se entra en la aplicación se construirán 4 objetos, 2 objetos portón y 2 objetos puerta con los valores y colores que
            // quieras.A continuación, se presentará el menú y actuará en consecuencia.
            listaPortones.Add(p);
            listaPortones.Add(p1);
            listaPortones.Add(p2);
            listaPortones.Add(p3);

            do
            {
                opcion = Util.Menu(); // Mostramos el menu y obtenemos la opcion elegida

                Console.Clear(); // Limpiamos pantalla
                switch (opcion)
                {
                    case 1:
                        // Abrir
                        // Pedimos el id del porton/puerta
                        id = Util.CapturaEntero("Introduzca el id del porton o puerta", 0, listaPortones.Count - 1);
                        listaPortones[id].Abrir();
                        break;
                    case 2:
                        // Cerrar
                        id = Util.CapturaEntero("Introduzca el id del porton o puerta", 0, listaPortones.Count - 1);
                        listaPortones[id].Cerrar();
                        break;
                    case 3:
                        // Bloquear
                        id = Util.CapturaEntero("Introduzca el id del porton", 0, listaPortones.Count - 1);
                        if (listaPortones[id].GetType().Name == "Puerta")
                            Console.WriteLine("ERROR");
                        else
                            listaPortones[id].Bloquear();
                        break;
                    case 4:
                        // Desbloquear
                        id = Util.CapturaEntero("Introduzca el id del porton", 0, listaPortones.Count - 1);
                        if (listaPortones[id].GetType().Name == "Puerta")
                            Console.WriteLine("ERROR");
                        else
                            listaPortones[id].DesBloquear();
                        break;
                    case 5:
                        // Mostrar
                        // Cabecera
                        Console.WriteLine("\tId  Estado\t\t     Alto  Ancho  Color");
                        Console.WriteLine("\t--  -----------------------  ----  -----  --------");
                        for (int i = 0; i < listaPortones.Count; i++)
                        {
                            Console.Write("\t{0}  ",Util.CuadraTexto(i.ToString(), 2, false));
                            listaPortones[i].Mostrar();
                        }
                        break;
                    case 6:
                        // Pintar
                        id = Util.CapturaEntero("Introduzca el id del porton o puerta a pintar", 0, listaPortones.Count - 1);
                        listaPortones[id].Color = EligeColor();
                        break;
                    case 7:
                        // Fabricar Puerta
                        
                        listaPortones.Add(FabricarPuerta(false));
                        break;
                    case 8:
                        // Fabricar Porton
                        listaPortones.Add(FabricarPuerta(true));
                        break;
                    case 9:
                        // Eliminar
                        id = Util.CapturaEntero("Introduzca el id del porton o puerta a eliminar", 0, listaPortones.Count - 1);
                        if(Util.PreguntaSiNo("¿Estás seguro que quieres eliminar?"))
                        {
                            if (listaPortones[id].GetType().Name == "Puerta")
                                Console.Write("\n\t¡La puerta ha sido eliminada!");
                            else
                                Console.Write("\n\t¡El porton ha sido eliminado!");
                            listaPortones.RemoveAt(id);
                        }
                        break;
                }

                if (opcion != 0)
                {
                    Console.Write("\n\n\tPulse UNA TECLA para VOLVER AL MENÚ");
                    Console.ReadKey(true);
                }
            } while (opcion != 0);
        }


        public static Porton FabricarPuerta(bool esPorton) // le falta una cláusula
        {
            if(esPorton)
                Console.WriteLine("\n\n\t----- Construyamos el porton ------");
            else
                Console.WriteLine("\n\n\t----- Construyamos la puerta ------");

            int alto = Util.CapturaEntero("\n\t¿Altura en cm?", 50, 250);
            int ancho = Util.CapturaEntero("\n\t¿Anchura en cm?", 30, 250);
            ColorPuerta color = EligeColor();

            Porton p = null;
            if (esPorton)
                p = new Porton(alto, ancho, color);
            else
                p = new Puerta(alto, ancho, color);
            return p;
        }


        public static ColorPuerta EligeColor()
        {
            Console.WriteLine("\n\tElige un color");
            for (int i = 0; i < vColorPuerta.Length; i++)
            {
                Console.ForegroundColor = vColorPuerta[i].Color;
                Console.Write("\n\t{0}) ██████", i);
            }

            Console.ForegroundColor = ConsoleColor.White;
            int indice = Util.CapturaNumPulsado("\n\t¿Color?", 0, vColorPuerta.Length - 1);

            return vColorPuerta[indice];
        }
    }
}
