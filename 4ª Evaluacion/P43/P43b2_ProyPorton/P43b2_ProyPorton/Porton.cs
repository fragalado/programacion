﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P43b1_ProyPorton
{
    class Porton
    {
        // Atributos

        int alto, ancho;
        ColorPuerta color;
        bool abierta = false;   // El porton estará por defecto cerrado
        bool bloqueada = false; // El porton estará por defecto desbloqueado

        // Constructores

        public Porton(int alto, int ancho, ColorPuerta color)
        {
            this.alto = alto;
            this.ancho = ancho;
            this.color = color;
        }

        public Porton(int alto, int ancho)
        {
            this.alto = alto;
            this.ancho = ancho;
            this.color = new ColorPuerta("Blanco", ConsoleColor.White);
        }

        // Propiedades básicas

        public int Alto { get => alto; set => alto = value; }
        public int Ancho { get => ancho; set => ancho = value; }
        public ColorPuerta Color { get => color; set => color = value; }
        public bool Abierta { get => abierta; set => abierta = value; }
        public bool Bloqueada { get => bloqueada; set => bloqueada = value; }

        // Propiedades especiales

        public virtual string Estado
        {
            get
            {
                string aux = (abierta) ? "abierta" : "cerrada";

                if (bloqueada)
                    aux += " y bloqueada";
                else
                    aux += " y desbloqueada";

                return aux;
            }
        }

        // Métodos

        public virtual void Abrir()
        {
            if (bloqueada)
                Console.Write("\n\t¡El porton está bloqueado!");
            else
            {
                if (!abierta) // Si esta cerrada
                {
                    abierta = true; // La abrimos
                    Console.Write("\n\t¡porton abierto!");
                }
                else
                    Console.Write("\n\t¡El porton ya estaba abierto!");
            }
        }

        public virtual void Cerrar()
        {
            if (bloqueada)
                Console.Write("\n\t¡El porton está bloqueado!");
            else
            {
                if (abierta) // Si esta abierta
                {
                    abierta = false; // La cerramos
                    Console.Write("\n\t¡porton abierto!");
                }
                else
                    Console.Write("\n\t¡El porton ya estaba abierto!");
            }
        }

        public void Bloquear()
        {
            if (!bloqueada)
            {
                bloqueada = true;
                Console.Write("\n\t¡porton bloqueado!");
            }
            else
                Console.Write("\n\t¡El porton ya estaba bloqueado!");
        }

        public void DesBloquear()
        {
            if (bloqueada)
            {
                bloqueada = false;
                Console.Write("\n\t¡porton desbloqueado!");
            }
            else
                Console.Write("\n\t¡El porton ya estaba desbloqueado!");
        }

        public virtual void Mostrar()
        {
            Console.WriteLine("{0}   {1}    {2}   {3}", Util.CuadraTexto(Estado,23, true), Alto, Ancho, Color.Nombre);
        }
    }
}
