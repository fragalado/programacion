﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoPuerta
{
    class Util
    {

		public static int CapturaEntero(string mensaje, int min, int max)
        {
            int valor = 0;
            bool ok = false;

            do
            {
                Console.Write("\n\t{0} [{1}..{2}]: ", mensaje, min, max);
                ok = Int32.TryParse(Console.ReadLine(), out valor);

                if (!ok)
                    Console.Write("\n\t** ERROR: Formato no correcto **");
                else if (valor < min || valor > max)
                    Console.Write("\n\t** ERROR: El valor no está dentro de los rangos **");
            } while (!ok || valor < min || valor > max);

            return valor;
        }

        public static int CapturaNumPulsado(string mensaje, int min, int max)
        {
            int valor = 0;
            bool ok = false;

            do
            {
                Console.Write("\n\t{0} [{1}..{2}]: ", mensaje, min, max);
                ok = Int32.TryParse(Console.ReadKey(true).KeyChar.ToString(), out valor);

                if(!ok)
                    Console.Write("\n\t** ERROR: Formato no correcto **");
                else if(valor < min || valor > max)
                    Console.Write("\n\t** ERROR: El valor no está dentro de los rangos **");
            } while (!ok || valor < min || valor > max);
			
			return valor;
        }

        public static int Menu()
        {
            Console.Clear();
			Console.WriteLine("\n\n\n\n");
			Console.WriteLine("\t\t\t╔═════════════════════╗");
            Console.WriteLine("\t\t\t║   MENÚ de PUERTA    ║");
            Console.WriteLine("\t\t\t╠═════════════════════╣");
            Console.WriteLine("\t\t\t║     1) Abrir        ║");
            Console.WriteLine("\t\t\t║                     ║");
            Console.WriteLine("\t\t\t║     2) Cerrar       ║");
			Console.WriteLine("\t\t\t║                     ║");
			Console.WriteLine("\t\t\t║     3) Mostrar      ║");
			Console.WriteLine("\t\t\t║                     ║");
            Console.WriteLine("\t\t\t║     4) Pintar       ║");
			Console.WriteLine("\t\t\t║                     ║");
			Console.WriteLine("\t\t\t║     5) Fabricar     ║");
			Console.WriteLine("\t\t\t║_____________________║");
            Console.WriteLine("\t\t\t║                     ║");
            Console.WriteLine("\t\t\t║     0) Salir        ║");
            Console.WriteLine("\t\t\t╚═════════════════════╝");

            return CapturaNumPulsado("\t\t\tPulse su opción", 0, 5);
        }

        public static ConsoleColor EligeColor()
        {
            ConsoleColor[] vColor = { ConsoleColor.DarkGray, ConsoleColor.Blue, ConsoleColor.Green, ConsoleColor.Cyan, ConsoleColor.Red, ConsoleColor.DarkMagenta, ConsoleColor.Yellow, ConsoleColor.White};

            Console.WriteLine("\n\tElige un color");
            for (int i = 0; i < vColor.Length; i++)
            {
                Console.ForegroundColor = vColor[i];
                Console.Write("\n\t{0}) ██████",i);
            }

            Console.ForegroundColor = ConsoleColor.White;
            int indice = CapturaNumPulsado("\n\t¿Color?", 0, vColor.Length - 1);            

            return vColor[indice];
        }

    }
}
