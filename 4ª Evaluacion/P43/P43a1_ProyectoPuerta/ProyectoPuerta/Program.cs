﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoPuerta
{
    class Program
    {
        static void Main(string[] args)
        {
			// Objeto del tipo Puerta
			Puerta p = new Puerta(100, 80);

			// Variables necesarias
			int opcion;

			do
			{
				opcion = Util.Menu();

				Console.Clear();
				switch ( opcion ) 
				{
					case 1:
						// Abrir
						p.Abrir();
						break;
					case 2:
                        // Cerrar
                        p.Cerrar();
                        break;
					case 3:
						// Mostrar
						p.Mostrar();
                        break;
					case 4:
						// Pintar
						p.Color = Util.EligeColor();
                        p.Mostrar();
                        break;
					case 5:
						// Fabricar
						p = FabricarPuerta();
                        p.Mostrar();
                        break;
				}

				if(opcion != 0)
				{
					Console.Write("\n\n\tPulse UNA TECLA para VOLVER AL MENÚ");
					Console.ReadKey(true);
				}
			} while (opcion != 0);
        }

		public static Puerta FabricarPuerta() // le falta una cláusula
		{
			Console.WriteLine("\n\n\t----- Construyamos la puerta ------");
			int alto = Util.CapturaEntero("\n\t¿Altura en cm?", 50, 250);
			int ancho = Util.CapturaEntero("\n\t¿Anchura en cm?", 30, 250);
			ConsoleColor color = Util.EligeColor();

			return new Puerta(alto, ancho, color);
		}

	}
}
