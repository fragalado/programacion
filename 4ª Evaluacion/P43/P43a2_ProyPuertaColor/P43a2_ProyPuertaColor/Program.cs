﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P43a2_ProyPuertaColor
{
    class Program
    {
        static ColorPuerta[] vColorPuerta = {new ColorPuerta("Gris", ConsoleColor.DarkGray), new ColorPuerta("Azul", ConsoleColor.Blue)
                    , new ColorPuerta("Verde", ConsoleColor.Green), new ColorPuerta("Cyan", ConsoleColor.Cyan), new ColorPuerta("Rojo", ConsoleColor.Red)
                    , new ColorPuerta("Magenta", ConsoleColor.DarkMagenta), new ColorPuerta("Amarillo", ConsoleColor.Yellow), new ColorPuerta("Blanco", ConsoleColor.White)};

        static void Main(string[] args)
        {
            // Objeto del tipo Puerta
            Puerta p = new Puerta(100, 80);

            // Variables necesarias
            int opcion;

            do
            {
                opcion = Util.Menu();

                Console.Clear();
                switch (opcion)
                {
                    case 1:
                        // Abrir
                        p.Abrir();
                        break;
                    case 2:
                        // Cerrar
                        p.Cerrar();
                        break;
                    case 3:
                        // Mostrar
                        p.Mostrar();
                        break;
                    case 4:
                        // Pintar
                        p.Color = EligeColor();
                        p.Mostrar();
                        break;
                    case 5:
                        // Fabricar
                        p = FabricarPuerta();
                        p.Mostrar();
                        break;
                }

                if (opcion != 0)
                {
                    Console.Write("\n\n\tPulse UNA TECLA para VOLVER AL MENÚ");
                    Console.ReadKey(true);
                }
            } while (opcion != 0);
        }


        public static Puerta FabricarPuerta() // le falta una cláusula
        {
            Console.WriteLine("\n\n\t----- Construyamos la puerta ------");
            int alto = Util.CapturaEntero("\n\t¿Altura en cm?", 50, 250);
            int ancho = Util.CapturaEntero("\n\t¿Anchura en cm?", 30, 250);
            ColorPuerta color = EligeColor();

            return new Puerta(alto, ancho, color);
        }

        public static ColorPuerta EligeColor()
        {
            Console.WriteLine("\n\tElige un color");
            for (int i = 0; i < vColorPuerta.Length; i++)
            {
                Console.ForegroundColor = vColorPuerta[i].Color;
                Console.Write("\n\t{0}) ██████", i);
            }

            Console.ForegroundColor = ConsoleColor.White;
            int indice = Util.CapturaNumPulsado("\n\t¿Color?", 0, vColorPuerta.Length - 1);

            return vColorPuerta[indice];
        }
    }
}
