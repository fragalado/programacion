﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P43a2_ProyPuertaColor
{
    class Puerta
    {
        // Atributos

        int alto, ancho;
        ColorPuerta color;
        bool abierta = false; //** Yo he controlado el estado así pero, si quieres puedes hacerlo de otro modo 

        // Constructores

        // Constructor 1 -> Recibe alto, ancho y color
        public Puerta(int alto, int ancho, ColorPuerta color)
        {
            this.alto = alto;
            this.ancho = ancho;
            this.color = color;
        }

        // Constructor 2 -> Recibe alto y ancho y se asigna el color blanco
        public Puerta(int alto, int ancho)
        {
            this.alto = alto;
            this.ancho = ancho;
            this.color = new ColorPuerta("Blanco", ConsoleColor.White);
        }

        // Propiedades básicas

        public int Alto { get => alto; set => alto = value; }
        public int Ancho { get => ancho; set => ancho = value; }
        public ColorPuerta Color { get => color; set => color = value; }
        public bool Abierta { get => abierta; set => abierta = value; }

        // Métodos
        public void Abrir()
        {
            if (!abierta) // Si esta cerrada
            {
                abierta = true; // La abrimos
                Console.Write("\n\t¡puerta abierta!");
            }
            else
                Console.Write("\n\t¡La puerta ya estaba abierta!");
        }

        public void Cerrar()
        {
            if (abierta) // Si esta abierta
            {
                abierta = false; // La cerramos
                Console.Write("\n\t¡puerta cerrada!");
            }
            else
                Console.Write("\n\t¡La puerta ya estaba cerrada!");
        }

        public void Mostrar()
        {
            // aquí falta una línea
            Console.WriteLine("\n\n\tEstado: {0}", (abierta) ? "abierta" : "cerrada");
            Console.WriteLine("\n\tAlto: {0}", alto);
            Console.WriteLine("\tAncho: {0}", ancho);

            Console.Write("\tColor: ");
            color.Mostrar();
        }
    }
}
