﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P43a2_ProyPuertaColor
{
    class ColorPuerta
    {
        // Atributos

        string nombre;
        ConsoleColor color;

        // Constructores

        public ColorPuerta(string nombre, ConsoleColor color)
        {
            this.nombre = nombre;
            this.color = color;
        }

        // Propiedades básicas

        public string Nombre { get => nombre; set => nombre = value; }
        public ConsoleColor Color { get => color; set => color = value; }

        // Métodos

        public void Mostrar()
        {
            Console.ForegroundColor = color;
            Console.WriteLine("████████ " + nombre);
            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}
