﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P43b1_ProyPorton
{
    class Program
    {
        static ColorPuerta[] vColorPuerta = {new ColorPuerta("Gris", ConsoleColor.DarkGray), new ColorPuerta("Azul", ConsoleColor.Blue)
                    , new ColorPuerta("Verde", ConsoleColor.Green), new ColorPuerta("Cyan", ConsoleColor.Cyan), new ColorPuerta("Rojo", ConsoleColor.Red)
                    , new ColorPuerta("Magenta", ConsoleColor.DarkMagenta), new ColorPuerta("Amarillo", ConsoleColor.Yellow), new ColorPuerta("Blanco", ConsoleColor.White)};

        static void Main(string[] args)
        {
            // Objeto del tipo Puerta
            Porton p = new Porton(100, 80);
            Porton p1 = new Porton(120, 50, vColorPuerta[4]);
            Porton p2 = new Porton(160, 80, vColorPuerta[3]);
            Porton p3 = new Porton(200, 80, vColorPuerta[7]);

            // Variables necesarias
            int opcion, id;
            List<Porton> listaPortones = new List<Porton>();

            // Cuando se entra en la aplicación se construirán 4 objetos portón con los valores y colores que
            // quieras.A continuación, se presentará el menú y actuará en consecuencia.
            listaPortones.Add(p);
            listaPortones.Add(p1);
            listaPortones.Add(p2);
            listaPortones.Add(p3);

            do
            {
                opcion = Util.Menu();

                Console.Clear();
                switch (opcion)
                {
                    case 1:
                        // Abrir
                        // Pedimos el id del porton
                        id = Util.CapturaEntero("Introduzca el id del porton", 0, listaPortones.Count - 1);
                        listaPortones[id].Abrir();
                        break;
                    case 2:
                        // Cerrar
                        id = Util.CapturaEntero("Introduzca el id del porton", 0, listaPortones.Count - 1);
                        listaPortones[id].Cerrar();
                        break;
                    case 3:
                        // Bloquear
                        id = Util.CapturaEntero("Introduzca el id del porton", 0, listaPortones.Count - 1);
                        listaPortones[id].Bloquear();
                        break;
                    case 4:
                        // Desbloquear
                        id = Util.CapturaEntero("Introduzca el id del porton", 0, listaPortones.Count - 1);
                        listaPortones[id].DesBloquear();
                        break;
                    case 5:
                        // Mostrar
                        // Cabecera
                        Console.WriteLine("\tId  Estado\t\t     Alto  Ancho  Color");
                        Console.WriteLine("\t--  -----------------------  ----  -----  --------");
                        for (int i = 0; i < listaPortones.Count; i++)
                        {
                            Console.Write("\t{0}  ",Util.CuadraTexto(i.ToString(), 2, false));
                            listaPortones[i].Mostrar();
                        }
                        break;
                    case 6:
                        // Pintar
                        id = Util.CapturaEntero("Introduzca el id del porton a pintar", 0, listaPortones.Count - 1);
                        listaPortones[id].Color = EligeColor();
                        break;
                    case 7:
                        // Fabricar
                        listaPortones.Add(FabricarPuerta());
                        break;
                    case 8:
                        // Eliminar
                        id = Util.CapturaEntero("Introduzca el id del porton a eliminar", 0, listaPortones.Count - 1);
                        if(Util.PreguntaSiNo("¿Estás seguro que quieres eliminar?"))
                        {
                            listaPortones.RemoveAt(id);
                            Console.Write("\n\t¡El porton ha sido eliminado!");
                        }
                        break;
                }

                if (opcion != 0)
                {
                    Console.Write("\n\n\tPulse UNA TECLA para VOLVER AL MENÚ");
                    Console.ReadKey(true);
                }
            } while (opcion != 0);
        }


        public static Porton FabricarPuerta() // le falta una cláusula
        {
            Console.WriteLine("\n\n\t----- Construyamos la puerta ------");
            int alto = Util.CapturaEntero("\n\t¿Altura en cm?", 50, 250);
            int ancho = Util.CapturaEntero("\n\t¿Anchura en cm?", 30, 250);
            ColorPuerta color = EligeColor();

            return new Porton(alto, ancho, color);
        }

        public static ColorPuerta EligeColor()
        {
            Console.WriteLine("\n\tElige un color");
            for (int i = 0; i < vColorPuerta.Length; i++)
            {
                Console.ForegroundColor = vColorPuerta[i].Color;
                Console.Write("\n\t{0}) ██████", i);
            }

            Console.ForegroundColor = ConsoleColor.White;
            int indice = Util.CapturaNumPulsado("\n\t¿Color?", 0, vColorPuerta.Length - 1);

            return vColorPuerta[indice];
        }
    }
}
