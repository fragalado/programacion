﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P43b1_ProyPorton
{
    class Porton : Puerta
    {
        // Atributos

        bool bloqueada = false; // La puerta estará por defecto desbloqueada

        // Constructores

        public Porton(int alto, int ancho, ColorPuerta color) : base(alto, ancho, color)
        {
        }

        public Porton(int alto, int ancho) : base(alto, ancho)
        {
        }

        // Propiedades básicas

        public bool Bloqueada { get => bloqueada; set => bloqueada = value; }

        // Propiedades especiales

        public override string Estado
        {
            get
            {
                string aux = base.Estado;

                if (bloqueada)
                    aux += " y bloqueada";
                else
                    aux += " y desbloqueada";

                return aux;
            }
        }

        // Métodos

        public override void Abrir()
        {
            if(bloqueada)
                Console.Write("\n\t¡El porton está bloqueado!");
            else
                base.Abrir();
        }

        public override void Cerrar()
        {
            if (bloqueada)
                Console.Write("\n\t¡El porton está bloqueado!");
            else
                base.Cerrar();
        }

        public void Bloquear()
        {
            if (!bloqueada)
            {
                bloqueada = true;
                Console.Write("\n\t¡porton bloqueado!");
            }
            else
                Console.Write("\n\t¡El porton ya estaba bloqueado!");
        }

        public void DesBloquear()
        {
            if (bloqueada)
            {
                bloqueada = false;
                Console.Write("\n\t¡porton desbloqueado!");
            }
            else
                Console.Write("\n\t¡El porton ya estaba desbloqueado!");
        }

        public override void Mostrar()
        {
            Console.WriteLine("{0}   {1}    {2}   {3}", Util.CuadraTexto(Estado,23, true), Alto, Ancho, Color.Nombre);
        }
    }
}
