﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P41b1_Paralelogramo
{
    class Rectangulo : Cuadrado
    {
        // Atributos

        int ladoLateral;

        // Constructores

        public Rectangulo(string nombre, int ladoBase, int ladoLateral):base(nombre, ladoBase)
        {
            this.ladoLateral = ladoLateral;
        }

        // Propiedades básicas

        public int LadoLateral { get => ladoLateral; set => ladoLateral = value; }

        // Propiedades especiales -> Las mismas que Cuadrado

        // Métodos -> Lo mismo que Cuadrado
    }
}
