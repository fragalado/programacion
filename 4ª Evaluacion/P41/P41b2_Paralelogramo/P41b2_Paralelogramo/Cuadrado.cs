﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P41b2_Paralelogramo
{
    class Cuadrado : Rectangulo
    {
        // Atributos -> Los de la clase Padre/Rectangulo

        // Constructores

        public Cuadrado(string nombre, int ladoBase):base(nombre, ladoBase, ladoBase)
        {
        }

        // Propiedades básicas -> Las de la clase Padre/Rectangulo

        // Propiedades especiales -> Las de la clase Padre/Rectangulo

        // Métodos -> Las de la clase Padre/Rectangulo
    }
}
