﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P41b2_Paralelogramo
{
    class Program
    {
        static void Main(string[] args)
        {
            // Cabecera
            Console.WriteLine("\tNombre  \tBase\tLateral\tÁngulo\tPerim. \tÁrea");
            Console.WriteLine("\t------  \t----\t-------\t------\t------\t----");

            // Cuadrado
            Cuadrado c = new Cuadrado("Cuadrado 1", 10);
            Console.WriteLine(c.ComoString());

            // Rectangulo
            Rectangulo r = new Rectangulo("Rectangulo 1", 3, 6);
            Console.WriteLine(r.ComoString());

            // Rombo
            Rombo rombo1 = new Rombo("Rombo 1", 6, 34.5);
            Console.WriteLine(rombo1.ComoString());

            // Romboide
            Romboide romboide1 = new Romboide("Romboide 1", 6, 12, 56.4324);
            Console.WriteLine(romboide1.ComoString());


            // con polimorfismo...
            Rectangulo c1 = new Cuadrado("Cuadrado 2", 5);
            Rectangulo rombo2 = new Rombo("Rombo 2", 12, 90);
            Rectangulo r2 = new Rectangulo("Rectangulo 2", 10, 4);
            Rectangulo romboide2 = new Romboide("Romboide 2", 5, 10, 45);
            Console.WriteLine(c1.ComoString());
            Console.WriteLine(rombo2.ComoString());
            Console.WriteLine(r2.ComoString());
            Console.WriteLine(romboide2.ComoString());

            // Construimos una tabla para guardar las 4 figuras (con polimorfismo) y presentamos con bucle
            Rectangulo[] vFiguras = {c1, rombo2, r2, romboide2};
            foreach (Rectangulo aux in vFiguras)
            {
                Console.WriteLine(aux.ComoString());
            }

            // Para salir
            Console.WriteLine("\n\n\n\tPulse una tecla para salir");
            Console.ReadKey(true);
        }
    }
}
