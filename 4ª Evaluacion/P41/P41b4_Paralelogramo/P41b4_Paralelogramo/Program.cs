﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P41b4_Paralelogramo
{
    class Program
    {
        static void Main(string[] args)
        {
            int opcion;
            List<Paralelogramo> listaFiguras = new List<Paralelogramo>(); // Donde guardaremos todas las figuras que construyamos
            //string nombre;
            int ladoBase, ladoLateral = 0;
            double angulo = 0;
            int contCuadrado = 0, contRectang = 0, contRomb = 0, contRoide = 0;
            string[] vFiguras = { "cuadrado", "rectángulo", "rombo", "romboide" };
            do
            {
                opcion = Util.Menu();
                Console.Clear();
                Paralelogramo r = null;

                if (opcion == 5)
                {
                    // Presentar Paralelogramos
                    // Cabecera
                    Console.WriteLine("\tNombre  \tBase\tLateral\tÁngulo\tPerim. \tÁrea");
                    Console.WriteLine("\t------  \t----\t-------\t------\t------\t----");
                    foreach (Paralelogramo aux in listaFiguras)
                    {
                        Console.WriteLine(aux.ComoString());
                    }
                }
                else if (opcion != 0)
                {
                    // Pedimos el lado base, ya que en todos los paralelogramos se pide el lado base
                    ladoBase = Util.CapturaEntero(String.Format("Introduzca el lado base del " + vFiguras[opcion - 1]), 1, 20);

                    // Pediremos el lado lateral si es para rectangulo o para romboide
                    if (opcion == 2 || opcion == 4)
                        ladoLateral = Util.CapturaEntero(String.Format("Introduzca el lado lateral del " + vFiguras[opcion - 1]), 1, 20);

                    // Pediremos el angulo si es para el rombo o para el romboide
                    if (opcion == 3 || opcion == 4)
                        angulo = Util.CapturaFloat(String.Format("Introduzca el angulo del " + vFiguras[opcion - 1]), 1, 90);

                    // Construiremos el paralelogramo segun la opcion escogida y lo añadiremos a la lista
                    if (opcion == 1)
                    {
                        contCuadrado++;
                        r = new Cuadrado(String.Format("cuad-" + contCuadrado), ladoBase);
                    }
                    else if (opcion == 2)
                    {
                        contRectang++;
                        r = new Rectangulo(String.Format("rectn-" + contRectang), ladoBase, ladoLateral);
                    }
                    else if (opcion == 3)
                    {
                        contRomb++;
                        r = new Rombo(String.Format("romb-" + contRomb), ladoBase, angulo);
                    }
                    else if (opcion == 4)
                    {
                        contRoide++;
                        r = new Romboide(String.Format("roide-" + contRoide), ladoBase, ladoLateral, angulo);
                    }
                    listaFiguras.Add(r);
                }
                if (opcion != 0)
                {
                    Console.Write("\n\tPulse una tecla para volver al menu");
                    Console.ReadKey(true);
                    Console.Clear();
                }
            } while (opcion != 0);


            // Para salir
            Console.WriteLine("\n\n\n\tPulse una tecla para salir");
            Console.ReadKey(true);
        }
    }
}
