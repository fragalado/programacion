﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P41b4_Paralelogramo
{
    class Cuadrado : Paralelogramo
    {
        // Atributos -> Los de la clase Padre/Paralelogramo

        // Constructores

        public Cuadrado(string nombre, int ladoBase) : base(nombre, ladoBase, ladoBase, 90)
        {
        }

        // Propiedades básicas -> Las de la clase Padre/Paralelogramo

        // Propiedades especiales -> Las de la clase Padre/Paralelogramo

        // Métodos
        public override string ComoString()
        {
            return String.Format("\t{0}\t{1}\t{2}\t{3}\t{4}\t{5}", Util.CuadraTexto(Nombre, 15, false), LadoBase, LadoBase, Angulo, Perimetro, Area);
        }
    }
}
