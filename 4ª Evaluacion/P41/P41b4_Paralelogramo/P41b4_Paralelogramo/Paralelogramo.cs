﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P41b4_Paralelogramo
{
    abstract class Paralelogramo
    {
        // Atributos

        string nombre;
        int ladoBase, ladoLateral;
        double angulo;

        // Constructores

        protected Paralelogramo(string nombre, int ladoBase, int ladoLateral, double angulo)
        {
            this.nombre = nombre;
            this.ladoBase = ladoBase;
            this.ladoLateral = ladoLateral;
            this.angulo = angulo;
        }

        // Propiedades básicas

        public string Nombre { get => nombre; set => nombre = value; }
        public int LadoBase { get => ladoBase; set => ladoBase = value; }
        public int LadoLateral { get => ladoLateral; set => ladoLateral = value; }
        public double Angulo { get => angulo; set => angulo = value; }

        // Propiedades especiales

        public virtual int Perimetro
        {
            get
            {
                return ladoBase + ladoBase + ladoLateral + ladoLateral;
            }
        }

        public virtual double Area
        {
            get
            {
                return Math.Round((double)ladoBase * ladoLateral, 2);
            }
        }

        // Métodos

        public abstract string ComoString(); // Metodo abstracto, solo se define
    }
}
