﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P41b4_Paralelogramo
{
    class Romboide : Paralelogramo
    {
        // Atributos

        // Constructores

        public Romboide(string nombre, int ladoBase, int ladoLateral, double angulo) : base(nombre, ladoBase, ladoLateral, angulo)
        {
        }

        // Propiedades básicas -> Las de la clase Padre

        // Propiedades especiales

        public override double Area
        {
            get
            {
                return Math.Round(LadoBase * LadoLateral * Math.Sin(Angulo * Math.PI / 180), 2);
            }
        }

        // Métodos
        public override string ComoString()
        {
            return String.Format("\t{0}\t{1}\t{2}\t{3}\t{4}\t{5}", Util.CuadraTexto(Nombre, 15, false), LadoBase, LadoLateral, Math.Round(Angulo, 2), Perimetro, Area);
        }
    }
}
