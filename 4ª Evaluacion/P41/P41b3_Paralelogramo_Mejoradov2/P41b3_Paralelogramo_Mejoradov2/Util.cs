﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P41b3_Paralelogramo_Mejoradov2
{
    class Util
    {
        public static string CuadraTexto(string txt, int longitud, bool conPuntos)
        {
            string aux;
            if (conPuntos)
                aux = ".............................................................";
            else
                aux = "                                                             ";

            txt += aux;
            return txt.Substring(0, longitud);
        }

        public static int Menu()
        {
            Console.Clear();
            Console.WriteLine("\n\n\t\t╔════════════════════════════════════╗");
            Console.WriteLine("\t\t║           Menú Múltiplos           ║");
            Console.WriteLine("\t\t╠════════════════════════════════════╣");
            Console.WriteLine("\t\t║                                    ║");
            Console.WriteLine("\t\t║   1) Construir Cuadrado            ║");
            Console.WriteLine("\t\t║                                    ║");
            Console.WriteLine("\t\t║   2) Construir Rectángulo          ║");
            Console.WriteLine("\t\t║                                    ║");
            Console.WriteLine("\t\t║   3) Construir Rombo               ║");
            Console.WriteLine("\t\t║                                    ║");
            Console.WriteLine("\t\t║   4) Construir Romboide            ║");
            Console.WriteLine("\t\t║                                    ║");
            Console.WriteLine("\t\t║   5) Presentar Paralelogramos      ║");
            Console.WriteLine("\t\t║____________________________________║");
            Console.WriteLine("\t\t║                                    ║");
            Console.WriteLine("\t\t║           0) Salir                 ║");
            Console.WriteLine("\t\t╚════════════════════════════════════╝");
            int opcion, min = 0, max = 5;
            bool ok;
            do
            {
                Console.Write("\n\t\t{0}: ", String.Format("Introduzca una opción"));
                ok = Int32.TryParse(Console.ReadKey().KeyChar.ToString(), out opcion);

                if (!ok)
                    Console.Write("\n\t\t** Error: El valor introducido no es un entero **");
                else if (opcion < min || opcion > max)
                    Console.Write("\n\t\t** Error: El valor introducido no está dentro del rango **");
            } while (!ok || opcion < min || opcion > max);
            return opcion;
        }

        public static int CapturaEntero(string txt, int min, int max)
        {
            int num;
            bool ok;
            do
            {
                Console.Write("\n\t{0} [{1}..{2}]: ", txt, min, max);
                ok = Int32.TryParse(Console.ReadLine(), out num);

                if (!ok)
                    Console.Write("\n\t** Error: El valor introducido no es un entero **");
                else if (num < min || num > max)
                    Console.Write("\n\t** Error: El valor introducido no está dentro del rango **");
            } while (!ok || num < min || num > max);

            return num;
        }

        public static float CapturaFloat(string txt, float min, float max)
        {
            double num;
            bool ok;
            do
            {
                Console.Write("\n\t{0} [{1}..{2}]: ", txt, min, max);
                ok = Double.TryParse(Console.ReadLine(), out num);

                if (!ok)
                    Console.Write("\n\t** Error: El valor introducido no es un entero **");
                else if (num < min || num > max)
                    Console.Write("\n\t** Error: El valor introducido no está dentro del rango **");
            } while (!ok || num < min || num > max);

            return (float)num;
        }
    }
}
