﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P41a2_EjPolimorfismo
{
    class Cliente
    {
        // Campos de la clase
        protected int numDNI;
        protected char letraDNI;
        protected string nombre, apellidos;
        protected Fecha fechaNac;


        //Propiedades de los campos
        public int NumDNI { get => numDNI; set => numDNI = value; }
        public char LetraDNI { get => letraDNI; set => letraDNI = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public string Apellidos { get => apellidos; set => apellidos = value; }
        public Fecha FechaNac { get => fechaNac; set => fechaNac = value; }


        //Propiedad Edad
        public byte Edad
        {
            get
            {
                int edad = 0;
                int anyo = DateTime.Now.Year;
                int mes = DateTime.Now.Month;
                int dia = DateTime.Now.Day;

                edad = anyo - fechaNac.Anyo;

                //Este IF es para evaluar que la persona todavía no ha cumplido años
                //en la fecha actual


                if ((mes < fechaNac.Mes) || (mes == fechaNac.Mes && dia < fechaNac.Dia))
                    edad--;

                return (byte)edad;
            }
        }

        //CONSTRUCTORES
        //Constructor 1 con todos los campos
        public Cliente(int numDNI, char letraDNI, string nombre, string apellidos, Fecha fechaNac)
        {
            this.NumDNI = numDNI;
            this.LetraDNI = letraDNI;
            this.Nombre = nombre;
            this.Apellidos = apellidos;
            this.FechaNac = fechaNac;
        }



        //MÉTODOS
        //Con override invalidamos el método ToString() que tenía la clase
        //y validamos el que implementamos aquí.

        public override string ToString()
        {
            return String.Format("\t{0}-{1}    {2} {3}", numDNI, letraDNI, Util.CuadraTexto(nombre.Trim() + " " + apellidos.Trim(), 27, ' '), fechaNac.FechaStringSp);
        }
    }
}
