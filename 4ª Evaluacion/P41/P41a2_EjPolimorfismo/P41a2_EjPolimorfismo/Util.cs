﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P41a2_EjPolimorfismo
{
    class Util
    {
        public static string CuadraTexto(string texto, int numCaracteres, char a)
        {
            if (texto.Length > numCaracteres)
                return texto.Substring(0, numCaracteres);
            while (texto.Length < numCaracteres)
                texto += a;
            return texto;
        }
    }
}
