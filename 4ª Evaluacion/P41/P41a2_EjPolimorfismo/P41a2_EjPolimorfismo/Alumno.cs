﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P41a2_EjPolimorfismo
{
    class Alumno : Cliente
    {
        //Campos de Alumno -> añadimos los campos notas (3) porque no están en la clase padre
        float n1, n2, n3;

        //Propiedades básicas de Alumno -> añadimos las propiedades de las notas (3)
        //porque no están en la clase padre
        public float N1 { get => n1; set => n1 = value; }
        public float N2 { get => n2; set => n2 = value; }
        public float N3 { get => n3; set => n3 = value; }

        //Constructor
        public Alumno(int numDNI, char letraDNI, string nombre, string apellidos, Fecha fechaNac, float n1, float n2, float n3) : base(numDNI, letraDNI, nombre, apellidos, fechaNac)
        {
            this.n1 = n1;
            this.n2 = n2;
            this.n3 = n3;
        }

        //Propiedad de sólo lectura -> Media de las notas
        public float Media
        {
            get
            {
                return (float)Math.Round((n1 + n2 + n3) / 3, 2);
            }
        }

        //MÉTODOS
        //Con override invalidamos el método ToString() que tenía la clase
        //y validamos el que implementamos aquí.

        public override string ToString()
        {
            return String.Format("    {0}-{1} {2} {3}  {4}  {5}  {6}\t{7}", NumDNI, LetraDNI, Edad, Util.CuadraTexto(Nombre + " " + Apellidos, 27, ' '), n1.ToString("0.0"), n2.ToString("0.0"), n3.ToString("0.0"), Media.ToString("0.00"));
        }
    }
}
