﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace P41a2_EjPolimorfismo
{
    class Program
    {
        static void Main(string[] args)
        {
            // Construimos un cliente con el constructor tipico
            Cliente c1 = new Cliente(53965130, 'T', "Francisco", "Gallego Dorado", new Fecha(2023, 12, 28));

            // Construimos un alumno
            Alumno a1 = new Alumno(34243242, 'D', "Daniel", "Galleg oDoraod", new Fecha(), 5.4f, 3.4f, 6.5f);

            // Creamos un cliente con polimorfismo
            Cliente a2 = new Alumno(43554353, 'L', "carmen", "aguilera", new Fecha(20200327), 4f, 5f, 9f);

            // Polimorfismo
            // Construimos la lista de clientes
            List<Cliente> listaClientes = new List<Cliente>();
            listaClientes.Add(c1);
            listaClientes.Add(a1);
            listaClientes.Add(a2);

            // Vamos a intentar modificar algunos campos de a2
            a2.Nombre = "Laura";
            // No vamos a poder modificar campos de la clase Alumno
            // a2.N1 = 5f; // No se puede hacer
            // a2.ToString(); // Hace el de Alumno


            // Vamos a mostrar los clientes de la lista
            foreach (Cliente c in listaClientes)
            {
                Console.WriteLine(c.ToString());
            }

            // Para salir
            Console.WriteLine("\n\nPulse una tecla para salir");
            Console.ReadKey();
        }
    }
}
