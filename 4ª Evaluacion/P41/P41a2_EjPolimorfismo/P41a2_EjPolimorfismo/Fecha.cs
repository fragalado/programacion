﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P41a2_EjPolimorfismo
{
    class Fecha
    {
        int anyo, mes, dia;

        public int[] maxDiasMes = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

        public static string[] vMeses = { "", "enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre" };

        public Fecha(int anyo, int mes, int dia)
        {
            this.anyo = anyo;
            this.mes = mes;
            this.dia = dia;

            if (EsBisiesto)
                maxDiasMes[2] = 29;
        }

        public Fecha()
        {
            anyo = DateTime.Now.Year;
            mes = DateTime.Now.Month;
            dia = DateTime.Now.Day;

            if (EsBisiesto)
                maxDiasMes[2] = 29;
        }

        public Fecha(int fecha)
        {
            anyo = fecha / 10000;
            mes = (fecha % 10000) / 100;
            dia = fecha % 100;
        }

        #region Propiedades 
        public int Anyo
        {
            get => anyo;
            set
            {
                anyo = value;
                if (EsBisiesto)
                    maxDiasMes[2] = 29;
                else
                    maxDiasMes[2] = 28;
            }
        }
        public int Mes { get => mes; set => mes = value; }
        public int Dia { get => dia; set => dia = value; }

        public int FechaEntero
        {
            get
            {

                return (anyo * 10000 + mes * 100 + dia);

                //string strFecha = anyo.ToString("00").Substring(anyo.ToString().Length - 2) + 
                //    mes.ToString("00") + 
                //    dia.ToString("00");
                //return Convert.ToInt32(strFecha);
            }

        }

        public string FechaStringSp
        {
            get
            {
                return (dia.ToString("00") + "/" + mes.ToString("00") + "/" + anyo);
            }

        }

        public string FechaStringTexto
        {
            get
            {
                return (dia + " de " + vMeses[mes] + " de " + anyo);
            }

        }

        public bool EsBisiesto
        {
            get
            {
                return ((anyo % 4 == 0 && anyo % 100 != 0) || anyo % 400 == 0);
            }
        }

        #endregion

        public void AvanzaDia()
        {
            dia++;
            if (dia > maxDiasMes[mes])
            {
                dia = 1;
                mes++;
            }
            if (mes > 12)
            {
                mes = 1;
                anyo++;

                if (EsBisiesto)
                    maxDiasMes[2] = 29;
                else
                    maxDiasMes[2] = 28;
            }
        }
    }
}
