﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P41a_ProyAlumnosHerencia
{
    class Cliente
    {
        // Atributos

        private int numDNI;
        private char letraDNI;
        private string nombre, apellidos;
        private Fecha fechaNac;

        // Constructores

        // Constructor 1 -> Recibe todos los campos
        public Cliente(int numDNI, char letraDNI, string nombre, string apellidos, Fecha fechaNac)
        {
            this.numDNI = numDNI;
            this.letraDNI = letraDNI;
            this.nombre = nombre;
            this.apellidos = apellidos;
            this.fechaNac = fechaNac;
        }

        // Propiedades básicas

        public int NumDNI { get => numDNI; set => numDNI = value; }
        public char LetraDNI { get => letraDNI; set => letraDNI = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public string Apellidos { get => apellidos; set => apellidos = value; }
        internal Fecha FechaNac { get => fechaNac; set => fechaNac = value; }

        // Propiedades especiales

        /**
         * Devuelve la edad en anyos del individuo
         */
        public int Edad
        {
            get
            {
                int mesActual = DateTime.Now.Month;
                int diaActual = DateTime.Now.Day;
                int anyoActual = DateTime.Now.Year;

                if (fechaNac.Mes < mesActual || (fechaNac.Mes == mesActual && fechaNac.Dia <= diaActual))
                    return anyoActual - fechaNac.Anyo;

                return anyoActual - fechaNac.Anyo - 1;
            }
        }

        // Métodos

        /**
         * Devuelve los datos del cliente con el siguiente formato:
         * "\t{0}-{1} {2} {3}", numDNI, letraDNI, «nombre apellidos», fechaNac. (El «nombre Apellidos» cuadrado a 27), (fechaNac como FechaStringSp)
         */
        public string ToString()
        {
            return String.Format("\t{0}-{1} {2} {3}", numDNI, LetraDNI, Util.CuadraTexto(nombre+ " "+apellidos, 27, false), fechaNac.FechaStringSp);
        }
    }
}
