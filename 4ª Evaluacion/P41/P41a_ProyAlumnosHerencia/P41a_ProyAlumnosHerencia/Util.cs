﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P41a_ProyAlumnosHerencia
{
    class Util
    {
        public static string CuadraTexto(string txt, int longitud, bool conPuntos)
        {
            string aux;

            if (conPuntos)
                aux = "...............................................................";
            else
                aux = "                                                               ";

            txt += aux;

            return txt.Substring(0, longitud);
        }
    }
}
