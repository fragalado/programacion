﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P41a_ProyAlumnosHerencia
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // Los datos se tomarán del fichero Alums3fNotas.txt que contiene los siguientes campos separados por ';'
            // numDNI, letraDNI, nombre, apellidos, fecha(AAAAMMDD), nota1, nota2, nota3

            // StreamReader para leer los datos del fichero
            StreamReader sr = new StreamReader(".\\Datos\\Alums3fNotas.txt", Encoding.Default);

            // Cabecera
            Console.WriteLine("    DNI    Edad      Nombre Apellidos       N1  N2  N3 \tMedia");
            Console.WriteLine(" ---------- -- ---------------------------- --- --- ---\t-----");

            // Recorremos el fichero y a la vez mostramos por pantalla
            string[] vAux;
            int numDNI;
            char letraDNI;
            string nombre, apellidos;
            Fecha fechaNac;
            float nota1, nota2, nota3;
            while(!sr.EndOfStream)
            {
                vAux = sr.ReadLine().Split(';');
                numDNI = Convert.ToInt32(vAux[0]);
                letraDNI = Convert.ToChar(vAux[1]);
                nombre = vAux[2];
                apellidos = vAux[3];
                fechaNac = new Fecha(Convert.ToInt32(vAux[4]));
                nota1 = Convert.ToSingle(vAux[5]);
                nota2 = Convert.ToSingle(vAux[6]);
                nota3 = Convert.ToSingle(vAux[7]);
                Alumno a = new Alumno(numDNI, letraDNI, nombre, apellidos, fechaNac, nota1, nota2, nota3);
                Console.WriteLine(a.ToString());
            }

            // Cerramos el StreamReader
            sr.Close();

            // Para salir
            Console.WriteLine("\n\n\n\tPulse una tecla para salir");
            Console.ReadKey(true);
        }
    }
}
