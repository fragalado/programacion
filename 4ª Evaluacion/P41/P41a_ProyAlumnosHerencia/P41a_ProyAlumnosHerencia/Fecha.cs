﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P41a_ProyAlumnosHerencia
{
    class Fecha
    {
        // Atributos

        private int anyo, mes, dia;

        static string[] vMeses = { "enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre" };
        static int[] vNumDiaMeses = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

        // Constructores

        // Constructor 1 -> Recibe los tres campos
        public Fecha(int anyo, int mes, int dia)
        {
            this.anyo = anyo;
            this.mes = mes;
            this.dia = dia;
        }

        // Constructor 2 -> Recibe la fecha como entero AAAAMMDD
        public Fecha(int fecha)
        {
            this.anyo = Convert.ToInt32(fecha.ToString().Substring(0, 4));
            this.mes = Convert.ToInt32(fecha.ToString().Substring(3, 2));
            this.dia = Convert.ToInt32(fecha.ToString().Substring(5, 2));
        }

        // Constructor 3 -> No recibe nada
        public Fecha()
        {
        }

        // Propiedades básicas

        public int Anyo { get => anyo; set => anyo = value; }
        public int Mes { get => mes; set => mes = value; }
        public int Dia { get => dia; set => dia = value; }

        // Propiedades especiales

        /**
         * Devuelve la fecha  como un entero con el formato AAAAMMDD
         */
        public int FechaEntero
        {
            get
            {
                return Convert.ToInt32(anyo.ToString("0000") + mes.ToString("00") + dia.ToString("00"));
            }
        }

        /**
         * Devuelve la fecha como un string con el formato DD/MM/AAAA
         */
        public string FechaStringSp
        {
            get
            {
                return dia.ToString("00") + "/" + mes.ToString("00") + "/" + anyo.ToString("0000");
            }
        }

        /**
         * Devuelve la fecha como la siguiente: 24 de marzo de 2014.
         */
        public string FechaStringTexto
        {
            get
            {
                return dia + " de " + vMeses[mes - 1] + " de " + anyo;
            }
        }

        /**
         * Devuelve true si el año es bisiesto y false en caso contrario
         */
        public bool EsBisiesto
        {
            get
            {
                return DateTime.IsLeapYear(anyo);
            }
        }

        // Métodos

        /**
         * Incrementa un día a la fecha. Se tendrán en cuenta los cambios de meses y/o anyos
         */
        public void AvanzaDia()
        {
            dia++; // Avanzamos un dia

            // Comprobamos si es mayor que el numero de dia del mes
            if (EsBisiesto)
                vNumDiaMeses[1] = 29;
            else
                vNumDiaMeses[1] = 28;

            if (dia > vNumDiaMeses[mes - 1])
            {
                dia = 1;
                mes++;
            }

            if(mes > 12)
            {
                mes = 1;
                anyo++;
                // Si el nuevo anyo es bisiesto tendremos que modificar el vector
                if (EsBisiesto)
                    vNumDiaMeses[1] = 29;
                else
                    vNumDiaMeses[1] = 28;
            }
        }
    }
}
