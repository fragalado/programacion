﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P41a_ProyAlumnosHerencia
{
    class Alumno : Cliente
    {
        // Atributos

        float nota1, nota2, nota3;

        // Constructores

        // Constructor 1 -> Constructor con todos los parametros
        public Alumno(int numDNI, char letraDNI, string nombre, string apellidos, Fecha fechaNac, float nota1, float nota2, float nota3) 
                : base(numDNI, letraDNI, nombre, apellidos, fechaNac)
        {
            this.nota1 = nota1;
            this.nota2 = nota2;
            this.nota3 = nota3;
        }

        // Propiedades básicas

        public float Nota1 { get => nota1; set => nota1 = value; }
        public float Nota2 { get => nota2; set => nota2 = value; }
        public float Nota3 { get => nota3; set => nota3 = value; }

        // Propiedades especiales

        /**
         * Propiedad de solo lectura que devuelve la media de las tres notas con un máximo de dos decimales
         */
        public float Media
        {
            get
            {
                return (float) Math.Round((nota1 + nota2 + nota3) / 3.0, 2);
            }
        }

        // Métodos

        /**
         * Devuelve los datos del alumno con el siguiente formato:
         * "\t{0}-{1} {2} {3}  {4}  {5}  {6}\t{7}", numDNI, letraDNI, edad, «nombre apellidos», nota1, nota2, nota3, Media. (El «nombre Apellidos» cuadrado a 27)
         */
        public string ToString()
        {
            return String.Format(" {0}-{1} {2} {3}  {4} {5} {6}  {7}", NumDNI
                                                                       , LetraDNI
                                                                       , Edad
                                                                       , Util.CuadraTexto(String.Format("{0} {1}", Nombre, Apellidos), 27, false)
                                                                       , Util.CuadraTexto(nota1.ToString(),3,false)
                                                                       , Util.CuadraTexto(nota2.ToString(), 3, false)
                                                                       , Util.CuadraTexto(nota3.ToString(), 3, false), Media);
        }
    }
}
