﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P41b_Paralelogramo
{
    class Rombo
    {
        // Atributos

        string nombre;
        int ladoBase;
        double angulo;

        // Constructores

        public Rombo(string nombre, int ladoBase, double angulo)
        {
            this.nombre = nombre;
            this.ladoBase = ladoBase;
            this.angulo = angulo;
        }

        // Propiedades básicas

        public string Nombre { get => nombre; set => nombre = value; }
        public int LadoBase { get => ladoBase; set => ladoBase = value; }
        public double Angulo { get => angulo; set => angulo = value; }

        // Propiedades especiales

        public int Perimetro
        {
            get
            {
                return ladoBase + ladoBase + ladoBase + ladoBase;
            }
        }

        public double Area
        {
            get
            {
                return Math.Round(ladoBase * ladoBase * Math.Sin(angulo * Math.PI / 180), 2);
            }
        }

        // Métodos
        public string ComoString()
        {
            return String.Format("\t{0}\t{1}\t{2}\t{3}\t{4}\t{5}", nombre, ladoBase, ladoBase, angulo, Perimetro, Area);
        }
    }
}
