﻿using P41b_Paralelogramo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P41b0_Paralelogramo
{
    class Program
    {
        static void Main(string[] args)
        {
            // Cabecera
            Console.WriteLine("\tNombre  \tBase\tLateral\tÁngulo\tPerim. \tÁrea");
            Console.WriteLine("\t------  \t----\t-------\t------\t------\t----");

            // Construimos Cuadrado
            Cuadrado c = new Cuadrado("Cuadrado 1", 10);
            Console.WriteLine(c.ComoString());

            // Construimos Rectangulo
            Rectangulo r = new Rectangulo("Rectangulo 1", 5, 8);
            Console.WriteLine(r.ComoString());

            // Construimos Rombo
            Rombo rombo1 = new Rombo("Rombo 1", 5, 30);
            Console.WriteLine(rombo1.ComoString());

            // Construimos Romboide
            Romboide romboide1 = new Romboide("Romboide 1", 5, 7, 32.5);
            Console.WriteLine(romboide1.ComoString());

            // Para salir
            Console.WriteLine("\n\n\n\tPulse una tecla para salir");
            Console.ReadKey(true);

        }
    }
}
