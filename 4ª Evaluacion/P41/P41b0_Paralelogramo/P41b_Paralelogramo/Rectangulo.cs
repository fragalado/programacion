﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P41b_Paralelogramo
{
    class Rectangulo
    {
        // Atributos

        string nombre;
        int ladoBase, ladoLateral;

        // Constructores

        public Rectangulo(string nombre, int ladoBase, int ladoLateral)
        {
            this.nombre = nombre;
            this.ladoBase = ladoBase;
            this.ladoLateral = ladoLateral;
        }

        // Propiedades básicas

        public string Nombre { get => nombre; set => nombre = value; }
        public int LadoBase { get => ladoBase; set => ladoBase = value; }
        public int LadoLateral { get => ladoLateral; set => ladoLateral = value; }

        // Propiedades especiales

        public int Perimetro
        {
            get
            {
                return ladoBase + ladoBase + ladoLateral + ladoLateral;
            }
        }

        public double Area
        {
            get
            {
                return Math.Round((double)ladoBase * ladoLateral, 2);
            }
        }

        // Métodos
        public string ComoString()
        {
            return String.Format("\t{0}\t{1}\t{2}\t{3}\t{4}\t{5}", nombre, ladoBase, ladoLateral, 90, Perimetro, Area);
        }
    }
}
