﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P41b_Paralelogramo
{
    class Cuadrado
    {
        // Atributos

        string nombre;
        int ladoBase; 

        // Constructores

        public Cuadrado(string nombre, int ladoBase)
        {
            this.nombre = nombre;
            this.ladoBase = ladoBase;
        }

        // Propiedades básicas

        public string Nombre { get => nombre; set => nombre = value; }
        public int LadoBase { get => ladoBase; set => ladoBase = value; }

        // Propiedades especiales

        public int Perimetro
        {
            get
            {
                return ladoBase + ladoBase + ladoBase + ladoBase;
            }
        }

        public double Area
        {
            get
            {
                return Math.Round((double)ladoBase * ladoBase, 2);
            }
        }

        // Métodos
        public string ComoString()
        {
            return String.Format("\t{0}\t{1}\t{2}\t{3}\t{4}\t{5}", nombre, ladoBase, ladoBase, 90, Perimetro, Area);
        }
    }
}
