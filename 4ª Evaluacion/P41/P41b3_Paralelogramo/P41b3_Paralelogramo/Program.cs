﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P41b3_Paralelogramo
{
    class Program
    {
        static void Main(string[] args)
        {
            int opcion;
            List<Rectangulo> listaFiguras = new List<Rectangulo>(); // Donde guardaremos todas las figuras que construyamos
            string nombre;
            int ladoBase, ladoLateral;
            double angulo;
            do
            {
                opcion = Util.Menu();
                Console.Clear();
                Rectangulo r;
                switch (opcion)
                {
                    case 1:
                        // Construir Cuadrado
                        // Pedimos el nombre y el lado base
                        Console.Write("\n\tIntroduzca el nombre del cuadrado: ");
                        nombre = Console.ReadLine();
                        ladoBase = Util.CapturaEntero("Introduzca el lado base del cuadrado", 1, 20);
                        // Construimos un cuadrado y lo añadimos a la lista
                        r = new Cuadrado(nombre, ladoBase);
                        listaFiguras.Add(r);
                        break;
                    case 2:
                        // Construir Rectángulo
                        // Pedimos el nombre ,el lado base y el lado lateral
                        Console.Write("\n\tIntroduzca el nombre del rectángulo: ");
                        nombre = Console.ReadLine();
                        ladoBase = Util.CapturaEntero("Introduzca el lado base del rectángulo", 1, 20);
                        ladoLateral = Util.CapturaEntero("Introduzca el lado lateral del rectángulo", 1, 20);
                        // Construimos un rectangulo y lo añadimos a la lista
                        r = new Rectangulo(nombre, ladoBase, ladoLateral);
                        listaFiguras.Add(r);
                        break;
                    case 3:
                        // Construir Rombo
                        // Pedimos el nombre ,el lado base y el angulo
                        Console.Write("\n\tIntroduzca el nombre del rombo: ");
                        nombre = Console.ReadLine();
                        ladoBase = Util.CapturaEntero("Introduzca el lado base del rombo", 1, 20);
                        angulo = Util.CapturaFloat("Introduzca el angulo del rombo", 1, 20);
                        // Construimos un rombo y lo añadimos a la lista
                        r = new Rombo(nombre, ladoBase, angulo);
                        listaFiguras.Add(r);
                        break;
                    case 4:
                        // Construir Romboide
                        // Pedimos todos los datos
                        Console.Write("\n\tIntroduzca el nombre del romboide: ");
                        nombre = Console.ReadLine();
                        ladoBase = Util.CapturaEntero("Introduzca el lado base del romboide", 1, 20);
                        ladoLateral = Util.CapturaEntero("Introduzca el lado lateral del romboide", 1, 20);
                        angulo = Util.CapturaFloat("Introduzca el angulo del romboide", 1, 20);
                        // Construimos un romboide y lo añadimos a la lista
                        r = new Romboide(nombre, ladoBase, ladoLateral, angulo);
                        listaFiguras.Add(r);

                        break;
                    case 5:
                        // Presentar Paralelogramos
                        // Cabecera
                        Console.WriteLine("\tNombre  \tBase\tLateral\tÁngulo\tPerim. \tÁrea");
                        Console.WriteLine("\t------  \t----\t-------\t------\t------\t----");
                        foreach (Rectangulo aux in listaFiguras)
                        {
                            Console.WriteLine(aux.ComoString());
                        }
                        break;
                }

                if(opcion != 0)
                {
                    Console.Write("\n\tPulse una tecla para volver al menu");
                    Console.ReadKey(true);
                    Console.Clear();
                }
            } while (opcion != 0);


            // Para salir
            Console.WriteLine("\n\n\n\tPulse una tecla para salir");
            Console.ReadKey(true);
        }
    }
}
