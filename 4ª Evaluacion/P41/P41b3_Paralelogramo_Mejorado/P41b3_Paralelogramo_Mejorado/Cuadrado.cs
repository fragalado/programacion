﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P41b3_Paralelogramo_Mejorado
{
    class Cuadrado : Rectangulo
    {
        // Atributos -> Los de la clase Padre/Rectangulo

        // Constructores

        public Cuadrado(string nombre, int ladoBase) : base(nombre, ladoBase, ladoBase)
        {
        }

        // Propiedades básicas -> Las de la clase Padre/Rectangulo

        // Propiedades especiales

        public override int Perimetro
        {
            get
            {
                return LadoBase + LadoBase + LadoBase + LadoBase;
            }
        }

        public override double Area
        {
            get
            {
                return Math.Round((double)LadoBase * LadoBase, 2);
            }
        }

        // Métodos
        public override string ComoString()
        {
            return String.Format("\t{0}\t{1}\t{2}\t{3}\t{4}\t{5}", Util.CuadraTexto(Nombre, 15, false), LadoBase, LadoBase, 90, Perimetro, Area);
        }
    }
}
