﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P42b_DivisionEnteraVector
{
    class Program
    {
        static void Main(string[] args)
        {
            // Construimos el vector
            int[] vNums = new int[20];

            // Lo cargamos con números al azar entre 0 y 100
            Random azar = new Random();
            for (int i = 0; i < vNums.Length; i++)
                vNums[i] = azar.Next(0, 100 + 1);

            // Mostramos los datos del vector
            int contador = 0;
            for (int i = 0; i < vNums.Length; i++)
            {
                if (i < 10)
                    Console.SetCursorPosition(10, 1 + i);
                else
                    Console.SetCursorPosition(25, 1 + contador++);


                if (i < 10)
                    Console.Write(" ");
                Console.Write("{0} - {1}", i, vNums[i]);
            }


            // Variables que utilizaremos
            int posicion, numeroDividir, cociente;
            bool hayError = false;
            string repetir = "";

            do
            {
                try
                {
                    // Pedimos la posición del vector a ver
                    Console.Write("\n\n\t  ¿Posición del vector para ver? [0..{0}]: ", vNums.Length - 1);
                    posicion = Convert.ToInt32(Console.ReadLine());

                    // Ahora mostramos la posición y el valor
                    Console.Write("\n\tUsted ha elegido la posición {0} - {1}", posicion, vNums[posicion]);

                    // Pedimos el número por el que lo quiere dividir
                    Console.Write("\n\tIntroduce el número por el que quieras dividir: ");
                    numeroDividir = Convert.ToInt32(Console.ReadLine());

                    // Realizamos la división y mostramos el resultado
                    cociente = vNums[posicion] / numeroDividir;
                    Console.Write("\n\tResultado de la división: {0} / {1} es: {2}", vNums[posicion], numeroDividir, cociente);

                    // Pedimos si quiere repetir el proceso
                    Console.Write("\n\t¿Quiere repetir? (si/fin): ");
                    repetir = Console.ReadLine();
                    repetir = repetir.ToLower();
                    if (repetir != "si" && repetir != "fin")
                        throw new Exception();

                    hayError = false; // Si llega aqui quiere decir que no hay errores
                }
                // Error al acceder a una matriz/coleccion con un indice mayor
                catch (IndexOutOfRangeException e)
                {
                    Console.Write("\n\t** ERROR: la posición indicada no existe en la tabla **");
                    //Console.Write("\n\t** ERROR: {0} **", e.Message);
                }
                // Error de formato
                catch (FormatException e)
                {
                    Console.Write("\n\t** ERROR: {0} **", e.Message);
                }
                // Aqui estarán las demás excepciones
                // * OverflowException
                // * división por cero
                catch (Exception e)
                {
                    Console.Write("\n\t** ERROR: {0} **", e.Message);
                }
                
            } while (hayError || repetir != "fin");


            // Para salir
            Console.WriteLine("\n\n\n\tPulse una tecla para salir");
            Console.ReadKey();
        }
    }
}
