﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P42a2_CapturaEnteroExcepcion
{
    class Program
    {
        static void Main(string[] args)
        {
            // Atributos
            byte estatura;
            float peso, imc;


            char opcion = 'a';
            bool hayError = false;
            do
            {
                Console.Clear();
                // Pedimos la estatura
                estatura = Convert.ToByte(CapturaEntero("Introduzca su estatura (en cm)", 50, 220, "** ERROR: La estatura tiene que estar entre {0} y {1} cm. **"));

                // Pedimos el peso
                peso = Convert.ToSingle(CapturaEntero("Introduzca su peso (en kg)", 20, 200, "** ERROR: El peso tiene que estar entre {0} y {1} kg. **"));

                // IMC
                imc = (float)Math.Round(peso / (float)Math.Pow((estatura / 100f), 2), 2);

                Console.WriteLine("\n\tTu IMC es " + imc);
                if (imc < 18.5)
                    Console.WriteLine("\tTE FALTA PESO");
                else if (18.5 <= imc && imc < 25)
                    Console.WriteLine("\tPeso saludable");
                else if (25 <= imc && imc < 30)
                    Console.WriteLine("\tTienes SOBREPESO");
                else if (30 <= imc && imc < 31)
                    Console.WriteLine("\tLEVE OBESIDAD");
                else
                    Console.WriteLine("\t¡¡ERES OBESO!!");

                // Preguntamos si se quiere salir
                do
                {
                    try
                    {
                        Console.Write("\n\t¿Quieres volver a hacerlo? (s=Si/n=No): ");
                        opcion = Console.ReadKey(true).KeyChar;
                        if (opcion != 's' && opcion != 'n')
                        {
                            hayError = true;
                            throw new Exception();
                        }
                        hayError = false; // Si llega aqui será porque no hay ERROR
                    }
                    catch (Exception)
                    {
                        Console.Write("\n\t** Error: No se ha pulsado la tecla correcta **");
                    }
                } while (hayError);
            } while (opcion != 'n');



            // Pulse una tecla para salir
            Console.Write("\n\n\n\tPulse una tecla para salir");
            Console.ReadKey();
        }

        public static int CapturaEntero(string txt, int min, int max, string mensajeError)
        {
            int num = 0;
            bool ok = false;
            do
            {
                Console.Write("\n\t{0} [{1}..{2}]: ", txt, min, max);
                try
                {
                    num = Convert.ToInt32(Console.ReadLine());

                    if (num < min || num > max)
                        throw (new OverflowException(String.Format(mensajeError, min, max)));

                    ok = true;
                }
                catch (OverflowException e1)
                {
                    Console.Write("\n\t" + e1.Message);
                }
                catch (Exception e)
                {
                    Console.Write("\n\t"+e.Message);
                }
            } while (!ok);

            return num;
        }
    }
}
