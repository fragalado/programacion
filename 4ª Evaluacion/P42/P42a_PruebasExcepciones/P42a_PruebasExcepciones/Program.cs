﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P42a_PruebasExcepciones
{
    class Program
    {
        static void Main(string[] args)
        {
            // Atributos
            byte estatura;
            float peso, imc;


            char opcion = 'a';
            bool hayError;
            do
            {
                hayError = false;
                Console.Clear();
                try
                {
                    // Pedimos la estatura
                    Console.Write("\n\tIntroduzca su estatura (en cm): ");
                    estatura = Convert.ToByte(Console.ReadLine());

                    if (estatura < 50 || estatura > 220)
                    {
                        throw (new OverflowException("\n\t** ERROR: La estatura tiene que estar entre 50 y 220 cm. **"));
                    }

                    // Pedimos el peso
                    Console.Write("\n\tIntroduzca su peso (en kg): ");
                    peso = Convert.ToSingle(Console.ReadLine());

                    if(peso < 20 || peso > 200)
                    {
                        throw (new OverflowException("\n\t** ERROR: El peso tiene que estar entre 20 y 200 kg. **"));
                    }


                    // IMC
                    imc = (float)Math.Round(peso / (float)Math.Pow((estatura / 100f), 2), 2);

                    Console.WriteLine("\n\tTu IMC es " +imc);
                    if (imc < 18.5)
                        Console.WriteLine("\tTE FALTA PESO");
                    else if(18.5 <= imc && imc < 25)
                        Console.WriteLine("\tPeso saludable");
                    else if(25 <= imc && imc < 30)
                        Console.WriteLine("\tTienes SOBREPESO");
                    else if(30 <= imc && imc < 31)
                        Console.WriteLine("\tLEVE OBESIDAD");
                    else
                        Console.WriteLine("\t¡¡ERES OBESO!!");
                }
                catch (OverflowException e1)
                {
                    hayError = true;
                    Console.WriteLine(e1.Message);
                }
                catch(Exception e)
                {
                    hayError = true;
                    Console.WriteLine("\n\t"+e.Message);
                }

                if(!hayError)
                {
                    // Preguntamos si se quiere salir
                    try
                    {
                        Console.Write("\n\t¿Quiere volver a hacerlo? (s=Si/n=No): ");
                        opcion = Console.ReadKey(true).KeyChar;
                        if (opcion != 's' && opcion != 'n')
                            throw new Exception();
                    }
                    catch (Exception)
                    {
                        Console.Write("\n\t** Error: No se ha pulsado la tecla correcta **");
                    }
                }
                else
                {
                    // Hay error, luego haremos que se pulse una tecla para seguir
                    Console.WriteLine("\n\tPulse una tecla para seguir");
                    Console.ReadKey(true);
                }
            } while (hayError || opcion != 'n');



            // Pulse una tecla para salir
            Console.Write("\n\n\n\tPulse una tecla para salir");
            Console.ReadKey();
        }
    }
}
