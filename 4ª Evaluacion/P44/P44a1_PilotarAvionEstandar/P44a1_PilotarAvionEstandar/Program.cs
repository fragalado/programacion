﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P44a1_PilotarAvionEstandar
{
    class Program
    {
        static void Main(string[] args)
        {
            // El punto de entrada se construirá un avión
            Avion a = new Avion("AAP", "007", "CSI-2019", 1000);

            int opcion;
            bool puedeSalir = false, hayError = false;
            do
            {
                Console.Clear();
                Console.Write("\n\tAvión: {0}-{1}.  Matrícula: {2}. Altitud máxima: {3}", a.Marca, a.Modelo, a.Matricula, a.AltitudMax);
                Console.Write("\n\n\tSituación actual: Altitud: {0}    Velocidad: {1}.", a.Altitud, a.Velocidad);
                opcion = Util.Menu();

                try
                {
                    switch (opcion)
                    {
                        case 1:
                            // Aumentar velocidad
                            a.aumentarVelocidad(Util.CapturaEntero("¿Cantidad a Aumentar la Velocidad?"));
                            break;
                        case 2:
                            // Disminuir velocidad
                            if (!a.estaSuelo && a.Velocidad == 100 || a.estaSuelo && a.Velocidad == 0)
                                throw (new Exception("** Error: No se puede disminuir la velocidad **"));
                            a.disminuirVelocidad(Util.CapturaEntero("¿Cantidad a Disminuir la Velocidad?"));
                            break;
                        case 3:
                            // Despegar
                            a.despegar();
                            break;
                        case 4:
                            // Aumentar altitud
                            if (a.estaSuelo)
                                throw (new Exception("** Error: Antes habría que despegar **"));
                            a.aumentarAltitud(Util.CapturaEntero("¿Cantidad a Aumentar la Altitud?"));
                            break;
                        case 5:
                            // Disminuir altitud
                            if (a.estaSuelo)
                                throw (new Exception("** Error: Antes habría que despegar **"));
                            a.disminuirAltitud(Util.CapturaEntero("¿Cantidad a Disminuir la Altitud?"));
                            break;
                        case 6:
                            // Aterrizar
                            a.aterrizar();
                            break;
                        case 0:
                            // Salir
                            // Para poder salir el avión tiene que estar en el suelo y parado
                            if (a.estaParado && a.estaSuelo)
                                puedeSalir = true;
                            else
                                Console.Write("ERROR");
                            break;
                    }
                }
                catch (Exception e)
                {
                    hayError = true;
                    Console.SetCursorPosition(8, 24);
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Write(e.Message);
                    Console.ResetColor();
                }
                if(!puedeSalir)
                    Console.ReadKey(true);
            } while (!puedeSalir);
        }
    }
}
