﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P44a1_PilotarAvionEstandar
{
    class Avion
    {
        // Atributos

        string marca, modelo, matricula;
        int altitudMax;
        int altitud = 0; // La altitud estará a 0
        int velocidad = 0; // La velocidad estará a 0

        // Constructores

        public Avion(string marca, string modelo, string matricula, int altitudMax)
        {
            this.marca = marca;
            this.modelo = modelo;
            this.matricula = matricula;
            this.altitudMax = altitudMax;
        }

        // Propiedades básicas

        public string Marca { get => marca; set => marca = value; }
        public string Modelo { get => modelo; set => modelo = value; }
        public string Matricula { get => matricula; set => matricula = value; }
        public int AltitudMax { get => altitudMax; set => altitudMax = value; }
        public int Altitud { get => altitud; set => altitud = value; }
        public int Velocidad { get => velocidad; set => velocidad = value; }

        // Propiedades especiales

        public bool estaSuelo
        {
            get
            {
                if (altitud == 0) // Si la altitud es igual a 0 quiere decir que esta en el suelo
                    return true;
                
                // Si llega aqui sera porque no esta en el suelo
                return false;
            }
        }

        public bool estaParado
        {
            get
            {
                if(velocidad == 0) return true;
                return false;
            }
        }

        // Métodos

        /**
         * Método que aumenta la velocidad del avion, recibe por parametros la cantidad a aumentar
         */
        public void aumentarVelocidad(int cantAumentar)
        {
            velocidad = velocidad + cantAumentar;
            Console.Write("\n\tOK:  Acelerando {0}. Pulse una tecla.", cantAumentar);
        }

        /**
         * Método que disminuye la velocidad del avion, recibe por parametros la cantidad a disminuir
         */
        public void disminuirVelocidad(int cantDisminuir)
        {
            if (!estaSuelo && (velocidad - cantDisminuir) < 100) // Si el avion esta en vuelo no puede bajar los 100km/h
                throw (new Exception("** Error: El avión no puede reducir tanto la velocidad **"));
            else
            { 
                velocidad = velocidad - cantDisminuir;
                Console.Write("\n\tOK:  Desacelerando {0}. Pulse una tecla.", cantDisminuir);
            }
        }

        /**
         * Método que eleva al avión hasta los 100m de altura. Para poder despegar el avión tiene que estar en el suelo
         * y a una velocidad de al menos 200Km/h
         */
        public void despegar()
        {
            if (estaSuelo && velocidad >= 200) // Si esta en el suelo y la velocidad es al menos 200km/h
            { 
                altitud = 100;
                Console.Write("\n\tOK:  El avión ha despegado. Pulse una tecla.");
            }
            else if (!estaSuelo)
                throw (new Exception("** Error: El avion ya está en el aire **"));
            else
                throw (new Exception("** Error: El avión tiene que estar en el suelo y la velocidad tiene que ser al menos 200Km/h **"));
        }

        /**
         * Método que aumenta la altitud del avión. Solo funcionará si el avión ya ha despegado.
         */
        public void aumentarAltitud(int cantAumentar)
        {
            if ((altitud + cantAumentar) > altitudMax)
                throw (new Exception(String.Format("** Error: Superaría la Altitud máxima de {0} m **", altitudMax)));
            
            // Si llega aqui será porque no ha saltado la excepcion
            altitud = altitud + cantAumentar;
            Console.Write("\n\tOK:  Ascendiendo {0}. Pulse una tecla.", cantAumentar);
        }

        /**
         * Método que disminuye la altitud del avión.
         */
        public void disminuirAltitud(int cantDisminuir)
        {
            if ((altitud - cantDisminuir) < 100)
                throw (new Exception("** Error: En vuelo no se puede bajar de 100m de altitud **"));

            // Si llega aqui será porque no ha saltado la excepcion
            altitud = altitud - cantDisminuir;
            Console.Write("\n\tOK:  Descendiendo {0}. Pulse una tecla.", cantDisminuir);
        }

        /**
         * Método que aterriza el avion, pone la altitud y la velocidad a 0.
         * Para poder aterrizar el avión tiene que estar a menos de 200m de altitud
         * y a una velocidad inferior a los 400km/h
         */
        public void aterrizar()
        {
            if (!estaSuelo && !(altitud < 200 && velocidad < 400))
                throw new Exception("** Error: Para aterrizar: Altitud < 200m y Velocidad < 400km/h **");
            else if(estaSuelo)
                throw new Exception("** Error: el avion no ha despegado aún, intenta despegar primero **");


            altitud = 0;
            velocidad = 0;
            Console.Write("\n\tOK:  el avion ha aterrizado. Pulse una tecla.");
        }
    }
}
