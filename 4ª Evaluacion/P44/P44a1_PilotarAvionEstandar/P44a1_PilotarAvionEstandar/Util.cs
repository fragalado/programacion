﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P44a1_PilotarAvionEstandar
{
    class Util
    {
        public static int Menu()
        {
            Console.WriteLine("\n\n\t\t\t╔════════════════════════════╗");
            Console.WriteLine("\t\t\t║    Opciones del Piloto     ║");
            Console.WriteLine("\t\t\t╠════════════════════════════╣");
            Console.WriteLine("\t\t\t║   1) Aumentar Velocidad    ║");
            Console.WriteLine("\t\t\t║   2) Disminuir Velocidad   ║");
            Console.WriteLine("\t\t\t║   3) Despegar              ║");
            Console.WriteLine("\t\t\t║   4) Aumentar Altitud      ║");
            Console.WriteLine("\t\t\t║   5) Disminuir Altitud     ║");
            Console.WriteLine("\t\t\t║   6) Aterrizar             ║");
            Console.WriteLine("\t\t\t║                            ║");
            Console.WriteLine("\t\t\t║   0) Salir                 ║");
            Console.WriteLine("\t\t\t║                            ║");
            Console.WriteLine("\t\t\t╚════════════════════════════╝");
            return CapturaTecla("Introduce una opción", 0, 6);
        }

        public static int CapturaEntero(string txt, int min, int max)
        {
            int num;
            bool ok;

            do
            {
                Console.Write("\n\t{0} [{1}..{2}]: ", txt, min, max);
                ok = Int32.TryParse(Console.ReadLine(), out num);

                if(!ok)
                    Console.Write("** Error: No se ha introducido un entero **");
                else if(num < min || num > max)
                    Console.Write("** Error: El valor no esta dentro del rango **");
            } while (!ok || num < min || num > max);

            return num;
        }

        public static int CapturaEntero(string txt)
        {
            int num;
            bool ok;

            do
            {
                Console.Write("\n\t{0}: ", txt);
                ok = Int32.TryParse(Console.ReadLine(), out num);

                if (!ok)
                    Console.Write("** Error: No se ha introducido un entero **");
            } while (!ok);

            return num;
        }

        public static int CapturaTecla(string txt, int min, int max)
        {
            int num;
            bool ok;

            do
            {
                Console.Write("\n\t{0} [{1}..{2}]: ", txt, min, max);
                ok = Int32.TryParse(Console.ReadKey(true).KeyChar.ToString(), out num);

                if (!ok)
                    Console.Write("** Error: No se ha introducido un entero **");
                else if (num < min || num > max)
                    Console.Write("** Error: El valor no esta dentro del rango **");
            } while (!ok || num < min || num > max);

            return num;
        }
    }
}
