﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P40a_ProyPersona
{
    class PersonaRepasoExamen
    {
        // Atributos

        string nombre, apellidos;
        int anyo, mes, dia; // Fecha de nacimiento



        // Constructores

        // Constructor 1 -> Constructor sin parametros
        public PersonaRepasoExamen()
        {
        }

        // Constructor 2 -> Constructor con todos los parametros
        public PersonaRepasoExamen(string nombre, string apellidos, int anyo, int mes, int dia)
        {
            this.nombre = nombre;
            this.apellidos = apellidos;
            this.anyo = anyo;
            this.mes = mes;
            this.dia = dia;
        }

        // Constructor 3 -> Constructor con parametros nombre y apellidos, y crea la fecha 01/01/2000
        public PersonaRepasoExamen(string nombre, string apellidos)
        {
            this.nombre = nombre;
            this.apellidos = apellidos;
            anyo = 2000;
            mes = 1;
            dia = 1;
        }

        // Propiedades básicas

        public string Nombre { get => nombre; set => nombre = value; }
        public string Apellidos { get => apellidos; set => apellidos = value; }
        public int Anyo { get => anyo; set => anyo = value; }
        public int Mes { get => mes; set => mes = value; }
        public int Dia { get => dia; set => dia = value; }

        // Propiedades especiales

        /**
         * Propiedad de solo lectura, devuelve la edad en anyos del individuo.
         */
        public int Edad
        {
            get
            {
                int anyoActual = DateTime.Now.Year, mesActual = DateTime.Now.Month, diaActual = DateTime.Now.Day;
                int edad;

                if (mesActual < mes || (mesActual == mes && diaActual < dia))
                    edad = anyoActual - anyo -1;
                else
                    edad = anyoActual - anyo;
               
                return edad;
            }
        }

        // Métodos

        /**
         * Método que presenta el nombre completo de la persona y la fecha de nacimiento en formato dia/mes/anyo
         */
        public void Mostrar()
        {
            Console.Write("\n\tNombre: {0} {1}; Fecha nacimiento: {2}/{3}/{4}; Edad: {5}", nombre, apellidos, dia, mes, anyo, Edad);
        }
    }
}
