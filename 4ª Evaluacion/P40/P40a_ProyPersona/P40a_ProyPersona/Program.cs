﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P40a_ProyPersona
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Persona p1 = new Persona(); // Constructor vacio
            Persona p2 = new Persona("Francisco", "Gallego Dorado", 2003, 12, 28); // Constructor 5 parámetros
            Persona p3 = new Persona("Daniel", "Gallego Dorado"); // Constructor 2 parámetros

            Persona[] vPersonas = new Persona[] {p2, p3};

            for (int i = 0; i < vPersonas.Length; i++)
            {
                vPersonas[i].Mostrar();
            }

            // Creamos una lista de personas para introducir p2 y p3
            List<Persona> listaPersona = new List<Persona>();
            listaPersona.Add(p2);
            listaPersona.Add(p3);

            foreach (Persona p in listaPersona)
            {
                Console.WriteLine(p.Nombre + " tiene " + p.Edad + " años");
            }




            // -------------------------------------------------- REPASO EXANEN ------------------------------------------------
            Console.WriteLine("\n\n\n\tPulse una tecla para mostrar repaso examen");
            Console.ReadKey(true);
            Console.Clear();

            PersonaRepasoExamen p4 = new PersonaRepasoExamen(); // Constructor vacio
            p4.Nombre = "alberto";
            p4.Apellidos = "rodriguez";
            p4.Anyo = 2022;
            p4.Mes = 3;
            p4.Dia = 22;

            PersonaRepasoExamen p5 = new PersonaRepasoExamen("francisco", "gallego", 2022, 04, 21);
            PersonaRepasoExamen p6 = new PersonaRepasoExamen("daniel", "gallego", 2022, 04, 22);
            PersonaRepasoExamen p7 = new PersonaRepasoExamen("manuel", "gallego", 2022, 04, 23);
            PersonaRepasoExamen p8 = new PersonaRepasoExamen("david", "fernandez");

            p4.Mostrar();
            p5.Mostrar();
            p6.Mostrar();
            p7.Mostrar();
            p8.Mostrar();



            // Para salir
            Pausa("salir");
        }

        static void Pausa(string txt)
        {
            Console.WriteLine("\n\n\tPulse una tecla para "+txt);
            Console.ReadKey(true);
        }
    }
}
