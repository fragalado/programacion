﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P40a_ProyPersona
{
    class Persona
    {

        // Atributos

        string nombre, apellidos;
        int anyo, mes ,dia; // datos de la fecha de nacimiento

        // Constructores

        // Constructor 1: Vacio
        public Persona()
        {
        }

            // Constructor 2: Recibe todos los atributos
        public Persona(string nombre, string apellidos, int anyo, int mes, int dia)
        {
            this.nombre = nombre;
            this.apellidos = apellidos;
            this.anyo = anyo;
            this.mes = mes;
            this.dia = dia;
        }

            // Constructor 3: Recibe solo el nombre y apellido y crea fecha 01/01/2000
        public Persona(string nombre, string apellidos)
        {
            this.nombre = nombre;
            this.apellidos = apellidos;
            anyo = 2000;
            mes = 1;
            dia = 1;
        }

        // Getters y Setters
        
        public string Nombre {
            get => nombre; 
            set => nombre = value; 
        }
        public string Apellidos { 
            get => apellidos; 
            set => apellidos = value; 
        }
        public int Anyo { 
            get => anyo; 
            set => anyo = value; 
        }
        public int Mes { 
            get => mes; 
            set => mes = value; 
        }
        public int Dia { 
            get => dia; 
            set => dia = value; 
        }

            // Propiedad edad
        public int Edad
        {
            get 
            {
                int edad = 0;
                int diaHoy = DateTime.Now.Day;
                int mesHoy = DateTime.Now.Month;
                int anyoHoy = DateTime.Now.Year;

                //if (mesHoy < mes || (mesHoy == mes && diaHoy < dia))
                //    edad = anyoHoy - anyo - 1;
                //else
                //    edad = anyoHoy - anyo;

                int fechaCumple = mes * 100 + dia;
                int fechaHoy = mesHoy * 100 + dia;

                if(fechaHoy < fechaCumple)
                    edad = anyoHoy - anyo - 1;
                else
                    edad = anyoHoy - anyo;

                return edad;
            }
        }

        // Métodos

        public void Mostrar()
        {
            // Mostramos el nombre completo y la fecha de nacimiento dd/mm/YYYY
            Console.WriteLine("Nombre completo: {0}; Fecha nacimiento: {1};", (nombre + " " + apellidos), String.Format("{0}/{1}/{2}",dia,mes,anyo));
        }

    }
}
