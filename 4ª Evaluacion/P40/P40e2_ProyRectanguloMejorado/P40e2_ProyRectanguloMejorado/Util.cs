﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P40e2_ProyRectanguloMejorado
{
    class Util
    {
        public static bool PreguntaSiNo(string txt)
        {
            char opcion;

            do
            {
                Console.Write("\n\t{0} (s=Si/n=No): ", txt);
                opcion = Console.ReadKey().KeyChar;

                if (opcion != 's' && opcion != 'n')
                    Console.Write("\n\t** Error: No se ha introducido una opción correcta **");
            } while (opcion != 's' && opcion != 'n');

            if (opcion == 's')
                return true;

            return false;
        }

        public static int CapturaEntero(string txt, int min, int max, int valorPorDefecto)
        {
            string captura;

            do
            {
                Console.Write("\n\t{0} ({1}..{2}): ", txt, min, max);
                captura = Console.ReadLine();

                if (captura == "")
                    return valorPorDefecto;
                else if (Convert.ToInt32(captura) < min || Convert.ToInt32(captura) > max)
                    Console.Write("\n\t** Error: El valor no está dentro del rango **");
            } while (Convert.ToInt32(captura) < min || Convert.ToInt32(captura) > max);

            return Convert.ToInt32(captura);
        }

        public static string CuadraTexto(string txt, int longitud, bool conPuntos)
        {
            string aux;
            if (conPuntos)
                aux = "............................................";
            else
                aux = "                                            ";

            txt += aux;

            return txt.Substring(0, longitud);
        }
    }
}
