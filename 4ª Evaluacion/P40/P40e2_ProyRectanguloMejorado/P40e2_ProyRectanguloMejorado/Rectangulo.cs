﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P40e2_ProyRectanguloMejorado
{
    class Rectangulo
    {
        // Atributos

        string nombre; // No ponemos private porque en .Net viene por defecto
        int ladoBase, ladoLateral;

        // Constructores
        // Constructor 1 -> Tendrá solo el constructor con todos los parámetros
        public Rectangulo(string nombre, int ladoBase, int ladoLateral)
        {
            this.nombre = nombre;
            this.ladoBase = ladoBase;
            this.ladoLateral = ladoLateral;
        }

        // Propiedades básicas

        public string Nombre { get => nombre; set => nombre = value; }
        public int LadoBase { get => ladoBase; set => ladoBase = value; }
        public int LadoLateral { get => ladoLateral; set => ladoLateral = value; }

        // Propiedades especiales

        public int Perimetro
        {
            get
            {
                // La suma de sus 4 lados
                return ladoBase + ladoBase + ladoLateral + ladoLateral;
            }
        }

        public int Area
        {
            get
            {
                // ladoBase * ladoLateral
                return ladoBase * ladoLateral;
            }
        }

        // Métodos

        /**
         * Devuelve los campos del rectángulo como una cadena con el formato siguiente:
         * \tnombre\tladoBase\tladoLateral\t\tPerimetro\tArea
         */
        public string RectanguloAString()
        {
            return String.Format("\t{0}\t{1}\t{2}\t\t{3}\t{4}", Util.CuadraTexto(nombre, 8, false), ladoBase, ladoLateral, Perimetro, Area);
        }
    }
}
