﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P40e2_ProyRectanguloMejorado
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // Construimos un objeto con los siguientes campos:
            // "Rectan-1", 10, 20.
            Rectangulo r1 = new Rectangulo("Rectan-1", 10, 20);

            // 1) Se limpia la pantalla y, utilizando RectanguloAString se mostrará el rectángulo
            Console.Clear();
            Console.WriteLine("\n\tNombre    \tBase  \tLateral \tPerím.\tArea");
            Console.WriteLine(" \t----------\t----  \t------- \t------\t------");
            Console.WriteLine(r1.RectanguloAString());

            // 2) Utilizando PreguntaSiNo preguntará: ¿Quieres Modificar?
            //En caso afirmativo:
            //te pedirá los nuevos datos con las siguientes condiciones:
            //    • El nombre no puede dejarse en blanco ni tener más de 8
            //    caracteres.
            //    • Las longitudes de los lados tiene que estar entre 1 y 100.
            //Luego se volverá al punto 1
            string nombre, valorPorDefectoNombre = r1.Nombre;
            int ladoBase, ladoLateral, posX, posY;
            int valorPorDefectoLadoBase = r1.LadoBase, valorPorDefectoLadoLateral = r1.LadoLateral;
            int auxiliar;
            while (Util.PreguntaSiNo("¿Quieres Modificar?"))
            {
                // Pedimos el nombre
                do
                {
                    Console.Write("\n\tIntroduzca el nombre del rectángulo: ");
                    nombre = Console.ReadLine();

                    if (nombre == "")
                    {
                        nombre = valorPorDefectoNombre;
                        posX = Console.CursorLeft;
                        posY = Console.CursorTop;
                        Console.SetCursorPosition(posX + 45, posY - 1);
                        Console.Write(nombre);
                    }
                    else if (nombre.Length > 8)
                        Console.Write("\n\t** Error: El nombre no puede tener más de 8 carácteres **");
                } while (nombre.Length > 8);

                // Pedimos el ladoBase
                auxiliar = Util.CapturaEntero("Introduzca el lado base del rectángulo", 1, 100, valorPorDefectoLadoBase);
                if(auxiliar == valorPorDefectoLadoBase)
                {
                    posX = Console.CursorLeft;
                    posY = Console.CursorTop;
                    Console.SetCursorPosition(posX + 57, posY - 1);
                    Console.Write(auxiliar);
                }
                ladoBase = auxiliar;

                // Pedimos el ladoLateral
                auxiliar = Util.CapturaEntero("Introduzca el lado lateral del rectángulo", 1, 100, valorPorDefectoLadoLateral);
                if (auxiliar == valorPorDefectoLadoLateral)
                {
                    posX = Console.CursorLeft;
                    posY = Console.CursorTop;
                    Console.SetCursorPosition(posX + 60, posY - 1);
                    Console.Write(auxiliar);
                }
                ladoLateral = auxiliar;

                // Modicamos el objeto Rectangulo
                r1.Nombre = nombre;
                r1.LadoBase = ladoBase;
                r1.LadoLateral = ladoLateral;

                // Mostramos los nuevos datos
                Console.Clear();
                Console.WriteLine("\n\tNombre    \tBase  \tLateral \tPerím.\tArea");
                Console.WriteLine(" \t----------\t----  \t------- \t------\t------");
                Console.WriteLine(r1.RectanguloAString());
            }

            // Para salir
            Console.WriteLine("\n\n\n\tPulse una tecla para salir");
            Console.ReadKey();
        }
    }
}
