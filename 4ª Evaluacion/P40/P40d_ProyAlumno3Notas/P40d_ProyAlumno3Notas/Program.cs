﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P40d_ProyAlumno3Notas
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // Construimos una lista de objetos de tipo Alumno
            List<Alumno> listAlumnos = new List<Alumno>();

            // StreamReader para leer los datos de los alumnos (Clientes.txt)
            StreamReader sr = new StreamReader(".\\Datos\\Clientes.txt", Encoding.Default);

            // Recorremos el fichero y a la vez guardamos en la lista objetos de tipo Alumno
            Random azar = new Random();
            string[] vCampos;
            float nota1, nota2, nota3;
            int anyo;
            while (!sr.EndOfStream)
            {
                // Las notas se obtendrán aleatoriamente, entre 3.0 y 9.9, ambos inclusive
                // EJ linea: 28255995/A/Sánchez Elegante/Álvaro/93/8/10
                vCampos = sr.ReadLine().Split('/');

                // Creamos un objeto con los campos leidos
                if (Convert.ToInt32(vCampos[4]) > DateTime.Now.Year % 2000)
                    anyo = 1900 + Convert.ToInt32(vCampos[4]);
                else
                    anyo = 2000 + Convert.ToInt32(vCampos[4]);
                Fecha f = new Fecha(anyo, Convert.ToInt32(vCampos[5]), Convert.ToInt32(vCampos[6]));
                nota1 = azar.Next(30, 99 +1) / 10f;
                nota2 = azar.Next(30, 99 + 1) / 10f;
                nota3 = azar.Next(30, 99 + 1) / 10f;
                Alumno a = new Alumno(Convert.ToInt32(vCampos[0]), vCampos[1][0], vCampos[3].Trim(), vCampos[2].Trim(), f, nota1, nota2, nota3);

                // Añadimos el objeto a la lista
                listAlumnos.Add(a);
            }

            // Cerramos el StreamReader
            sr.Close();

            // 1) A continuación se recorrá la lista y se mostrará cada Alumno
            Console.WriteLine("     DNI    Edad    Apellidos, Nombre             N1\t N2\t N3\tMedia");
            Console.WriteLine(" ---------- ---- ----------------------------    ---\t---\t---\t-----");
            foreach (Alumno res in listAlumnos) 
            {
                Console.WriteLine(" {0}  {1}  {2}    {3}\t{4}\t{5}\t{6}", res.NumDNI + "-" + res.LetraDNI
                                                                      , res.Edad
                                                                      , Util.CuadraTexto(res.Apellidos + ", " + res.Nombre, 28, false)
                                                                      , res.Nota1
                                                                      , res.Nota2
                                                                      , res.Nota3
                                                                      , res.Media);
            }

            // 2) Preguntará si quiere guardar los datos antes de salir
            // Si se responde Sí, se guardarán los datos de los alumnos, notas incluidas, en un fichero de nombre
            // Alums3fNotas.txt
            char opcion = Util.CapturaCaracter("¿Quieres guardar los datos antes de salir? (s=Sí, n=No)", 's', 'n');
            if(opcion == 's')
            {
                // Para informar que se estan guardando los datos
                Console.Write("\n\tDatos guardándose...");
                // Creamos un StreamWriter para escribir en el fichero
                StreamWriter sw = File.CreateText(".\\Datos\\Alums3fNotas.txt");

                // Ej de linea: 45875214;H;Filomena;Española;19950210;7;8;9
                foreach (Alumno res in listAlumnos)
                {
                    sw.Write(res.NumDNI + ";");
                    sw.Write(res.LetraDNI + ";");
                    sw.Write(res.Apellidos + ";");
                    sw.Write(res.Nombre + ";");
                    sw.Write(res.FechaNac.Anyo + "" +res.FechaNac.Mes.ToString("00") + "" + res.FechaNac.Dia + ";");
                    sw.Write(res.Nota1 + ";");
                    sw.Write(res.Nota2 + ";");
                    sw.WriteLine(res.Nota3);
                }

                // Cerramos el StreamWriter
                sw.Close();
                Console.Write("\n\tSe ha completado el guardado");
            }

            Console.WriteLine("\n\n\tGracias por usar el programa!");
            // Para salir
            Console.WriteLine("\n\n\n\tPulse una tecla para salir");
            Console.ReadKey(true);
        }
    }
}
