﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P40d_ProyAlumno3Notas
{
    class Alumno
    {
        // Atributos

        private int numDNI;
        private char letraDNI;
        private string nombre, apellidos;
        private Fecha fechaNac; // <-- Fecha de nacimiento
        private float nota1, nota2, nota3;

        // Constructores
        // Constructor 1 -> Vacío
        public Alumno()
        {

        }
        // Constructor 2 -> Con todos los campos
        public Alumno(int numDNI, char letraDNI, string nombre, string apellidos, Fecha fechaNac, float nota1, float nota2, float nota3)
        {
            this.numDNI = numDNI;
            this.letraDNI= letraDNI;
            this.nombre = nombre;
            this.apellidos= apellidos;
            this.fechaNac= fechaNac;
            this.nota1 = nota1; 
            this.nota2 = nota2;
            this.nota3= nota3;
        }
        // Constructor 3 -> Recibe la fecha de la siguiente forma: 19950210
        public Alumno(int numDNI, char letraDNI, string nombre, string apellidos, string fechaNac, float nota1, float nota2, float nota3)
        {
            this.numDNI = numDNI;
            this.letraDNI = letraDNI;
            this.nombre = nombre;
            this.apellidos = apellidos;
            int dia = Convert.ToInt32(fechaNac.Substring(5, 2));
            int mes = Convert.ToInt32(fechaNac.Substring(3, 2));
            int anyo = Convert.ToInt32(fechaNac.Substring(0, 4));
            this.fechaNac = new Fecha(anyo, mes, dia);
            this.nota1 = nota1;
            this.nota2 = nota2;
            this.nota3 = nota3;
        }

        // Propiedades básicas

        public int NumDNI { get => numDNI; set => numDNI = value; }
        public char LetraDNI { get => letraDNI; set => letraDNI = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public string Apellidos { get => apellidos; set => apellidos = value; }
        public float Nota1 { get => nota1; set => nota1 = value; }
        public float Nota2 { get => nota2; set => nota2 = value; }
        public float Nota3 { get => nota3; set => nota3 = value; }
        internal Fecha FechaNac { get => fechaNac; set => fechaNac = value; }

        // Propiedades especiales

        /**
         * Propiedad de solo lectura
         * Devuelve la edad del alumno.
         */
        public int Edad
        {
            get
            {
                int edad , dia = DateTime.Now.Day, mes = DateTime.Now.Month, anyo = DateTime.Now.Year;
                edad = anyo - fechaNac.Anyo;
                if ((mes < fechaNac.Mes) || (mes == fechaNac.Mes && dia < fechaNac.Dia))
                    edad--;

                return edad;
            }
        }

        /**
         * Propiedad de solo lectura
         * Devuelve la media de las tres notas con un máximo de 2 decimales.
         */
        public float Media
        {
            get
            {
                return (float)Math.Round((nota1 + nota2 + nota3) / 3 , 2);
            }
        }

        // Métodos

        /**
         * Método que presenta los datos siguientes:
         * DNI completo; edad; apellidos, nombre; nota1; nota2; nota3; Media (la media con dos decimales siempre)
         */
        public void Mostrar()
        {
            Console.WriteLine("{0}-{1}; {2}; {3}, {4}; {5}; {6}; {7}; {8}", numDNI
                                                                          , letraDNI
                                                                          , Edad
                                                                          , apellidos
                                                                          , nombre
                                                                          , nota1
                                                                          , nota2
                                                                          , nota3
                                                                          , Media.ToString("0.00"));
        }
    }
}
