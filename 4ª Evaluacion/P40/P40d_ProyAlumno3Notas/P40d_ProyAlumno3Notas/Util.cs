﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P40d_ProyAlumno3Notas
{
    class Util
    {
        public static string CuadraTexto(string txt, int longitud, bool conPuntos)
        {
            string aux;

            if (conPuntos)
                aux = "..............................................................";
            else
                aux = "                                                              ";

            txt += aux;

            return txt.Substring(0, longitud);
        }

        public static char CapturaCaracter(string txt, char opcion1, char opcion2)
        {
            char opcion;

            do
            {
                Console.Write("\n\t{0}: ", txt);
                opcion = Console.ReadKey().KeyChar;
                if(opcion != opcion1 && opcion != opcion2)
                    Console.Write("\n\t** Error: No se ha introducido una opción correcta **");
            } while (opcion != opcion1 && opcion != opcion2);

            return opcion;
        }
    }
}
