﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P40d2_ProyAlum3Notas
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // Construimos una lista de objetos de tipo Alumno
            List<Alumno> listAlumnos = new List<Alumno>();

            // StreamReader para leer los datos de los alumnos (Alums3fNotas.txt)
            StreamReader sr = File.OpenText(".\\Datos\\Alums3fNotas.txt");

            // Construimos objetos de tipo Alumno con los datos del fichero
            string[] vAux;
            while(!sr.EndOfStream)
            {
                // Ej: 28255995;A;Sánchez Elegante;Álvaro;19930810;5,4;6,6;4,9
                vAux = sr.ReadLine().Split(';');
                Alumno alumno = new Alumno(Convert.ToInt32(vAux[0]),
                                           Convert.ToChar(vAux[1]),
                                           vAux[3].Trim(),
                                           vAux[2].Trim(),
                                           vAux[4].Trim(),
                                           Convert.ToSingle(vAux[5]),
                                           Convert.ToSingle(vAux[6]),
                                           Convert.ToSingle(vAux[7]));
                listAlumnos.Add(alumno); // Añadimos el objeto a la lista
            }

            // Cerramos el StreamReader
            sr.Close();

            // 1) Se recorrerá la lista y se mostrará cada alumno
            foreach (Alumno aux in listAlumnos)
            {
                aux.Mostrar();
            }

            // 2) Después preguntará "¿Quieres modificar alguna nota? (S=Si, n=No):"
            // Si se responde Sí, pedirá el número de dni del alumno cuya nota se quier modificar y se presentará
            // cada nota preguntando "¿Modificar? (s=Si, n=No):" y se actuará en consecuencia.
            // 3) Se repetirá el punto 2 hasta que se responda que no
            bool opcion, modificado = false;
            int numDNI;
            
            opcion = Util.PreguntaSiNo("¿Quieres modificar alguna nota?");
            if (opcion)
                modificado = true;
            while (opcion)
            {
                // Pedimos el numDNI del alumno a modificiar
                numDNI = Util.CapturaEntero("Introduzca el numero de dni del alumno a modificar", 0, 99999999);
                
                // Buscamos el dni
                foreach (Alumno aux in listAlumnos)
                {
                    if (aux.NumDNI == numDNI)
                    {
                        // Preguntamos Nota1
                        Console.Write("\n\tNota 1: " + aux.Nota1);
                        if (Util.PreguntaSiNo("¿Modificar?"))
                        {
                            aux.Nota1 = Util.CapturaEntero("Nueva Nota 1:", 1, 10);
                        }

                        // Preguntamos Nota2
                        Console.Write("\n\tNota 2: " + aux.Nota2);
                        if (Util.PreguntaSiNo("¿Modificar?"))
                        {
                            aux.Nota2 = Util.CapturaEntero("Nueva Nota 2:", 1, 10);
                        }

                        // Preguntamos Nota3
                        Console.Write("\n\tNota 3: " + aux.Nota3);
                        if (Util.PreguntaSiNo("¿Modificar?"))
                        {
                            aux.Nota3 = Util.CapturaEntero("Nueva Nota 3:", 1, 10);
                        }
                        break;
                    }
                }

                // Volvemos a preguntar
                opcion = Util.PreguntaSiNo("¿Quieres modificar alguna nota?");
            }

            // 4) Si se ha modificado alguna nota preguntará "¿Quieres guardar los cambios antes de salir? (s=Si, n=No):"
            if (modificado && Util.PreguntaSiNo("¿Quieres guardar los cambios antes de salir?"))
            {
                // StreamWriter para escribir en el fichero
                StreamWriter sw = File.CreateText(".\\Datos\\Alums3fNotas.txt"); // Borrará los datos anteriores

                // Recorremos la lista y vamos escribiendo los datos, con el siguient formato:
                // Ej: 28255995;A;Sánchez Elegante;Álvaro;19930810;5,4;6,6;4,9
                foreach (Alumno aux in listAlumnos)
                {
                    sw.Write(aux.NumDNI + ";"); // NumDNI
                    sw.Write(aux.LetraDNI + ";"); // LetraDNI
                    sw.Write(aux.Apellidos + ";"); // Apellidos
                    sw.Write(aux.Nombre + ";"); // Nombre
                    sw.Write(aux.FechaNac.Anyo.ToString("0000") + "" +aux.FechaNac.Mes.ToString("00") + "" + aux.FechaNac.Dia.ToString("00") + ";"); // FechaNac con el formato AAAAMMDD
                    sw.Write(aux.Nota1 + ";"); // Nota1
                    sw.Write(aux.Nota2 + ";"); // Nota2
                    sw.WriteLine(aux.Nota3); // Nota3
                }

                // Cerramos el StreamWriter
                sw.Close();

                // Mensaje para el usuario para que sepa que se han guardado los datos
                Console.Write("\n\n\t¡Los datos han sido guardados correctamente!");
            }

            Console.WriteLine("\n\n\tGracias por usar el programa!");
            // Para salir
            Console.WriteLine("\n\n\n\tPulse una tecla para salir");
            Console.ReadKey(true);
        }
    }
}