﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace P40d2_ProyAlum3Notas
{
    class Util
    {
        public static string CuadraTexto(string txt, int longitud, bool conPuntos)
        {
            string aux;

            if (conPuntos)
                aux = "..............................................................";
            else
                aux = "                                                              ";

            txt += aux;

            return txt.Substring(0, longitud);
        }

        public static char CapturaCaracter(string txt, char opcion1, char opcion2)
        {
            char opcion;

            do
            {
                Console.Write("\n\t{0}: ", txt);
                opcion = Console.ReadKey().KeyChar;
                if (opcion != opcion1 && opcion != opcion2)
                    Console.Write("\n\t** Error: No se ha introducido una opción correcta **");
            } while (opcion != opcion1 && opcion != opcion2);

            return opcion;
        }

        public static bool PreguntaSiNo(string txt)
        {
            char opcion;
            do
            {
                Console.Write("\n\t{0} (s=Si, n=No): ", txt);
                opcion = Console.ReadKey().KeyChar;

                if (opcion != 's' && opcion != 'S' && opcion != 'n' && opcion != 'N')
                    Console.Write("\n\t** Error: La opción pulsada no es correcta **");
            } while (opcion != 's' && opcion != 'S' && opcion != 'n' && opcion != 'N');

            if (opcion == 's' || opcion == 'S')
                return true;

            return false;
        }

        public static int CapturaEntero(string txt, int min, int max)
        {
            int num;
            bool ok;

            do
            {
                Console.Write("\n\t{0} ({1}..{2}): ", txt, min, max);
                ok = Int32.TryParse(Console.ReadLine(), out num);

                if (!ok)
                    Console.Write("\n\t** Error: No se ha introducido un numero **");
                else if (num < min || num > max)
                    Console.Write("\n\t** Error: El numero no está dentro del rango **");
            } while (!ok || num < min || num > max);

            return num;
        }
    }
}
