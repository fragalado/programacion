﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P40b_ProyFecha
{
    class Util
    {
        // Métodos
        public static bool PreguntaSiNo(string txt)
        {
            char opcion;
            do
            {
                Console.WriteLine("\t{0} (s/n):", txt);
                opcion = Console.ReadKey(true).KeyChar;
                
                if(opcion != 's' && opcion != 'n')
                    Console.WriteLine("** Error: No se ha introducido una opcion valida **");
            } while (opcion != 's' && opcion != 'n');

            if (opcion == 's')
                return true;

            return false;
        }

        public static int CapturaEntero(string txt, int min, int max)
        {
            int valor;
            bool ok;
            do
            {
                Console.Write("\n\t{0} [{1}..{2}]: ", txt, min, max);
                ok = Int32.TryParse(Console.ReadLine(), out valor);
                if (!ok)
                {
                    Console.Write("\n\t** Error: No se ha introducido un valor entero **");
                }
                else if (valor < min || valor > max)
                    Console.Write("\n\t** Error: No se ha introducido un valor dentro del rango **");
            } while (!ok || valor < min || valor > max);
            return valor;
        }
    }
}
