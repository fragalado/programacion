﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P40b_ProyFecha
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // Construimos un objeto fecha, con la fecha de hoy
            Fecha f1 = new Fecha();
            int diasAvance = 0;

            // 1) Presentará al usuario: «La fecha es... y la presentará en los tres formatos posibles.
            // 2) Utilizando PreguntaSiNo preguntará: «¿quieres cambiar de
            //fecha ? (s / n)». En caso afirmativo te pedirá los datos de la
            //fecha a utilizar, en el orden adecuado: año, seguido del mes
            //y el día* .Con esos datos actualizará la fecha del objeto.
            do
            {
                Console.Clear();
                Console.WriteLine("\n\tLa fecha actual es...");
                Console.WriteLine("\n\t" +f1.FechaEntero);
                Console.WriteLine("\n\t" +f1.FechaStringSp);
                Console.WriteLine("\n\t" +f1.FechaStringTexto);

                if(Util.PreguntaSiNo("¿quieres cambiar de fecha?"))
                {
                    f1.Anyo = Util.CapturaEntero("Introduzca el año", 1800, DateTime.Now.Year);
                    f1.Mes = Util.CapturaEntero("Introduzca el mes", 1, 12);
                    f1.Dia = Util.CapturaEntero("Introduzca el dia", 1, f1.VDiaMeses[f1.Mes - 1]);

                    Console.WriteLine("La nueva fecha es: " + f1.FechaStringSp);
                }

                diasAvance = Util.CapturaEntero("Numero de dias a avanzar", 0, 1000);

                for (int i = 0; i < diasAvance; i++)
                    f1.AvanzaDia();
            } while (diasAvance != 0);


            // Para salir
            Console.WriteLine("\n\n\n\tPulse una tecla para salir");
            Console.ReadKey(true);
        }
    }
}
