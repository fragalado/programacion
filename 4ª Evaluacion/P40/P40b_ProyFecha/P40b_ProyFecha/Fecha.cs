﻿using Microsoft.SqlServer.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P40b_ProyFecha
{
    class Fecha
    {
        // Atributos

        private int anyo;
        private int mes;
        private int dia;

        // Atributo especial

        private int[] vDiaMeses = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

        // Constructores

        // Constructor 1 -> Vacío
        public Fecha()
        {
            // Toma los datos de la fecha de hoy
            DateTime hoy = DateTime.Now;

            this.anyo = hoy.Year;
            this.mes = hoy.Month;
            this.dia = hoy.Day;
        }
            // Constructor 2 -> Todos las propidades
        public Fecha(int anyos, int mes, int dia)
        {
            this.anyo = anyos;
            this.mes = mes;
            this.dia = dia;
        }

        // Getter y Setter
        public int Anyo { get => anyo; 
            set
            {
                anyo = value;

                if (EsBisiesto)
                    vDiaMeses[1] = 29;
                else
                    vDiaMeses[1] = 28;
            } }
        public int Mes { get => mes; set => mes = value; }
        public int Dia { get => dia; set => dia = value; }
        public int[] VDiaMeses { get => vDiaMeses; set => vDiaMeses = value; }

        // Propiedades especiales
        public int FechaEntero
        {
            get
            {
                // Devuelve la fecha como un entero con el formato: AAMMDD donde AA son las 2 ultimas cifras del anyo
                return Convert.ToInt32((anyo % 100).ToString("00") + mes.ToString("00") + dia.ToString("00"));
            }
        }

        public string FechaStringSp
        {
            get
            {
                // devuelve la fecha en un string con el formato típico español: DD/MM/AAAA
                return dia.ToString("00") + "/" + mes.ToString("00") + "/" + anyo.ToString("0000");
            }
        }

        public string FechaStringTexto
        {
            get
            {
                // devuelve la fecha como la siguiente: 24 de marzo de 2014.
                string[] vMeses = { "enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre" };
                return dia + " de " + vMeses[mes - 1] + " de " + anyo;
            }
        }

        public bool EsBisiesto
        {
            get
            {
                return DateTime.IsLeapYear(anyo);
            }
        }

        // Métodos

        /**
         * Incrementa un dia a la fecha. Por supuesto, se tendrán en cuenta los cambios de meses y/o años
         */
        public void AvanzaDia()
        {
            dia++;

            if (dia > vDiaMeses[mes - 1])
            {
                dia = 1;
                mes++;
            }
            if(mes > 12)
            {
                mes = 1;
                anyo++;

                // Comprobamos si el nuevo anyo es bisiesto
                if (EsBisiesto)
                    vDiaMeses[1] = 29;
                else
                    vDiaMeses[1] = 28;
            }
        }
    }
}
