﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P40a_ProyCliente
{
    class Cliente
    {
        // Atributos

        private int numDNI;
        private char letraDNI;
        private string nombre, apellidos;
        byte anyo, mes, dia; // <-- Fecha de nacimiento

        // Constructores -> Constructor vacío

        // Getter y Setter

        public int NumDNI { get => numDNI; set => numDNI = value; }
        public char LetraDNI { get => letraDNI; set => letraDNI = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public string Apellidos { get => apellidos; set => apellidos = value; }
        public byte Anyo { get => anyo; set => anyo = value; }
        public byte Mes { get => mes; set => mes = value; }
        public byte Dia { get => dia; set => dia = value; }

        public int Edad
        {
            get
            {
                // Obtenemos la fecha actual
                DateTime fechaActual = DateTime.Now;

                // Obtenemos el dia, el mes y el anyo actual a partir de la fecha actual
                int diaActual = fechaActual.Day;
                int mesActual = fechaActual.Month;
                int anyoActual = fechaActual.Year;

                // Si el mes actual es anterior al mes de la fecha de nacimiento ó
                // si el mes actual es igual al mes de la fecha de nacimiento Y el dia actual es anterior al dia de nacimieno
                // Devolveremos la edad - 1
                // Si no devolvemos la edad
                int edad;
                if (mesActual < mes || (mesActual == mes && diaActual < dia))
                    edad = anyoActual - Anyo4Cifras - 1;
                else
                    edad = anyoActual - Anyo4Cifras;

                // Devolvemos la edad
                return edad;
            }
        }

        public int Anyo4Cifras
        {
            get
            {
                int anyoHoy2Cifras = DateTime.Now.Year % 100;

                if (anyo > anyoHoy2Cifras)
                    return 1900 + anyo;

                return 2000 + anyo;
            }
        }


        // Métodos

        /**
         * Método que presenta por pantalla los datos del cliente con el siguiente formato: "\t{0}-{1} {2} {3}/{4}/{5}"
         * Para: numDNI, letraDNI, «nombre Apellidos» (cuadrados a 27), dia, mes, año (con cuatro cifras).
         * EJ: 28255995-A  Álvaro Sánchez Elegante      10/08/1993
         */
        public void Mostrar()
        {
            Util p = new Util();
            Console.Write("\t{0}-{1} {2} {3}/{4}/{5}", numDNI
                                                         , letraDNI
                                                         , p.CuadraTexto(String.Format(nombre + " " + apellidos), 27, false)
                                                         , p.CuadraNumero(Convert.ToString(dia), 2)
                                                         , p.CuadraNumero(Convert.ToString(mes), 2)
                                                         , Anyo4Cifras);
        }
    }
}
