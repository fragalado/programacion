﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P40a_ProyCliente
{
    class Util
    {
        public string CuadraTexto(string txt, int longitud, bool conPuntos)
        {
            string aux;

            if (conPuntos)
                aux = "........................................................";
            else
                aux = "                                                        ";

            txt += aux;

            return txt.Substring(0, longitud);
        }

        public string CuadraNumero(string txt, int longitud)
        {
            if (txt.Length < longitud)
                txt = "0" + txt;

            return txt;
        }
    }
}
