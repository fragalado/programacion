﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace P40a_ProyCliente
{
    internal class Program
    {
        const string RUTA = ".\\Datos\\Clientes.txt";
        static void Main(string[] args)
        {
            // Se construirá una lista de objetos de tipo Cliente. 
            List<Cliente> listClientes = new List<Cliente>();

            // Para construir los objetos que se cargarán en la lista, se obtendrán los datos del fichero Clientes.txt.
            // Los registros de ese fichero tienen los campos separados por '/' en el siguiente orden:
            // No DNI / Letra DNI / Apellidos / Nombre / año / mes / día.

            // StreamReader para leer los datos del fichero
            StreamReader sr = new StreamReader(RUTA, Encoding.Default);

            // Vector auxiliar
            string[] vAux;
            while(!sr.EndOfStream)
            {
                // EJ: 28255995/A/Sánchez Elegante/Álvaro/93/8/10
                // Separamos los campos '/' y los guardamos en un vector
                vAux = sr.ReadLine().Split('/');

                // A partir de los campos del vector vamos creando objetos Cliente y añadiendolos a la lista
                Cliente c = new Cliente();
                c.NumDNI = Convert.ToInt32(vAux[0]);
                c.LetraDNI = Convert.ToChar(vAux[1]);
                c.Apellidos= vAux[2].Trim();
                c.Nombre = vAux[3].Trim();
                c.Anyo = Convert.ToByte(vAux[4]);
                c.Mes = Convert.ToByte(vAux[5]);
                c.Dia = Convert.ToByte(vAux[6]);

                // Añadimos a la lista
                listClientes.Add(c);
            }

            // Cerramos el StreamReader
            sr.Close();

            foreach (Cliente c in listClientes)
            {
                c.Mostrar();
                Console.WriteLine("    Edad: " + c.Edad);
            }


            // Para salir
            Console.Write("\n\n\n\tPulse una tecla para salir");
            Console.ReadKey(true);
        }
    }
}
