﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P40e1_ProyRectangulo
{
    class Util
    {
        public static bool PreguntaSiNo(string txt)
        {
            char opcion;

            do
            {
                Console.Write("\n\t{0} (s=Si/n=No): ", txt);
                opcion = Console.ReadKey().KeyChar;

                if (opcion != 's' && opcion != 'n')
                    Console.Write("\n\t** Error: No se ha introducido una opción correcta **");
            } while (opcion != 's' && opcion != 'n');

            if (opcion == 's')
                return true;

            return false;
        }

        public static int CapturaEntero(string txt, int min, int max)
        {
            int num;
            bool ok;

            do
            {
                Console.Write("\n\t{0} ({1}..{2}): ", txt, min, max);
                ok = Int32.TryParse(Console.ReadLine(), out num);

                if (!ok)
                    Console.Write("\n\t** Error: No se ha introducido un numero **");
                else if (num < min || num > max)
                    Console.Write("\n\t** Error: El numero no está dentro del rango **");
            } while (!ok || num < min || num > max);

            return num;
        }

        public static string CuadraTexto(string txt, int longitud, bool conPuntos)
        {
            string aux;
            if (conPuntos)
                aux = "............................................";
            else
                aux = "                                            ";

            txt += aux;

            return txt.Substring(0, longitud);
        }
    }
}
