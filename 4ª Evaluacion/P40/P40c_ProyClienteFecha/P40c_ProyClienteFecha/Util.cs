﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P40c_ProyClienteFecha
{
    class Util
    {
        public static string CuadraTexto(string txt, int longitud, bool conPuntos)
        {
            string aux;
            if (conPuntos)
                aux = "............................................................";
            else
                aux = "                                                            ";

            txt += aux;

            return txt.Substring(0, longitud);
        }

        public static void Pausa(string txt)
        {
            Console.WriteLine("\n\n\tPulse una tecla para {0}", txt);
            Console.ReadKey(true);
        }
    }
}
