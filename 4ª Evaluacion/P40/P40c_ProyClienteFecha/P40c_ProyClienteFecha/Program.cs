﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace P40c_ProyClienteFecha
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // Prueba Constructor 1
            // Cliente(int numDNI, char letraDNI, string nombre, string apellidos, Fecha fechaNac)
            Fecha f1 = new Fecha();
            Cliente c1 = new Cliente(53965130, 'T', "Francisco", "Gallego Dorado", f1);
            Console.WriteLine("\tConstructor 1:");
            c1.mostrar();
            Util.Pausa("mostrar el constructor 2");
            Console.Clear();

            // Prueba Constructor 2
            //Cliente(int numDNI, char letraDNI, string nombre, string apellidos, byte anyo, byte mes, byte dia)
            Cliente c2 = new Cliente(53965130, 'T', "Francisco", "Gallego Dorado", 21, 02, 28);
            Console.WriteLine("\n\tConstructor 2:");
            c2.mostrar();
            Util.Pausa("mostrar el constructor 3");
            Console.Clear();

            // Prueba Constructor 3
            // Cliente(string registro) FICHERO
            StreamReader sr = new StreamReader("C:\\Users\\csi22\\Documents\\Programacion 2022\\x4ª Evaluacion\\P40a_ProyCliente\\P40a_ProyCliente\\bin\\Debug\\Datos\\Clientes.txt", Encoding.Default);
            List<Cliente> listaClientes = new List<Cliente>();
            while (!sr.EndOfStream)
            {
                Cliente c = new Cliente(sr.ReadLine());
                listaClientes.Add(c);
            }
            sr.Close();
            Console.WriteLine("\n\tConstructor 3:");
            Console.WriteLine("\n\t   DNI         Nombre Apellidos          Fecha Nac.  Edad");
            Console.WriteLine("\t----------  ---------------------------  ----------  ---- ");
            foreach (Cliente aux in listaClientes)
            {
                Console.WriteLine("\t{0}  {1}\t {2}   {3}", aux.NumDNI +"-"+ aux.LetraDNI, Util.CuadraTexto(aux.Nombre.Trim() + " " + aux.Apellidos.Trim(), 27, false),
                                                            aux.FechaNac.FechaStringSp, aux.Edad);
            }
            Util.Pausa("mostrar el constructor 4");
            Console.Clear();

            // Prueba Constructor 4
            // Cliente(int numDNI, char letraDNI, string nombre, string apellidos, DateTime fecha)
            DateTime fecha = DateTime.Now;
            Cliente c4 = new Cliente(53965130, 'T', "Francisco", "Gallego Dorado", fecha);
            Console.WriteLine("\n\tConstructor 4:");
            c4.mostrar();
            Util.Pausa("mostrar el constructor 5");
            Console.Clear();

            // Prueba Constructor 5
            // Cliente(int numDNI, char letraDNI, string nombre, string apellidos)
            Cliente c5 = new Cliente(53965130, 'T', "Francisco", "Gallego Dorado");
            Console.WriteLine("\n\tConstructor 5:");
            c5.mostrar();
            Util.Pausa("mostrar el constructor 6");
            Console.Clear();

            // Prueba Constructor 6
            // Cliente(int numDNI, char letraDNI, string nombre, string apellidos, string fechaString)
            Cliente c6 = new Cliente(53965130, 'T', "Francisco", "Gallego Dorado", "29/02/2020");
            Console.WriteLine("\n\tConstructor 6:");
            c6.mostrar();
            Util.Pausa("mostrar el constructor 7");
            Console.Clear();

            // Prueba Constructor 7
            // Cliente(int numDNI, char letraDNI, string nombre, string apellidos, int fechaEntero)
            Cliente c7 = new Cliente(53965130, 'T', "Francisco", "Gallego Dorado", 190828);
            Console.WriteLine("\n\tConstructor 7:");
            c7.mostrar();
            Util.Pausa("mostrar el constructor 8");
            Console.Clear();

            // Prueba Constructor 8
            // Cliente(int numDNI, char letraDNI, string nombre, string apellidos, string fechaString)
            Cliente c8 = new Cliente(53965130, 'T', "Francisco", "Gallego Dorado", "13 de diciembre de 2003");
            Console.WriteLine("\n\tConstructor 8:");
            c8.mostrar();
            Console.WriteLine("\tEdad constructor 8: " + c8.Edad);
            Util.Pausa("mostrar el constructor 9");
            Console.Clear();



            // Esto es en clase
            // Constructor c5
            Cliente c9 = new Cliente(24567454, 'G', "Juan", "Alcántaro");

            // Constructor c7
            Cliente c10 = new Cliente(5612395, 'A', "Pepe", "Martínez", 201201);

            c9.mostrar();
            Util.Pausa("mostrar el constructor 10");
            Console.Clear();
            c10.mostrar();


            // Para salir
            Console.WriteLine("\n\n\n\tPulse una tecla para salir");
            Console.ReadKey(true);
        }
    }
}
