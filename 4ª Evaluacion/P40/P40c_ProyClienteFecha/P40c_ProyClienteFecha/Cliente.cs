﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Net.Mime.MediaTypeNames;

namespace P40c_ProyClienteFecha
{
    internal class Cliente
    {
        // Atributos

        private int numDNI;
        private char letraDNI;
        private string nombre, apellidos;
        private Fecha fechaNac; // <-- Fecha de nacimiento

        // Variable para algunos constructores que trabajan con ficheros
        private string[] vCampos;

        // Constructores
        // Constructor 1: Con todos los campos
        public Cliente(int numDNI, char letraDNI, string nombre, string apellidos, Fecha fechaNac)
        {
            this.numDNI = numDNI;
            this.letraDNI = letraDNI;
            this.nombre = nombre;
            this.apellidos = apellidos;
            this.fechaNac = fechaNac;
        }
        // Constructor 2: Todos menos la fechaNac, recibe el año con sus dos últimos digitos
        public Cliente(int numDNI, char letraDNI, string nombre, string apellidos, byte anyo, byte mes, byte dia)
        {
            this.numDNI = numDNI;
            this.letraDNI = letraDNI;
            this.nombre = nombre;
            this.apellidos = apellidos;
            int anyoCompleto = (anyo < (DateTime.Now.Year + 1)%100) ? anyo+2000 : anyo + 1900;
            fechaNac = new Fecha(anyoCompleto, mes, dia);
        }
        // Constructor 3: Recibe un registro (fichero).
        public Cliente(string registro)
        {
            // EJ registro: 28255995/A/Sánchez Elegante/Álvaro/93/8/10
            vCampos = registro.Split('/');
            numDNI = Convert.ToInt32(vCampos[0]);
            letraDNI = Convert.ToChar(vCampos[1]);
            nombre = vCampos[3];
            apellidos = vCampos[2];
            int anyo = Convert.ToInt32(vCampos[4]);
            int anyoCompleto = anyo < (DateTime.Now.Year + 1)%100 ? anyo + 2000 : anyo + 1900;
            fechaNac = new Fecha(anyoCompleto, Convert.ToInt32(vCampos[5]), Convert.ToInt32(vCampos[6]));
        }
        // Constructor 4: Recibe todos los parámetros pero con la fecha con el tipo de datos DateTime
        public Cliente(int numDNI, char letraDNI, string nombre, string apellidos, DateTime fecha)
        {
            this.numDNI = numDNI;
            this.letraDNI = letraDNI;
            this.nombre = nombre;
            this.apellidos = apellidos;
            fechaNac = new Fecha(fecha.Year, fecha.Month, fecha.Day);
        }
        // Constructor 5: Que no recibe la fecha y crea una fecha con la fecha actual
        public Cliente(int numDNI, char letraDNI, string nombre, string apellidos)
        {
            this.numDNI = numDNI;
            this.letraDNI = letraDNI;
            this.nombre = nombre;
            this.apellidos = apellidos;
            fechaNac = new Fecha();
        }
        // Constructor 6: Recibe la fecha como string de la propiedad FechaStringSp
        // Y 
        // Constructor 8: Recibe la fecha como un string de la propiedad FechaStringTexto
        public Cliente(int numDNI, char letraDNI, string nombre, string apellidos, string fechaString)
        {
            this.numDNI = numDNI;
            this.letraDNI = letraDNI;
            this.nombre = nombre;
            this.apellidos = apellidos;

            // Condicional
            if (fechaString.Contains('/'))
            {
                // fechaStringSp -> DD/MM/AAAA
                vCampos = fechaString.Split('/');
                int dia = Convert.ToInt32(vCampos[0]);
                int mes = Convert.ToInt32(vCampos[1]);
                int anyo = Convert.ToInt32(vCampos[2]);
                fechaNac = new Fecha(anyo, mes, dia);
            }
            else
            {
                string[] vMeses = { "enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre" };
                // fechaStringTexto -> DD de [mes en formato string] de AAAA
                string[] separatingStrings = { "de" };
                vCampos = fechaString.Split(separatingStrings, System.StringSplitOptions.RemoveEmptyEntries);
                int dia = Convert.ToInt32(vCampos[0].Trim());
                int mes = 0;
                for (int i = 0; i < vMeses.Length; i++)
                {
                    if (vMeses[i] == vCampos[1].Trim())
                        mes = i + 1;
                }
                int anyo = Convert.ToInt32(vCampos[2].Trim());
                fechaNac = new Fecha(anyo, mes, dia);
            }
        }
        // Constructor 7: Recibe la fecha como un int de la propiedad FechaEntero
        public Cliente(int numDNI, char letraDNI, string nombre, string apellidos, int fechaEntero)
        {
            this.numDNI = numDNI;
            this.letraDNI = letraDNI;
            this.nombre = nombre;
            this.apellidos = apellidos;
            // fechaEntero -> AAMMDD
            int dia = Convert.ToInt32(fechaEntero.ToString().Substring(4, 2));
            int mes = Convert.ToInt32(fechaEntero.ToString().Substring(2, 2));
            int anyo = Convert.ToInt32(fechaEntero.ToString().Substring(0, 2));
            int anyoCompleto = anyo < (DateTime.Now.Year + 1)%100 ? anyo + 2000 : anyo + 1900;
            fechaNac = new Fecha(anyo, mes, dia);
        }

        // Propiedades

        public int NumDNI { get => numDNI; set => numDNI = value; }
        public char LetraDNI { get => letraDNI; set => letraDNI = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public string Apellidos { get => apellidos; set => apellidos = value; }
        public Fecha FechaNac { get => fechaNac; set => fechaNac = value; }

        // Propiedades especiales

        public int Edad
        {
            get
            {
                // Segun la fechaNac obtendremos la edad
                int edad = DateTime.Now.Year - fechaNac.Anyo;

                if ((fechaNac.Mes > DateTime.Now.Month) || (fechaNac.Mes == DateTime.Now.Month && fechaNac.Dia > DateTime.Now.Day))
                    edad--;
                // edad = (fechaNac.Mes > DateTime.Now.Month) || (fechaNac.Mes == DateTime.Now.Month && fechaNac.Dia > DateTime.Now.Day) ? DateTime.Now.Year - fechaNac.Anyo - 1 : DateTime.Now.Year - fechaNac.Anyo;

                // Devolvemos la edad
                return edad;
            }
        }

        // Métodos
        public void mostrar()
        {
            // int numDNI, char letraDNI, string nombre, string apellidos, Fecha fechaNac
            Console.WriteLine("\n\tDNI: " + numDNI + letraDNI +
                "; Nombre Completo: " + nombre + ", " +apellidos + "; Fecha Nacimiento: " + fechaNac.FechaStringSp);
        }
    }
}
