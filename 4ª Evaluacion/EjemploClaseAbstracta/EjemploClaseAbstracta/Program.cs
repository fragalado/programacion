﻿using System;
using System.Collections.Generic;


namespace EjemploClaseAbstracta
{
    abstract class FiguraGeometrica
    {
        public abstract double Area();
        public virtual void Mensaje()
        {
            Console.WriteLine("\n\t Este método no es abstracto pero sí virtual, lo que implica que lo puedo sobreescribir en clases hijas.");
        }

    }

    //Clase círculo
    class Circulo : FiguraGeometrica
    {
        //Atributo
        int radio;

        //Constructor
        public Circulo(int radio)
        {
            this.radio = radio;
        }

        // Implementamos el método abstracto en esta clase hija
        public override double Area()
        {
            return (radio * radio * Math.PI);
        }

        //Sobreescribimos el método virtual Mensaje() en esta clase hija
        public override void Mensaje()
        {
            base.Mensaje();
        }

    }


    //Clase cuadrado
    class Cuadrado : FiguraGeometrica
    {
        //Atributos
        private int lado;

        public Cuadrado(int lado)
        {
            this.lado = lado;
        }

        // Implementamos el método abstracto en esta clase hija
        public override double Area()
        {
            return lado * lado;
        }

        //Sobreescribimos el método virtual Mensaje() en esta clase hija
        public override void Mensaje()
        {
            base.Mensaje();
        }
    }

    //Clase rectángulo
    class Rectangulo : FiguraGeometrica
    {
        //atributos
        int ladoBase;
        int ladoAltura;

        public Rectangulo(int ladoBase, int ladoAltura)
        {
            this.ladoBase = ladoBase;
            this.ladoAltura = ladoAltura;
        }

        // Implementamos el método abstracto en esta clase hija
        public override double Area()
        {
            return ladoBase * ladoAltura;
        }

        //Sobreescribimos el método virtual Mensaje() en esta clase hija
        public override void Mensaje()
        {
            base.Mensaje();
        }
    }

    //Clase principal Program
    class Program
    {
        static void Main(string[] args)
        {
            List<FiguraGeometrica> listaFiguras = new List<FiguraGeometrica>();

            //listaFiguras.Add(new FiguraGeometrica()); //<-- Esto daría error. Pruébalo

            // Hacemos polimorfismo con las hijas de nuestra clase abstracta FiguraGeometrica
            listaFiguras.Add(new Circulo(24));
            listaFiguras.Add(new Cuadrado(24));
            listaFiguras.Add(new Rectangulo(24, 20));

            Console.WriteLine("Las áreas de las figuras son: \n");
            foreach (FiguraGeometrica figura in listaFiguras)
            {
                Console.WriteLine("\t " + figura.Area());
                figura.Mensaje();
            }
            Console.ReadKey(true);
        }
    }
}
