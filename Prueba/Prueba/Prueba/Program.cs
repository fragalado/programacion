﻿using System;
using System.Media;
using System.Runtime.Remoting.Metadata.W3cXsd2001;

// Alumno: Gallego, Francisco José
/*
 * Pedira nombre y edad
 * y contestara adecuadamente
 */
namespace Prueba
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string nombre, captura;
            Console.Write("\n\tDime tu nombre: "); // WriteLine da un salto de línea
            nombre = Console.ReadLine(); // Siempre devuelve tipo string

            int edad;
            Console.Write("\tBuenos días, " + nombre+ ".\n \tPor favor, dime tu edad: ");
            // Console.Write("\tBuenos días, {0}. Por favor, dime tu edad", nombre);
            captura = Console.ReadLine();
            edad = Convert.ToInt32(captura);    // Para convertir de string a int

            Console.WriteLine("\t{0}: tienes {1} años el año que viene cumples {2}", nombre, edad, edad + 1);
            Console.WriteLine("\tEl número de trienios cumplidos es: " + edad / 3);
            /*   ES LO MISMO QUE LO DE ARRIBA PERO NOS AHORRAMOS EL \N
            Console.Write("\t{0}: tienes {1} años el año que viene cumples {2}", nombre, edad, edad + 1);
            Console.Write("\n\tEl número de trienios cumplidos es: " + edad / 3);
            */
            Console.Write("\n\n\tPulse 'INTRO' para salir");
            Console.ReadLine();

            // CTRL + K + F   para cuadrar todo el texto que tengamos seleccionado

        }
    }
}
