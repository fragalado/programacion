﻿using System;

class PruebasMetodosPosicion
{
    static void Main(string[] args)
    {
        #region--- No tocar este código -------
        for (int f = 10; f < 20; f++)
        {
            Console.BackgroundColor = ConsoleColor.DarkGreen;
            Console.SetCursorPosition(0, f);
            Console.Write("{0}                                                                              ", f);
            Console.ResetColor();
        }
        #endregion

        //----- Escribe a partir de esta línea tu código de prueba -------


        Console.Write("\n\n\n\t\tPulsa una tecla para salir:");
        Console.ReadKey();
    }

    // Escribe aquí el método

}

