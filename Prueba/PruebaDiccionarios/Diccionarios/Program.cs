﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diccionarios
{
    class Program
    {
        static void Main(string[] args)
        {
            // Declaracion
            Dictionary<int, string> dicClientes = new Dictionary<int, string>();

            // ------------------------------ AÑADIR -------------------------------
            PulsaTecla("añadir valores al diccionario");
            // Añadir valores a un diccionario
            dicClientes.Add(1, "Fran Gallego");
            dicClientes.Add(3, "Roberto Fernandez");
            // dicClientes.Add(1, "Alfredo Gallego"); // System.ArgumentException: 'Ya se agregó un elemento con la misma clave.'
            dicClientes.Add(2, "Jose Gallego");

            foreach (KeyValuePair<int, string> aux in dicClientes)
            {
                Console.WriteLine(aux.Key + ": " + aux.Value);
            }


            // ------------------------------- ELIMINAR ----------------------------------
            PulsaTecla("eliminar valor del diccionario");
            // Eliminar valores de un diccionario
            dicClientes.Remove(1);

            foreach (KeyValuePair<int, string> aux in dicClientes)
            {
                Console.WriteLine(aux.Key + ": " + aux.Value);
            }


            // ----------------------------- BUSCAR -----------------------------------
            PulsaTecla("buscar por id");

            if(dicClientes.ContainsKey(1))
                Console.WriteLine("El diccionario contiene la clave 1");

            if(dicClientes.ContainsKey(2))
                Console.WriteLine("El diccionario contiene la clave 2");


            // ------------------------------------- OBTENER --------------------------------
            PulsaTecla("obtener el valor");

            Console.WriteLine(dicClientes[2]); // Obtenemos el valor de la clave 2
                                               // No es como las listas que se accede al indice 2

            Console.WriteLine(dicClientes[3]); // La clave 3 estaría en el indice 1, pero ponemos 3 porque accedemos a la clave

            // Para salir
            PulsaTecla("salir");
        }

        static void PulsaTecla(string txt)
        {
            Console.WriteLine("\n\n\tPulse una tecla para {0}",txt);
            Console.ReadKey(true);
            Console.Clear();
        }
    }
}
