﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.NetworkInformation;

class PruebasMetodosPosicion
{
    static void Main(string[] args)
    {
        #region--- No tocar este código -------
        for (int f = 10; f < 20; f++)
        {
            Console.BackgroundColor = ConsoleColor.DarkGreen;
            Console.SetCursorPosition(0, f);
            Console.Write("{0}                                                                              ", f);
            Console.ResetColor();
        }
        #endregion

        //----- Escribe a partir de esta línea tu código de prueba -------
        //LimpiaFila(12);
        //Pausa("para aprobar la asignatura", 14);
        //Print("para probar print", 12, false);
        //Print("para probar print", 14, true);

        //Probar("VectorNones", 20);
        //int[] vector = VectorNones(15);
        //for (int i = 0; i < vector.Length; i++)
        //{
        //    if (i % 5 == 0)
        //        Console.WriteLine();
        //    Console.Write("\t{0}",vector[i]);
        //}

        //Probar("VectorNotasAzar", 29);
        //double[] vector2 = VectorNotasAzar(20);
        //for (int i = 0; i < vector2.Length; i++)
        //{
        //    if (i % 5 == 0)
        //        Console.WriteLine();
        //    Console.Write("\t{0}", vector2[i].ToString("0.0"));
        //}

        //Probar("MuestravectorAlreves", 38);
        //MuestravectorAlreves(vector2);

        //Probar("MediaVector", 62);
        //// Si el metodo no devolviese 3 decimales: Console.Write(MediaVector(vector).ToString("0.000"));
        //Console.Write(MediaVector(vector));

        //Probar("MuestraNormalOrdenadoInvertido", 76);
        //MuestraNormalOrdenadoInvertido(vector);

        //Probar("VectorIdsAzar", 20);
        //int[] vector3 = VectorIdsAzar(87);
        //MuestraVector(vector3, true);


        Console.Write("\n\n\n\t\tPulsa una tecla para salir");
        Console.ReadKey();
    }

    // Escribe aquí el método

    static int Menu()
    {
        int opcion = 0;
        Console.WriteLine("\n\n\t\t╔═════════════════════════╗");
        Console.WriteLine("\t\t║   Ordenar datos por...  ║");
        Console.WriteLine("\t\t╠═════════════════════════╣");
        Console.WriteLine("\t\t║   1) id                 ║");
        Console.WriteLine("\t\t║                         ║");
        Console.WriteLine("\t\t║   2) Apellidos, Nombre  ║");
        Console.WriteLine("\t\t║                         ║");
        Console.WriteLine("\t\t║   3) Edad (creciente)   ║");
        Console.WriteLine("\t\t║                         ║");
        Console.WriteLine("\t\t║   4) Altura             ║");
        Console.WriteLine("\t\t║                         ║");
        Console.WriteLine("\t\t║   5) Peso               ║");
        Console.WriteLine("\t\t║                         ║");
        Console.WriteLine("\t\t║   6) Nombre Apellidos   ║");
        Console.WriteLine("\t\t║                         ║");
        Console.WriteLine("\t\t║   7) Sin ordenar        ║");
        Console.WriteLine("\t\t║_________________________║");
        Console.WriteLine("\t\t║                         ║");
        Console.WriteLine("\t\t║      0)  Salir          ║");
        Console.WriteLine("\t\t╚═════════════════════════╝");


        // Utilizamos CapturaEntero para capturar la opcion
        opcion = CapturaEntero("Introduzca una opcion", 0, 7);
        return opcion;
    }

    static int CapturaEntero(string txt, int min, int max)
    {
        int num;
        bool ok;
        do
        {
            Console.Write("\n\t{0} [{1}..{2}]: ", txt, min, max);
            ok = Int32.TryParse(Console.ReadLine(), out num);

            if (!ok)
                Console.WriteLine("\n\t** Error: El valor introducido no es un numero **");
            else if (num < min || num > max)
                Console.WriteLine("\n\t** Error: El valor introducido no está dentro del rango **");
        } while (!ok || num < min || num > max);

        return num;
    }


    private static void LimpiaFila(int fila)
    {
        Console.SetCursorPosition(0, fila);
        Console.Write("                                                                                     ");
        Console.SetCursorPosition(0, fila);
    }
    
    private static void Pausa(string txt, int fila) 
    {
        LimpiaFila(fila);
        Console.Write("Pulse una tecla para {0}",txt);
        Console.ReadKey(true);
        Console.SetCursorPosition(0, fila);
        Console.Write("                                                                                     ");
    }

    private static void Print(string txt, int fila, bool conReadKey)
    {
        LimpiaFila(fila);
        Console.Write("{0}", txt);
        if (conReadKey)
            Console.ReadKey();
    }

    private static int[] VectorNones(int numE)
    {
        int[] vector = new int[numE];
        // 1ª opción
        int numeroImpar = -1;
        for (int i = 0; i < numE; i++)
        {
            vector[i] = numeroImpar+=2;
        }

        /* 2ª opción
        int numeroImpar = 1;
        for (int i = 0; i < numE; i++)
        {
            vector[i] = numeroImpar;
            numeroImpar += 2;
        } */

        /* 3ª opcion
        vector[0] = 1;
        for(int i=1; i < vector.Length; i++){
            vector[i] = vector[i-1] + 2;
        }
        */
        return vector;
    }

    private static double[] VectorNotasAzar(int tamanyo)
    {
        double[] vector = new double[tamanyo];
        Random azar = new Random();
        for (int i = 0; i < tamanyo; i++)        
            vector[i] = azar.Next(30, 99+1)/10.0;
        return vector;
    }


    private static void MuestravectorAlreves(double[] vector)
    {
        for (int i = vector.Length-1; i>=0 ; i--)
        {
            if (i < 10)
                Console.Write(" ");
            Console.WriteLine("{0}) {1}",i, vector[i]);
        }
    }


    private static double MediaVector(int[] vector)
    {
        int suma = 0;
        for (int i = 0; i < vector.Length; i++)
            suma += vector[i];

        return Math.Round(suma * 1.0 / vector.Length, 3);
    }

    private static void MuestraNormalOrdenadoInvertido(int[] vector)
    {
        Console.Clear();
        // Orden ascendente segun posicion
        for (int i = 0; i < vector.Length; i++)
        {
            Console.SetCursorPosition(4, i + 2);
            if (i < 10)
                Console.Write(" ");
            Console.Write("{1}) {0}", vector[i], i);
        }

        // Ordenado segun valor
        List<int> list = new List<int>(); // Más facil: List<int> list = new List<int>(vector)
            // Recorremos el vector y guardamos los valores en la lista
        for (int i = 0; i < vector.Length; i++)
            list.Add(vector[i]);

            // Ordenamos la lista
        list.Sort();
            // Imprimimos la lista
        for (int i = 0; i < vector.Length; i++)
        {
            Console.SetCursorPosition(16, i + 2);
            if (i < 10)
                Console.Write(" ");
            Console.Write("{1}) {0}", list[i], i);
        }

        // Ordenado descendente segun posicion
        int contador = 0;
        for (int i = vector.Length - 1; i >= 0; i--)
        {
            Console.SetCursorPosition(28, contador++ + 2);
            if (i < 10)
                Console.Write(" ");
            Console.Write("{1}) {0}", vector[i], i);
        }
    }

    // tamanyo debe de ser menor de 90
    private static int[] VectorIdsAzar(int tamanyo)
    {
        // Creamos un vector int[]
        int[] vector = new int[tamanyo];
        // Vamos a cargarlo con valores al azar de dos cifras, pero sin repetir ninguno, para ello:
        // Creamos una variable de tipo Random
        Random azar = new Random();
        // Vamos a crear un bucle y dentro de este bucle vamos a hacer otro bucle donde va a comprobar
        // si el número al azar ya se encuentre en el array
        int numeroAzar;
        int contador;
        for (int i = 0; i < vector.Length; i++)
        {
            // Inicializamos las variables
            numeroAzar = azar.Next(10, 99 + 1);
            contador = 0;
            // Vamos a comprobar
            while (contador < i + 1)
            {
                if (vector[contador++] == numeroAzar)
                {
                    numeroAzar = azar.Next(10, 99 + 1);
                    contador = 0;
                }
            }
            // Cuando salga del bucle será porque el numeroAzar no se encuentra
            // en ninguna posición del array
            vector[i] = numeroAzar;
        }

        /* FORMA FACIL
        for (int i = 0; i < vector.Length; i++)
        {
            // Inicializamos las variables
            numeroAzar = azar.Next(10, 99 + 1);
            while(vector.Contains(numeroAzar))
                numeroAzar = azar.Next(10, 99 + 1);
            vector[i] = numeroAzar;
        }*/
        return vector;
    }

    private static void MuestraVector(int[] vEnt, bool ordenado)
    {
        List<int> listVector = new List<int>(vEnt);
        if (ordenado)
            listVector.Sort();
        for (int i = 0; i < listVector.Count; i++)
        {
            Console.Write("\n\t");
            if (i < 10)
                Console.Write(" ");
            Console.Write("{0}) {1}", i, listVector[i]);

        }
    }

    private static void Probar(string txt, int num)
    {
        Print(String.Format("Para probar {0}",txt), num, true);
        Console.Write("\n\n");
    }
}

