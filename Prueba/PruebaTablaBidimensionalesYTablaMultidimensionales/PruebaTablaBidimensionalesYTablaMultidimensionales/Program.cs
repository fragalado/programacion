﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaTablaBidimensionalesYTablaMultidimensionales
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // declaración de un array de 50 x 70
            int[,] tabla2D = new int[50,70];
            // declaración y construcción de un array de 3 x 5
            int[,] tabla2Dv2 = { {1,2,3,0,8}, {2,4,6,8,0}, {3,5,7,9,1} };

            Console.WriteLine("\t Tamaño total = {0}; en {1} filas x {2} columnas\n",tabla2Dv2.Length, tabla2Dv2.GetLength(0), tabla2Dv2.GetLength(1));

            // Mostrar todos los elementos de la tabla
            for (int i = 0; i < tabla2Dv2.GetLength(0); i++)
            {
                Console.Write("\t");
                for (int j = 0; j < tabla2Dv2.GetLength(1); j++)
                {
                    Console.Write("{0}  ", tabla2Dv2[i,j]);
                }
                Console.WriteLine();
            }

            MostrarDiferentesMetodos("mostrar solo los 4 primeros números de las 2 primeras filas");
            // Mostrar solo los 4 primeros números de las 2 primeras filas
            Console.Write("\n\n\n");
            for (int i = 0; i < 2; i++)
            {
                Console.Write("\t");
                for (int j = 0; j < 4; j++)
                {
                    Console.Write("{0}  ", tabla2Dv2[i, j]);
                }
                Console.WriteLine();
            }

            MostrarDiferentesMetodos("mostrar fila elegida");
            // Mostrar fila elegida
            int fila = CapturaEntero("\n\t¿Qué fila quieres leer?", 0, tabla2Dv2.GetLength(0)-1);

            Console.Write("\t");
            for (int i = 0; i < tabla2Dv2.GetLength(1); i++)
            {
                Console.Write("{0}  ", tabla2Dv2[fila, i]);
            }

            MostrarDiferentesMetodos("mostrar columna elegida");
            // Mostrar columna elegida
            int columna = CapturaEntero("\n\t¿Qué columna quieres leer?", 0, tabla2Dv2.GetLength(1) - 1);

            for (int i = 0; i < tabla2Dv2.GetLength(0); i++)
            {
                Console.Write("\t{0}\n", tabla2Dv2[i, columna]);
            }

            MostrarDiferentesMetodos("mostrar el elemento de la fila y columna elegida elegida");
            // Mostrar el elemento de la fila y columna elegida
            int fila2=CapturaEntero("\n\t¿Qué fila quieres leer?", 0, tabla2Dv2.GetLength(0) - 1);
            int columna2 = CapturaEntero("\n\t¿Qué columna quieres leer?", 0, tabla2Dv2.GetLength(1) - 1);
            Console.Write("\t"+tabla2Dv2[fila2, columna2]);

            MostrarDiferentesMetodos("cambiar el elemento de una fila y columna");
            // Cambiar el elemento de una fila y columna
            int fila3 = CapturaEntero("\n\t¿Qué fila quieres cambiar?", 0, tabla2Dv2.GetLength(0) - 1);
            int columna3 = CapturaEntero("\n\t¿Qué columna quieres cambiar?", 0, tabla2Dv2.GetLength(1) - 1);
            tabla2Dv2[fila3, columna3] = CapturaEntero(String.Format("\n\t¿Qué número quieres poner? Fila:{0}; Columna:{1}; Valores:",fila3,columna3),-100,100);
            for (int i = 0; i < tabla2Dv2.GetLength(0); i++) // Para mostrar la tabla entera
            {
                Console.Write("\t");
                for (int j = 0; j < tabla2Dv2.GetLength(1); j++)
                {
                    Console.Write("{0}  ", tabla2Dv2[i, j]);
                }
                Console.WriteLine();
            }


            // Para salir
            Console.Write("\n\n\n\tPulse una tecla para salir");
            Console.ReadKey();
        }

        static int CapturaEntero(string texto, int min, int max)
        {
            bool esCorrecto;
            int valor;
            do
            {
                Console.Write("{0} [{1}..{2}]: ", texto, min, max);

                esCorrecto = Int32.TryParse(Console.ReadLine(), out valor);
                if (!esCorrecto)
                    Console.WriteLine("\n\n\t** Error: el valor introducido no es un número entero");
                else if (valor < min || valor > max)
                {
                    esCorrecto = false;
                    Console.WriteLine("\n\n\t** Error: el valor introducido no está dentro del rango");
                }
            } while (!esCorrecto);
            return valor;
        }

        static void MostrarDiferentesMetodos(string texto)
        {
            Console.Write("\n\n\tPulse una tecla para {0}",texto);
            Console.ReadKey(true);
            Console.Clear();
        }
    }
}
