﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaDoWhile1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // Presentar los 100 primersos números enteros de izquierda a derecha separados por tabulador

            int i = 0;
            while (i <= 100)
            {
                Console.Write("{0}\t", i++);
            }
            Console.Write("\n");
            int ab = 0;
            do
            {
                Console.Write("{0}\t", ab++);
            } while (ab <= 100);
            Console.Write("\n\n\tPulse una tecla para salir");
            Console.ReadKey();

        }
    }
}
