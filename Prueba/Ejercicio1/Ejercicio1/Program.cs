﻿/*
 * Ejercicio: Crear un programa en C# que lea una lista de números desde el usuario y calcule la suma de todos los números pares en la lista.

    El programa debe pedir al usuario que ingrese una serie de números separados por comas.
    El programa debe separar cada número ingresado en un arreglo utilizando el método string.Split(',').
    El programa debe recorrer cada elemento del arreglo y verificar si es par o no utilizando el operador módulo %.
    Si el número es par, debe agregarlo a una variable acumuladora.
    Al final del recorrido del arreglo, el programa debe imprimir en pantalla la suma de todos los números pares encontrados.
    Nota: El programa debe ser capaz de manejar entradas vacías y números no válidos, y debe mostrar un mensaje de error apropiado en cualquiera de estos casos.
*/
using System;

namespace Ejercicio1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // Declaramos las variables que vamos a necesitar
            int suma = 0;
            int numero;
            bool ok;

            // Pedimos al usuario una serie de numeros
            Console.Write("\n\tIntroduce una serie de números [26,151,123...]: ");

            // Separamos cada número ingresado y guardamos en una tabla/vector
            string[] numerosIntroducidos = Console.ReadLine().Split(','); // Aqui ya tenemos todos los 'numeros' guardados en distintas unidades

            // Hacemos un bucle para comprobar si los numeros son par o impar, si es par guardamos en una variable suma
            for (int i = 0; i < numerosIntroducidos.Length; i++)
            {
                ok = Int32.TryParse(numerosIntroducidos[i], out numero);
                if(!ok)
                {
                    Console.Write("\n\tEl valor '{0}' no es un número", numerosIntroducidos[i]);
                }
                else if (EsPar(numero))
                        suma += numero;
            }

            // Imprimimos por pantalla la suma de todos los números pares
            Console.Write("\n\n\tLa suma de los números pares es: {0}",suma);




            // Para salir
            Pausa("salir");
        }

        static void Pausa(string txt)
        {
            Console.Write("\n\n\n\tPulse una tecla para {0}", txt);
            Console.ReadKey();
        }

        static bool EsPar(int numero)
        {
            if (numero % 2 == 0)
                return true;

            return false;
        }
    }
}
