﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.Versioning;
using System.Text;
using System.Threading.Tasks;

namespace Prueba1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            /*
             * PRUEBA PARA SEPARAR UN STRING POR ';'
             */
            //string linea = "149;Sánchez Elegante, Álvaro;3,5;4,6;4,05";
            //string[] lineaSeparado = linea.Split(';');
            //for (int i = 0; i < lineaSeparado.Length; i++)
            //{
            //    Console.WriteLine(lineaSeparado[i]);
            //}

            /*
             * PRUEBA PARA PROBAR LOS STRING
             */
            //string cadena1 = "José Antonio";
            //string cadena2 = cadena1;
            //string cadena3 = String.Copy(cadena1);
            //string cadena4 = "José Antonio";
            //string cadena5 = "JosÉ Antonio";

            //// Devuelve true porque las dos variables apuntan a la misma direccion de memoria
            //Console.WriteLine(cadena1 == cadena2);

            //Console.WriteLine(cadena1 == cadena3);

            //Console.WriteLine(cadena1 == cadena4);

            //Console.WriteLine(cadena1 == cadena5);

            //// Asi se comparan las direcciones de los objetos, no se compara el contenido
            //Console.WriteLine((object)cadena4 == (object)cadena5);
            //Console.WriteLine((object)cadena1 == (object)cadena3);
            //Console.WriteLine((object)cadena1 == (object)cadena2);

            //Console.WriteLine((object)cadena1 == (object)cadena4);


            /*
             * OBTENER DATOS:
             * 126Sánchez Elegante  Álvaro      1,78,82  
             */
            //string texto = "126Sánchez Elegante  Álvaro      1,78,82  ";
            //byte[] tabId = new byte[1];
            //string[] tabAlum = new string[1];
            //float[] tabNotas = new float[1];


            //tabId[0] = Convert.ToByte(texto.Substring(0, 3));

            //Console.WriteLine(tabId[0]);


            /**
             * SEPARAR ESPACIOS:
             * "Arenas Mata       Daniel Luis "
             */
            //string texto = "Arenas Mata       Daniel Luis ";

            //string[] tablaApellidos = new string[1];
            //string[] tablaNombres = new string[1];
            //string[] tablaAlums = new string[1];

            //tablaApellidos[0] = texto.Substring(0, 18);
            //tablaNombres[0] = texto.Substring(18, 12);
            //tablaAlums[0] = tablaApellidos[0].Trim() + ", " +  tablaNombres[0].Trim();

            //Console.Write(tablaAlums[0]);



            // DATETIME
            //DateTime fecha = DateTime.Now;
            //Console.Write(fecha);

            // Para salir
            Console.Write("\n\n\tPulse una tecla para salir");
            Console.ReadKey();
        }
    }
}
