﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaSaltoDeLinea
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int cont = 0;
            int num = 10;
            while (num < 70)
            {
                if (cont % 6 == 0)
                    Console.Write("\n\t" + num);
                else
                    Console.Write("\t" + num);
                num++;
                cont++;
            }
            Console.Write("\nPulse tecla");
            Console.ReadKey();
        }
    }
}
