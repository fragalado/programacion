﻿using System;
using System.Collections.Generic;

namespace PruebaListas
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // Declaración
            List<int> miLista; // Despues de esto miLista vale Null
            // Declaración y construccion
            List<int> miLista2 = new List<int>();

            // Diferentes tipos de errores
            //miLista[2] = 23;
            //miLista2[2] = 23;

            Pausa("añadir 154 a miLista2");
            // Añadir un elemento a miLista2
            miLista2.Add(154);
            
            // Recorrer miLista2
            for (int i = 0; i < miLista2.Count; i++)
            {
                Console.Write("\n\t{0}) {1}", i, miLista2[i]);
            }

            Pausa("insertar el 12 a miLista2 en la primera posición");
            // Insertar un elemento a miLista2
            miLista2.Insert(0,12);
            
            // Recorrer miLista2
            for (int i = 0; i < miLista2.Count; i++)
            {
                Console.Write("\n\t{0}) {1}", i, miLista2[i]);
            }

            Pausa("eliminar el 200 de miLista2 que no existe");
            // Elimar un elemento a miLista2
            bool loHaBorrado = miLista2.Remove(200);
            if (loHaBorrado)
                Console.Write("El número 200 lo ha borrado");
            else
                Console.Write("\tEl número 200 no existe");

            Pausa("eliminar el 154 de miLista2");
            // Elimar un elemento a miLista2
            loHaBorrado = miLista2.Remove(154);
            if (loHaBorrado)
                Console.Write("\tEl número 154 lo ha borrado");
            else
                Console.Write("El número 154 no existe");

            // Recorrer miLista2
            for (int i = 0; i < miLista2.Count; i++)
            {
                Console.Write("\n\t{0}) {1}", i, miLista2[i]);
            }


            Pausa("volver a cargar el 154");
            miLista2.Add(154);
            // Recorrer miLista2
            for (int i = 0; i < miLista2.Count; i++)
            {
                Console.Write("\n\t{0}) {1}", i, miLista2[i]);
            }

            Pausa("eliminar el último elemento de miLista2");
            miLista2.RemoveAt(miLista2.Count-1);
            // Recorrer miLista2
            for (int i = 0; i < miLista2.Count; i++)
            {
                Console.Write("\n\t{0}) {1}", i, miLista2[i]);
            }


            Pausa("añadir a la lista 20 números al azar entre 100 y 500");
            Random azar = new Random();
            for (int i = 0; i < 20; i++)
            {
                miLista2.Add(azar.Next(100,500+1));
            }
            // Recorrer miLista2
            for (int i = 0; i < miLista2.Count; i++)
            {
                Console.Write("\n\t{0}) {1}", i, miLista2[i]);
            }

            
            Pausa("ordenar la lista presentándola a la derecha de la anterior");
            Console.Clear();
            for (int i = 0; i < miLista2.Count; i++)
            {
                Console.Write("\n\t{0}) {1}", i, miLista2[i]);
            }
            miLista2.Sort();
            for (int i = 0; i < miLista2.Count; i++)
            {
                Console.SetCursorPosition(20, i+1);
                Console.Write("{0}) {1}", i, miLista2[i]);
            }

            
            Pausa("invertir la lista presentándola a la derecha de la anterior");
            Console.Clear();
            for (int i = 0; i < miLista2.Count; i++)
            {
                Console.Write("\n\t{0}) {1}", i, miLista2[i]);
            }
            miLista2.Sort();
            for (int i = 0; i < miLista2.Count; i++)
            {
                Console.SetCursorPosition(20, i +1);
                Console.Write("{0}) {1}", i, miLista2[i]);
            }
            miLista2.Reverse();
            for (int i = 0; i < miLista2.Count; i++)
            {
                Console.SetCursorPosition(33, i + 1);
                Console.Write("{0}) {1}", i, miLista2[i]);
            }



            // Pulsa para salir
            Pausa("salir");
        }

        static void Pausa(string texto)
        {
            Console.WriteLine("\n\n\n\tPulsa una tecla para {0} y presentarla", texto);
            Console.ReadKey(true);
        }
    }
}
