﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaDoWhile2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            /*  Te pide un número positivo de dos cifras. 
                Si NO cumple el requisito presentará un mensaje de error.
                y volverá a pedir el número
                Si es correcto presenta otro mensaje “Correcto. El número elegido es X”.
            */

            Console.Write("\n\tIntroduce un número entero de dos cifras: ");
            int numero = Convert.ToInt32(Console.ReadLine());

            while (numero < 10 || numero > 99)
            {
                Console.Clear();
                // Si ha entrado es porque el número era erróneo
                Console.Write("\n\t** Error: El número no es de dos cifras **");

                Console.Write("\n\tVuelve a introducir un número entero de dos cifras: ");
                numero = Convert.ToInt32(Console.ReadLine());
            }


            do
            {
                Console.Clear();
                Console.Write("\n\tIntroduce un número entero de dos cifras: ");
                numero = Convert.ToInt32(Console.ReadLine());

                if (numero < 10 || numero > 99)
                {
                    Console.Write("\n\t** Error: El número no es de dos cifras **");
                }
            } while (numero < 10 || numero > 99);

            Console.Clear();
            Console.Write("\n\tCorrecto: El número elegido es {0}",numero);
            Console.Write("\n\n\n\tPulse una tecla para salir");
            Console.ReadKey();
        }
    }
}
