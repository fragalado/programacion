﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P33b2_LeerDatosEnTxtSeparadoresCampos
{
    internal class Program
    {
        static string CARPETAORIGEN = "C:.\\Datos\\";
        static void Main(string[] args)
        {
            // Declaramos las variables que vamos a necesitar
            string ruta;
            string[] vectorStringSeparado;

            // --------------- Obtener ruta y leer fichero ---------------
            // Obtenemos la ruta usando CapturaRuta()
            ruta = CapturaRuta();
            // Creamos una variable tipo StreamReader y abrimos el fichero con la ruta
            StreamReader sr = File.OpenText(ruta);
            /*
             * El problema nos dice que tiene que abrir AlumNotas.txt, luego podemos hacer lo siguiente:
             * StreamReader sr = File.OpentText(CARPETAORIGEN + "AlumNotas.txt");
             */


            //-------------- Mostramos los datos  -----------------
            Console.Clear();
            Console.WriteLine("     Id  Alumno\t\t\t\tProg    Ed      BD      Media");
            Console.WriteLine("     -----------------------------------------------------------------");

            // Una vez que ya tenemos las tablas llenas de valores, vamos a mostrar
            double media;
            double notaProg, notaEd, notaBD;
            
            // Ahora creamos un bucle para ir cogiendo cada linea del fichero e ir imprimiendo a la vez
            while (!sr.EndOfStream)
            {
                vectorStringSeparado = sr.ReadLine().Split(';');
                // Una vez que hemos guardado los datos separados por ';' podemos imprimir
                notaProg = Convert.ToDouble(vectorStringSeparado[2]);
                notaEd = Convert.ToDouble(vectorStringSeparado[3]);
                notaBD = Convert.ToDouble(vectorStringSeparado[4]);
                media = (notaProg + notaEd + notaBD )/ 3.0;
                Console.WriteLine("\n     {0} {1}\t {2}\t{3}\t{4}\t{5}", vectorStringSeparado[0], CuadrarTexto(vectorStringSeparado[1], 30, true), notaProg, notaEd, notaBD, media);
            }

            // Una vez que ya tenemos todas las filas guardadas, vamos a cerrar el fichero
            sr.Close();

            // Para salir
            Pausa("salir");
        }

        /*
 * Metodo para obtener la ruta de un fichero
 * Entrada: Nada
 * Salida: Un string con la ruta
 */
        private static string CapturaRuta()
        {
            string nombreFichero;
            bool existeRuta;
            do
            {
                Console.Write("\n\t¿nombre del fichero (sin extension)?: ");
                nombreFichero = Console.ReadLine();
                existeRuta = File.Exists(CARPETAORIGEN + nombreFichero + ".txt");
                if (!existeRuta)
                    Console.Write("\n\t** Error: El nombre introducido no existe **");
            } while (!existeRuta);

            return CARPETAORIGEN + nombreFichero + ".txt";
        }

        private static void Pausa(string txt)
        {
            Console.Write("\n\n\n\tPulse una tecla para {0}", txt);
            Console.ReadKey(true);
        }

        static string CuadrarTexto(string texto, int numeroCaracteres, bool conPuntos)
        {
            if (conPuntos)
                texto += "..............................................";
            else
                texto += "                                              ";

            return texto.Substring(0, numeroCaracteres);
        }
        /**
         * Se puede hacer lo mismo pero haciendo sobre carga de métodos
         */

    }
}
