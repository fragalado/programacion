﻿using System;
using System.Collections.Generic;
using System.IO;

namespace P33b3_LeerDeTxtSeparadoresCampos
{
    internal class Program
    {
        static string CARPETAORIGEN = "C:.\\Datos\\";
        static void Main(string[] args)
        {
            // Declaramos las variables que vamos a necesitar
            string[] vectorStringSeparado;
            List<string> listaDatos = new List<string>();

            // --------------- Obtener ruta y leer fichero ---------------
            // Creamos una variable tipo StreamReader y abrimos el fichero con la ruta
            StreamReader sr = File.OpenText(CARPETAORIGEN + "AlumNotas.txt");

            // Ahora recorremos el fichero y guardamos en una lista todos los datos
            while(!sr.EndOfStream)
            {
                // Guardamos la linea leida en un vector, separado por ';'
                vectorStringSeparado = sr.ReadLine().Split(';');
                // Ahora vamos a guardar los valores del vector en una lista, pero guardando como primer valor el nombre del alumno
                listaDatos.Add(vectorStringSeparado[1] + ";" + vectorStringSeparado[0] + ";" + vectorStringSeparado[2] + ";" + vectorStringSeparado[3] + ";" + vectorStringSeparado[4]);
            }

            // Una vez que ya tenemos todas las filas guardadas, vamos a cerrar el fichero
            sr.Close();

            // Ahora con hacer un Sort() ya tendremos los alumnos ordenados por el nombre
            listaDatos.Sort();

            //-------------- Mostramos los datos  -----------------
            Console.Clear();
            Console.WriteLine("     Id  Alumno\t\t\t\tProg    Ed      BD      Media");
            Console.WriteLine("     -----------------------------------------------------------------");

            // Declaramos las variables necesarias para las notas y la media
            double media;
            double notaProg, notaEd, notaBD;

            for (int i = 0; i < listaDatos.Count; i++)
            {
                // Guardamos en el vector, el valor de la lista (Sánchez Elegante, Álvaro;149;3,5;4,6;4,05) y separamos por ';'
                vectorStringSeparado = listaDatos[i].Split(';');
                // Guardamos los valores en las variables anteriormente declaradas
                notaProg = Convert.ToDouble(vectorStringSeparado[2]);
                notaEd = Convert.ToDouble(vectorStringSeparado[3]);
                notaBD = Convert.ToDouble(vectorStringSeparado[4]);
                media = (notaProg+notaEd+notaBD) / 3.0;
                // Imprimimos por pantalla
                Console.WriteLine("     {0} {1} {2}\t{3}\t{4}\t{5}", vectorStringSeparado[1], CuadrarTexto(vectorStringSeparado[0], 30, true), vectorStringSeparado[2], vectorStringSeparado[3], vectorStringSeparado[4], Math.Round(media,2));
            }

            // Para salir
            Pausa("salir");
        }

        /*
* Metodo para obtener la ruta de un fichero
* Entrada: Nada
* Salida: Un string con la ruta
*/
        private static string CapturaRuta()
        {
            string nombreFichero;
            bool existeRuta;
            do
            {
                Console.Write("\n\t¿nombre del fichero (sin extension)?: ");
                nombreFichero = Console.ReadLine();
                existeRuta = File.Exists(CARPETAORIGEN + nombreFichero + ".txt");
                if (!existeRuta)
                    Console.Write("\n\t** Error: El nombre introducido no existe **");
            } while (!existeRuta);

            return CARPETAORIGEN + nombreFichero + ".txt";
        }

        private static void Pausa(string txt)
        {
            Console.Write("\n\n\n\tPulse una tecla para {0}", txt);
            Console.ReadKey(true);
        }

        static string CuadrarTexto(string texto, int numeroCaracteres, bool conPuntos)
        {
            if (conPuntos)
                texto += "..............................................";
            else
                texto += "                                              ";

            return texto.Substring(0, numeroCaracteres);
        }
        /**
         * Se puede hacer lo mismo pero haciendo sobre carga de métodos
         */

    }
}
