﻿/* Leer Datos En Fichero.Txt Con Separadores de Campos: 
Realiza un programa que lea el fichero AlumNotas.txt que tienes en la carpeta Datos. 
Se sabe que cada fila contiene los campos: id, nombre, nota1, nota2 y nota3 separados por ‘;’. 
A partir de estas filas obtenidas, rellena una tabla de byte tabIds, otra de string tabAlums 
y otra de float tabNotas de tres columnas. 
A continuación presentar los datos con su cabecera
 Importante: 
 1.	Utilizar Ruta Relativa y mantener la estructura de archivos que se te entrega. 
 2.	El archivo debe estar abierto el menor tiempo posible.
 3.	Se puede utilizar una lista auxiliar pero tienes que actuar como si no se supiera 
    el número de alumnos que guarda el fichero.
 */
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace LeerDatosEnTxtSeparadoresCampos
{
    class Program
    {
        static string CARPETAORIGEN = "C:.\\Datos\\";
        static void Main(string[] args)
        {
            // Declaramos las variables que vamos a necesitar
            string ruta;
            string[] vectorStringSeparado;

            // --------------- Obtener ruta y leer fichero ---------------
            // Obtenemos la ruta usando CapturaRuta()
            ruta = CapturaRuta();
            // Creamos una variable tipo StreamReader y abrimos el fichero con la ruta
            StreamReader sr = File.OpenText(ruta);
            /*
             * El problema nos dice que tiene que abrir AlumNotas.txt, luego podemos hacer lo siguiente:
             * StreamReader sr = File.OpentText(CARPETAORIGEN + "AlumNotas.txt");
             */

            // ---------------------- Guardar datos en sus respectivas tablas ------------------------
            // Creamos una lista donde vamos a guardar todas las filas del fichero
            List<string> listaAuxiliar = new List<string>();

            // Ahora creamos un bucle para ir guardando las lineas del fichero
            while(!sr.EndOfStream)
            {
                listaAuxiliar.Add(sr.ReadLine());
            }

            // Una vez que ya tenemos todas las filas guardadas, vamos a cerrar el fichero
            sr.Close();


            // Ahora creamos la tabla tabIds de tipo byte que guardará los ids
            byte[] tabIds = new byte[listaAuxiliar.Count];

            // Otra de string tabAlums que guardará los nombres de los alumnos
            string[] tabAlums = new string[listaAuxiliar.Count];

            // Otra tabla de floats con 3 columnas, que guardará las notas
            float[,] tabNotas = new float[listaAuxiliar.Count, 3];

            /*
             * Una vez que tenemos inicializadas las tablas, vamos a tener que ir línea a línea leyendo los datos
             * y guardando en sus respectivas tablas
             */

            for (int i = 0; i < listaAuxiliar.Count; i++)
            {
                // EJ: 149;Sánchez Elegante, Álvaro;3,5;4,6;4,05
                // Tenemos que separar por el ';', para ello guardamos en un vector los valores separados por ';'
                vectorStringSeparado = listaAuxiliar[i].Split(';');

                // Ahora recorremos la tabla lineaSeparada para guardar los elementos en sus respectivas tablas
                tabIds[i] = Convert.ToByte(vectorStringSeparado[0]);
                tabAlums[i] = vectorStringSeparado[1];
                for (int j = 0; j < tabNotas.GetLength(1); j++)
                    tabNotas[i, j] = float.Parse(vectorStringSeparado[j + 2]);
            }


            //-------------- Mostramos los datos  -----------------
            Console.Clear();
            Console.WriteLine("     Id  Alumno\t\t\t\tProg    Ed      BD      Media");
            Console.WriteLine("     -----------------------------------------------------------------");

            // Una vez que ya tenemos las tablas llenas de valores, vamos a mostrar
            double media;

            // Version 1
            //for (int i = 0; i < listaAuxiliar.Count; i++)
            //{
            //    media = (tabNotas[i, 2] + tabNotas[i, 1] + tabNotas[i, 0]) / 3.0;
            //    Console.WriteLine("\n     {0} {1}\t {2}\t{3}\t{4}\t{5}", tabIds[i], CuadrarTexto(tabAlums[i], 30, true), tabNotas[i,0], tabNotas[i, 1], tabNotas[i, 2], Math.Round(media,2) );
            //}

            // Versión 2 -> Cuadrando todos los campos del registro
            //for (int i = 0; i < listaAuxiliar.Count; i++)
            //{
            //    media = (tabNotas[i, 2] + tabNotas[i, 1] + tabNotas[i, 0]) / 3.0;
            //    Console.WriteLine("     {0} {1} {2}{3}{4}{5}", tabIds[i], CuadrarTexto(tabAlums[i], 30, true), CuadrarTexto(tabNotas[i, 0].ToString(), 8, false), CuadrarTexto(tabNotas[i, 1].ToString(), 8, false), CuadrarTexto(tabNotas[i, 2].ToString(), 8, false), Math.Round(media, 2));

            //}

            //--- Versión 3: Sin cuadrar pero  usando SetCursorPosition
            int filaPantalla = 2;
            for (int i = 0; i < listaAuxiliar.Count; i++)
            {
                media = Math.Round((tabNotas[i, 0] + tabNotas[i, 1] + tabNotas[i, 2]) / 3, 2);
                Console.SetCursorPosition(5, filaPantalla);
                Console.Write("{0} {1}", tabIds[i], tabAlums[i]);
                Console.SetCursorPosition(40, filaPantalla);
                Console.Write("{0}\t{1}\t{2}\t{3}", tabNotas[i, 0], tabNotas[i, 1], tabNotas[i, 2], media);
                filaPantalla++;
            }

            // Para salir
            Pausa("salir");
        }
        /*
         * Metodo para obtener la ruta de un fichero
         * Entrada: Nada
         * Salida: Un string con la ruta
         */
        private static string CapturaRuta()
        {
            string nombreFichero;
            bool existeRuta;
            do
            {
                Console.Write("\n\t¿nombre del fichero (sin extension)?: ");
                nombreFichero = Console.ReadLine();
                existeRuta = File.Exists(CARPETAORIGEN + nombreFichero + ".txt");
                if (!existeRuta)
                    Console.Write("\n\t** Error: El nombre introducido no existe **");
            } while (!existeRuta);

            return CARPETAORIGEN + nombreFichero + ".txt";
        }

        private static void Pausa(string txt)
        {
            Console.Write("\n\n\n\tPulse una tecla para {0}",txt);
            Console.ReadKey(true);
        }

        static string CuadrarTexto(string texto, int numeroCaracteres, bool conPuntos)
        {
            if (conPuntos)
                texto += "..............................................";
            else
                texto += "                                              ";

            return texto.Substring(0,numeroCaracteres);
        }
        /**
         * Se puede hacer lo mismo pero haciendo sobre carga de métodos
         */


    }
}
