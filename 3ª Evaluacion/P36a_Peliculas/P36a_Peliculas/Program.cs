﻿/**
    Donde
     No es el número de orden en que está la película en el fichero
     Película es el nombre de la película. Como ves en la muestra, se le ha
    dado un ancho de 37 rellenando con puntos.
     Valor. Valoración que se le ha dado: Sólo la/s letra/s sin los paréntesis.
     Form.: Formato, en mayúsculas, en el que está grabada la película: AVI o
    MKV o MPG... (viene indicado por la extensión del fichero).
     Tamaño: En Gb con dos decimales.

    Después de presentar las películas, preguntará:
    ¿Nombre del fichero donde guardar los datos? (Intro = salir sin guardar)
    El programa actuará en consecuencia, es decir:
     Si el usuario sólo pulsa Intro, el programa concluirá sin guardar los
    datos.
     Si, por el contrario, el usuario introduce un nombre, construirá el fichero
    en la carpeta Datos, con el nombre indicado y la extensión “.TXT”. Si ya
    existía un fichero con ese nombre, sería reemplazado.
    En este caso guardará los registros con campos separados por ‘;’. No se
    guarda el número de orden.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace P36a_Peliculas
{
    internal class Program
    {
        const string RUTA = "C:\\zDatosPrueba\\Peliculas\\pelis.txt";
        static void Main(string[] args)
        {
            // Declaramos las variable StreamReader
            StreamReader sr = File.OpenText(RUTA);

            // Declaramos las variables que vamos a utilizar
            int contador=1;
            string[] vFila;
            string nombrePelicula, valorPelicula, formatoPelicula;
            float tamanyoPelicula;

            // Mostramos la cabecera
            Console.WriteLine("\n    Nº     Película\t\t\t\tValor\tForm.\tTamaño");
            Console.WriteLine("    ---    -------------------------------------------- -----  -----   ------");
            // Creamos un bucle para leer cada línea del fichero
            while (!sr.EndOfStream)
            {
                // Ejemplo línea: 23/12/2008  10:55     1.252.870.144 300 extendida(BB).avi
                vFila = sr.ReadLine().Split('(');
                // Aqui tendremos dos cosas
                // 1: 23/12/2008  10:55     1.252.870.144 300 extendida
                // 2: BB).avi
                nombrePelicula = vFila[0].Substring(36).Trim();
                tamanyoPelicula = Convert.ToSingle(vFila[0].Substring(22, 13)); // Ahora tenemos que convertirlo a GB
                tamanyoPelicula = tamanyoPelicula / 1000000;
                valorPelicula = 

                // Imprimimos por consola
                Console.WriteLine("    {0}     {1}\t{2}\t{3}\t{4}", 
                    contador, CuadraTexto(nombrePelicula, 37, true), valorPelicula, formatoPelicula, Math.Round(tamanyoPelicula, 2));
            }

            // Cerramos el StreamReader
            sr.Close();

            // Para salir
            Pausa("salir");
        }

        private static void Pausa(string txt)
        {
            Console.WriteLine("\n\n\tPulse una tecla para " + txt);
            Console.ReadKey(true);
        }

        private static string CuadraTexto(string texto, int longitud, bool conPuntos)
        {
            // Declaramos una variable auxialiar
            string aux;
            // Condición
            if (conPuntos)
                aux = "................................................................................";
            else
                aux = "                                                                                ";

            // Le añadimos a nuestro texto la variable auxiliar
            texto += aux;
            // Devolvemos
            return texto.Substring(0, longitud);
        }
    }
}
