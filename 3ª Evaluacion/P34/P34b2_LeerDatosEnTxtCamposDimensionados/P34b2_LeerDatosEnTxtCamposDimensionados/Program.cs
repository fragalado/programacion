﻿// Alumno: Francisco José, Gallego Dorado
using System;
using System.Collections.Generic;
using System.IO;

namespace P34b2_LeerDatosEnTxtCamposDimensionados
{
    internal class Program
    {
        static void Main(string[] args)
        {
            StreamReader sr = File.OpenText("C:\\zDatosPrueba\\fNotasCds.txt");

            // Cada fila contiene un registro en el que cada campo se ha dimensionado con el
            // siguiente número de caracteres:
            //          id: 3; Apellidos: 18; Nombre: 12; nota1: 3; nota2: 3; nota3: 3.

            // Vamos a guardar cada fila del archivo en una lista, para ello hacemos un bucle que finalizará
            // cuando llegue a la ultima linea
            List<string> listaAuxiliar = new List<string>();
            while (!sr.EndOfStream)
            {
                listaAuxiliar.Add(sr.ReadLine());
            }            
            // Cerramos el archivo, ya que no lo vamos a necesitar más
            sr.Close();

            // Declaramos las tablas
            byte[] tabIds = new byte[listaAuxiliar.Count];
            string[] tabAlums = new string[listaAuxiliar.Count];
            float[,] tabNotas = new float[listaAuxiliar.Count, 3];

            // Creamos dos tablas nuevas para los nombres de los alumnos
            string[] tablaApellidos = new string[listaAuxiliar.Count];
            string[] tablaNombres = new string[listaAuxiliar.Count];

            // Creamos un bucle para recorrer cada linea del archivo (utilizando la lista donde hemos guardado las lineas)
            for (int i = 0; i < listaAuxiliar.Count; i++)
            {
                tabIds[i] = Convert.ToByte(listaAuxiliar[i].Substring(0, 3));
                tablaApellidos[i] = listaAuxiliar[i].Substring(3, 18);
                tablaNombres[i] = listaAuxiliar[i].Substring(21, 12);
                tabAlums[i] = tablaApellidos[i].Trim() + ", " + tablaNombres[i].Trim();
                tabNotas[i, 0] = Convert.ToSingle(listaAuxiliar[i].Substring(33, 3));
                tabNotas[i, 1] = Convert.ToSingle(listaAuxiliar[i].Substring(36, 3));
                tabNotas[i, 2] = Convert.ToSingle(listaAuxiliar[i].Substring(39, 3));
            }

            // Mostramos los datos
            Console.WriteLine("     Id  Alumno\t\t\t\tProg    Ed      BD      Media");
            Console.WriteLine("     -----------------------------------------------------------------");

            double media;
            for (int i = 0; i < listaAuxiliar.Count; i++)
            {
                media = Math.Round((tabNotas[i, 0] + tabNotas[i, 1] + tabNotas[i, 2]) / 3.0, 2);
                Console.WriteLine("     {0} {1} {2}\t{3}\t{4}\t{5}", tabIds[i], CuadraTexto(tabAlums[i], 30, false), tabNotas[i, 0], tabNotas[i, 1], tabNotas[i, 2], media);
            }


            // Para salir
            Pausa("salir");
        }

        private static void Pausa(string txt)
        {
            Console.WriteLine("\n\n\tPulse una tecla para " + txt);
            Console.ReadKey(true);
        }

        private static string CuadraTexto(string texto, int longitud, bool conPuntos)
        {
            string aux;
            if (conPuntos)
                aux = "................................................................................";
            else
                aux = "                                                                                ";
            texto += aux;
            return texto.Substring(0, longitud);
        }

    }
}
