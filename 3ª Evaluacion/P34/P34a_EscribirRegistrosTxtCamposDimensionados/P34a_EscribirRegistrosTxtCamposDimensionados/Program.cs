﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace P34a_EscribirRegistrosTxtCamposDimensionados
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // Construye dos tablas tApell y tNomb con los datos.
            string[] tApell = { "Sánchez Elegante", "Arenas Mata", "García Solís", "Rodríguez Vázquez", "Hurtado Miranda", "Pinto Mirinda", "Barrios Garrobo", "Márquez Salazar", "Medina Gómez", "Alonso Pérez", "López Mora", "González Chaparro", "Ferrer Jiménez", "Morales Moncayo", "Fernández Perea", "Blanco Roldán", "Navarro Romero", "Aguilar Rubio", "Baena Fernández", "Barco Ramírez", "Delegado Rodríguez", "Duque Martínez" };
            string[] tNomb = { "Álvaro", "Daniel Luis", "Juan Manuel", "Agustín", "Fco. Javier", "José Manuel", "María", "Carlos", "José Carlos", "Juan Luis", "Daniel", "Carmen", "Jacobo", "Alejandro", "Francisco", "Alicia", "Francisco", "Ángela", "Constantino", "Mariló", "Rafaela", "Antonio" };

            // 1) Construimos una tabla de byte tIds con el id de cada alumno, que serán números al azar
            //    de tres cifras (no dos) SIN QUE EXISTA NINGUNO REPETIDO.
            int tamanyo = tApell.Count();
            byte[] tIds = new byte[tamanyo];








            // Para salir
            Pausa("salir");
        }
        
        private static void Pausa(string txt)
        {
            Console.WriteLine("\n\n\tPulse una tecla para {0}", txt);
            Console.ReadKey(true);
        }

        private static byte[] RellenaTablaByteConIdTresCifras(int tamanyo)
        {
            // Declaramos una variable de tipo Random para los numeros aleatorios
            Random azar = new Random();

            // Declaramos e inicializamos la tabla
            byte[] tabla = new byte[tamanyo];
            // Declaramos una lista en la que iremos metiendo numeros de tres cifras (hasta 255)
            List<byte> listaNumerosTresCifras = new List<byte>();

            // Recorremos la tabla y vamos añadiendo numeros de tres cifras aleatorios
            // Si el número ya se encuentra en la 'listaNumerosTresCifras', vamos a tener que buscar otro numero
            byte numero;
            for (int i = 0; i < tabla.Count(); i++)
            {
                numero = Convert.ToByte(azar.Next(100, 255 + 1));
                // Mientras el número aleatorio esté dentro de la lista vamos a segui buscando numeros
                while (!listaNumerosTresCifras.Contains(numero))
                {
                    numero = Convert.ToByte(azar.Next(100, 255 + 1));
                }
                // Una vez obtenemos un número que no está dentro de la lista, lo añadimos
                listaNumerosTresCifras.Add(numero);
            }


        }
    }
}
