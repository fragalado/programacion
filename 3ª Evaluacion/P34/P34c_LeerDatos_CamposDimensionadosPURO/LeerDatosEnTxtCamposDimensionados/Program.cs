﻿/*Leer Datos En Fichero.Txt Con Campos dimensionados puros (sin salto de línea)

Si te has dado cuenta, en la práctica anterior, los datos no estaban guardados realmente 
como campos dimensionados: se les había colocado un separador de registros (el salto de línea).
Realiza un programa que lea el fichero AlumNotas_CDpuro.txt  que tienes en la carpeta Datos. 
Este archivo contiene los datos que se guardaron en la práctica de escritura de datos 
con campos dimensionados P34a (pero sin saltos de líneas):
		Dato →			id	Apellidos  Nombre  Nota1  Nota2  Nota3
		Nº Caracteres	3	   18	     12	     3	    3	   3

A partir de estos datos, rellena las tres tablas siguientes:
  ●	Una de byte tabIds
  ●	otra de string tabAlums, con los «Apellidos, Nombre» de cada alumno, 
  ●	y otra de float tabNotas de tres columnas.

A continuación presentar los datos con su cabecera y la media de cada alumno
 Importante: 
 1.	Utilizar Ruta Relativa y mantener la estructura de archivos que se te entrega. 
 2.	El archivo debe estar abierto el menor tiempo posible.
 3.	Se puede utilizar una lista auxiliar pero tienes que actuar como si no se supiera 
    el número de alumnos que guarda el fichero.
 */
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

class Program
{
	static void Main(string[] args)
	{
		// Realiza un programa que lea el fichero AlumNotas_CDpuro.txt que tienes en la carpeta Datos.
		// Creamos una variable de tipo StreamReader para leer el fichero
		// Para ello abrimos primero el fichero
		StreamReader sr = File.OpenText(".\\Datos\\AlumNotas_CDpuro.txt");

		// Como todos los datos están en una misma línea, vamos a crear una variable string para almacenar los datos
		string lineaDeDatos = sr.ReadLine();

		// Cerramos ya el fichero
		sr.Close();

		// Vamos a guardar los datos en una lista
		List<string> listaAuxiliar = new List<string>();

		// Bucle for para guardar los datos correspondientes en la lista
		for (int i = 0; i < lineaDeDatos.Length; i+=42)
		{
			listaAuxiliar.Add(lineaDeDatos.Substring(i, 42));
        }

        // Una vez que ya tenemos la lista con los datos en sus respectivas registros
        // Creamos las tablas donde vamos a almacenar los datos
        // Declaramos las tablas
        byte[] tabIds = new byte[listaAuxiliar.Count];
        string[] tabAlums = new string[listaAuxiliar.Count];
        float[,] tabNotas = new float[listaAuxiliar.Count, 3];

        // Creamos dos tablas nuevas para los nombres de los alumnos, sin espacios
        string[] tablaApellidos = new string[listaAuxiliar.Count];
        string[] tablaNombres = new string[listaAuxiliar.Count];

        // Creamos un bucle para recorrer cada linea del archivo (utilizando la lista donde hemos guardado las lineas)
        for (int i = 0; i < listaAuxiliar.Count; i++)
        {
            tabIds[i] = Convert.ToByte(listaAuxiliar[i].Substring(0, 3));
            tablaApellidos[i] = listaAuxiliar[i].Substring(3, 18);
            tablaNombres[i] = listaAuxiliar[i].Substring(21, 12);
            tabAlums[i] = tablaApellidos[i].Trim() + ", " + tablaNombres[i].Trim();
            tabNotas[i, 0] = Convert.ToSingle(listaAuxiliar[i].Substring(33, 3));
            tabNotas[i, 1] = Convert.ToSingle(listaAuxiliar[i].Substring(36, 3));
            tabNotas[i, 2] = Convert.ToSingle(listaAuxiliar[i].Substring(39, 3));
        }

        // Mostramos los datos
        Console.WriteLine("     Id  Alumno\t\t\t\tProg    Ed      BD      Media");
        Console.WriteLine("     -----------------------------------------------------------------");

        double media;
        for (int i = 0; i < listaAuxiliar.Count; i++)
        {
            media = Math.Round((tabNotas[i, 0] + tabNotas[i, 1] + tabNotas[i, 2]) / 3.0, 2);
            Console.WriteLine("     {0} {1} {2}\t{3}\t{4}\t{5}", tabIds[i], CuadraTexto(tabAlums[i], 30, false), tabNotas[i, 0], tabNotas[i, 1], tabNotas[i, 2], media);
        }


        // Para salir
        Pausa("salir");
    }

    private static void Pausa(string txt)
    {
        Console.WriteLine("\n\n\tPulse una tecla para " + txt);
        Console.ReadKey(true);
    }

    private static string CuadraTexto(string texto, int longitud, bool conPuntos)
    {
        string aux;
        if (conPuntos)
            aux = "................................................................................";
        else
            aux = "                                                                                ";
        texto += aux;
        return texto.Substring(0, longitud);
    }


}
