﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace P35b_CamposSeparadosACamposDimensionados
{
    internal class Program
    {
        const string RUTA = "C:\\zDatosPrueba\\Pacientes\\PacientesPeso_CS.txt";
        const string RUTA2 = "C:\\zDatosPrueba\\Pacientes\\PacientesPeso_CD.txt";
        static void Main(string[] args)
        {

            // Declaramos la variable de tipo StreamReader para leer el fichero
            StreamReader sr = new StreamReader(RUTA, Encoding.Default);
            // Declaramos la variable de tipo StreamWriter para escribr en el fichero
            StreamWriter sw = File.CreateText(RUTA2);

            // Declaramos una tabla/vector de tipo string, donde guardaremos los campos de cada fila del fichero
            string[] vFila;

            // Un bucle para leer cada línea del fichero
            while (!sr.EndOfStream)
            {
                // Ejemplo de fila: 168$Sánchez Elegante, Álvaro$648095635$19990507$162$73,5
                vFila = sr.ReadLine().Split('$');

                // Escribimos en el fichero PacientesPeso_CD
                // ID
                sw.Write(CuadraTexto(vFila[0], 3, false));
                // Nombre y apellidos
                sw.Write(CuadraTexto(vFila[1], 28, false));
                // Movil
                sw.Write(CuadraTexto(vFila[2], 9, false));
                // Fecha Nacimiento
                sw.Write(CuadraTexto(vFila[3], 8, false));
                // Altura
                sw.Write(CuadraTexto(vFila[4], 3, false));
                // Peso
                sw.WriteLine(CuadraTexto(vFila[5], 5, false));
            }

            // Cerramos los StreamReader y StreamWriter
            sr.Close();
            sw.Close();


            // 2) A continuación leerá los datos de este último fichero
            //    y los presentará en pantalla con su cabecera.
            StreamReader sr2 = File.OpenText(RUTA2);

            // Mostramos los datos
            Console.WriteLine("     Id  Apellidos, Nombre\t\tMovil\t\tFecha Nac.\tAlt.\tPeso");
            Console.WriteLine("     --------------------------------   ---------       ----------      ---     -----");

            // Un bucle para leer cada línea del fichero
            byte id;
            string captura, nombreAlumno, movil, fechaNac;
            int altura, contador=0;
            float peso, pesoMedia=0;
            while(!sr2.EndOfStream)
            {
                // Guardamos en una variable la linea
                captura = sr2.ReadLine();
                // Igualamos cada variabel a su valor y convertimos
                id = Convert.ToByte(captura.Substring(0, 3));
                nombreAlumno = captura.Substring(3, 28);
                movil = captura.Substring(31, 9);
                fechaNac = captura.Substring(40, 8);
                altura = Convert.ToInt32(captura.Substring(48, 3));
                peso = Convert.ToSingle(captura.Substring(51, 5));

                // Añadimos el peso a pesoMedia
                pesoMedia += peso;

                // Mostramos por pantalla los datos
                Console.WriteLine("     {0} {1}\t{2}\t{3}\t{4}\t{5}", 
                    CuadraTexto(id.ToString(), 3), CuadraTexto(nombreAlumno, 28, false), movil, CreaFechaNacimiento(fechaNac), altura, CuadraTexto(peso.ToString(), 5));

                // Contamos
                contador++;
            }

            // Imprimimos el peso medio de todos los pacientes
            Console.WriteLine("\n\n\tPeso medio de todos los pacientes es: {0}", Math.Round(pesoMedia / contador, 1));


            sr2.Close();
            // Para salir
            Pausa("salir");
        }

        private static void Pausa(string txt)
        {
            Console.Write("\n\n\tPulse una tecla para " + txt);
            Console.ReadKey(true);
        }

        private static string CuadraTexto(string texto, int longitud, bool conPuntos)
        {
            // Variable auxiliar
            string auxiliar;

            // Condicional
            if (conPuntos)
                auxiliar = "....................................................................................";
            else
                auxiliar = "                                                                                    ";

            // Añadimos al valor del texto la variable auxiliar
            texto += auxiliar;
            
            // Devolvemos un substring del texto con la longitud
            return texto.Substring(0, longitud);
        }

        private static string CuadraTexto(string texto, int tamanyo)
        {
            string auxiliar = " ";
            if (texto.Length < tamanyo)
            {
                auxiliar += texto;
            }
            else
                auxiliar = texto;

            return auxiliar;
        }

        private static string CreaFechaNacimiento(string fechaSinFormato)
        {
            // Declaramos las variables que vamos a necesitar
            string dia, mes, anyo;

            // Recorremos la fecha sin formato y le damos los valores a las variables
            // Ejemplo de fecha sin formato: 19990507
            // AAAAMMDD
            anyo = fechaSinFormato.Substring(0,4);
            mes = fechaSinFormato.Substring(4, 2);
            dia = fechaSinFormato.Substring(6, 2);

            // Devolvemos la fecha con formato Español
            return String.Format("{0}/{1}/{2}", dia, mes,anyo);
        }

    }
}
