﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P35a_CamposDimensionadosACamposSeparados
{
    internal class Program
    {
        const string RUTA = "C:\\zDatosPrueba\\AlumnosNotas\\AlumNotas_CD.txt";
        const string RUTA2 = "C:\\zDatosPrueba\\AlumnosNotas\\AlumNotas_CS.txt";
        static void Main(string[] args)
        {
            // Creamos las variables de tipo StreamReader y StreamWriter
            StreamReader sr = File.OpenText(RUTA);
            StreamWriter sw = File.CreateText(RUTA2);

            // Declaramos las variables que vamos a utilizar
            string captura;
            byte id;
            string nombre;
            float nota1, nota2, nota3;
            // Bucle para recorrer cada línea del fichero
            while (!sr.EndOfStream)
            {
                captura = sr.ReadLine();
                // Tendremos algo tal que asi:
                // '149Sánchez Elegante, Álvaro  3,54,67,0'
                id = Convert.ToByte(captura.Substring(0, 3));
                nombre = captura.Substring(3, 26);
                nota1 = Convert.ToSingle(captura.Substring(29, 3));
                nota2 = Convert.ToSingle(captura.Substring(32, 3));
                nota3 = Convert.ToSingle(captura.Substring(35, 3));

                // Una vez que ya tenemos todos los datos guardados, vamos a escribirlas en el fichero AlumNotas_CS
                // Escribiremos los campos separados por ';'
                sw.Write(id+ ";");
                sw.Write(nombre.Trim() + ";");
                sw.Write(nota1 + ";");
                sw.Write(nota2 + ";");
                sw.WriteLine(nota3);
            }

            // Cerramos el StreamReader y el StreamWriter
            sr.Close();
            sw.Close();

            // Mostramos los datos
            Console.WriteLine("     Id  Alumno\t\t\t\tProg    Ed      BD      Media");
            Console.WriteLine("     -----------------------------------------------------------------");
            
            // Declaramos un StreamReader para leer el fichero
            StreamReader sr2 = File.OpenText(RUTA2);

            string[] tabla;
            double media;
            while(!sr2.EndOfStream)
            {
                // Vamos a separar por ';' cada campo y lo guardamos en una tabla
                tabla = sr2.ReadLine().Split(';');

                // Ahora ya podemos mostrar los datos
                nota1 = Convert.ToSingle(tabla[2]);
                nota2 = Convert.ToSingle(tabla[3]);
                nota3 = Convert.ToSingle(tabla[4]);
                media = Math.Round(Convert.ToSingle(nota1 + nota2 + nota3) / 3.0, 2);
                Console.WriteLine("     {0}  {1}\t{2}\t{3}\t{4}\t{5}", 
                        Convert.ToByte(tabla[0]), CuadraTexto(tabla[1], 28, false), nota1, nota2, nota3, media);
            }

            // Para salir
            Pausa("salir");
        }

        private static void Pausa(string txt)
        {
            Console.Write("\n\n\tPulse una tecla para {0}",txt);
            Console.ReadKey(true);
        }

        private static string CuadraTexto(string texto, int longitud,bool conPuntos)
        {
            string cadena;
            if (conPuntos)
                cadena = "..............................................................................";
            else
                cadena = "                                                                              ";

            texto += cadena;

            return texto.Substring(0, longitud);
        }
    }
}
