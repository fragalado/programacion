﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

class Program
{

	const string RUTA = ".\\Datos\\Pacientes.txt";
	static void Main(string[] args)
	{
		// Creamos un StreamReader para leer de Pacientes.txt
		StreamReader sr =new StreamReader(RUTA, Encoding.Default);

		// Leemos los datos y guardamos en una lista auxiliar
		// Creamos la lista auxiliar
		List<string> listAuxiliar = new List<string>();
		List<string> listOrdenada = new List<string>(listAuxiliar);

        // Bucle while para leer cada linea del fichero y guardarla en la lista auxiliar
        while (!sr.EndOfStream)
			listAuxiliar.Add(sr.ReadLine());

		// Cerramos el StreamReader
		sr.Close();

		// Declaramos la tabla auxiliar
		int tamanyo = listAuxiliar.Count();
		string[] vectorAuxiliar;

		// Mostramos el Menu y guardamos el valor devuelto en una variable opcion
		int opcion = Menu();
		float pesoMedio = 0;
		string registro, nombre, apellidos;

		for (int i = 0; i < tamanyo; i++)
			listOrdenada.Add(listAuxiliar[i]);

        // Si la opcion es distinta de 0 vamos a hacer las operaciones
        while (opcion != 0)
		{
            // Segun por lo que queramos ordenar vamos a duplicar el campo y ponerlo en la primera posicion de la lista
            // de este modo siempre vamos a tener los campos empezado por la segunda posicion.
            for (int i = 0; i < tamanyo; i++)
            {
                // EJ: 143;Malaventura Zabala, Luis;636798977;19790306;155;68,6
                registro = listAuxiliar[i];
                vectorAuxiliar = registro.Split(';');

                switch (opcion)
                {
                    case 7: // Si estamos en el caso 7 vamos a tener que añadir algo delante para que no de error
                    case 1:
                        // ORDENAR POR ID
                        registro = CuadraNumero(vectorAuxiliar[0], 3) + ";" + registro;
                        break;
                    case 2:
                        // ORDENAR POR APELLIDOS, NOMBRE
                        registro = vectorAuxiliar[1] + ";" + registro;
                        break;
                    case 3:
                        // ORDENAR POR EDAD
                        registro = CuadraNumero(CalcularEdad(vectorAuxiliar[3]), 3) + ";" + registro;
                        break;
                    case 4:
                        // ORDENAR POR ALTURA
                        registro = CuadraNumero(vectorAuxiliar[4], 3) + ";" + registro;
                        break;
                    case 5:
                        // ORDENAR POR PESO
                        registro = CuadraNumero(Convert.ToSingle(vectorAuxiliar[5]), 5) + ";" + registro;
                        break;
                    case 6:
                        // ORDENADOR POR NOMBRE APELLIDOS
                        nombre = vectorAuxiliar[1].Split(',')[1].Trim();
                        apellidos = vectorAuxiliar[1].Split(',')[0].Trim();
                        registro = nombre + " " + apellidos + ";" + registro;
                        break;
                }

                listOrdenada[i] = registro;
            }

            // Ahora que ya tenemos la lista ordenada por el campo introducido
            // Vamos a mostrar por pantalla
            // Primero borramos la pantalla
            Console.Clear();
            // Cabecera
            Console.WriteLine("     Id  Paciente\t\t\tMovil\t\tFecha Nac.\tAlt.\tPeso");
            Console.WriteLine("     --- ----------------------------   ---------       ----------      ---     -----");

            // Imprimimos
            // Si la opción es distinta de 7 vamos a ordenar la lista
            if (opcion != 7)
                listOrdenada.Sort();
            for (int i = 0; i < tamanyo; i++)
            {
                vectorAuxiliar = listOrdenada[i].Split(';');
                Console.WriteLine("     {0} {1}\t{2}\t{3}\t{4}\t{5}", CuadraNumero(vectorAuxiliar[1], 3),
                                                                        CuadraTexto(vectorAuxiliar[2], 28, false),
                                                                        vectorAuxiliar[3],
                                                                        FormateoFecha(vectorAuxiliar[4]),
                                                                        vectorAuxiliar[5],
                                                                        CuadraNumero(vectorAuxiliar[6], 5));
            }

			// Volvemos a mostrar el menu cuando pulsemos una tecla
			Console.Write("\n\n\tPulse una tecla para volver al menu");
			Console.ReadKey(true);
			// Limpiamos la consola
			Console.Clear();
            opcion = Menu();
        }



        // Para salir
        Console.WriteLine("\n\n\t Pulsa una tecla para salir");
		Console.ReadKey();
	}


	static int Menu()
    {
        int opcion = 0;
		Console.WriteLine("\n\n\t\t╔═════════════════════════╗");
		Console.WriteLine("\t\t║   Ordenar datos por...  ║");
		Console.WriteLine("\t\t╠═════════════════════════╣");
		Console.WriteLine("\t\t║   1) id                 ║");
		Console.WriteLine("\t\t║                         ║");
		Console.WriteLine("\t\t║   2) Apellidos, Nombre  ║");
		Console.WriteLine("\t\t║                         ║");
		Console.WriteLine("\t\t║   3) Edad (creciente)   ║");
		Console.WriteLine("\t\t║                         ║");
		Console.WriteLine("\t\t║   4) Altura             ║");
		Console.WriteLine("\t\t║                         ║");
		Console.WriteLine("\t\t║   5) Peso               ║");
		Console.WriteLine("\t\t║                         ║");
		Console.WriteLine("\t\t║   6) Nombre Apellidos   ║");
		Console.WriteLine("\t\t║                         ║");
		Console.WriteLine("\t\t║   7) Sin ordenar        ║");
		Console.WriteLine("\t\t║_________________________║");
		Console.WriteLine("\t\t║                         ║");
		Console.WriteLine("\t\t║      0)  Salir          ║");
		Console.WriteLine("\t\t╚═════════════════════════╝");


		// Utilizamos CapturaEntero para capturar la opcion
		opcion = CapturaEntero("Introduzca una opcion", 0, 7);
		return opcion;
	}

	static int CapturaEntero(string txt, int min, int max)
	{
		int num;
		bool ok;
		do
		{
			Console.Write("\n\t{0} [{1}..{2}]: ", txt, min, max);
			ok = Int32.TryParse(Console.ReadLine(), out num);

			if (!ok)
				Console.WriteLine("\n\t** Error: El valor introducido no es un numero **");
			else if(num < min || num > max)
                Console.WriteLine("\n\t** Error: El valor introducido no está dentro del rango **");
        } while (!ok || num < min || num > max);

		return num;
	}

	static string CuadraTexto(string txt, int longitud, bool conPuntos)
	{
		string aux;
		if (conPuntos)
			aux = "........................................................................................";
		else
			aux = "                                                                                        ";
		// Le añadimos la variable auxiliar al texto
		txt += aux;
		// Devolvemos con la longitud deseada
		return txt.Substring(0, longitud);
	}

	static string CuadraNumero(string txt, int longitud)
	{
		string aux = "                       " + txt;
		return aux.Substring(aux.Length - longitud);
	}

    static string CuadraNumero(float num, int longitud)
    {
        string aux = "                                                    ";
        aux += num.ToString("0.0");
        return aux.Substring(aux.Length - longitud);
    }

    static string CalcularEdad(string fechaSinFormato)
	{
        // EJ: 19790306
        int dia = Convert.ToInt32(fechaSinFormato.Substring(fechaSinFormato.Length - 2));
        int mes = Convert.ToInt32(fechaSinFormato.Substring(4, 2));
        int anyo = Convert.ToInt32(fechaSinFormato.Substring(0, 4));

		DateTime nacimiento = new DateTime(anyo, mes, dia);
		int edad = DateTime.Today.AddTicks(-nacimiento.Ticks).Year - 1;
		return edad.ToString();
	}

	static string FormateoFecha(string fechaSinFormato)
	{
		// EJ: 19790306
		string dia = fechaSinFormato.Substring(fechaSinFormato.Length - 2);
        string mes = fechaSinFormato.Substring(4, 2);
        string anyo = fechaSinFormato.Substring(0, 4);

        return dia + "/" + mes + "/" + anyo;
	}

}

