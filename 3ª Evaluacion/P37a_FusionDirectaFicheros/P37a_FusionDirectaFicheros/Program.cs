﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace P37a_FusionDirectaFicheros
{
    internal class Program
    {
        const string RUTA = ".\\Datos\\n1.txt";
        const string RUTA2 = ".\\Datos\\n2.txt";
        const string RUTA3 = ".\\Datos\\n3.txt";

        static void Main(string[] args)
        {
            // Para leer n1 y n2
            StreamReader sr1 = File.OpenText(RUTA);
            StreamReader sr2 = File.OpenText(RUTA2);

            // Para escribir en n3
            StreamWriter sw = File.CreateText(RUTA3);

            int n1 = 0, n2 = 0;
            bool escritoN1 = true, escritoN2=true, finalN1=false, finalN2=false;

            while(!sr1.EndOfStream || !sr2.EndOfStream)
            {
                // Si no he llegado al final de sr1 y acabo de escribir un dato de N1, leo el siguiente
                if (!sr1.EndOfStream && escritoN1)
                {
                    n1 = Convert.ToInt32(sr1.ReadLine());
                }

                // Si no he llegado al final de sr2 y acabo de escribir un dato de N2, leo el siguiente
                if (!sr2.EndOfStream && escritoN2)
                {
                    n2 = Convert.ToInt32(sr2.ReadLine());
                }

                // Comparamos n1 con n2 para averiguar cual de los dos es menor y que n1 no haya llegado al final
                if (n1 < n2 && !finalN1)
                {
                    sw.WriteLine(n1);
                    escritoN1 = true;
                    escritoN2 = false;

                    if (sr1.EndOfStream)
                        finalN1 = true;
                }

                // Comparamos n2 con n1 para averiguar cual de los dos es menor y que n2 no haya llegado al final
                if (n2 < n1 && !finalN2)
                {
                    sw.WriteLine(n2);
                    escritoN2 = true;
                    escritoN1 = false;

                    if (sr2.EndOfStream)
                        finalN2 = true;
                }

                // Cuando es el final de n1, no entra en el if, por tanto, no escribe ese numero en n3
                if (finalN1)
                {
                    sw.WriteLine(n2);
                    escritoN1 = false;
                    escritoN2 = true;
                }

                // Cuando es el final de n2, no entra en el if, por tanto, no escribe ese numero en n3
                if (finalN2)
                {
                    sw.WriteLine(n1);
                    escritoN1 = true;
                    escritoN2 = false;
                }
            }

            sr1.Close();
            sr2.Close();
            sw.Close();

            // Para salir
            Pausa("salir");
        }

        static void Pausa(string txt)
        {
            Console.WriteLine("\n\n\n\tPulse una tecla para "+txt);
            Console.ReadKey(true);
        }
    }
}
