﻿using System;
using System.IO;

namespace P32a_LeerFicheroTXT
{
    internal class Program
    {
        // Declaramos una constante que contendrá la ruta
        static string CARPETAORIGEN = "C:\\zDatosPrueba\\";
        static void Main(string[] args)
        {
            //// Declaramos las variables que vamos a necesitar
            //string ruta, linea, parrafoMasLargo;
            //int numeroCaracteresMaximo, cont = 0;
            //// Obtenemos la ruta usando CapturaRuta
            //ruta = CapturaRuta();

            //Pausa("mostrar la versión 1");
            //Console.Clear();
            //// Versión 1:
            //// Hacer que vaya leyendo las líneas del fichero indicado y las muestre en pantalla
            //StreamReader sr = File.OpenText(ruta);

            //while(!sr.EndOfStream)
            //{
            //    linea= sr.ReadLine();
            //    Console.WriteLine(linea);
            //}

            //// Cerramos el StreamReader
            //sr.Close();

            //Pausa("mostrar la versión 2");
            //Console.Clear();
            //// Versión 2
            //// Creamos un StreamReader
            //StreamReader sr2 = File.OpenText(ruta);

            //// Instalar en una carpeta Datos dentro del directorio de trabajo por defecto y colocar en
            //// dicha carpeta los archivos a leer
            //CARPETAORIGEN = "C:.\\Datos\\";

            //// CapturaRuta comprobará la existencia del fichero, mediante ruta relativa
            //ruta = CapturaRuta();

            //// Después de mostrar el fichero, presentará cuántos párrafos (líneas) tiene y repetirá el 
            //// párrafo más largo indicando el número de carácteres que tiene
            //while (!sr2.EndOfStream)
            //{
            //    linea = sr2.ReadLine();
            //    Console.WriteLine(linea);
            //    cont++; // De esta forma contamos cuántas líneas tiene el fichero
            //}

            //// Cerramos el StreamReader
            //sr2.Close();


            // ------------------------------------------- PRACTICAR PARA EXAMEM ---------------------------------------------


            //// Pedimos el nombre del fichero usando CapturaRuta
            //string rutaFichero = CapturaRuta();

            //// El programa va a leer linea a linea el fichero y va a mostrarlas por pantalla
            //// Creamos un StreamReader para leer el fichero
            //StreamReader sr = File.OpenText(rutaFichero);

            //// Leemos e imprimimos
            //while(!sr.EndOfStream)
            //{
            //    Console.WriteLine(sr.ReadLine());
            //}

            //// Cerramos el StreamReader
            //sr.Close();


            // Version 2

            // Usando ruta relativa
            CARPETAORIGEN = ".\\Datos\\";

            // Capturamos la ruta del fichero usando CapturaRuta
            string ruta = CapturaRuta();

            // Creamos un StreamReader para leer el fichero
            StreamReader sr = File.OpenText(ruta);
            string linea, lineaMasLarga;
            int numLineas = 0;

            // Imprimimos la primera linea
            linea = sr.ReadLine();
            Console.WriteLine(linea);
            lineaMasLarga = linea;


            while (!sr.EndOfStream)
            {
                linea = sr.ReadLine();
                // Imprimimos
                Console.WriteLine(linea);

                // Comprobamos si la linea es mar larga que la guardada
                if (linea.Length > lineaMasLarga.Length)
                    lineaMasLarga = linea;

                // Contamos el numero de lineas
                numLineas++;
            }

            // Cerramos el StreamReader
            sr.Close();

            // Mostramos el numero de lineas y la linea mas larga
            Console.WriteLine("\n\tNumero de lineas del fichero: " + numLineas);
            Console.WriteLine("\n\tLinea mas larga: {0}; Numero de caracteres : {1}", lineaMasLarga, lineaMasLarga.Length);

            // Para salir
            Pausa("salir");
        }

        //private static string CapturaRuta()
        //{
        //    string nombreFichero;
        //    bool existeRuta;
        //    do
        //    {
        //        Console.Write("\n\t¿nombre del fichero (sin extension)?: ");
        //        nombreFichero = Console.ReadLine();
        //        existeRuta = File.Exists(CARPETAORIGEN + nombreFichero + ".txt");
        //        if (!existeRuta)
        //            Console.Write("\n\t** Error: El nombre introducido no existe **");
        //    } while (!existeRuta);

        //    return CARPETAORIGEN + nombreFichero + ".txt";
        //}

        // ------------------------------------------- PRACTICAR PARA EXAMEM ---------------------------------------------
        
        private static string CapturaRuta()
        {
            // Pedimos al usuario un nombre de un fichero sin extensión
            Console.Write("\n\t¿Nombre del fichero? [sin extension]: ");
            string nombreFichero = Console.ReadLine();

            // Comprobamos si existe el fichero en la ruta
            // Mientras no exista el nombre seguiremos pidiendolo
            while (!File.Exists(CARPETAORIGEN + "" + nombreFichero + ".txt"))
            {
                Console.WriteLine("\n\t** Error: El fichero no existe **");
                Console.Write("\n\t¿Nombre del fichero? [sin extension]: ");
                nombreFichero = Console.ReadLine();
            }

            return CARPETAORIGEN + "" + nombreFichero + ".txt";
        }

        private static void Pausa(string txt)
        {
            Console.Write("\n\n\tPulse una tecla para {0}",txt);
            Console.ReadKey(true);
        }
    }
}
