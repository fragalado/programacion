﻿// Alumno: Gallego Dorado, Francisco José
using System;
using System.Collections.Generic;
using System.IO;

namespace P31c_GuardaPrimos
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //// Guardamos en una variable el límite superior
            //int top = CapturaEntero("¿Cuál es el límite superior?", 10, 10000);
            //// Guardamos en una lista todos los números anteriores a limiteSup, usando ListaDePrimos
            //List<int> lista = ListaDePrimos(top);
            //// Guardará los valores de esa lista, separados por el carácter ';' en un fichero de nombre primos.txt
            //StreamWriter sw = File.CreateText(@"C:\zDatosPrueba\P31c\primos.txt");

            //sw.Write(lista[0]);
            //for (int i = 1; i < lista.Count; i++)
            //{
            //    sw.Write(";"+ lista[i]);
            //}

            //// Cerramos el StreamWriter
            //sw.Close();


            // ------------------------------------- PRACTICAR PARA EXAMEN ---------------------------------------


            // Preguntamos el limite Superior
            int top = CapturaEntero("Cual es el limite superior", 10, 10000);

            // Creamos una lista y llamamos al metodo ListaDePrimos
            List<int> listPrimos = ListaDePrimos(top);

            // Creamos una variable StreamWriter para escribir en el fichero primos.txt
            StreamWriter sw = File.CreateText("C:\\zDatosPrueba\\p31c.txt");

            // Bucle para escribir todos los valores de la lista en el fichero en 5 columnas bien alineadas
            // Cabecera
            sw.WriteLine("Numeros primos menores de " + top);
            for (int i = 0; i < listPrimos.Count; i++)
            {
                // Hacemos un salto de linea cuando tengamos 5 columnas
                if(i % 5 == 0)
                {
                    sw.WriteLine();
                }
                sw.Write("\t{0}", CuadraTexto(listPrimos[i].ToString(),top.ToString().Length));
            }

            // Cerramos la variable StreamWriter
            sw.Close();


            // Para salir
            Console.Write("\n\n\n\tPulse una tecla para salir");
            Console.ReadKey();
        }

        //private static bool EsPrimo(int num)
        //{
        //    // Comprobamos si el numero es 0 o 1, si es devolveremos que no es primo
        //    if (num == 0 || num == 1)
        //        return false;

        //    for (int i = 2; i <= num/2; i++)
        //    {
        //        if (num % i == 0)
        //            return false; // Si llega aquí será porque el número NO es primo
        //    }

        //    // Si llega hasta aquí será porque el número ES primo
        //    return true;
        //}

        //private static List<int> ListaDePrimos(int limiteSup)
        //{
        //    List<int> lista = new List<int>();

        //    // Creamos un bucle para añadir a la lista los números primos anteriores a limiteSup
        //    for (int i = 0; i < limiteSup; i++)
        //    {
        //        if(EsPrimo(i)) // Si el número ES primo va a entrar y se va a añadir a la lista
        //            lista.Add(i);
        //    }

        //    // Devolvemos la lista con todos los números primos anteriores a limiteSup
        //    return lista;
        //}

        private static int CapturaEntero(string txt, int min, int max)
        {
            int num;
            bool ok;

            do
            {
                Console.Write("\n\t{0} [{1}..{2}]: ", txt, min, max);
                ok = Int32.TryParse(Console.ReadLine(), out num);

                if (!ok)
                    Console.Write("\n\t** Error: El valor introducido no es un entero **");
                else if (num < min || num > max)
                    Console.Write("\n\t** Error: El valor introducido no está dentro del rango **");

            } while (!ok || num < min || num > max);

            return num;
        }



        // ------------------------------------- PRACTICAR PARA EXAMEN ---------------------------------------

        private static bool EsPrimo(int num)
        {
            // Comprobamos si num es primo o no
            // Si es primo devolvemos TRUE
            // Si no es primo devolvemos FALSE

            // Comprobamos si el numero es 0 o 1, si lo es devolvemos false
            if (num == 0 || num == 1)
                return false;

            for (int i = 2; i <= num / 2; i++)
            {
                if(num % i == 0)
                    return false;
            }

            // Si llega aqui será porque el numero es primo
            return true;
        }

        /**
         * Construye el método ListaDePrimos que...
            • Recibe: un entero limiteSup.
            • Hace: Construye una lista con todos los números primos anteriores a limiteSup.
            • Devuelve: la lista construida.
         */
        private static List<int> ListaDePrimos(int limiteSup)
        {
            // Construimos una lista de enteros
            List<int> lista = new List<int>();

            for (int i = 0; i < limiteSup; i++)
            {
                if(EsPrimo(i))
                    lista.Add(i); // Si entra aqui es porque el numero es primo
            }

            // Devolvemos la lista
            return lista;
        }

        private static string CuadraTexto(string txt, int longitud)
        {
            string aux = "                                      ";
            aux += txt;
            // Devolvemos el string
            return aux.Substring(aux.Length - 1 - longitud);
        }


    }
}
