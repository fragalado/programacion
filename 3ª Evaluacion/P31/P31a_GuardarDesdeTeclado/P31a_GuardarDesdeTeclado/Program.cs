﻿// Alumno: Gallego Dorado,  Francisco José

// Hacer un programa que vaya leyendo las frases que el usuario
// teclea y las guarde en un fichero de texto. Terminará cuando la
// frase introducida sea "fin"(esa frase no deberá guardarse en el
// fichero).


using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace P31a_GuardarDesdeTeclado
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // Declaramos un stream de escritura
            StreamWriter sw;

            // ------------------------------------- Distintas formas ------------------------------------------
            // Construimos con el nombre del fichero elegido por nosotros
            //sw = File.CreateText(@"C:\zDatosPrueba\frases\frases1.txt");

            // Construimos con el nombre del fichero elegido por nosotros, con AppendText
            //sw = File.AppendText(@"C:\zDatosPrueba\frases\frases2.txt");

            // Construimos con el nombre del fichero elegido por el usuario
            //Console.Write("\n\t¿nombre del fichero de texto?: ");
            //sw = File.CreateText(String.Format(@"C:\zDatosPrueba\frases\{0}.txt", Console.ReadLine()));

            // Versión construyéndolo con uno de sus constructores
            sw = new StreamWriter(@"C:\zDatosPrueba\frases\frases1.txt", false, Encoding.Unicode); // Esto hace lo mismo que los de arriba
            // -------------------------------------------------------------------------------------------------


            // Pedimos el texto y guardamos en una variable
            Console.Write("\n\tComienza a escribir el texto. Escribe fin para salir:\n\t");
            string frase = Console.ReadLine();

            // Creamos un bucle que entrará si frase != "fin"
            while (frase != "fin")
            {
                // Escribimos la frase en el fichero
                sw.WriteLine(frase);

                // Volvemos a pedir otra frase
                Console.Write("\n\tComienza a escribir el texto. Escribe fin para salir:\n\t");
                frase = Console.ReadLine();
            }

            // Cerramos nuestro fichero asociado a sw
            sw.Close();



            // ------------------------------------- Avanzado -----------------------------------------

            // Pedimos al usuario que introduzca el nombre del archivo donde se van a guardar las frases
            Console.Write("\n\t¿nombre del archivo? (sin formato): ");

            // Construimos usando un constructor
            sw = new StreamWriter(String.Format(@"C:\zDatosPrueba\frases\{0}.txt", Console.ReadLine()), false, Encoding.Unicode);
            // sw = new StreamWriter(@"C:\zDatosPrueba\frases\"+Console.ReadLine()+".txt", false, Encoding.Unicode); // OTRA FORMA


            // Pedimos el texto y guardamos en una variable
            Console.Write("\n\tComienza a escribir el texto. Escribe fin para salir:\n\t");
            frase = Console.ReadLine();

            // Creamos un bucle que entrará si frase != "fin"
            while (frase != "fin")
            {
                // Escribimos la frase en el fichero
                sw.WriteLine(frase);

                // Volvemos a pedir otra frase
                Console.Write("\n\tComienza a escribir el texto. Escribe fin para salir:\n\t");
                frase = Console.ReadLine();
            }

            // Cerramos nuestro fichero asociado a sw
            sw.Close();


            // --------------------------------- PRACTICAR EXAMEN --------------------------------------

            //// Declaramos una variable StreamWriter para escribir en un fichero
            //StreamWriter sw = File.CreateText("C:\\zDatosPrueba\\p11a.txt");

            //// Ahora vamos a pedir frases al usuario y la vamos a ir en el fichero
            //// Vamos a dejar de pedir cuando el usuario introduzca "fin" (esta frase no deberá guardarse en el fichero), luego vamos a hacer un bucle para controlarlo
            //// Pedimos la primera frase
            //Console.Write("\n\n\tIntroduzca una frase: ");
            //string fraseUsuario = Console.ReadLine();

            //while (!fraseUsuario.Equals("fin"))
            //{
            //    // Imprimimos en el fichero
            //    sw.WriteLine(fraseUsuario);

            //    // Volvemos a pedir una frase
            //    Console.Write("\n\n\tIntroduzca una frase: ");
            //    fraseUsuario = Console.ReadLine();
            //}

            //// Cerramos la variable StreamWriter
            //sw.Close();




            // Para salir
            Pausa("salir");
        }

        static void Pausa(string txt)
        {
            Console.Write("\n\n\n\tPulse una tecla para {0}", txt);
            Console.ReadKey(true);
        }
    }
}
