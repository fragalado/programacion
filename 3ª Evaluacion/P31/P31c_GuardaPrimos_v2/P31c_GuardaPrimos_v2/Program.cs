﻿using System;
using System.Collections.Generic;
using System.IO;

namespace P31c_GuardaPrimos_v2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // Guardamos en una variable el límite superior
            int top = CapturaEntero("¿Cuál es el límite superior?", 10, 10000);
            // Guardamos en una lista todos los números anteriores a limiteSup, usando ListaDePrimos
            List<int> lista = ListaDePrimos(top);
            // Guardará los valores de esa lista, separados por el carácter ';' en un fichero de nombre primos.txt
            StreamWriter sw = File.CreateText(String.Format(@"C:\zDatosPrueba\primos Menores de {0}.txt", top));

            // La primera línea del archivo será una cabecera:
            sw.WriteLine("Números primos menores de {0}", top);

            // Aparecen los primos (sin ';') pero en cinco columnas bien alineadas
            for (int i = 0; i < lista.Count; i++)
            {
                // Cada 5 columnas damos un salto de línea
                if(i % 5 == 0)
                    sw.WriteLine();

                sw.Write("\t");
                // Si el número es menor de 10 damos un salto
                if (lista[i] < 10)
                    sw.Write(" ");
                // Si el número es menor de 100 damos un salto
                if (lista[i] < 100)
                    sw.Write(" ");
                // Si el número es menor de 1000 damos un salto
                if (lista[i] < 1000)
                    sw.Write(" ");

                sw.Write(lista[i]);
            }

            // Cerramos el StreamWriter
            sw.Close();

            // Para salir
            Console.Write("\n\n\n\tPulse una tecla para salir");
            Console.ReadKey();

        }

        private static bool EsPrimo(int num)
        {
            // Comprobamos si el numero es 0 o 1, si es devolveremos que no es primo
            if (num == 0 || num == 1)
                return false;

            for (int i = 2; i <= num / 2; i++)
            {
                if (num % i == 0)
                    return false; // Si llega aquí será porque el númer NO es primo
            }

            // Si llega hasta aquí será porque el número ES primo
            return true;
        }

        private static List<int> ListaDePrimos(int limiteSup)
        {
            List<int> lista = new List<int>();

            // Creamos un bucle para añadir a la lista los números primos anteriores a limiteSup
            for (int i = 0; i < limiteSup; i++)
            {
                if (EsPrimo(i)) // Si el número ES primo va a entrar y se va a añadir a la lista
                    lista.Add(i);
            }

            // Devolvemos la lista con todos los números primos anteriores a limiteSup
            return lista;
        }

        private static int CapturaEntero(string txt, int min, int max)
        {
            int num;
            bool ok;

            do
            {
                Console.Write("\n\t{0} [{1}..{2}]: ", txt, min, max);
                ok = Int32.TryParse(Console.ReadLine(), out num);

                if (!ok)
                    Console.Write("\n\t** Error: El valor introducido no es un entero **");
                else if (num < min || num > max)
                    Console.Write("\n\t** Error: El valor introducido no está dentro del rango **");

            } while (!ok || num < min || num > max);

            return num;
        }

    }
}
