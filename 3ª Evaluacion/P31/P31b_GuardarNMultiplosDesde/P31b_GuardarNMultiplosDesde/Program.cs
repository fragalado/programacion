﻿using System;
using System.Collections.Generic;
using System.IO;

namespace P31b_GuardarNMultiplosDesde
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //// Declaramos las variables que vamos a utilizar
            //int num, cantMult, numDesde;
            //int[] vEnt;
            //string nombreFichero;

            //// 1)
            //// Llamamos a CapturaEntero para pedir valores
            //num = CapturaEntero("numero", 10, 99);
            //cantMult = CapturaEntero("Cantidad de múltiplos", 0, 300);
            //numDesde = CapturaEntero("Número a partir del cual hallar los múltiplos", 0, 1000);

            //// Llamamos al metodo NMultiplosDesde
            //vEnt = NMultiplosDesde(num, cantMult, numDesde);

            //// 2)
            //// Pedimos el nombre del fichero en el que guardar los valores de la tabla
            //Console.Write("\n\t¿nombre del fichero en el que guardar los valores de la tabla?: ");
            //nombreFichero = Console.ReadLine();
            //// El programa añadirá la extensión .txt al nombre del fichero y lo construirá.
            //StreamWriter sw = File.CreateText("C:\\zDatosPrueba\\P31b\\" + nombreFichero+".txt");

            //// 3)
            //// Por último guardará en dicho fichero todos los valores, separados entre sí por el carácter ";"
            //for (int i = 0; i < vEnt.Length; i++)
            //{
            //    sw.Write(vEnt[i]+"; ");
            //}


            //// Cerramos el StreamWriter
            //sw.Close();




            // -------------------------------------- PRACTICAR PARA EXAMEN -----------------------------------------------


            // 1) 
            // Pedimos un numero de dos cifras
            int numero = CapturaEntero("Introduce un numero de dos cifras", 10, 99);

            // La cantidad de sus múltiplos a representar
            int cantMultiplos = CapturaEntero("Cantidad de multiplos a representar", 1, 100);

            // Numero a partir del cual hallar los multiplos
            int numDesde = CapturaEntero("Numero a partir del cual hallar los multiplos", 0, 100);

            // Llamamos al metodo
            int[] vector = NMultiplosDesde(numero, cantMultiplos, numDesde);

            // 2)
            // Pedimos el nombre del fichero en el que vamos a guardar los valores de la tabla
            Console.Write("\n\t¿Nombre del fichero? [sin formato]: ");
            string nombreFichero = Console.ReadLine();

            // Construimos un vector StreamWriter para escribir en el fichero
            StreamWriter sw = File.CreateText(String.Format("C:\\zDatosPrueba\\{0}.txt", nombreFichero));

            // 3)
            // Guardamos en el fichero todos los valores separados por ';'
            // Imprimimos el primero para que el ultimo no tengo ';'
            sw.Write(vector[0]);
            for (int i = 1; i < vector.Length; i++)
            {
                sw.Write(";" + vector[i]);
            }

            // Cerramos el StreamWriter
            sw.Close();


            // Para salir
            Console.Write("\n\n\tPulse una tecla para salir");
            Console.ReadKey(true);
        }

        private static int[] NMultiplosDesde(int num, int cantidad, int numDesde)
        {
            // Construimos un vector de tamanyo cantidad
            int[] vMultiplos = new int[cantidad];

            // Guardamos en el vector los multiplos de num a partir de numDesde (sin incluir este)
            // Calculamos el primer multiplo de num
            int multiplo = (numDesde / num) * num;
            if (multiplo <= numDesde)
                multiplo += num;

            for (int i = 0; i < cantidad; i++)
            {
                vMultiplos[i] = multiplo;
                multiplo += num;
            }

            return vMultiplos;
        }

        //private static int[] NMultiplosDesde(int num, int cantidad, int numDesde)
        //{
        //    int[] vector = new int[cantidad];
            
        //    // Multiplos
        //    int multiplo = (numDesde / num ) * num;

        //    if (multiplo <= numDesde) // No se incluye numDesde
        //        multiplo += num;

        //    // Hacemos bucle para meter los valores dentro del vector
        //    for (int i = 0; i < cantidad; i++)
        //    {
        //        vector[i] = multiplo;
        //        multiplo += num;
        //    }

        //    return vector;
        //}

        private static int CapturaEntero(string txt, int min, int max)
        {
            int num;
            bool ok;
            do
            {
                Console.Write("\n\t¿{0}? [{1}..{2}]: ", txt, min , max);
                ok = Int32.TryParse(Console.ReadLine(), out num);
                // Comprobamos errores
                if (!ok)
                    Console.Write("\n\t** Error: El valor no es un entero **");
                else if(num < min || num > max)
                    Console.Write("\n\t** Error: El valor no está dentro del rango **");
            } while (!ok || num < min || num > max);

            return num;
        }
    }
}
