﻿// Alumno: Gallego, Francisco José
/*
 * Introduces la cantidad de billetes de 20, billetes de 5 y monedas de 2 euros.
 * El programa averigua el total de euros y lo desglosa en billetes de 50, de 10 y monedas de 1.
 * 
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesglosaEuros2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int b20, b5, m2;
            Console.Write("\n\tIntroduce la cantidad de billetes de 20: ");
            b20 = Convert.ToInt32(Console.ReadLine());
            Console.Write("\n\tIntroduce la cantidad de billetes de 5: ");
            b5 = Convert.ToInt32(Console.ReadLine());
            Console.Write("\n\tIntroduce la cantidad de monedas de 2 euros: ");
            m2 = Convert.ToInt32(Console.ReadLine());

            int total = b20*20 + b5*5 + m2*2;

            int b50, b10, m1;
            b50 = total / 50;
            b10 = (total - 50 * b50) / 10;      //( total % 50 )/ 10
            m1 = (total - 50 * b50 - 10 * b10);    // ( total % 50 )% 10

            Console.Write("\n\tTenemos {6} euros en total. {0} billetes de 20, {1} billetes de 5, {2} monedas de 2 euros.\n\tEsto se convierte en {3} billetes de 50,{4} billetes de 10 y {5} monedas de 1 euro", b20, b5, m2, b50, b10, m1, total);
            Console.Read();
        }
    }
}
