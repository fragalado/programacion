﻿// Alumno: Gallego Dorado, Francisco José
/*
 * Se introduce el radio de un círculo e imprimirá la longitud de su circunferencia
 * y su área longitud = 2*pi*radio ; área = pi*radio*2
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P11a_Circulo
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int radio;
            string radioAInt;
            Console.Write("\n\tIntroduce el radio del círculo: ");
            radioAInt =  Console.ReadLine();
            radio = Convert.ToInt32(radioAInt);
            double pi = System.Math.PI;
            double longitud = 2*pi*radio;
            double area = pi * Math.Pow(radio, 2);

            Console.Write("\n\tLa longitud de su circunferencia es: " + longitud);
            Console.Write("\n\tEl área es: " + area);
            Console.Write("\n\n\n\tPulse INTRO para salir");
            Console.ReadLine();


        }
    }
}
