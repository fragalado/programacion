﻿// Alumno: Gallego, Francisco José
/*
 * Introduces una cantidad de euros y la desglosa en billetes de 20, billetes de 5 y monedas de 1 euros.
 * 
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesglosaEuros1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int euros;
            Console.Write("\n\tIntroduce la cantidad de euros: ");
            euros = Convert.ToInt32(Console.ReadLine());
            int billete20 = euros / 20;
            int billete5 = (euros - 20*billete20) / 5;
            int monedas1 = (euros - 5*billete5 -20*billete20) / 1;
            //int billete5 = (euros % 20) / 5
            //int monedas1 = (euros % 20) % 5
            

            Console.Write("\n\tTenemos {0} de euros, serían {1} billetes de 20, {2} billetes de 5 y {3} monedas de euro.", euros,billete20,billete5, monedas1);
            Console.Read();
        }
    }
}
