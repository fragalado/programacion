﻿// Alumno: Gallego Dorado, Francisco José 
/* P11f)	Intercambia enteros: 
 Añade el código necesario, SIN MODIFICAR EL QUE YA ESTÁ ESCRITO, para que el programa haga lo siguiente:
 Se introducen los valores de dos variables enteros (x e y) 
 Se intercambia sus valores, es decir, el valor de x tiene que quedar en y, y viceversa.
 Para comprobar que el intercambio se ha realizado correctamente, se escribirán sus valores, antes y después del intercambio. 
*/
using System;
using System.Collections.Generic;
using System.Text;

class Program
{
    static void Main(string[] args)
    {
        int x, y;


        Console.Write("\nDime el valor de la variable x: ");
        // Almacenamos el valor escrito en 'x'
        x = Convert.ToInt32(Console.ReadLine());

        Console.Write("\nDime el valor de la variable y: ");
        // Almacenamos el valor escrito en 'y'
        y = Convert.ToInt32(Console.ReadLine());

        Console.WriteLine("\n\tValores ANTES del intercambio:  \t x={0};  y={1}", x, y);
        /* 
            Declaramos e inicializamos una nueva variable llamada res, para poder almacenar el valor de
            'y' y no se nos pierda.
        */
        int res = y;
        y = x;
        x = res;
        // (x, y) = (y, x)
        Console.WriteLine("\n\tValores DESPUÉS del intercambio:\t x={0};  y={1}", x, y);

        Console.Write("\n\n\n\t\tPulsa Intro para salir");
        Console.ReadLine();
    }
}
