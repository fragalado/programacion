﻿// Alumno: Gallego, Francisco José
/*
 * Pide 3 números y haz la media de los 3 dando sus decimales.
 * 
 * 
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NumerosEnteros
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int a, b, c;
            string convertirN1, convertirN2, convertirN3;
            Console.Write("\n\tIntroduzca el primer numero: ");
            convertirN1 = Console.ReadLine();
            Console.Write("\tIntroduzca el segundo numero: ");
            convertirN2 = Console.ReadLine();
            Console.Write("\tIntroduzca el tercer numero: ");
            // convertirN3 = Console.ReadLine();
            // Lo mismo pero sin utilizar la variable convertir

            a = Convert.ToInt32(convertirN1);
            b = Convert.ToInt32(convertirN2);
            c = Convert.ToInt32(Console.ReadLine());
            double abc = (double)(a + b + c);
            double media = abc / 3;

            // ó double media = (a+b+c) /3.0;
            // ó double media = Convert.ToDouble(a+b+c)/3;
            Console.Write("\n\tLa media de los tres números es: "+media);
            Console.Read();
        }
    }
}
