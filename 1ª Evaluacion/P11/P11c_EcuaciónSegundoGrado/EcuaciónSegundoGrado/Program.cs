﻿// Alumno:
/*
 * Se introducen los tres coeficientes a,b y c (enteros) de la ecuación de segundo grado
 * (aX^2 + bX + c= 0) y devuelve las dos soluciones: x1 y x2
 * 
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EcuaciónSegundoGrado
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int a, b, c;
            Console.Write("\n\tIntroduzca el valor de 'a': ");
            a = Convert.ToInt32(Console.ReadLine());
            Console.Write("\n\tIntroduzca el valor de 'b': ");
            b = Convert.ToInt32(Console.ReadLine());
            Console.Write("\n\tIntroduzca el valor de 'c': ");
            c = Convert.ToInt32(Console.ReadLine());

            double x1 = (-b + Math.Sqrt(b * b - 4 * a * c)) / (2 * a);
            double x2 = (-b - Math.Sqrt(b * b - 4 * a * c)) / (2 * a);

            Console.WriteLine("\n\tLa solucion 'x1' es: " + x1);
            Console.Write("\n\tLa solucion 'x2' es: " + x2);

            Console.Write("\n\n\tPulse INTRO para salir");
            x1 = Math.Round(x1, 3);    // Para redondear a 3 decimales
            Console.Read();
        }
    }
}
