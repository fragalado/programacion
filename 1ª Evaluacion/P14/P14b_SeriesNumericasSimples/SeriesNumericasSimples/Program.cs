﻿using System;

namespace SeriesNumericasSimples
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int opcion = 0;
            do
            {
                Console.Clear();
                Console.WriteLine("\n\n\n");
                Console.WriteLine("\t\t╔═════════════════════════════════════════╗");
                Console.WriteLine("\t\t║                   Menu                  ║");
                Console.WriteLine("\t\t╠═════════════════════════════════════════╣");
                Console.WriteLine("\t\t║ 1) Enteros positivos menores de 200.    ║");
                Console.WriteLine("\t\t║                                         ║");
                Console.WriteLine("\t\t║ 2) Los números pares menores de 200.    ║");
                Console.WriteLine("\t\t║                                         ║");
                Console.WriteLine("\t\t║ 3) Los números impares entre 200 y 500. ║");
                Console.WriteLine("\t\t║_________________________________________║");
                Console.WriteLine("\t\t║                                         ║");
                Console.WriteLine("\t\t║                0) Salir                 ║");
                Console.WriteLine("\t\t╚═════════════════════════════════════════╝");
                Console.Write("\t\tIntroduce una opción: ");
                opcion = Console.ReadKey().KeyChar - '0';

                switch (opcion)
                {
                    case 1:
                        Console.Clear();
                        Console.Write("\n\t\t\t\t\tEnteros positivos menores de 200\n\n");
                        int i = 1;
                        while (i < 200)
                        {   
                            Console.Write("{0}\t",i++);
                        }
                        break;
                    case 2:
                        Console.Clear();
                        Console.Write("\n\t\t\t\t\tLos números pares menores de 200\n\n");
                        i = 2;
                        while (i < 200)
                        {
                            Console.Write("{0}\t", i);
                            i += 2;
                        }
                        break;
                    case 3:
                        Console.Clear();
                        Console.Write("\n\t\t\t\t\tLos números impares entre 200 y 500\n\n");
                        //i = 200;
                        //while (i <= 500 && i >= 200)
                        //{
                        //    if ((i % 2) != 0)
                        //    {
                        //        Console.Write("{0}\t", i);
                        //    }
                        //    i++;
                        //}
                        i = 201;
                        while (i <= 500 && i >= 200)
                        {
                            Console.Write("{0}\t", i);
                            i += 2;
                        }
                        break;
                    case 0:
                        Console.Write("\n\tPulse una tecla para salir");
                        Console.ReadKey();
                        break;
                    default:
                        Console.Clear();
                        Console.Write("\n\t** Error: Opción no válida **");
                        break;
                }

                if (opcion != 0)
                {
                    Console.Write("\n\tPulse una tecla para volver al menu");
                    Console.ReadKey();
                }
            } while (opcion != 0);


            
        }
    }
}
