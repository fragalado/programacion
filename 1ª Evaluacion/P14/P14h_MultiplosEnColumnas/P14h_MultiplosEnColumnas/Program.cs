﻿// Alumno: Gallego Dorado, Francisco José
using System;

namespace P14h_MultiplosEnColumnas
{
    internal class Program
    {
        static void Main(string[] args)
        {
            /*
            int cant, num, min, nc;
            bool ok;
            // Pedimos cantidad de multiplos
            Console.Write("\n\tCantidad de múltiplos a presentar [100..300]: ");
            ok = Int32.TryParse(Console.ReadLine(), out cant);

            // Comprobamos que cant está dentro del rango, si no está volvemos a pedir
            while (cant < 100 || cant > 300)
            {
                Console.Write("\n\t** Error: No entra en el rango **");
                Console.Write("\n\tCantidad de múltiplos a presentar [100..300]: ");
                ok = Int32.TryParse(Console.ReadLine(), out cant);
            }

            // Pedimos e numero que vamos a presentar sus multiplos
            Console.Write("\n\tNúmero del que vamos a presentar sus múltiplos [11..77]: ");
            ok = Int32.TryParse(Console.ReadLine(), out num);

            // Comprobamos que el número está dentro del rango
            while (num <11 || num > 77)
            {
                Console.Write("\n\t** Error: No entra en el rango **");
                Console.Write("\n\tNúmero del que vamos a presentar sus múltiplos [11..77]: ");
                ok = Int32.TryParse(Console.ReadLine(), out num);
            }

            // Pedimos el numero a partir del cual calculamos los multiplos
            Console.Write("\n\tValor a partir del cual hallaremos los múltiplos [1000..2000]: ");
            ok = Int32.TryParse(Console.ReadLine(), out min);

            // Comprobamos que entra en el rango
            while (min < 1000 || min > 2000)
            {
                Console.Write("\n\t** Error: No entra en el rango **");
                Console.Write("\n\tValor a partir del cual hallaremos los múltiplos [1000..2000]: ");
                ok = Int32.TryParse(Console.ReadLine(), out min);
            }

            // Pedimos el número de columnas:
            Console.Write("\n\tNúmero de columnas de la presentación [3..8]: ");
            ok = Int32.TryParse(Console.ReadLine(), out nc);

            // Comprobamos que entra en el rango:
            while (nc<3 || nc>8)
            {
                Console.Write("\n\t** Error: No entra en el rango **");
                Console.Write("\n\tNúmero de columnas de la presentación [3..8]: ");
                ok = Int32.TryParse(Console.ReadLine(), out nc);
            }


            // Limpiamos la pantalla
            Console.Clear();

            // Presentamos el titulo
            Console.Write("\n\t{0} múltiplos de {1}, a partir de {2} en {3} columnas\n\n",cant,num,min,nc);

            int multiplo = (min / num) * num;

            // si ha salido menor:
            if (multiplo < min)
                multiplo += num;

            for (int i = 0; i < cant; i++)
            {
                if (i % nc == 0)
                    Console.WriteLine();

                Console.Write("\t"+multiplo);
                multiplo += num;
            }


            Console.Write("\n\n\tPulse una tecla para salir");
            Console.ReadKey();*/
            int cant, min, num, nc;
            bool esCorrecto;

            do
            {
                Console.Write("\n\tCantidad de múltiplos a presentar? [100..300]: ");
                esCorrecto = Int32.TryParse(Console.ReadLine(), out cant);
                // Forma 1
                if (!esCorrecto)// <-- Compruebo si hemos introducido un entero
                    Console.WriteLine("\n** Error: Valor introducido no válido **");
                else if (cant < 100 || cant > 300)
                    Console.WriteLine("\n** Error: El número no está en el rango pedido **");
            } while (!esCorrecto || cant < 100 || cant > 300);

            // Captura del valor inicial min
            do
            {
                Console.Write("\n\t¿Número del que obtener sus múltiplos? [11..77]: ");
                esCorrecto = Int32.TryParse(Console.ReadLine(), out num);
                // Forma 2
                if (!esCorrecto)// <-- Compruebo si hemos introducido un entero
                    Console.WriteLine("\n** Error: Valor introducido no válido **");
                else if (num < 11 || num > 77)
                {
                    Console.WriteLine("\n** Error: El número no está en el rango pedido **");
                    esCorrecto = false;
                }
            } while (!esCorrecto);

            // Captura del valor inicial min
            do
            {
                Console.Write("\n\t¿Valor mínimo? [1000..2000]: ");
                esCorrecto = Int32.TryParse(Console.ReadLine(), out min);
                if (!esCorrecto)// <-- Compruebo si hemos introducido un entero
                    Console.WriteLine("\n** Error: Valor introducido no válido **");
                // Forma 2
                else if (min < 1000 || min > 2000)
                {
                    Console.WriteLine("\n** Error: El número no está en el rango pedido **");
                    esCorrecto = false;
                }
            } while (!esCorrecto);

            // Captura del número de columnas en las que lo voy a presentar  nc
            do
            {
                Console.Write("\n\t¿Número de columnas? [3..8]: ");
                esCorrecto = Int32.TryParse(Console.ReadLine(), out nc);
                if (!esCorrecto)// <-- Compruebo si hemos introducido un entero
                    Console.WriteLine("\n** Error: Valor introducido no válido **");
                else if (nc < 3 || nc > 8)
                {
                    Console.WriteLine("\n** Error: El número no está en el rango pedido **");
                    esCorrecto = false;
                }

            } while (!esCorrecto);

            // cálculo del primer múltiplo suponiendo que el límite inferior está incluido
            int multiplo = (min / num) * num;

            if (multiplo < min)
                multiplo += num;

            // presentación
            Console.Clear();
            Console.WriteLine("\n{0} primeros múltiplos de {1} a partir de {2} en {3} columnas", cant, num, min, nc);

            for (int i = 0; i < cant; i++)
            {
                // cuando el contador sea múltiplo del nº de columnas...
                // Salto de línea
                if (i % nc == 0)
                    Console.WriteLine();

                Console.Write("{0}\t", multiplo); // <-- Presentamos el múltiplo
                multiplo += num;                  // <-- obtenemos el siguiente
            }


            Console.Write("\n\n\tPulsa una tecla para salir");
            Console.ReadKey();
        }

        static int CapturaEntero(string cadena, int min, int max)
        {
            int numero;
            bool ok;
            do
            {
                Console.Write("\n\t{2} [{0}..{1}]: ", min, max, cadena);
                ok = Int32.TryParse(Console.ReadLine(), out numero);
                if (!ok)
                    Console.Write("\n\t** Error: El valor introducido no es un entero **");
                else if (ok && (numero < min || numero > max))
                    Console.Write("\n\t** Error: El valor introducido no está entre los límites indicados");

            } while (!ok || numero < min || numero > max);
            return numero;
        }
    }
}
