﻿// Alumno: Gallego Dorado, Francisco José
using System;

namespace P14j_CosteTrabajadoresV3
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int opcion, numDiasObra, numTrabajadores, numTrabajadoresTotal=0, dias, euros=0, eurosTotal=0;
            bool ok;
            Console.Write("\n\t¿Cuántos días dura la obra? ");
            ok = Int32.TryParse(Console.ReadLine(), out numDiasObra);

            // Comprobamos que se ha introducido bien
            while (!ok)
            {
                Console.Write("\n\t** Error: Dato mal introducido **");
                Console.Write("\n\t¿Cuántos días dura la obra? ");
                ok = Int32.TryParse(Console.ReadLine(), out numDiasObra);
            }
            do
            {
                Console.Clear();
                Console.Write("\n\tid Profesión      euros/h   hs/Dia");
                Console.Write("\n\t__ _________      _______   ______");
                Console.Write("\n\n\t1  Albañil         12        8    ");
                Console.Write("\n\t2  Gruista         15        7    ");
                Console.Write("\n\t3  Electricista    16        5");
                Console.Write("\n\t4  Fontanero       14        5    ");
                Console.Write("\n\t5  Oficinista      10        2    ");
                Console.Write("\n\t  Elija el trabajador (0=salir) ");

                Console.Write("\n\n\t¿Qué tipo de empleado necesitas? ");
                opcion = Console.ReadKey().KeyChar - '0';

                if (opcion > 5 || opcion < 0)
                {
                    Console.Write("\n\n\t** Error: Número id introducido mal **");
                }
                else if(opcion <5 && opcion > 0)
                {

                    string profesion = "";
                    switch (opcion)
                    {
                        case 1:
                            profesion = "albañiles";
                            euros = 12 * 8;
                            break;
                        case 2:
                            profesion = "gruistas";
                            euros = 15 * 7;
                            break;
                        case 3:
                            profesion = "electricistas";
                            euros = 16 * 5;
                            break;
                        case 4:
                            profesion = "fontaneros";
                            euros = 14 * 5;
                            break;
                        case 5:
                            profesion = "oficinistas";
                            euros = 10 * 2;
                            break;
                    }

                    Console.Write("\n\t¿Cuántos {0} necesita ? ", profesion);
                    numTrabajadores = Convert.ToInt32(Console.ReadLine());
                    // Almacenamos los trabajadores en el numero total
                    numTrabajadoresTotal += numTrabajadores;
                    // Pedimos cuantos dias
                    Console.Write("\n\t¿Cuántos días lo vas a necesitar [1..{0}]? ",numDiasObra);
                    dias = Convert.ToInt32(Console.ReadLine());

                    // Comprobamos que dias esta dentro del rango:
                    while (dias < 1 || dias > numDiasObra)
                    {
                        Console.Write("\n\t** Error: No entra en el rango **");
                        Console.Write("\n\t¿Cuántos días lo vas a necesitar? ");
                        dias = Convert.ToInt32(Console.ReadLine());
                    }
                    
                    // Guardamos los euros en los euros totales.
                    eurosTotal += euros*numTrabajadores*dias;

                    // Para que cuando terminemos nos pida una tecla para volver a empezar
                    if (opcion != 0)
                    {
                        Console.Write("\n\tPulse una tecla para volver al menú");
                        Console.ReadKey();
                    }
                }
            } while (opcion != 0);

            Console.Write("\n\tEl coste total de los {0} trabajadores es de {1} euros", numTrabajadoresTotal, eurosTotal);

            Console.Write("\n\n\n\tPulse una tecla para salir");
            Console.ReadKey();
        }
    }
}
