﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiplosNumeroDosCifras
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int opcion;
            do
            {
                Console.Clear();
                Console.WriteLine("\n\n\n");
                Console.WriteLine("\t\t╔════════════════════════════════╗");
                Console.WriteLine("\t\t║              Menú              ║");
                Console.WriteLine("\t\t╠════════════════════════════════╣");
                Console.WriteLine("\t\t║                                ║");
                Console.WriteLine("\t\t║ 1) MultiplosMenoresDe300       ║");
                Console.WriteLine("\t\t║                                ║");
                Console.WriteLine("\t\t║ 2) CienPrimerosMultiplos       ║");
                Console.WriteLine("\t\t║                                ║");
                Console.WriteLine("\t\t║ 3) MultiplosEntreLimites       ║");
                Console.WriteLine("\t\t║                                ║");
                Console.WriteLine("\t\t║ 4) MultiplosDesde              ║");
                Console.WriteLine("\t\t║________________________________║");
                Console.WriteLine("\t\t║                                ║");
                Console.WriteLine("\t\t║         0) Salir               ║");
                Console.WriteLine("\t\t╚════════════════════════════════╝");
                Console.Write("\t\tIntroduce una opción: ");
                opcion = Console.ReadKey().KeyChar - '0';
                
                //Borramos la pantalla
                Console.Clear();

                if (opcion <= 4 && opcion >= 1)
                {
                    // Pedimos un valor de dos cifras
                    Console.Write("Introduce un numero de dos cifras: ");
                    int num = Convert.ToInt32(Console.ReadLine());

                    //Borramos la pantalla
                    Console.Clear();

                    switch (opcion)
                    {
                        case 1:
                            // *****************MultiplosMenoresDe300*******************
                            Console.WriteLine("\t** MultiplosMenoresDe300 **");

                            // while
                            int aux = num;
                            while (aux <= 300)
                            {
                                if (aux % num == 0)
                                    Console.Write("\t" + aux);
                                aux++;
                            }
                            break;

                        case 2:
                            // **********CienPrimerosMultiplos****************
                            Console.WriteLine("\t** CienPrimerosMultiplos **");

                            // while
                            int aux1 = 1;
                            while (aux1 <= 100)
                            {
                                Console.Write("\t" + (aux1 * num));
                                aux1++;
                            }
                            break;

                        case 3:
                            // **********MultiplosEntreLimites****************
                            Console.WriteLine("\t** MultiplosEntreLimites **");

                            // while
                            int i = 500;
                            while (i >= 500 && i <= 700)
                            {
                                if (i % num == 0)
                                    Console.Write("\t" + i);
                                i++;
                            }
                            break;

                        case 4:
                            // **********MultiplosDesde****************
                            Console.WriteLine("\t** MultiplosDesde **");

                            // while
                            int auxiliar = 1;
                            int contador = 0;
                            while (contador < 80)
                            {
                                if (num * auxiliar >= 700)
                                {
                                    Console.Write("\t" + (num * auxiliar));
                                    contador++;
                                }
                                auxiliar++;
                            }
                            break;


                            /* Profesor
                            const int MIN=700, CANT=80;
                            multiplo= (MIN/num) * num;
                            if(multiplo < MIN)
                                multiplo += num;
                            */
                    }
                }
                else if(opcion == 0)
                    Console.Write("\n\tGracias por usar el menu");
                else
                    Console.Write("\n\t ** Error: Opción no válida **");

                if (opcion != 0)
                {
                    Console.Write("\n\tPulse una tecla para volver al menú");
                    Console.ReadKey();
                }
            } while (opcion!=0);


            

            


            


            


            



            Console.Write("\n\n\n\tPulse una tecla para salir");
            Console.ReadKey();
        }
    }
}
