﻿// Alumno: Gallego Dorado, Francisco José
using System;

namespace P14i_MultiplicarSumando
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int a, b, aux, resultado;
            bool ok;
            char opcion;

            do
            {
                // Pedimos a
                Console.Write("\n\tIntroduce el primer valor: ");
                ok = Int32.TryParse(Console.ReadLine(), out a);

                // Comprobamos que a es un número, si ok es false va a entrar:
                while (!ok)
                {
                    Console.Write("\n\t** Error: Valor mal introducido **");
                    Console.Write("\n\tIntroduce el primer valor: ");
                    ok = Int32.TryParse(Console.ReadLine(), out a);
                }
                // Pedimos b
                Console.Write("\n\tIntroduce el segundo valor: ");
                ok = Int32.TryParse(Console.ReadLine(), out b);

                // Comprobamos que b es un número, si ok es false va a entrar:
                while (!ok)
                {
                    Console.Write("\n\t** Error: Valor mal introducido **");
                    Console.Write("\n\tIntroduce el segundo valor: ");
                    ok = Int32.TryParse(Console.ReadLine(), out b);
                }

                Console.Write("\n\tEl producto {0} x {1} =",a,b);
                // Si a es menor que b vamos a realizar un intercambio, queremos que 'a' sea el mayor siempre
                if (a < b)
                {
                    aux = a;
                    a = b;
                    b = aux;
                }
                // Vamos a tener que poner resultado a cero, para cuando se reinicie no se mantenga el valor anterior
                resultado = 0;
                for (int i = 0; i < b; i++)
                    resultado += a;
                Console.Write(" " + resultado); // MISMO ---> Console.Write(resultado);

                Console.Write("\n\n\nSi pulsas s o S (=Si) se limpia la pantalla y repetirá el proceso.");
                Console.Write("\nSi pulsas n o N (=No) acabará el programa.");
                opcion = Console.ReadKey().KeyChar;
                while (opcion != 's' && opcion != 'S' && opcion != 'n' && opcion != 'N')
                {
                    Console.Write("\n\t** Error: Tecla errónea **");
                    Console.Write("\n\n\nSi pulsas s o S (=Si) se limpia la pantalla y repetirá el proceso.");
                    Console.Write("\nSi pulsas n o N (=No) acabará el programa.");
                    Console.Write("\nIntroduce una opción: ");
                    opcion = Console.ReadKey().KeyChar;
                }

                if (opcion != 'n' && opcion != 'N')
                {
                    Console.WriteLine("\n\nPulse una tecla para Volver al menú");
                    Console.ReadKey();
                    Console.Clear();
                }

            } while (opcion != 'n' && opcion != 'N');


            Console.Write("\n\n\tPulse una tecla para salir");
            Console.ReadKey();
        }
    }
}
