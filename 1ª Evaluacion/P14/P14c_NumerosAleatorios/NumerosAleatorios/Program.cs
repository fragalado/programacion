﻿/*
    Acierta de tres
    
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AciertaDeTres
{
    internal class Program
    {
        static void Main(string[] args)
        {

            //Random generador = new Random();

            //numAzar = generador.Next(min, max);
            //Cuando el minimo es 0 se puede omitir
            //numAzar = generador.Next(max + 1);

            Random generador = new Random();
            int numAzar = generador.Next(10, 20+1);

            // Version mala
            //Console.Write("\n\tIntroduce un número entre 10 y 20: ");
            //int numero = Convert.ToInt32(Console.ReadLine());
            //if (numAzar == numero)
            //    Console.Write("\n\tFelicidades has acertado!!!");
            //else
            //{
            //    Console.Write("\n\tIntroduce un número entre 10 y 20: ");
            //    numero = Convert.ToInt32(Console.ReadLine());

            //    if (numAzar == numero)
            //        Console.Write("\n\tFelicidades has acertado!!!");
            //    else
            //    {
            //        Console.Write("\n\tIntroduce un número entre 10 y 20: ");
            //        numero = Convert.ToInt32(Console.ReadLine());
            //        if (numAzar == numero)
            //            Console.Write("\n\tFelicidades has acertado!!!");
            //        else
            //            Console.Write("\n\tHas fallado de nuevo, el número era el {0}.",numAzar);
            //    }
            //}


            // Version wena
            //int numero = 0;
            //int i = 1;
            //do
            //{
            //    Console.Clear();
            //    Console.Write("\n\tIntroduce un número entre 10 y 20: ");
            //    numero = Convert.ToInt32(Console.ReadLine());

            //    if(numero > 20 || numero < 10)
            //    {
            //        Console.Write("\n\t** Error: Número no válido **");
            //    }
            //    else
            //    {
            //        if (numAzar == numero)
            //            Console.Write("\n\tFelicidades has acertado!!!");
            //        else
            //            Console.Write("\n\tHas fallado, te quedan {0} oportuniades.", 3 - i);
            //    }
            //    i++;

            //    // Para que no se reinicie del tiron
            //    if (true)
            //    {
            //        Console.Write("\n\tPulse una tecla para continuar");
            //        Console.ReadKey();
            //    }
            //} while (i <= 3 && numAzar != numero);

            //if (numAzar != numero)
            //{
            //    Console.Write("\n\n\tHas fallado de nuevo, el número era el {0}.", numAzar);
            //}


            // Avanzado 1
            //int numero = 0;
            //int i = 1;
            //do
            //{
            //    Console.Clear();
            //    Console.Write("\n\tIntroduce un número entre 10 y 20: ");
            //    numero = Convert.ToInt32(Console.ReadLine());

            //    if (numero > 20 || numero < 10)
            //    {
            //        Console.Write("\n\t** Error: Número no válido **");
            //    }
            //    else
            //    {
            //        if (numAzar == numero)
            //            Console.Write("\n\tFelicidades has acertado!!!");
            //        else
            //        {
            //            if(numero > numAzar)
            //                Console.Write("\n\tTe has pasado :(");
            //            else
            //                Console.Write("\n\tTe has quedado corto :(");
            //            Console.Write("\n\tTe quedan {0} oportuniades.", 3 - i);
            //        }

            //    }
            //    i++;

            //    // Para que no se reinicie del tiron
            //    if (numAzar != numero)
            //    {
            //        Console.Write("\n\tPulse una tecla para continuar");
            //        Console.ReadKey();
            //    }
            //} while (i <= 3 && numAzar != numero);

            //if (numAzar != numero)
            //{
            //    Console.Write("\n\n\tHas fallado de nuevo, el número era el {0}.", numAzar);
            //}


            // Avanzado 2
            int numero = 0;
            int i = 1;
            int puntos = 100;
            do
            {
                Console.Clear();
                Console.Write("\n\tIntroduce un número entre 10 y 20: ");
                numero = Convert.ToInt32(Console.ReadLine());

                if (numero > 20 || numero < 10)
                {
                    Console.Write("\n\t** Error: Número no válido **");
                }
                else
                {
                    if (numAzar == numero)
                    {
                        Console.Write("\n\tFelicidades has acertado!!!");
                        Console.Write("\n\tTienes {0} puntos", puntos);
                    }
                    else
                    {
                        if (numero > numAzar)
                            Console.Write("\n\tTe has pasado :(");
                        else
                            Console.Write("\n\tTe has quedado corto :(");

                        puntos -= 40;
                        Console.Write("\n\tTienes {0} puntos", puntos);
                        Console.Write("\n\tTe quedan {0} oportuniades.", 3 - i);
                    }

                }
                i++;

                // Para que no se reinicie solo
                if (numAzar != numero)
                {
                    Console.Write("\n\tPulse una tecla para continuar");
                    Console.ReadKey();
                }
            } while (i <= 3 && numAzar != numero);

            if (numAzar != numero)
            {
                Console.Write("\n\n\tHas fallado de nuevo, el número era el {0}.", numAzar);
            }


            // Basica con while


            // Profesor Basico
            //int numAcertar, numUsuario;
            //int cont = 0;

            //Console.Write("\n\tPatrón: Dame un número entre el 10 y el 20: ");
            //numAcertar = Convert.ToInt32(Console.ReadLine());
            ////Si A se ha equivocado le indicamos que lo vuelva a intentar
            //while (numAcertar < 10 || numAcertar > 20)
            //{
            //    Console.Write("\n\tPor favor, entre 10 y 20 inclusive: ");
            //    numAcertar = Convert.ToInt32(Console.ReadLine());
            //}
            //Console.Clear();

            //do
            //{
            //    cont++;
            //    Console.Write("\n\tAdivina un número entre 10 al 20: ");
            //    numUsuario = Convert.ToInt32(Console.ReadLine());
            //    // comprobamos si falla para el mensaje
            //    if (numAcertar != numUsuario)
            //        Console.Write("\n\tFallo en el intento número {0}: Inténtelo de nuevo.",cont);
            //} while (numAcertar != numUsuario);

            ////cuando llegue aqui es porque ha acertado
            //Console.Write("\n\t!Coooorrecctooo¡; Acertaste en {0} intentos",cont);


            // Profesor 


            Console.Write("\n\n\n\tPulse una tecla para salir");
            Console.ReadKey();
        }
    }
}
