﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MenuMultiplos
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int opcion;
            int multiplo, cant, min = 0, max=0;
            do
            {
                Console.Clear();
                Console.WriteLine("\n\n\n");
                Console.WriteLine("\t\t╔════════════════════════════════╗");
                Console.WriteLine("\t\t║              Menú              ║");
                Console.WriteLine("\t\t╠════════════════════════════════╣");
                Console.WriteLine("\t\t║                                ║");
                Console.WriteLine("\t\t║ 1) MultiplosMenoresDe300       ║");
                Console.WriteLine("\t\t║                                ║");
                Console.WriteLine("\t\t║ 2) CienPrimerosMultiplos       ║");
                Console.WriteLine("\t\t║                                ║");
                Console.WriteLine("\t\t║ 3) MultiplosEntre500y700       ║");
                Console.WriteLine("\t\t║                                ║");
                Console.WriteLine("\t\t║ 4) OchentaMultiplosDesde700    ║");
                Console.WriteLine("\t\t║________________________________║");
                Console.WriteLine("\t\t║                                ║");
                Console.WriteLine("\t\t║         0) Salir               ║");
                Console.WriteLine("\t\t╚════════════════════════════════╝");
                Console.Write("\t\tIntroduce una opción: ");
                opcion = Console.ReadKey().KeyChar - '0';

                //Borramos la pantalla
                Console.Clear();

                if (opcion <= 4 && opcion >= 1)
                {
                    // Pedimos un valor de dos cifras
                    Console.Write("Introduce un numero de dos cifras: ");
                    int num = Convert.ToInt32(Console.ReadLine());

                    // Comprobamos que es un número de dos cifras, y si no lo es vamos a pedir hasta que lo sea
                    while (num <10 || num >99)
                    {
                        Console.Write("** Error: El número introducido no tiene dos cifras");
                        Console.Write("\nIntroduce un numero de dos cifras: ");
                        num = Convert.ToInt32(Console.ReadLine());
                    }

                    //Borramos la pantalla una vez que tengamos el numero correcto
                    Console.Clear();

                    switch (opcion)
                    {
                        case 1:
                            max = 300; // <--- Aqui ponemos el numero maximo que queremos (teniendo en cuenta que tambien entra)
                            // *****************MultiplosMenoresDe300*******************
                            Console.WriteLine("\t** Multiplos de {0} Menores de {1} **",num, max);

                            // while
                            int aux = num;
                            int cont = 0; // <-- Creamos esta variable para poder hacer 6 columnas
                            while (aux <= max)
                            {
                                if (cont % 6 == 0)
                                    Console.Write("\n\t" + aux);
                                else
                                    Console.Write("\t" + aux);
                                aux +=num;
                                cont++;
                            }

                            /* OPCION PROFESOR
                            multiplo = num;
                            while (multiplo < max)
                            {
                                Console.Write("\t"+ multiplo);
                                multiplo += num;
                            }
                            */
                            break;

                        case 2:
                            cant = 100; // <--- Aqui ponemos la cantidad de multiplos que queremos
                            // **********CienPrimerosMultiplos****************
                            Console.WriteLine("\t** Cien Primeros Multiplos de {0} **", num);

                            // while
                            int aux1 = 1;
                            while (aux1 <= cant)
                            {
                                Console.Write("\t" + (aux1 * num));
                                aux1++;
                            }

                            /* OPCION PROFESOR
                            for (int p = 1; p <= cant; p++)
                                Console.Write("\t"+(p*num));
                            */
                            break;

                        case 3:
                            min = 500; // <--- Aqui ponemos el minimo del intervalo (tambien entra)
                            max = 700; // <--- Aqui ponemos el maximo del intervalo (tambien entra)
                            // **********MultiplosEntreLimites****************
                            Console.WriteLine("\t** Multiplos de {0} Entre Limites {1} y {2} **",num, min,max);

                            // while
                            int i = min;
                            while (i >= min && i <= max)
                            {
                                if (i % num == 0)
                                    Console.Write("\t" + i);
                                i++;
                            }

                            /* OPCION PROFESOR
                            multiplo = (min/num)*num;
                            if (multiplo < min)
                                multiplo += num;

                            while (multiplo<=700)
                            {
                                Console.Write("\t"+multiplo);
                                multiplo += num;
                            }
                            */
                            break;

                        case 4:
                            min = 700; // <--- Aqui ponemos desde que numero va a empezar a hacer multiplos
                            cant = 80; // <--- Aqui ponemos cuantos multiplos queremos
                            // **********MultiplosDesde****************
                            Console.WriteLine("\t** Ochenta Multiplos de {0} Desde {1} **",num, min);

                            // while
                            int auxiliar = 1;
                            int contador = 0;
                            while (contador < 80)
                            {
                                if (num * auxiliar >= min)
                                {
                                    Console.Write("\t" + (num * auxiliar));
                                    contador++;
                                }
                                auxiliar++;
                            }

                            /* OPCION PROFESOR
                            multiplo = (min / num) * num;
                            if (multiplo < min)
                                multiplo += num;

                            for (int l = 0; l < cant; l++)
                            {
                                Console.Write("{0}\t", multiplo);
                                multiplo += num;
                            }
                            */
                            break;
                    }
                }
                else if (opcion == 0)
                    Console.Write("\n\tGracias por usar el menu");
                else
                    Console.Write("\n\t ** Error: Opción no válida **");

                // Para que no se reinicie solo
                if (opcion != 0)
                {
                    Console.Write("\n\n\tPulse una tecla para volver al menú");
                    Console.ReadKey();
                }
            } while (opcion != 0);

            Console.Write("\n\n\tPulse una tecla para salir");
            Console.ReadKey();
        }
    }
}
