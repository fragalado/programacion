﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MenuOK
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int opcInt;
            do
            {
                Console.Clear();
                Console.WriteLine("\n\n\n");
                Console.WriteLine("\t\t╔════════════════════════════════╗");
                Console.WriteLine("\t\t║              Menú              ║");
                Console.WriteLine("\t\t╠════════════════════════════════╣");
                Console.WriteLine("\t\t║ 1) Ecuaciones de segundo grado ║");
                Console.WriteLine("\t\t║                                ║");
                Console.WriteLine("\t\t║ 2) Desglosar Euros             ║");
                Console.WriteLine("\t\t║                                ║");
                Console.WriteLine("\t\t║ 3) Círculo: Perímetro y Área   ║");
                Console.WriteLine("\t\t║                                ║");
                Console.WriteLine("\t\t║ 4) Presenta Fechas             ║");
                Console.WriteLine("\t\t║________________________________║");
                Console.WriteLine("\t\t║                                ║");
                Console.WriteLine("\t\t║         0) Salir               ║");
                Console.WriteLine("\t\t╚════════════════════════════════╝");
                Console.Write("\t\tIntroduce una opción: ");

                opcInt = Console.ReadKey().KeyChar - '0'; // <-- Las dos líneas anteriores en una sola operación
                Console.Clear(); //<-- Limpiamos la pantalla

                switch (opcInt)
                {
                    case 1: // 
                        Console.WriteLine("\n\t\t---- Ecuaciones de segundo grado ----");
                        int a, b, c, discriminante;
                        double x1, x2;

                        Console.WriteLine("\n\n\tResolvemos Ecuación de segundo grado  (aX2 + bX + c = 0) ");

                        // Console.ReadLine()mos los coeficientes
                        Console.Write("\n\tCoeficiente a: ");
                        a = Convert.ToInt32(Console.ReadLine()); // <-- Lo convertimos a entero

                        Console.Write("\n\tCoeficiente b: ");
                        b = Convert.ToInt32(Console.ReadLine());

                        Console.Write("\n\tCoeficiente c: ");
                        c = Convert.ToInt32(Console.ReadLine());

                        discriminante = b * b - 4 * a * c;

                        if (discriminante < 0)
                        {
                            Console.WriteLine("\n\n\t** Error: Discriminante negativo **");
                        }
                        else
                        {
                            x1 = (-b + Math.Sqrt(discriminante)) / (2 * a);
                            x2 = (-b - Math.Sqrt(discriminante)) / (2 * a);

                            // Las mostramos
                            Console.WriteLine("\n\n Las soluciones de la ecuación de segundo grado  {0}X2 + {1}X + {2} = 0:", a, b, c);
                            Console.WriteLine("\n\t X1 = " + x1);
                            Console.WriteLine("\n\t X2 = " + x2);
                        }
                        break;
                    case 2:
                        Console.WriteLine("\n\t\t---- Desglosar Euros ----");
                        int billete20, billete5, moneda1, dinero, resto;
                        Console.Write("\n\tIntroduzca Cantidad de dinero: ");
                        dinero = Convert.ToInt32(Console.ReadLine());

                        if (dinero < 1)
                            Console.WriteLine("\n\n\t No hay nada que desglosar");
                        else
                        {

                            billete20 = dinero / 20;
                            resto = dinero % 20;
                            billete5 = resto / 5;
                            moneda1 = resto % 5;

                            Console.Write("\n\n\t{0} euros son:\n", dinero);  // Imprimimos por pantalla la cantidad de euros

                            // versión simple
                            //if (billete20 > 0)
                            //    Console.WriteLine("\t\t{0} billete/s de 20", billete20);
                            //if (billete5 > 0)
                            //    Console.WriteLine("\t\t{0} billete/s de 5", billete5);
                            //if (moneda1 > 0)
                            //    Console.WriteLine("\t\t{0} moneda/s de 1", moneda1);


                            //Versión avanzada
                            if (billete20 > 0)  // Si hay algún billete de 20
                            {
                                Console.Write("\t{0} billete", billete20); // Mostrará la cantidad de los billetes de 20

                                if (billete20 > 1) // Si hay más de 1 billete de 20 añadirá una "s" como plural
                                    Console.Write("s");

                                Console.Write(" de 20 euros");

                                if (billete5 > 0 && moneda1 > 0)
                                    Console.Write(", ");
                                else
                                    if (billete5 > 0 || moneda1 > 0)
                                    Console.Write(" y ");
                                else
                                    Console.Write(". ");
                            }

                            // vamos con los billetes de 5 euros
                            if (billete5 > 0)  // Si hay algún billete de 5 Euros
                            {
                                Console.Write(" {0} billete", billete5); // Mostrará la cantidad de los billetes de 5

                                if (billete5 > 1) // Si hay más de 1 billete de 5 añadirá una "s" como plural
                                    Console.Write("s");

                                Console.Write(" de 5 euros");

                                if (moneda1 > 0)
                                    Console.Write(" y ");
                            }

                            // vamos con las monedas de euro
                            if (moneda1 > 0)  // Si hay alguna moneda 
                            {
                                Console.Write(" {0} moneda", moneda1); // Mostrará la cantidad de monedas

                                if (moneda1 > 1) // Si hay más de 1 moneda añadirá una "s" como plural
                                    Console.Write("s");

                                Console.Write(" de euro");
                            }
                        }
                        break;
                    case 3:
                        Console.WriteLine("\n\t\t---- Perímetro y Área del Círculo ----");
                        int radio;
                        double perimetro, area;
                        string captura;

                        Console.Write("Introduce el radio: ");  // <-- Pedimos el radio
                        captura = Console.ReadLine();           // <-- Capturamos lo que escriba el usuario como cadena
                        radio = Convert.ToInt32(captura);       // <-- Convertimos a entero lo que haya escrito el usuario

                        // Calculamos el perímetro y el área
                        perimetro = 2 * Math.PI * radio;
                        area = Math.PI * radio * radio;

                        // Presentamos al usuario los resultados de forma ordenada
                        Console.WriteLine("Para un círculo de radio {0}:\n\t\tperímetro= {1}\n\t\t     área= {2}", radio, perimetro, area);
                        break;
                    case 4:
                        Console.WriteLine("\n\t\t---- Presentar Fecha ----");
                        int año, mes, dia, maxDiasDelMes = 0;
                        string nombreMes = ""; // <-- La inicializamos fuera del if

                        Console.Write("\n\t\tIndroduce el año: ");
                        año = Convert.ToInt32(Console.ReadLine());

                        Console.Write("\n\t\tIndroduce el mes [1..12]: ");
                        mes = Convert.ToInt32(Console.ReadLine());


                        if (mes < 1 || mes > 12) // <-- Mes incorrecto
                            Console.WriteLine("\n\t\t Error: Mes Fuera de rango");
                        else
                        {
                            if (mes == 2) // Si es febrero
                                maxDiasDelMes = 28;
                            // Meses de 30 días
                            else if (mes == 4 || mes == 6 || mes == 9 || mes == 11)
                                maxDiasDelMes = 30;
                            // El resto de los meses 
                            else
                                maxDiasDelMes = 31;

                            Console.Write("\n\t\tIndroduce el día [1..{0}]: ", maxDiasDelMes);
                            dia = Convert.ToInt32(Console.ReadLine());

                            // - día incorrecto -    --------------- mes de 30 días --------------------------------    - mes de 28 días - 
                            if (dia < 1 || dia > maxDiasDelMes)
                                Console.WriteLine("\n\t\t Error: Día Fuera de rango");
                            else
                            {
                                // Si llegamos aquí es que tanto el mes como el día es correcto
                                if (mes == 1)
                                    nombreMes = " Enero ";
                                else if (mes == 2)
                                    nombreMes = " Febrero ";
                                else if (mes == 3)
                                    nombreMes = " Marzo ";
                                else if (mes == 4)
                                    nombreMes = " Abril ";
                                else if (mes == 5)
                                    nombreMes = " Mayo ";
                                else if (mes == 6)
                                    nombreMes = " Junio ";
                                else if (mes == 7)
                                    nombreMes = " Julio ";
                                else if (mes == 8)
                                    nombreMes = " Agosto ";
                                else if (mes == 9)
                                    nombreMes = " Septiembre ";
                                else if (mes == 10)
                                    nombreMes = " Octubre ";
                                else if (mes == 11)
                                    nombreMes = " Nomviembre ";
                                else if (mes == 12)
                                    nombreMes = " Diciembre ";

                                Console.WriteLine("\n\n\t\t La fecha es: {0} de {1} de {2} ", dia, nombreMes, año);
                            }
                        }
                        break;
                    case 0:
                        Console.Write("\n\n\t\tPulsa una tecla para salir");
                        Console.ReadKey();
                        break;

                    default:
                        Console.WriteLine("\n\t\t-------- Error --------");
                        Console.Write("\n\n\t\tOpción no válida");
                        break;
                }
                if (opcInt != 0)
                {
                    Console.Write("\n\n\t\tPulsa una tecla para volver al menú");
                    Console.ReadKey();
                }
            } while (opcInt != 0);


            /*  
                int opcInt = 1;
                while (opcInt != 0)
                {
                    Console.Clear();
                    Console.WriteLine("\n\n\n");
                    Console.WriteLine("\t\t╔════════════════════════════════╗");
                    Console.WriteLine("\t\t║              Menú              ║");
                    Console.WriteLine("\t\t╠════════════════════════════════╣");
                    Console.WriteLine("\t\t║ 1) Ecuaciones de segundo grado ║");
                    Console.WriteLine("\t\t║                                ║");
                    Console.WriteLine("\t\t║ 2) Desglosar Euros             ║");
                    Console.WriteLine("\t\t║                                ║");
                    Console.WriteLine("\t\t║ 3) Círculo: Perímetro y Área   ║");
                    Console.WriteLine("\t\t║                                ║");
                    Console.WriteLine("\t\t║ 4) Presenta Fechas             ║");
                    Console.WriteLine("\t\t║________________________________║");
                    Console.WriteLine("\t\t║                                ║");
                    Console.WriteLine("\t\t║         0) Salir               ║");
                    Console.WriteLine("\t\t╚════════════════════════════════╝");
                    Console.Write("\t\tIntroduce una opción: ");

                    opcInt = Console.ReadKey().KeyChar - '0'; // <-- Las dos líneas anteriores en una sola operación
                    Console.Clear(); //<-- Limpiamos la pantalla

                    switch (opcInt)
                    {
                        case 1: // 
                            Console.WriteLine("\n\t\t---- Ecuaciones de segundo grado ----");
                            int a, b, c, discriminante;
                            double x1, x2;

                            Console.WriteLine("\n\n\tResolvemos Ecuación de segundo grado  (aX2 + bX + c = 0) ");

                            // Console.ReadLine()mos los coeficientes
                            Console.Write("\n\tCoeficiente a: ");
                            a = Convert.ToInt32(Console.ReadLine()); // <-- Lo convertimos a entero

                            Console.Write("\n\tCoeficiente b: ");
                            b = Convert.ToInt32(Console.ReadLine());

                            Console.Write("\n\tCoeficiente c: ");
                            c = Convert.ToInt32(Console.ReadLine());

                            discriminante = b * b - 4 * a * c;

                            if (discriminante < 0)
                            {
                                Console.WriteLine("\n\n\t** Error: Discriminante negativo **");
                            }
                            else
                            {
                                x1 = (-b + Math.Sqrt(discriminante)) / (2 * a);
                                x2 = (-b - Math.Sqrt(discriminante)) / (2 * a);

                                // Las mostramos
                                Console.WriteLine("\n\n Las soluciones de la ecuación de segundo grado  {0}X2 + {1}X + {2} = 0:", a, b, c);
                                Console.WriteLine("\n\t X1 = " + x1);
                                Console.WriteLine("\n\t X2 = " + x2);
                            }
                            break;
                        case 2:
                            Console.WriteLine("\n\t\t---- Desglosar Euros ----");
                            int billete20, billete5, moneda1, dinero, resto;
                            Console.Write("\n\tIntroduzca Cantidad de dinero: ");
                            dinero = Convert.ToInt32(Console.ReadLine());

                            if (dinero < 1)
                                Console.WriteLine("\n\n\t No hay nada que desglosar");
                            else
                            {

                                billete20 = dinero / 20;
                                resto = dinero % 20;
                                billete5 = resto / 5;
                                moneda1 = resto % 5;

                                Console.Write("\n\n\t{0} euros son:\n", dinero);  // Imprimimos por pantalla la cantidad de euros

                                // versión simple
                                //if (billete20 > 0)
                                //    Console.WriteLine("\t\t{0} billete/s de 20", billete20);
                                //if (billete5 > 0)
                                //    Console.WriteLine("\t\t{0} billete/s de 5", billete5);
                                //if (moneda1 > 0)
                                //    Console.WriteLine("\t\t{0} moneda/s de 1", moneda1);


                                //Versión avanzada
                                if (billete20 > 0)  // Si hay algún billete de 20
                                {
                                    Console.Write("\t{0} billete", billete20); // Mostrará la cantidad de los billetes de 20

                                    if (billete20 > 1) // Si hay más de 1 billete de 20 añadirá una "s" como plural
                                        Console.Write("s");

                                    Console.Write(" de 20 euros");

                                    if (billete5 > 0 && moneda1 > 0)
                                        Console.Write(", ");
                                    else
                                        if (billete5 > 0 || moneda1 > 0)
                                        Console.Write(" y ");
                                    else
                                        Console.Write(". ");
                                }

                                // vamos con los billetes de 5 euros
                                if (billete5 > 0)  // Si hay algún billete de 5 Euros
                                {
                                    Console.Write(" {0} billete", billete5); // Mostrará la cantidad de los billetes de 5

                                    if (billete5 > 1) // Si hay más de 1 billete de 5 añadirá una "s" como plural
                                        Console.Write("s");

                                    Console.Write(" de 5 euros");

                                    if (moneda1 > 0)
                                        Console.Write(" y ");
                                }

                                // vamos con las monedas de euro
                                if (moneda1 > 0)  // Si hay alguna moneda 
                                {
                                    Console.Write(" {0} moneda", moneda1); // Mostrará la cantidad de monedas

                                    if (moneda1 > 1) // Si hay más de 1 moneda añadirá una "s" como plural
                                        Console.Write("s");

                                    Console.Write(" de euro");
                                }
                            }
                            break;
                        case 3:
                            Console.WriteLine("\n\t\t---- Perímetro y Área del Círculo ----");
                            int radio;
                            double perimetro, area;
                            string captura;

                            Console.Write("Introduce el radio: ");  // <-- Pedimos el radio
                            captura = Console.ReadLine();           // <-- Capturamos lo que escriba el usuario como cadena
                            radio = Convert.ToInt32(captura);       // <-- Convertimos a entero lo que haya escrito el usuario

                            // Calculamos el perímetro y el área
                            perimetro = 2 * Math.PI * radio;
                            area = Math.PI * radio * radio;

                            // Presentamos al usuario los resultados de forma ordenada
                            Console.WriteLine("Para un círculo de radio {0}:\n\t\tperímetro= {1}\n\t\t     área= {2}", radio, perimetro, area);
                            break;
                        case 4:
                            Console.WriteLine("\n\t\t---- Presentar Fecha ----");
                            int año, mes, dia, maxDiasDelMes = 0;
                            string nombreMes = ""; // <-- La inicializamos fuera del if

                            Console.Write("\n\t\tIndroduce el año: ");
                            año = Convert.ToInt32(Console.ReadLine());

                            Console.Write("\n\t\tIndroduce el mes [1..12]: ");
                            mes = Convert.ToInt32(Console.ReadLine());


                            if (mes < 1 || mes > 12) // <-- Mes incorrecto
                                Console.WriteLine("\n\t\t Error: Mes Fuera de rango");
                            else
                            {
                                if (mes == 2) // Si es febrero
                                    maxDiasDelMes = 28;
                                // Meses de 30 días
                                else if (mes == 4 || mes == 6 || mes == 9 || mes == 11)
                                    maxDiasDelMes = 30;
                                // El resto de los meses 
                                else
                                    maxDiasDelMes = 31;

                                Console.Write("\n\t\tIndroduce el día [1..{0}]: ", maxDiasDelMes);
                                dia = Convert.ToInt32(Console.ReadLine());

                                // - día incorrecto -    --------------- mes de 30 días --------------------------------    - mes de 28 días - 
                                if (dia < 1 || dia > maxDiasDelMes)
                                    Console.WriteLine("\n\t\t Error: Día Fuera de rango");
                                else
                                {
                                    // Si llegamos aquí es que tanto el mes como el día es correcto
                                    if (mes == 1)
                                        nombreMes = " Enero ";
                                    else if (mes == 2)
                                        nombreMes = " Febrero ";
                                    else if (mes == 3)
                                        nombreMes = " Marzo ";
                                    else if (mes == 4)
                                        nombreMes = " Abril ";
                                    else if (mes == 5)
                                        nombreMes = " Mayo ";
                                    else if (mes == 6)
                                        nombreMes = " Junio ";
                                    else if (mes == 7)
                                        nombreMes = " Julio ";
                                    else if (mes == 8)
                                        nombreMes = " Agosto ";
                                    else if (mes == 9)
                                        nombreMes = " Septiembre ";
                                    else if (mes == 10)
                                        nombreMes = " Octubre ";
                                    else if (mes == 11)
                                        nombreMes = " Nomviembre ";
                                    else if (mes == 12)
                                        nombreMes = " Diciembre ";

                                    Console.WriteLine("\n\n\t\t La fecha es: {0} de {1} de {2} ", dia, nombreMes, año);
                                }
                            }
                            break;
                        case 0:
                            Console.Write("\n\n\t\tPulsa una tecla para salir");
                            Console.ReadKey();
                            break;

                        default:
                            Console.WriteLine("\n\t\t-------- Error --------");
                            Console.Write("\n\n\t\tOpción no válida");
                            break;
                    }
                    if (opcInt != 0)
                    {
                        Console.Write("\n\n\t\tPulsa una tecla para volver al menú");
                        Console.ReadKey();
                    }
                }


            */


        }
    }
}
