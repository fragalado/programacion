﻿// Alumno:Gallego Dorado, Francisco José
/*
    Construye una constante COLUMNAS = 7.
    La práctica muestra la tabla de caracteres ASCII, desde
    el 32 al 255 en 7 columnas.
    Hay que presentar el código y el carácter.
    Ejemplo: 67) C 68) D 69) E.
*/
using System;

namespace PresentaCaracteresEnColumnas
{
    internal class Program
    {
        const int COLUMNAS = 7;
        static void Main(string[] args)
        {
            // Creamos e inicializamos las variables que vamos a necesitar
            int minimo = 32;
            int maximo = 255;
            int contador = 0;

            // Escribimos el titulo
            Console.Write("\n\tPresenta Caracteres En Columnas\n\n");

            // Creamos un bucle for para ahorrarnos codigo
            for (int cod = 32; cod >= minimo && cod <= maximo; cod++)
            {
                if (contador == COLUMNAS)
                {
                    Console.WriteLine();
                    contador = 0;
                }
                /*
                if (contador%COLUMNAS==0)
                    Console.Write("\n");
                */

                /* Y NOS AHORRAMOS EL CONTADOR++ DE ABAJO
                if (contador++ %COLUMNAS==0)
                    Console.Write("\n");
                */
                if (cod <100)
                    Console.Write(" ");

                Console.Write("{0}) {1}\t", cod, (char)cod);
                contador++;
            }

            // Para que no se reinicie solo
            Console.Write("\n\n\tPulse una tecla para salir");
            Console.ReadKey();
        }
    }
}
