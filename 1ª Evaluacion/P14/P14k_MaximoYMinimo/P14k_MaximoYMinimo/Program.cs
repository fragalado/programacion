﻿using System;

namespace P14k_MaximoYMinimo
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Random random = new Random();
            int num;
            int limiteMinimo = random.Next(100), limiteMaximo= random.Next(300, 501);
            int posicionMaximo =0, posicionMinimo=0;
            int maximo = limiteMinimo, minimo = limiteMaximo;

            for (int i = 1; i <= 50; i++)
            {
                num = random.Next(limiteMinimo, limiteMaximo + 1);
                Console.WriteLine("\t{1}- {0}", num,i);

                if (num > maximo)
                {
                    maximo = num;
                    posicionMaximo = i;
                }
                if (num < minimo)
                {
                    minimo = num;
                    posicionMinimo = i;
                }
            }

            //Presentamos el maximo y el minimo
            Console.Write("\n\tEl valor Máximo es el {0} y está en la posición {1}",maximo, posicionMaximo);
            Console.Write("\n\tEl valor Mínimo es el {0} y está en la posición {1}", minimo, posicionMinimo);

            Console.Write("\n\tPulse una tecla para salir");
            Console.ReadKey();
        }
    }
}
