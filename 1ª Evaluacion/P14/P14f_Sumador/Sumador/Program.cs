﻿// Alumno: Gallego, Francisco José
/*
    Sumador1:
    El usuario va introduciendo sumandos hasta que se introduce 0. Al introducir este valor, el programa
    devuelve la suma de los números introducidos hasta el momento y la media aritmética de todos.
    Para hallar la media no se tendrá encuenta el valor 0 de salida.
    Sumador2:
    Lo mismo, pero en lugar de salir con 0, se sale pulsando sólo Intro. En este caso, el valor 0
    se considera uno de los posibles y entra en la media.
*/
using System;

namespace Sumador
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int opcion, numero, suma, contador;
            bool ok;
            do
            {
                Console.Clear();
                Console.Write("\n\t\t1.Sumador1");
                Console.Write("\n\t\t2.Sumador2");
                Console.Write("\n\t\t0.Salir\n\n\n");
                Console.Write("\n\tIntrodce una opción: ");
                opcion = Console.ReadKey().KeyChar - '0';

                Console.Clear();
                switch (opcion)
                {
                    case 0:
                        Console.Write("\n\tGracias por usar el menú");
                        Console.ReadKey();
                        break;
                    case 1:
                        //Sumador1:
                        Console.Write("\n\t** Sumador 1 **\n");

                        // Variables
                        numero=0;
                        suma = 0;
                        ok=true;
                        contador = 0;

                        // Creamos un bucle que vaya preguntando numeros hasta que se introduzca el cero
                        do
                        {
                            Console.Write("\n\tIntroduce un número: ");
                            ok = Int32.TryParse(Console.ReadLine(), out numero);

                            if (!ok)
                                Console.Write("\n\t** Error: No has introducido un numero entero **");
                            else if (numero == 0)
                                Console.Write("\n\tGracias por usar el programa");
                            else
                            {
                                suma += numero;
                                contador++;
                            }


                        } while (numero != 0 || numero == 0 && !ok);

                        // Para mostrar en pantalla la suma y la media aritmetica
                        Console.Write("\n\n\tSuma: {0}", suma);
                        if (suma == 0)
                            Console.Write("\n\tMedia: 0");
                        else
                            Console.Write("\n\tMedia: {0}", suma / (contador * 1.0));
                        break;
                    case 2:
                        //Sumador2:
                        Console.Write("\n\t** Sumador 2 **\n");

                        //Variables
                        numero = 0;
                        suma = 0;
                        ok = true;
                        contador = 0;

                        // Creamos un bucle que vaya preguntando numeros hasta que se introduzca el cero
                        do
                        {
                            Console.Write("\n\tIntroduce un número: ");
                            ok = Int32.TryParse(Console.ReadLine(), out numero);

                            if (!ok)
                                Console.Write("\n\tGracias por usar el programa");
                            else
                            {
                                suma += numero;
                                contador++;
                            }
                        } while (ok);

                        // Para mostrar en pantalla la suma y la media aritmetica
                        Console.Write("\n\n\tSuma: {0}", suma);
                        if (suma == 0)
                            Console.Write("\n\tMedia: 0");
                        else
                            Console.Write("\n\tMedia: {0}", suma / (contador * 1.0));
                        break;
                }

                if (opcion != 0)
                {
                    Console.Write("\n\tPulse una tecla para volver al menú");
                    Console.ReadKey();
                }
            } while (opcion != 0);

            // Para terminar el programa
            Console.Write("\n\n\tPulse una tecla para salir");
            Console.ReadKey();
        }
    }
}
