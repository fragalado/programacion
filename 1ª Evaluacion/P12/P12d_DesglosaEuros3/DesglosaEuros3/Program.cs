﻿/*
    Mejora la práctca DesglosaEuros1 de modo que no imprima alguno de los billetes/monedas
     desglosados si la cantidad es cero.
    Ej:
        73€ son 3 billetes de 20, 2 billetes de 5 y 3 monedas de 1.
        70€ son 3 billetes de 20 y 2 billetes de 5.
        23€ son 1 billete de 20 y 3 monedas de 1.
        13€ son 2 billetes de 5 y 3 monedas de 1.
        10€ son 2 billetes de 5.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesglosaEuros3
{
    internal class Program
    {
        static void Main(string[] args)
        {
            
        }
    }
}
