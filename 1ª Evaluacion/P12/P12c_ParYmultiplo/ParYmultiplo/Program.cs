﻿// Alumno: Gallego, Francisco José
/*
 * El programa pide un número y te presenta en pantalla si es par o impar,
 * y si es múltiplo de 3.
 * Ejemplo: El número 75 es IMPAR y además múltiplo de 3
 *          El número 76 es PAR pero no es múltiplo de 3
 *          El número es IMPAR pero no es múltiplo de 3
 *          El número es PAR y además múltiplo de 3
 *    Si se escribe un cero sólo debe presentar:
 *    El cero NO es PAR ni IMPAR
 */         
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParYmultiplo
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.Write("\n\tIntroduce un número: ");
            int numero = Convert.ToInt32(Console.ReadLine());

            if (numero == 0)
            {
                Console.Write("\n\tEl cero NO es PAR ni IMPAR");
            } 
            else if (numero % 2 == 0)
            {
                if (numero % 3 == 0)
                {
                    Console.Write("\n\tEl número {0} es PAR y además múltiplo de 3", numero);
                }
                else
                {
                    Console.Write("\n\tEl número {0} es PAR pero no es múltiplo de 3", numero);
                }
            }
            else
            {
                if (numero % 3 == 0)
                {
                    Console.Write("\n\tEl número {0} es IMPAR y además múltiplo de 3", numero);
                }
                else
                {
                    Console.Write("\n\tEl número {0} es IMPAR pero no es múltiplo de 3", numero);
                }
            }

            /*
             if (numero == 0){Console.Write("\n\tEl cero NO es PAR ni IMPAR");} 
            else if (numero % 2 == 0){
                if (numero % 3 == 0){Console.Write("\n\tEl número {0} es PAR y además múltiplo de 3", numero);}
                else{Console.Write("\n\tEl número {0} es PAR pero no es múltiplo de 3", numero);}}
            else{
                if (numero % 3 == 0){Console.Write("\n\tEl número {0} es IMPAR y además múltiplo de 3", numero);}
                else{Console.Write("\n\tEl número {0} es IMPAR pero no es múltiplo de 3", numero);}} 
            */

            /*      PROFESOR:
                
                   
                if(numero == 0)
                    Console.WriteLine("El cero NO es par ni impar
                else{
                Console.Write("El número {0} ",numero);
                if(numero % 2 == 0)
                    Console.Write("es PAR");
                else
                    Console.Write("es IMPAR");
                
                if(x % 3 == 0)
                    Console.WriteLine("y es múltiplo de 3");
                else
                    Console.WriteLine("pero NO es múltiplo de 3");
                }
                
                
             */

            Console.ReadLine();
        }
    }
}
