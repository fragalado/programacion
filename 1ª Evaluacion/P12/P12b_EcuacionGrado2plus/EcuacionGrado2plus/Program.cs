﻿// Alumno: Gallego Dorado, Francisco José
/*
 * Repetir la prática de segungo grado haciendo que, si el discriminante da negativo, presente un mensaje 
 * advirtiéndolo en lugar del resultado.
 * EJ: **Sin soluciones reales: Discriminante negativo**
 * 
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EcuacionGrado2plus
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int a, b, c;
            Console.Write("\n\tIntroduzca el valor de 'a': ");
            a = Convert.ToInt32(Console.ReadLine());
            Console.Write("\n\tIntroduzca el valor de 'b': ");
            b = Convert.ToInt32(Console.ReadLine());
            Console.Write("\n\tIntroduzca el valor de 'c': ");
            c = Convert.ToInt32(Console.ReadLine());
            
            if ((b * b - 4 * a * c) < 0)
            {
                Console.Write("\t**Sin soluciones reales: Discriminante negativo**");
            }
            else
            {
                double x1 = (-b + Math.Sqrt(b * b - 4 * a * c)) / (2 * a);
                double x2 = (-b - Math.Sqrt(b * b - 4 * a * c)) / (2 * a);

                Console.WriteLine("\n\tLa solucion 'x1' es: " + x1);
                Console.Write("\n\tLa solucion 'x2' es: " + x2);
            }

            
            
            

            Console.Write("\n\n\tPulse INTRO para salir");
            //x1 = Math.Round(x1, 3);    Para redondear a 3 decimales
            Console.Read();
        }
    }
}
