﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P12b_DiaSemana
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // Pedimos al usuario un día
            Console.Write("\n\tIntroduza un número entre 0 y 6 (0=Domingo, 6=Sábado): ");
            int dia = Convert.ToInt32(Console.ReadLine());
            string nombreDia = "";

            //Comprobamos que el día está dentro del rango
            if (dia > 6 || dia < 0)
            {
                Console.Write("\n\t** Error: Día fuera de rango (0-6) **");
            }
            else
            {
                //Pasamos el día por consola
                switch (dia)
                {
                    case 0:
                        nombreDia = "Domingo";
                        break;
                    case 1:
                        nombreDia = "Lunes";
                        break;
                    case 2:
                        nombreDia = "Martes";
                        break;
                    case 3:
                        nombreDia = "Miércoles";
                        break;
                    case 4:
                        nombreDia = "Jueves";
                        break;
                    case 5:
                        nombreDia = "Viernes";
                        break;
                    case 6:
                        nombreDia = "Sábado";
                        break;
                }
                Console.Write("\n\tHoy es " + nombreDia);

                // Pedimos al usuario cuántos días quiere avanzar
                Console.Write(". Introduce cuántos días quieres avanzar: ");
                int numDias = Convert.ToInt32(Console.ReadLine());

                // Calculamos que día es y pasamos por pantalla
                dia += numDias;
                if (dia > 6)
                {
                    dia = dia % 7;
                }
                if (numDias == 1)
                    Console.Write("\n\tEstamos a {0} y dentro de {1} día será ", nombreDia, numDias);
                else
                    Console.Write("\n\tEstamos a {0} y dentro de {1} días será ", nombreDia, numDias);

                switch (dia)
                {
                    case 0:
                        Console.Write("Domingo");
                        break;
                    case 1:
                        Console.Write("Lunes");
                        break;
                    case 2:
                        Console.Write("Martes");
                        break;
                    case 3:
                        Console.Write("Miércoles");
                        break;
                    case 4:
                        Console.Write("Jueves");
                        break;
                    case 5:
                        Console.Write("Viernes");
                        break;
                    case 6:
                        Console.Write("Sábado");
                        break;
                }
            }

            Console.Write("\n\n\t\tPulse INTRO para salir");
            Console.ReadLine();
        }
    }
}
