﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapturaEntero2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //
            Console.Write("\n\tDime el primer valor: ");
            int a = Convert.ToInt32(Console.ReadLine());
            Console.Write("\n\tDime el segundo valor: ");
            int b = Convert.ToInt32(Console.ReadLine());
            //Limpiamos la pantalla
            Console.Clear();
            
            if (a == b) // Si los dos números son iguales lanzamos un error
            {
                Console.Write("\n\tError: No pueden ser el mismo número");
            }
            else
            {
                if (b < a)
                {
                    Console.Write("\n\tIntroduce un número entre {0} y {1}: ", b, a);
                    int numero = Convert.ToInt32(Console.ReadLine());
                    if (b <= numero && numero <= a)
                    {
                        Console.Write("\n\tCorrecto. El número elegido es " + numero);
                    }
                    else
                        Console.Write("\n\tNúmero fuera de rango");

                }
                else
                {
                    Console.Write("\n\tIntroduce un número entre {0} y {1}: ",a,b);
                    int numero = Convert.ToInt32(Console.ReadLine());
                    if (a <= numero && numero <= b)
                    {
                        Console.Write("\n\tCorrecto. El número elegido es " + numero);
                    }
                    else
                        Console.Write("\n\tNúmero fuera de rango");
                }
            }
            Console.ReadLine();
        }
    }
}
