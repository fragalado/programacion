﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapturaEntero0
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.Write("\n\tDime un número entero positivo de dos cifras: ");
            string numeroString = Console.ReadLine();
            int numero = Convert.ToInt32(numeroString);
            if (numeroString.Length == 2)
            {
                Console.Write("\n\tCorrecto. El número elegido es "+numero);
            }
            else
                Console.Write("\n\tNúmero fuera de rango");

            /* OTRA OPCION
            Console.Write("\n\tDime un número entero positivo de dos cifras: ");
            int numero = Convert.ToInt32(Console.ReadLine());

            if(numero < 10 || numer > 99){
                Console.Write("** Error: Número fuera de rango **");
            } else{
                Console.Write("Correcto. El número elegido es: "+numero);
            }
            */
            Console.ReadLine();
        }
    }
}
