﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace PresentarFecha0
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int anyo, mes, dia;
            string nombreMes="";
            //Pedimos el año
            Console.Write("\n\tIntroduzca el año: ");
            anyo = Convert.ToInt32(Console.ReadLine());
            //Pedimos el mes
            Console.Write("\n\tIntroduzca el mes: ");
            mes = Convert.ToInt32(Console.ReadLine());

            // Comprobamos que el mes es correcto
            if (mes < 1 || mes >12)
            {
                Console.Write("\n\t** Error: Mes fuera de rango **");
            }
            else
            {
                //Pedimos el dia
                Console.Write("\n\tIntroduzca el dia: ");
                dia = Convert.ToInt32(Console.ReadLine());

                //Comprobamos si el dia es incorrecto
                if ((dia > 31 || dia < 1 ) || 
                    (dia > 30 && (mes==4 || mes==6 || mes==9 || mes==11)) || // Meses de 30 dias
                    (dia > 28 && mes==2)) // febrero
                {
                    Console.Write("\n\t** Error: Día fuera de rango **");
                }
                else
                {
                    // Aqui tanto el mes como el dia son correctos
                    if (mes==1)
                        nombreMes = "Enero";
                    else if (mes==2)
                        nombreMes = "Febrero";
                    else if (mes == 3)
                        nombreMes = "Marzo";
                    else if (mes == 4)
                        nombreMes = "Abril";
                    else if (mes == 5)
                        nombreMes = "Mayo";
                    else if (mes == 6)
                        nombreMes = "Junio";
                    else if (mes == 7)
                        nombreMes = "Julio";
                    else if (mes == 8)
                        nombreMes = "Agosto";
                    else if (mes == 9)
                        nombreMes = "Setiembre";
                    else if (mes == 10)
                        nombreMes = "Octubre";
                    else if (mes == 11)
                        nombreMes = "Noviembre";
                    else if (mes == 12)
                        nombreMes = "Diciembre";

                    Console.Write("\n\t{0} de {1} de {2}.", dia, nombreMes, anyo);
                }
                

            }



            /*    OTRA FORMA
            // Comprobamos que el mes es correcto
            if (mes < 1 || mes >12)
            {
                Console.Write("\n\t** Error: Mes fuera de rango **");
            }
            else
            {
                //Pedimos el dia
                Console.Write("\n\tIntroduzca el dia: ");
                dia = Convert.ToInt32(Console.ReadLine());

                //Comprobamos si el dia es incorrecto
                if ((dia > 31 || dia < 1 ) || 
                    (dia > 30 && (mes==4 || mes==6 || mes==9 || mes==11)) || // Meses de 30 dias
                    (dia > 28 && mes==2)) // febrero
                {
                    Console.Write("\n\t** Error: Día fuera de rango **");
                }
                else
                {
                    // Aqui tanto el mes como el dia son correctos
                    Console.Write("\n\tLa fecha es: {0} de",dia);
                    if (mes==1)
                        Console.Write(" Enero");
                    else if (mes==2)
                        Console.Write(" Febrero");
                    else if (mes == 3)
                        Console.Write(" Marzo");
                    else if (mes == 4)
                        Console.Write(" Abril");
                    else if (mes == 5)
                        Console.Write(" Mayo");
                    else if (mes == 6)
                        Console.Write(" Junio");
                    else if (mes == 7)
                        Console.Write(" Julio");
                    else if (mes == 8)
                        Console.Write(" Agosto");
                    else if (mes == 9)
                        Console.Write(" Setiembre");
                    else if (mes == 10)
                        Console.Write(" Octubre");
                    else if (mes == 11)
                        Console.Write(" Noviembre");
                    else if (mes == 12)
                        Console.Write(" Diciembre");
                    
                    Console.Write(" de "+anyo);

                }
                

            }
             */

            Console.ReadLine();
        }
    }
}
