﻿// Alumno: Gallego Dorado, Francisco José
/*
 * El programa pide tu nombre y tu edad. Si tu edad es 18 o más presentará un mensaje:
 *  "Buenos días fulanito. Ya eres mayor de edad". En caso contrario presenta otro mensaje:
 *  "Buenos días fulanito. Te faltan x años para ser mayor de edad"
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MayorDeEdad
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.Write("\n\tDime tu nombre: ");
            string nombre = Console.ReadLine();
            Console.Write("\n\tDime tu edad: ");
            int edad = Convert.ToInt32(Console.ReadLine());

            if (edad >= 18){
                Console.Write("\n\tBuenos días {0}. Ya eres mayor de edad", nombre);
            }
            else { 
                if (18 - edad == 1)
                {
                    Console.Write("\n\tBuenos días {0}. Te falta {1} año para ser mayor de edad", nombre, 18 - edad);
                }
                else
                {
                    Console.Write("\n\tBuenos días {0}. Te faltan {1} años para ser mayor de edad", nombre, 18 - edad);
                }
            }
            Console.Write("\n\n\tPulse INTRO para salir");
            Console.ReadLine();
        }
    }
}
