﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapturaEntero1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // Mensaje al usuario
            Console.Write("\n\tDime un rango (Primero valor mínimo): ");
            int a = Convert.ToInt32(Console.ReadLine());
            Console.Write("\n\tDime un rango (Ahora valor máximo): ");
            int b = Convert.ToInt32(Console.ReadLine());
            // Limpiamos la pantalla
            Console.Clear();

            if (b < a) // Si el mínimo es menor que el máximo da un error.
            {
                Console.Write("\n\tError: No puede ser el máximo menor que el mínimo");
            }
            else // Los demás casos
            {
                Console.Write("\n\tIntroduce un número entre {0} y {1}: ",a,b);
                int numero = Convert.ToInt32(Console.ReadLine());
                if (a <= numero && numero <=b) // Comprobamos que el nuevo número esta dentro del rango
                {
                    Console.Write("\n\tCorrecto. El número elegido es " + numero);
                }
                else// Si no esta dentro del rango..
                    Console.Write("\n\tNúmero fuera de rango");
            }

            Console.ReadLine();
        }
    }
}
