﻿//Alumno:Gallego Dorado, Francisco José
/*P13e_Profesiones
 Tenemos una empresa que tiene 5 tipos de empleados. En la tabla siguiente se muestra
 cada tipo su sueldo por hora y el numero de horas que trabajan diariamente en la empresa

       id           Profesión          euros/h      hs/dia

         1            Albañil               12           8
         2            Gruista               15           7
         3            Electricista          16           5
         4            Fontanero             14           5
         5            Oficinista            10           2

El programa servirá para calcular cuánto nos va a costar un empleado para un trabajo,
sabiendo el número de días que tendría que trabajar

Al usuario se le presenta la tabla anterior y se le pregunta
 *    ¿Qué tipo de empleado necesitas [1..5]?
 *    ¿Cuántos días lo vas a necesitar?

Escribirá
 *    Un <tipo de empleado> durante <nº de días>  cuesta <tantos euros>

 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace CosteTrabajadores
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.Write("\n\tid Profesión      euros/h   hs/Dia");
            Console.Write("\n\t__ _________      _______   ______");
            Console.Write("\n\n\t1  Albañil         12        8    ");
            Console.Write("\n\t2  Gruista         15        7    ");
            Console.Write("\n\t3  Electricista    16        5");
            Console.Write("\n\t4  Fontanero       14        5    ");
            Console.Write("\n\t5  Oficinista      10        2    ");

            Console.Write("\n\n\t¿Qué tipo de empleado necesitas? ");
            int opcion = Console.ReadKey().KeyChar - '0';

            if (opcion > 5 || opcion < 1)
            {
                Console.Write("\n\n\t** Error: Número id introducido mal **");
            }
            else
            {
                Console.Write("\n\t¿Cuántos días lo vas a necesitar? ");
                int dias = Convert.ToInt32(Console.ReadLine());

                int euros = 0;
                string profesion = "";
                switch (opcion)
                {
                    case 1:
                        euros = (8 * dias) * 12;
                        profesion = "Albañil";
                        break;
                    case 2:
                        euros = (7 * dias) * 15;
                        profesion = "Gruista";
                        break;
                    case 3:
                        euros = (5 * dias) * 16;
                        profesion = "Electricista";
                        break;
                    case 4:
                        euros = (5 * dias) * 14;
                        profesion = "Fontanero";
                        break;
                    case 5:
                        euros = (2 * dias) * 10;
                        profesion = "Oficinista";
                        break;
                }

                Console.Write("\n\n\tUn {2} durante {0} días cuesta {1} euros", dias, euros, profesion);
            }
            
            

            Console.Write("\n\n\n\tPulse una tecla para salir");
            Console.ReadKey();
        }
    }
}
