﻿// Alumno: Gallego Dorado, Francisco José
/*
    Utilizando el “dibujo del menú” que se te entrega, realiza un programa que te
    presenta el Menú y te permite elegir una de sus opciones.
        • Si se elige una opción correcta lo único que hará será limpiar la pantalla e
        imprimir en el centro de la misma la opción elegida. Por ejemplo en el caso de
        la opción 1:

    Ha elegido la opción no 1: “Lista de Clientes”
    Importante: el título de la opción tiene que aparecer entre comillas dobles
        • Si se elige una opción incorrecta se dará el correspondiente mensaje de error.
*/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Menu0
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("\n\n\n\n\t\t\t Nombre Apellido1");
            Console.WriteLine("\t\t\t╔══════════════════════════════╗");
            Console.WriteLine("\t\t\t║             MENU             ║");
            Console.WriteLine("\t\t\t╠══════════════════════════════╣");
            Console.WriteLine("\t\t\t║                              ║");
            Console.WriteLine("\t\t\t║    1.- Lista de Clientes     ║");
            Console.WriteLine("\t\t\t║    2.- Añadir Cliente        ║");
            Console.WriteLine("\t\t\t║    3.- Eliminar Cliente      ║");
            Console.WriteLine("\t\t\t║    4.- Modificar Datos       ║");
            Console.WriteLine("\t\t\t║                              ║");
            Console.WriteLine("\t\t\t║          0) Salir            ║");
            Console.WriteLine("\t\t\t║                              ║");
            Console.WriteLine("\t\t\t╚══════════════════════════════╝");
            Console.Write("\t\tIntroduce una opción: ");

            int opcion = Convert.ToInt32(Console.ReadLine());
            // Para no tener que`pulsar el intro: int opcion = Console.ReadKey().KeyChar -'0';

            Console.Clear();
            switch (opcion)
            {
                case 0:
                    Console.Write("\n\t\t\tHa elegido la opción nº 0: \"Salir\"" );
                    break;
                case 1:
                    Console.Write("\n\t\t\tHa elegido la opción nº 1: \"Lista de Clientes\"");
                    break;
                case 2:
                    Console.Write("\n\t\t\tHa elegido la opción nº 2: \"Añadir Cliente\"");
                    break;
                case 3:
                    Console.Write("\n\t\t\tHa elegido la opción nº 3: \"Eliminar Cliente\"");
                    break;
                case 4:
                    Console.Write("\n\t\t\tHa elegido la opción nº 4: \"Modificar Datos\"");
                    break;
                default:
                    Console.Write("\n\t\t\t** Error: Opción no válida **");
                    break;
            }

            Console.Write("\n\t\t\tPulse INTRO para salir");
            Console.ReadLine();
        }
    }
}
