﻿// Alumno: Gallego Dorado, Francisco José
/*
    Se le pide al usuario que introduzca un número entre 0 y 6, cada uno de
    los cuales indica el día de la semana en que nos encontramos, siendo
    [0-> Domingo, 1-> Lunes, 2-> Martes, ... 6-> Sábado]. Luego pregunta
    cuántos días quiere avanzar numDias y el programa calcula, de la forma
    más eficiente, en qué día de la semana caerá dentro de numDias.
    Nota: Adjunto un ejecutable de muestra
*/
using System;

namespace DiaDeLaSemana
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // Pedimos al usuario un día
            Console.Write("\n\tIntroduza un número entre 0 y 6 (0=Domingo, 6=Sábado): ");
            int dia = Convert.ToInt32(Console.ReadLine());
            string nombreDia = "";

            //Comprobamos que el día está dentro del rango
            if (dia > 6 || dia <0)
            {
                Console.Write("\n\t** Error: Día fuera de rango (0-6) **");
            }
            else
            {
                //Pasamos el día por consola
                if (dia == 0)
                    nombreDia = "Domingo";
                else if (dia == 1)
                    nombreDia = "Lunes";
                else if (dia == 2)
                    nombreDia = "Martes";
                else if (dia == 3)
                    nombreDia = "Miércoles";
                else if (dia == 4)
                    nombreDia = "Jueves";
                else if (dia == 5)
                    nombreDia = "Viernes";
                else if (dia == 6)
                    nombreDia = "Sábado";
                Console.Write("\n\tHoy es " + nombreDia);

                // Pedimos al usuario cuántos días quiere avanzar
                Console.Write(". Introduce cuántos días quieres avanzar: ");
                int numDias = Convert.ToInt32(Console.ReadLine());

                // Calculamos que día es y pasamos por pantalla
                dia += numDias;
                if (dia > 6)
                {
                    dia = dia % 7;
                }
                if (numDias==1)
                    Console.Write("\n\tEstamos a {0} y dentro de {1} día será ", nombreDia, numDias);
                else
                    Console.Write("\n\tEstamos a {0} y dentro de {1} días será ", nombreDia, numDias);

                if (dia == 0)
                    Console.Write("Domingo");
                else if (dia == 1)
                    Console.Write("Lunes");
                else if (dia == 2)
                    Console.Write("Martes");
                else if (dia == 3)
                    Console.Write("Miércoles");
                else if (dia == 4)
                    Console.Write("Jueves");
                else if (dia == 5)
                    Console.Write("Viernes");
                else if (dia == 6)
                    Console.Write("Sábado");
            }

            Console.Write("\n\n\t\tPulse INTRO para salir");
            Console.ReadLine();
        }
    }
}
