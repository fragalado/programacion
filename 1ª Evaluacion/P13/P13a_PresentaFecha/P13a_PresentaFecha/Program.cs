﻿// Alumno: Gallego Dorado, Francisco José
/*
    Se le pide al usuario que introduzca un año, luego mes y luego
    día. Si la fecha es correcta, la presentará escribiendo el mes por
    su nombre.
    Ejemplos:
    año=2014; mes=10; día=8  8 de Octubre de 2014
    año=2014; mes=15;  Mes Fuera de rango
    año=2014; mes=2; día=31  Día Fuera de rango
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P13a_PresentaFecha
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int anyo, mes, dia;
            string nombreMes = "";
            //Pedimos el año
            Console.Write("\n\tIntroduzca el año: ");
            anyo = Convert.ToInt32(Console.ReadLine());
            //Pedimos el mes
            Console.Write("\n\tIntroduzca el mes: ");
            mes = Convert.ToInt32(Console.ReadLine());

            // Comprobamos que el mes es correcto
            if (mes < 1 || mes > 12)
            {
                Console.Write("\n\t** Error: Mes fuera de rango **");
            }
            else
            {
                //Pedimos el dia
                Console.Write("\n\tIntroduzca el dia: ");
                dia = Convert.ToInt32(Console.ReadLine());

                //Comprobamos si el dia es incorrecto
                if ((dia > 31 || dia < 1) ||
                    (dia > 30 && (mes == 4 || mes == 6 || mes == 9 || mes == 11)) || // Meses de 30 dias
                    (dia > 28 && mes == 2)) // febrero
                {
                    Console.Write("\n\t** Error: Día fuera de rango **");
                }
                else
                {
                    // Aqui tanto el mes como el dia son correctos
                    switch (mes)
                    {
                        case 1:
                            nombreMes = "Enero";
                            break;
                        case 2:
                            nombreMes = "Febrero";
                            break;
                        case 3:
                            nombreMes = "Marzo";
                            break;
                        case 4:
                            nombreMes = "Abril";
                            break;
                        case 5:
                            nombreMes = "Mayo";
                            break;
                        case 6:
                            nombreMes = "Junio";
                            break;
                        case 7:
                            nombreMes = "Julio";
                            break;
                        case 8:
                            nombreMes = "Agosto";
                            break;
                        case 9:
                            nombreMes = "Septiembre";
                            break;
                        case 10:
                            nombreMes = "Octubre";
                            break;
                        case 11:
                            nombreMes = "Noviembre";
                            break;
                        case 12:
                            nombreMes = "Diciembre";
                            break;
                    }

                    Console.Write("\n\t{0} de {1} de {2}.", dia, nombreMes, anyo);
                }


            }
            Console.ReadLine();
        }
    }
}
