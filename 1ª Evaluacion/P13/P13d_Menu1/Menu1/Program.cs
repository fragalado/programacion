﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Menu1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // Prueba KeyChar:
            char caracter;
            Console.Write("\n\tPulsa una tecla: ");
            caracter = Console.ReadKey().KeyChar;
            int codigo = (int)caracter;
            Console.Write("\n\tHe pulsado el carácter '{0}', cuyo código es el {1}", caracter, codigo);
           
            Console.Write("\n\n\n\tPulse una tecla para salir");
            Console.ReadKey();
        }
    }
}
